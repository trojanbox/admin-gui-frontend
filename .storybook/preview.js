import { QueryClient, QueryClientProvider } from 'react-query';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { RouterContext } from 'next/dist/shared/lib/router-context';
import Image from 'next/image';

import { BodyScrollLockProvider } from '../src/context/bodyScrollLock';
import { ThemeProvider, scale, theme } from '../src/scripts/gds';

// Image = props => <img {...props} />;

export const parameters = {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
        hideNoControlsWarning: true,
    },
    viewport: {
        viewports: INITIAL_VIEWPORTS,
    },
    paddings: {
        values: [
            { name: 'None', value: '0' },
            { name: 'Small', value: '16px' },
            { name: 'Medium', value: '32px' },
            { name: 'Large', value: '64px' },
        ],
        default: 'Medium',
    },
    backgrounds: {
        grid: { cellSize: scale(1) },
        values: theme.colors && Object.entries(theme.colors).map(([name, value]) => ({ name, value })),
    },
    options: {
        storySort: {
            order: ['Intro', 'Autokits', 'Components'],
        },
    },
    nextRouter: {
        Provider: RouterContext.Provider,
    },
    parameters: {
        nextRouter: {
            path: '/products/[id]',
            asPath: '/products/catalog',
            query: {
                id: 'catalog',
            },
        },
    },
};

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 10000,
            retry: 0,
            refetchOnWindowFocus: false,
        },
    },
});


export const decorators = [
    Story => {
        return (
            <ThemeProvider theme={theme}>
                <BodyScrollLockProvider>
                    <QueryClientProvider client={queryClient}>
                        <Story />
                    </QueryClientProvider>
                </BodyScrollLockProvider>
            </ThemeProvider>
        );
    },
];
