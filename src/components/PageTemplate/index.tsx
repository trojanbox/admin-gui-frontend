import { CSSObject } from '@emotion/core';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { FC, ReactNode, useMemo } from 'react';

import Block from '@components/Block';

import { scale, typography, useTheme } from '@scripts/gds';
import { useLinkCSS, useMedia } from '@scripts/hooks';

import ArrowLeftIcon from '@icons/small/arrowLeft.svg';

interface PageTemplateProps {
    // Header
    h1?: ReactNode;
    // Children for controls pannel
    controls?: ReactNode;
    // flag to render children without Block wrapper
    customChildren?: boolean;
    // Children for aside pannel
    aside?: ReactNode;
    // asyde styles
    asideStyles?: CSSObject;
    // backlink
    backlink?: { href?: string | { pathname: string; query: string }; text: string };
}

export const PageTemplate: FC<PageTemplateProps> = ({
    h1,
    controls,
    children,
    aside,
    customChildren = false,
    asideStyles,
    backlink,
}) => {
    const { colors } = useTheme();
    const { md } = useMedia();
    const linkStyles = useLinkCSS();
    const { back } = useRouter();

    const isNotFirstPage = useMemo(() => window?.history?.state?.idx > 0, []);

    return (
        <section css={{ display: 'flex', minHeight: '100%', flexDirection: 'column' }}>
            <div
                css={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    gap: scale(2),
                    alignItems: 'center',
                    marginBottom: scale(2),
                }}
            >
                <div>
                    {backlink &&
                        (backlink.href ? (
                            <Link href={backlink.href} passHref>
                                <a css={{ ...linkStyles, marginBottom: scale(1, true) }}>
                                    <ArrowLeftIcon
                                        css={{ marginRight: scale(1, true), verticalAlign: 'sub !important' }}
                                    />
                                    {backlink.text}
                                </a>
                            </Link>
                        ) : (
                            <button
                                type="button"
                                onClick={() => back()}
                                disabled={!isNotFirstPage}
                                css={{ ...linkStyles, marginBottom: scale(1, true) }}
                            >
                                <ArrowLeftIcon css={{ marginRight: scale(1, true), verticalAlign: 'sub !important' }} />
                                {backlink.text}
                            </button>
                        ))}

                    {h1 && <h1 css={{ ...typography('h1'), margin: 0 }}>{h1}</h1>}
                </div>
                {controls && (
                    <aside
                        css={{
                            position: 'relative',
                            display: 'flex',
                            alignItems: 'center',
                            gap: scale(1),
                            padding: scale(1),
                            borderRadius: 2,
                            background: colors?.white,
                            [md]: {
                                flexWrap: 'wrap',
                                width: '100%',
                            },
                        }}
                    >
                        {controls}
                    </aside>
                )}
            </div>
            <div
                css={{
                    flexGrow: 1,
                    display: 'flex',
                    alignItems: 'flex-start',
                    gap: scale(2),
                    [md]: { flexWrap: 'wrap' },
                }}
            >
                {customChildren ? (
                    children
                ) : (
                    <Block css={{ minHeight: '100%' }}>
                        <Block.Body>{children}</Block.Body>
                    </Block>
                )}
                {aside && (
                    <Block
                        as="aside"
                        css={{
                            width: scale(38),
                            [md]: {
                                width: '100%',
                                order: -1
                            },
                            flexShrink: 0,
                            ...asideStyles,
                        }}
                    >
                        <Block.Body>{aside}</Block.Body>
                    </Block>
                )}
            </div>
        </section>
    );
};
