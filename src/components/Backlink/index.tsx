import Link from 'next/link';
import { ReactNode } from 'react';

import { scale } from '@scripts/gds';
import { useLinkCSS } from '@scripts/hooks';

import ArrowLeftIcon from '@icons/small/arrowLeft.svg';

const Backlink = ({ href, children, className }: { className?: string; href: string; children: ReactNode }) => {
    const linkStyles = useLinkCSS();

    return (
        <Link href={href} passHref>
            <a className={className} css={{ ...linkStyles, marginBottom: scale(1, true) }}>
                <ArrowLeftIcon css={{ marginRight: scale(1, true), verticalAlign: 'sub !important' }} />
                {children}
            </a>
        </Link>
    );
};

export default Backlink;
