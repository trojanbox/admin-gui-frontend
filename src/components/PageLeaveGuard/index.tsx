import { useFormikContext } from 'formik';
import { useRouter } from 'next/router';
import type { ParsedUrlQuery } from 'querystring';
import { useCallback, useEffect, useRef, useState } from 'react';
import { useFormContext } from 'react-hook-form';

import { useModalsContext } from '@context/modal';

import { StatusSimplePopup } from '@components/StatusSimplePopup';
import useForm from '@components/controls/future/Form/useForm';

import { Button } from '@scripts/gds';

type ValueOrPromise<T> = T | Promise<T>;
type UrlOrString = { pathname: string; query: ParsedUrlQuery } | string;
type SubmitUrl = void | UrlOrString | boolean;

interface PageLeaveGuardProps<T extends Record<string, any>> {
    isTrackParameters?: boolean;
    onSubmit?: (values: T) => ValueOrPromise<SubmitUrl>;
}

const useIsomorphicForm = (): Pick<
    ReturnType<typeof useFormikContext>,
    'dirty' | 'submitForm' | 'validateForm' | 'resetForm'
> & {
    getValues(): any;
    markErrorFields(): void;
} => {
    const oldConsoleWarning = console.warn;
    console.warn = () => {};
    const formikContext = useFormikContext();
    console.warn = oldConsoleWarning;
    const formContext = useFormContext();
    const form = useForm()!;

    if (!formikContext && !formContext) {
        throw new Error('PageLeaveGuard can only exist as a descendant of formik or react-hook-form');
    }

    if (formContext)
        return {
            dirty: formContext.formState.isDirty,
            resetForm: () => formContext.reset(),
            submitForm: () =>
                new Promise(resolve => {
                    if (form) form.onSubmitHandler(formContext.getValues());
                    resolve(formContext.getValues());
                }),
            validateForm: async () => {
                await formContext.trigger();

                return formContext.formState.errors;
            },
            getValues: formContext.getValues,
            markErrorFields: () =>
                Object.keys(formContext.getValues()).forEach(fieldName => formContext.trigger(fieldName)),
        };

    return {
        ...formikContext,
        getValues() {
            return () => formikContext.values;
        },
        markErrorFields: () =>
            formikContext.setTouched(
                Object.keys(formikContext.values as object).reduce((acc, key) => {
                    acc[key] = true;
                    return acc;
                }, {} as Record<string, boolean>)
            ),
    };
};

const PageLeaveGuard = <T extends Record<string, any>>({
    isTrackParameters = false,
    onSubmit,
}: PageLeaveGuardProps<T>) => {
    const [isOpen, setIsOpen] = useState(false);
    const [isLoading, setLoading] = useState(false);

    const close = () => setIsOpen(false);
    const { dirty, submitForm, validateForm, resetForm, getValues, markErrorFields } = useIsomorphicForm();

    const open = useCallback(() => setIsOpen(true), []);

    const { events, push, pathname } = useRouter();
    const urlRef = useRef('');

    const dirtyRef = useRef<boolean>();
    dirtyRef.current = dirty;

    const { appendModal } = useModalsContext();

    useEffect(() => {
        const confirmationMessage = 'Вы не сохранили изменения';

        const beforeUnloadHandler = (e: BeforeUnloadEvent) => {
            if (!dirtyRef.current) return;

            (e || window.event).returnValue = confirmationMessage;
            return confirmationMessage; // Gecko + Webkit, Safari, Chrome etc.
        };

        const beforeRouteHandler = (url: string) => {
            if (!dirtyRef.current) return;

            if (!isTrackParameters) {
                if (pathname !== url) {
                    urlRef.current = url;
                    open();
                    events.emit('routeChangeError');
                    const error = `Route change to "${url}" was aborted (this error can be safely ignored). See https://github.com/zeit/next.js/issues/2476.`;
                    throw error;
                }
            } else {
                const match = /.*\?.*/.test(url);

                if (match && pathname !== url) return;
                urlRef.current = url;
                open();
                events.emit('routeChangeError');
                const error = `Route change to "${url}" was aborted (this error can be safely ignored). See https://github.com/zeit/next.js/issues/2476.`;
                throw error;
            }
        };
        if (dirty) {
            window.addEventListener('beforeunload', beforeUnloadHandler);
            events.on('routeChangeStart', beforeRouteHandler);
        } else {
            window.removeEventListener('beforeunload', beforeUnloadHandler);
            events.off('routeChangeStart', beforeRouteHandler);
        }
        return () => {
            window.removeEventListener('beforeunload', beforeUnloadHandler);
            events.off('routeChangeStart', beforeRouteHandler);
        };
    }, [dirty, events, pathname, open, isTrackParameters]);

    return (
        <StatusSimplePopup
            close={close}
            isOpen={isOpen}
            title="Сохранить изменения?"
            text="Вы внесли изменения на странице, но не сохранили их"
            theme="warning"
            footer={
                <>
                    <Button
                        theme="secondary"
                        onClick={() => {
                            close();
                            resetForm({
                                values: getValues(),
                            });
                            setTimeout(() => push(urlRef.current), 100);
                        }}
                        disabled={isLoading}
                    >
                        Не сохранять
                    </Button>
                    <Button
                        onClick={async () => {
                            const errors = await validateForm();
                            if (Object.keys(errors).length) {
                                appendModal({
                                    title: 'Невозможно сохранить',
                                    message: 'Устраните ошибки в форме',
                                    theme: 'error',
                                });
                                markErrorFields();
                                close();
                                return;
                            }

                            try {
                                setLoading(true);
                                if (onSubmit) {
                                    await onSubmit(getValues());
                                } else {
                                    await submitForm();
                                }
                            } catch (e) {
                                console.error(e);
                                close();
                                return;
                            }

                            setLoading(false);

                            resetForm();
                            close();
                            setTimeout(() => push(urlRef.current), 100);
                        }}
                        disabled={isLoading}
                    >
                        Сохранить
                    </Button>
                </>
            }
        />
    );
};

export default PageLeaveGuard;
