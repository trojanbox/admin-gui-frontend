import { FC, ReactNode } from 'react';

import Popup from '@controls/future/Popup';

import { scale, typography, useTheme } from '@scripts/gds';

import SuccessIcon from '@icons/20/checkCircle.svg';
import ErrorIcon from '@icons/20/closedCircle.svg';
import DeleteIcon from '@icons/20/delete.svg';
import InfoIcon from '@icons/20/info.svg';
import WarningIcon from '@icons/20/warning.svg';

interface StatusPopupProps {
    isOpen: boolean;
    close: () => void;
    title: string;
    text: string;
    theme: 'info' | 'error' | 'warning' | 'success' | 'delete';
    footer: ReactNode;
}

const Icon = ({ theme, className }: { theme: StatusPopupProps['theme']; className?: string }) => {
    const { colors } = useTheme();

    switch (theme) {
        case 'success': {
            return <SuccessIcon css={{ fill: colors?.success }} className={className} />;
        }
        case 'warning': {
            return <WarningIcon css={{ fill: colors?.warning }} className={className} />;
        }
        case 'error': {
            return <ErrorIcon css={{ fill: colors?.danger }} className={className} />;
        }
        case 'delete': {
            return <DeleteIcon css={{ fill: colors?.danger }} className={className} />;
        }
        case 'info':
        default: {
            return <InfoIcon css={{ fill: colors?.primary }} className={className} />;
        }
    }
};

export const StatusSimplePopup: FC<StatusPopupProps> = ({ isOpen, close, title, text, theme = 'info', footer }) => (
    <Popup open={isOpen} onClose={close} size="sm" hasCloser={false}>
        <Popup.Header css={{ border: 0 }}>
            <div css={{ display: 'flex' }}>
                <Icon css={{ marginRight: scale(2), flexShrink: 0 }} theme={theme} />
                <p css={{ ...typography('h3'), marginBottom: scale(1, true) }}>{title}</p>
            </div>
        </Popup.Header>
        {text && <Popup.Content css={{ paddingTop: 0 }}>{text}</Popup.Content>}
        {footer && <Popup.Footer>{footer}</Popup.Footer>}
    </Popup>
);
