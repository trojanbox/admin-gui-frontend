import IndeterminateCheckbox from '../IndeterminateCheckbox';
import { ExtendedColumn, ExtendedRow } from '../types';

export const getSelectColumn = (maxRowSelect = 0, name = ''): ExtendedColumn => ({
    accessor: 'select',

    Header: ({ getToggleAllRowsSelectedProps }: { getToggleAllRowsSelectedProps: any }) =>
        maxRowSelect ? null : (
            <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} id="header" parentTableName={name} />
        ),

    Cell: ({ row, selectedFlatRows }: { row: ExtendedRow; selectedFlatRows?: ExtendedRow[] }) => {
        const disabled = maxRowSelect > 0 && selectedFlatRows?.length === maxRowSelect && !row.isSelected;

        return (
            <button type="button" onClick={e => e.stopPropagation()} css={{ display: 'grid', alignItems: 'center' }}>
                <IndeterminateCheckbox
                    {...(row?.getToggleRowSelectedProps && row.getToggleRowSelectedProps())}
                    id={name !== '' ? `${row.id}-${name}` : row.id}
                    parentTableName={name}
                    disabled={disabled}
                />
            </button>
        );
    },
    disableSortBy: true,
});
