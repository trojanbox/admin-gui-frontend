import { ReactNode } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

import { AuthProvider } from '@context/auth';
import { BodyScrollLockProvider } from '@context/bodyScrollLock';
import { CommonProvider } from '@context/common';
import { ModalProvider } from '@context/modal';

import { STALE_TIME } from '@scripts/constants';
import { ThemeProvider, theme } from '@scripts/gds';

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: STALE_TIME,
            retry: 0,
            refetchOnWindowFocus: process.env.NODE_ENV === 'production',
        },
    },
});

const AppProviders = ({ children }: { children: ReactNode }) => (
    <ThemeProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
            <CommonProvider>
                <BodyScrollLockProvider>
                    <ModalProvider>
                        <AuthProvider>{children}</AuthProvider>
                    </ModalProvider>
                </BodyScrollLockProvider>
            </CommonProvider>
            <ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
        </QueryClientProvider>
    </ThemeProvider>
);

export default AppProviders;
