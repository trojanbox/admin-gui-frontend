import { Row } from '@tanstack/react-table';
import { FC, ReactNode, useEffect, useState } from 'react';
import { followCursor } from 'tippy.js';

import Tooltip from '@components/controls/Tooltip';
import { ContentBtn } from '@components/controls/Tooltip/ContentBtn';

import { Layout, scale } from '@scripts/gds';

import TipIcon from '@icons/small/status/tip.svg';

import { TooltipItem, TrProps } from '../types';
import { DefaultTr } from './utils';

const RowTooltipWrapper = ({
    Tr = DefaultTr,
    row,
    getTooltipForRow,
    children,
}: {
    row?: Row<any>;
    children: ReactNode | ReactNode[];
    getTooltipForRow: (row: Row<any>) => TooltipItem[];
    Tr?: FC<TrProps<any>>;
}) => {
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        const callback = (e: KeyboardEvent) => {
            if (e.key === 'Escape') setVisible(false);
        };
        if (visible) {
            document.addEventListener('keydown', callback);
        }
        return () => {
            document.removeEventListener('keydown', callback);
        };
    }, [setVisible, visible]);

    const tooltipContent = getTooltipForRow(row!);

    return (
        <Tooltip
            content={
                <ul>
                    {tooltipContent.map(t => (
                        <li key={t.text}>
                            <ContentBtn
                                type={t.type}
                                onClick={async e => {
                                    e.stopPropagation();

                                    await Promise.resolve(t.action());
                                    setVisible(false);
                                }}
                                disabled={t.disabled}
                                css={{display: 'flex', width: '100%'}}
                            >
                                <Layout
                                    type="grid"
                                    cols={['1fr', `${scale(2)}px`]}
                                    gap={t.disabled && t.disabledHint ? 0 : scale(2)}
                                    align="center"
                                    css={{width: '100%'}}
                                >
                                    <Layout.Item grow={1}>{t.text}</Layout.Item>
                                    {t.disabled && t.disabledHint && (
                                        <Layout.Item align="end" justify="end">
                                            <Tooltip content={t.disabledHint} arrow>
                                                <div
                                                    role="button"
                                                    css={{ verticalAlign: 'middle', }}
                                                >
                                                    <TipIcon />
                                                </div>
                                            </Tooltip>
                                        </Layout.Item>
                                    )}
                                </Layout>
                            </ContentBtn>
                        </li>
                    ))}
                </ul>
            }
            plugins={[followCursor]}
            followCursor="initial"
            arrow
            theme="light"
            placement="bottom"
            minWidth={scale(36)}
            disabled={tooltipContent.length === 0}
            appendTo={() => document.body}
            visible={visible}
            onClickOutside={() => setVisible(false)}
        >
            <Tr
                row={row}
                onContextMenu={e => {
                    e.preventDefault();
                    setVisible(true);
                }}
            >
                {children}
            </Tr>
        </Tooltip>
    );
};

export default RowTooltipWrapper;
