import Link from 'next/link';
import { useMemo } from 'react';

import { DateFormaters } from '@scripts/constants';
import { formatDate } from '@scripts/helpers';
import { useLinkCSS } from '@scripts/hooks';

import { InfoListItemCommonType } from '../types';

export const useGetInfoItemValue = (props: InfoListItemCommonType) => {
    const linkStyles = useLinkCSS();

    return useMemo(() => {
        switch (props.type) {
            case 'boolean':
                return props.value ? 'да' : 'нет';
            case 'date':
                return props.value ? formatDate(new Date(props.value), DateFormaters.DATE_AND_TIME) : '-';
            case 'link':
                return (
                    <Link passHref href={props.link}>
                        <a css={linkStyles}>{props.value}</a>
                    </Link>
                );
            default:
                return props.value || '-';
        }
    }, [linkStyles, props]);
};
