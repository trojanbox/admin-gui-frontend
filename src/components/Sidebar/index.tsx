import { useRouter } from 'next/router';
import { FC, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import Media from 'react-media';

import { useTheme } from '@scripts/gds';
import { useMedia, useOnClickOutside } from '@scripts/hooks';

import { List } from './List';
import { MenuItemProps } from './types';

interface SidebarProps {
    menuItems: MenuItemProps[];
    isCutDown: boolean;
    cutDownHandler: () => void;
    setOverlay?: (overlay: boolean) => void;
    isSidebarOpenAdaptive?: boolean;
    closeMenu?: () => void;
}

const Sidebar: FC<SidebarProps> = ({
    menuItems = [],
    isCutDown,
    cutDownHandler,
    setOverlay,
    isSidebarOpenAdaptive,
    closeMenu,
    ...props
}) => {
    const { md } = useMedia();
    const { shadows, layout } = useTheme();

    const [menu, setMenu] = useState<MenuItemProps[][]>([menuItems]);
    useEffect(() => {
        setMenu([menuItems]);
    }, [menuItems]);

    const { pathname } = useRouter();

    const getMatch = useCallback((link?: string) => link && new RegExp(link).test(pathname), [pathname]);

    /** Найдем активные пункты меню, соответсвующие урлу */
    const activeMenuPoints = useMemo(() => {
        const points: MenuItemProps[] = [];
        const findActive = (items?: MenuItemProps[]): MenuItemProps | undefined => {
            if (!items) return undefined;
            const finded = items.find(i => {
                if (i.link) return getMatch(i.link);
                return findActive(i.subMenu);
            });
            if (finded) points.push(finded);
            return finded;
        };
        findActive(menuItems);
        return points;
    }, [getMatch, menuItems]);

    /** Установим выбранные пользователем активные пункты */
    const [activeMenuPointsByUser, setActiveMenuPointsByUser] = useState<string[]>([]);

    const menuRef = useRef<HTMLDivElement | null>(null);

    useOnClickOutside(menuRef, (evt: any) => {
        if (isSidebarOpenAdaptive) {
            evt.preventDefault();
            evt.stopPropagation();
        }

        setMenu([menuItems]);
        if (closeMenu) closeMenu();
        if (setOverlay) setOverlay(false);
        setActiveMenuPointsByUser([]);
    });

    return (
        <nav
            css={{
                display: 'flex',
                position: 'relative',
                zIndex: 3,
                [md]: {
                    position: 'fixed',
                    overflow: 'auto',
                    height: '100%',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    boxShadow: shadows?.box,
                },
            }}
            ref={menuRef}
            {...props}
        >
            <Media query={{ minWidth: layout?.breakpoints.md || 1024 }}>
                {isDesktop =>
                    isDesktop || isSidebarOpenAdaptive ? (
                        <>
                            {menu.map((items, index) => (
                                <List
                                    key={index}
                                    setMenu={setMenu}
                                    items={items}
                                    level={index}
                                    allLevels={menu.length}
                                    isCutDown={isDesktop ? isCutDown : false}
                                    cutDownHandler={cutDownHandler}
                                    activeMenuPoints={activeMenuPoints}
                                    activeMenuPointsByUser={activeMenuPointsByUser}
                                    setActiveMenuPointsByUser={setActiveMenuPointsByUser}
                                    onItemNavigate={() => {
                                        if (!isDesktop) {
                                            closeMenu?.();
                                        }
                                    }}
                                />
                            ))}
                        </>
                    ) : null
                }
            </Media>
        </nav>
    );
};

export default Sidebar;
