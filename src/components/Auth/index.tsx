import * as Yup from 'yup';

import Form from '@components/controls/Form';
import Password from '@components/controls/Password';

import { ErrorMessages } from '@scripts/constants';
import { Button, scale, typography, useTheme } from '@scripts/gds';
import { onlyEnglishLettersDigits, regEmail } from '@scripts/regex';
import { useMemo } from 'react';

interface AuthProps {
    logIn: (values: { login: string; password: string }) => void;
    isLoading?: boolean;
}

const Auth = ({ logIn, isLoading }: AuthProps) => {
    const { shadows, colors } = useTheme();

    const numberValidationWithLength = useMemo(
        () =>
            Yup.string()
                .test(`number-length`, 'Неверный формат логина', val => {
                    if (onlyEnglishLettersDigits.test(String(val))) return true;
                    if (regEmail.test(String(val))) return true;
                    return false;
                })
                .required(ErrorMessages.REQUIRED),
        []
    );

    return (
        <div
            css={{
                minHeight: '100vh',
                display: 'grid',
                placeItems: 'center',
            }}
        >
            <main
                css={{
                    width: scale(45),
                    boxShadow: shadows?.big,
                    padding: `${scale(3)}px ${scale(4)}px`,
                    backgroundColor: colors?.white,
                }}
            >
                <h1 css={{ ...typography('h1'), textAlign: 'center', marginTop: 0 }}>Авторизация</h1>
                <Form
                    onSubmit={({ login, password }) => logIn({ login, password })}
                    initialValues={{ login: '', password: '' }}
                    validationSchema={Yup.object().shape({
                        login: numberValidationWithLength,
                        password: Yup.string().min(6, ErrorMessages.MIN_SYMBOLS(6)).required(ErrorMessages.REQUIRED),
                    })}
                >
                    <Form.FastField name="login" label="Логин" css={{ marginBottom: scale(2) }} type="email" />
                    <Form.FastField name="password" label="Пароль" css={{ marginBottom: scale(4) }} autoComplete="off">
                        <Password />
                    </Form.FastField>
                    <Button type="submit" block disabled={isLoading}>
                        Войти
                    </Button>
                </Form>
            </main>
        </div>
    );
};

export default Auth;
