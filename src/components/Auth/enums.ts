export const RolesEnum = {
    // Общие администраторы
    ADMIN_ADMIN: 101, // Администратор (ЦО)
    ADMIN_SUPER: 102, // Супер-администратор

    MANAGER_SELLER: 103,
    MANAGER_CLIENT: 104,

    // Кросс-сервисные пользователи
    ADMIN_MERCHANT_ADMINISTRATOR: 115, // Администратор магазина +
    ADMIN_SEO_MANAGER: 116, // SEO менеджер +
    /* ADMIN_CONTENT_MANAGER: 121, // Контент-менеджер магазина
    ADMIN_SECURITY_OFFICER: 122, // Офицер ИБ */

    // OMS
    ADMIN_OMS_SHIFT_MANAGER: 120, // OMS. Администратор смены +
    ADMIN_OMS_ORDER_COLLECTOR: 119, // OMS. Сборщик +
    ADMIN_OMS_ORDER_PICKER: 111, // OMS. Комплектовщик +
    ADMIN_OMS_LOGISTIC: 112,
    ADMIN_OMS_STORE_EMPLOYEE: 114,
    ADMIN_OMS_CALL_CENTER: 113, // OMS. Оператор Колл-центра +
    /* ADMIN_OMS_PICKUP_POINT: 125, // OMS. Сотрудник пункта выдачи
    ADMIN_OMS_EXPORT_AUTHORIZED: 126, // OMS. Уполномоченный на экспорт */

    // PIM
    ADMIN_CATEGORY_MANAGER: 123, // Категорийный менеджер
    PIM_MANAGER: 106,

    // Merchant
    ADMIN_COMMUNICATION_MANAGER: 124, // Менеджер по коммуникациям c клиентом

    // Пользователи для интеграций с другими сервисами
    ADMIN_CREATIO: 117, // CRM Creatio
    ADMIN_FISCAL: 118, // Администратор фискализации
    ADMIN_SBERMARKET: 127, // Администратор интеграции со СберМаркет (нужно для авторизации в вебхуках)

    // ===== ВИТРИНА =====
    SHOWCASE_CUSTOMER: 401,
    SHOWCASE_GUEST: 403,

    // SAS
    SAS_SELLER_OPERATOR: 201,
    SAS_SELLER_ADMIN: 202,
} as const;

export type Roles = typeof RolesEnum[keyof typeof RolesEnum];

export const generalAdmins = [RolesEnum.ADMIN_SUPER, RolesEnum.ADMIN_ADMIN];

export const OMSRoles = [
    RolesEnum.ADMIN_SUPER,
    RolesEnum.ADMIN_ADMIN,
    RolesEnum.ADMIN_MERCHANT_ADMINISTRATOR,
    RolesEnum.ADMIN_OMS_SHIFT_MANAGER,
    RolesEnum.ADMIN_OMS_ORDER_COLLECTOR,
    RolesEnum.ADMIN_OMS_CALL_CENTER,
    // RolesEnum.ADMIN_OMS_EXPORT_AUTHORIZED,
    RolesEnum.ADMIN_OMS_ORDER_PICKER,
    // RolesEnum.ADMIN_OMS_PICKUP_POINT,
    // RolesEnum.ADMIN_OMS_EXPORT_AUTHORIZED,
];