import { Editor } from '@tinymce/tinymce-react';
import { FieldHelperProps, FieldInputProps } from 'formik';

export const TextEditor = ({
    name,
    field,
    helpers,
    disabled = false,
}: {
    name?: string;
    field?: FieldInputProps<string>;
    helpers?: FieldHelperProps<string>;
    disabled?: boolean;
}) => {
    const textValue = field?.value;
    const onChange = helpers ? (newValue: string) => helpers?.setValue(newValue) : undefined;
    return (
        <Editor
            onEditorChange={onChange}
            value={textValue}
            init={{
                menubar: false,
                plugins: 'link image code anchor lists',
                toolbar:
                    'undo redo | h1 h2 h3 h4 | bold italic | bullist numlist| forecolor backcolor | alignleft aligncenter alignright alignjustify | link anchor | image | code',
            }}
            apiKey={process.env.NEXT_PUBLIC_TINY_MCE_API_KEY}
            textareaName={field?.name || name}
            disabled={disabled}
        />
    );
};

export default TextEditor;
