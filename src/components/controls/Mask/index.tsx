import { FieldHelperProps, FieldInputProps, FieldMetaProps } from 'formik';
import { IMaskInput } from 'react-imask';

import { useFieldCSS } from '@scripts/hooks';

import Legend from '../Legend';

export interface MaskProps {
    /** Mask for input */
    mask: string;
    /** Formik helpers object (inner) */
    helpers?: FieldHelperProps<string>;
    /** Placeholder for mask */
    placeholderChar?: string;
    /** Is show placholder */
    lazy?: boolean;
    /** from formik */
    field?: FieldInputProps<any>;
    /** from formik */
    meta?: FieldMetaProps<any>;
    label?: string;
    className?: string;
}

const Mask = ({
    mask,
    label,
    meta,
    field,
    helpers,
    placeholderChar = '_',
    lazy = true,
    className,
    ...props
}: MaskProps) => {
    const value = field?.value || undefined;
    const { basicFieldCSS } = useFieldCSS(meta);
    return (
        <div className={className}>
            <Legend label={label} meta={meta} />
            <IMaskInput
                mask={mask}
                value={value}
                {...props}
                css={basicFieldCSS}
                lazy={lazy}
                placeholderChar={placeholderChar}
                onAccept={(val: string) => {
                    if (helpers) helpers.setValue(val);
                }}
            />
        </div>
    );
};

export default Mask;
