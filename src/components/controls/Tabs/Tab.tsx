import cn from 'classnames';
import { HTMLProps, ReactNode } from 'react';
import { Tab as ReactTab } from 'react-tabs';

import { typography, useTheme } from '@scripts/gds';

export interface TabsTabProps extends HTMLProps<HTMLLIElement> {
    /** Heading content */
    children: ReactNode;
    /** Disabled tab */
    disabled?: boolean;
    /** Counter for tab */
    count?: number;
    /** Need marker */
    marker?: boolean;
    /** Mark as has errors */
    isError?: boolean;
}

export const TabsTab = ({ children, className, count, marker, isError, ...props }: TabsTabProps) => {
    const baseClass = 'tabs__tab';
    const classes = cn(baseClass, className);
    const { components } = useTheme();
    const tabsTheme = components?.Tabs;

    return (
        <ReactTab
            className={classes}
            selectedClassName={`${baseClass}--selected`}
            disabledClassName={`${baseClass}--disabled`}
            {...props}
        >
            <span
                css={
                    marker || isError
                        ? {
                              paddingRight: (tabsTheme?.markerMargin || 0) + (tabsTheme?.markerSize || 0),
                              position: 'relative',
                              '&::before': {
                                  content: "''",
                                  position: 'absolute',
                                  right: tabsTheme?.markerRight,
                                  top: tabsTheme?.markerTop,
                                  borderRadius: '50%',
                                  width: tabsTheme?.markerSize,
                                  height: tabsTheme?.markerSize,
                                  background: isError ? tabsTheme?.errorColor : tabsTheme?.markerBg,
                                  '[class$=--disabled] &': {
                                      background: 'inherit',
                                  },
                              },
                          }
                        : undefined
                }
            >
                {children}
                {typeof count === 'number' ? (
                    <span
                        css={{
                            ...(tabsTheme?.counterTypography && typography(tabsTheme.counterTypography)),
                            display: 'inline-block',
                            verticalAlign: 'text-bottom',
                            padding: tabsTheme?.counterPadding,
                            background: tabsTheme?.counterBg,
                            border: tabsTheme?.counterBorder,
                            borderRadius: tabsTheme?.counterBorderRadius,
                            marginLeft: tabsTheme?.counterMargin,
                            color: tabsTheme?.counterColor,
                            '[class$=--disabled] &': {
                                color: 'inherit',
                                borderColor: tabsTheme?.counterDisabledBorderColor,
                                background: tabsTheme?.counterDisabledBg,
                            },
                        }}
                    >
                        {count}
                    </span>
                ) : null}
            </span>
        </ReactTab>
    );
};

export default TabsTab;
