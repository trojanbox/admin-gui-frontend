import type { useLazyLoadingProps } from '../Select/presets/useLazyLoading';
import type { OptionShape } from '../Select/types';
import type { SelectWithTagsProps } from '../SelectWithTags/types';

export interface AutocompleteAsyncProps
    extends Omit<SelectWithTagsProps, 'options' | 'selected' | 'value' | 'onInput'> {
    asyncSearchFn: useLazyLoadingProps['optionsFetcher'];

    multiple?: boolean;

    /**
     * Фетчер-функция для получения опций из массива уже выбранных значений
     * @param values массив выбранных значений
     */
    asyncOptionsByValuesFn(values: any[]): Promise<OptionShape[]>;

    clearOnSelect?: boolean;

    onInput?: SelectWithTagsProps['onInput'];
}
