import { HTMLProps, forwardRef } from 'react';

import { colors, scale, typography } from '@scripts/gds';

import CloseIcon from '@icons/small/closed.svg';

export interface TagProps extends HTMLProps<Omit<HTMLButtonElement, 'type'>> {
    onDelete?: () => void;
}

const Tag = ({ children, onDelete, onClick, disabled, tabIndex = onClick ? 0 : -1, ...props }: TagProps, ref: any) => (
    <button
        {...props}
        tabIndex={tabIndex}
        type="button"
        data-role="tag-button"
        ref={ref}
        onClick={onClick}
        css={{
            cursor: onClick ? 'pointer' : 'default',
            padding: `2px ${scale(1)}px`,
            overflow: 'hidden',
            minHeight: scale(3),
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            background: colors.white,
            border: `1px solid ${colors.grey400}`,
            ':hover': {
                background: colors.lightBlue,
            },
            color: colors.black,
            ...(disabled && {
                opacity: 0.5,
                background: colors.grey200,
            }),
            ...typography('bodySm'),
        }}
    >
        {children}
        {onDelete && (
            <span
                role="button"
                tabIndex={0}
                css={{ cursor: 'pointer', display: 'flex', flexShrink: 0, flexGrow: 1, ':hover': { opacity: 0.5 } }}
                title="Удалить"
                onClick={e => {
                    e.stopPropagation();
                    onDelete?.();
                }}
                onKeyDown={event => {
                    if ([' ', 'Enter'].includes(event.key)) {
                        event.stopPropagation();
                        onDelete?.();
                    }
                }}
            >
                <CloseIcon
                    css={{
                        fill: 'currentColor',
                        width: scale(2),
                        height: scale(2),
                        marginLeft: scale(1, true),
                    }}
                />
            </span>
        )}
    </button>
);

export default forwardRef(Tag) as typeof Tag;
