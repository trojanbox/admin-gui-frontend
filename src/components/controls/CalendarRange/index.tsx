import { useFormikContext } from 'formik';
import { FC, useRef } from 'react';
import { useFormContext } from 'react-hook-form';

import CalendarInput from '@components/controls/CalendarInput';
import Form from '@components/controls/Form';
import FutureForm from '@controls/future/Form';

import { Layout, scale } from '@scripts/gds';
import { useMedia } from '@scripts/hooks';

interface CalendarRangeProps {
    label?: string;
    nameFrom: string;
    nameTo: string;
}

const useIsomorphicValues = () => {
    const oldConsoleWarning = console.warn;
    console.warn = () => {};
    const formikContext = useFormikContext();
    console.warn = oldConsoleWarning;
    const formContext = useFormContext();

    if (!formikContext && !formContext) {
        throw new Error('CalendarRange can only exist as a descendant of formik or react-hook-form');
    }

    if (formContext) {
        const values = formContext.watch();

        return {
            values,
            form: 'rhf'
        };
    }

    return {
        values: formikContext.values as Record<string, any>,
        form: 'formik'
    };
};

const CalendarRange: FC<CalendarRangeProps> = ({ nameFrom, nameTo, label }) => {
    const { values, form } = useIsomorphicValues();
    const startDate = values[nameFrom] ? +values[nameFrom] : NaN;
    const endDate = values[nameTo] ? +values[nameTo] : NaN;
    const { md, xxl, xl, xs } = useMedia();

    const endRef = useRef<HTMLDivElement>(null);

    const FormField = form === 'rhf' ? FutureForm.Field : Form.Field;

    return (
        <Layout cols={{ xxxl: 2, xxs: 1 }}>
            <Layout.Item col={1}>
                <FormField name={nameFrom}>
                    <CalendarInput
                        label={label}
                        placeholder="От"
                        maxDate={endDate}
                        calendarProps={{
                            selectedFrom: startDate,
                            selectedTo: endDate,
                        }}
                        onChange={() => {
                            setTimeout(() => {
                                if (endRef?.current && !endDate) {
                                    endRef?.current?.click();
                                    endRef?.current?.focus();
                                }
                            }, 0);
                        }}
                    />
                </FormField>
            </Layout.Item>
            <Layout.Item
                col={1}
                align="end"
                css={{
                    position: 'relative',
                    '&::before': {
                        content: "'–'",
                        position: 'absolute',
                        left: -15,
                        bottom: scale(1),
                        [xxl]: {
                            left: -scale(3, true),
                        },
                        [xl]: {
                            left: -15,
                        },
                        [md]: {
                            left: -scale(3, true),
                        },
                        [xs]: {
                            content: 'none',
                        },
                    },
                }}
            >
                <FormField name={nameTo}>
                    <CalendarInput
                        placeholder="До"
                        minDate={startDate}
                        calendarProps={{
                            selectedFrom: startDate,
                            selectedTo: endDate,
                        }}
                        placement="bottom-end"
                        ref={endRef}
                    />
                </FormField>
            </Layout.Item>
        </Layout>
    );
};
export default CalendarRange;
