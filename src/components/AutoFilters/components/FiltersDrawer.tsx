import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';

import Checkbox from '@components/controls/Checkbox';
import Form from '@components/controls/Form';
import Tabs from '@components/controls/Tabs';
import Drawer from '@components/controls/future/Drawer';

import { Button, Layout, scale } from '@scripts/gds';
import { protectFieldName } from '@scripts/helpers';

import DragIcon from '@icons/small/dragAndDrop.svg';

import { useFiltersDrawerLocalHelper } from '../hooks';
import { FiltresDrawer } from '../types';
import { FilterOrderHelper } from './FilterOrderHelper';

const FiltersDrawer = ({
    isOpen,
    closeHandler,
    filtersSettings,
    setFiltersSettings,
    filters,
    filtersObject,
}: FiltresDrawer) => {
    const { formStyles, ulStyles, liStyles, initialValues, filterOrder, filtersOrdered, setFilterOrder, onDragEnd } =
        useFiltersDrawerLocalHelper({
            filtersSettings,
            filters,
        });

    return (
        <Drawer open={isOpen} onClose={closeHandler} placement="left">
            <Drawer.Header title="Настройка фильтров" onClose={closeHandler} />

            <Form
                initialValues={initialValues}
                enableReinitialize
                onSubmit={() => setFiltersSettings(filterOrder)}
                css={formStyles}
            >
                <Drawer.Content>
                    <FilterOrderHelper setFilterOrder={setFilterOrder} />

                    <Tabs>
                        <Tabs.List>
                            <Tabs.Tab>Показывать</Tabs.Tab>
                            <Tabs.Tab>Сортировать</Tabs.Tab>
                        </Tabs.List>

                        <Tabs.Panel>
                            <Layout cols={1} gap={scale(2)}>
                                {filtersOrdered?.map(f => (
                                    <Layout.Item key={f.code}>
                                        <Form.FastField name={protectFieldName(f.code)}>
                                            <Checkbox>{f.name}</Checkbox>
                                        </Form.FastField>
                                    </Layout.Item>
                                ))}
                            </Layout>
                        </Tabs.Panel>

                        <Tabs.Panel>
                            <DragDropContext onDragEnd={onDragEnd}>
                                <Droppable droppableId="column-order">
                                    {droppableprops => (
                                        <ul
                                            ref={droppableprops.innerRef}
                                            {...droppableprops.droppableProps}
                                            css={ulStyles}
                                        >
                                            {filterOrder.map((f, index) => (
                                                <Draggable key={f} draggableId={f} index={index}>
                                                    {(provided, snapshot) => (
                                                        <li
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                            css={{
                                                                ...liStyles,
                                                                ...(snapshot.isDragging && {
                                                                    svg: { opacity: 1 },
                                                                }),
                                                            }}
                                                        >
                                                            <DragIcon />
                                                            {filtersObject[f]}
                                                        </li>
                                                    )}
                                                </Draggable>
                                            ))}

                                            {droppableprops.placeholder}
                                        </ul>
                                    )}
                                </Droppable>
                            </DragDropContext>
                        </Tabs.Panel>
                    </Tabs>
                </Drawer.Content>

                <Drawer.Footer>
                    <Button type="button" theme="fill" block onClick={closeHandler}>
                        Отменить
                    </Button>

                    <Button type="submit" block onClick={closeHandler}>
                        Сохранить
                    </Button>
                </Drawer.Footer>
            </Form>
        </Drawer>
    );
};

export default FiltersDrawer;
