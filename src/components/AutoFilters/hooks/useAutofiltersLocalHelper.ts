import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo } from 'react';

import { MetaField } from '@api/common/types';

import { useLocalStorage } from '@scripts/hooks';

import { AutofiltersLocalHelper } from '../types';
import { usePopupHelper } from './usePopupHelper';

export const useAutofiltersLocalHelper = ({
    meta,
    manualHiddenFilters,
    filtersSettingsName,
    queryPart,
    onResetFilters,
}: AutofiltersLocalHelper) => {
    const {
        push,
        pathname,
        query: { tab, id },
    } = useRouter();

    const filters = useMemo(
        () => meta?.fields.filter(f => f.filter && !manualHiddenFilters?.includes(f.code)),
        [manualHiddenFilters, meta?.fields]
    );

    const filtersObject = useMemo(
        () =>
            filters?.reduce((acc, filter) => {
                acc[filter.code] = filter.name;
                return acc;
            }, {} as Record<string, string>) || {},
        [filters]
    );

    const [filtersSettings, setFiltersSettings] = useLocalStorage<string[]>(
        filtersSettingsName || `${pathname}FilterSettings`,
        []
    );

    useEffect(() => {
        if (filtersSettings.length === 0 && meta?.default_filter) setFiltersSettings(meta?.default_filter);
    }, [meta?.default_filter, filtersSettings, setFiltersSettings]);

    const filtersToShow = useMemo(
        () =>
            filtersSettings.reduce((acc, settingName) => {
                const findedFilter = filters?.find(f => f.code === settingName);
                if (findedFilter && !manualHiddenFilters?.includes(findedFilter?.code)) acc.push(findedFilter);
                return acc;
            }, [] as MetaField[]),
        [filters, filtersSettings, manualHiddenFilters]
    );

    const resetFormHelper = useCallback(() => {
        if (onResetFilters) onResetFilters();
        else
            push({
                pathname,
                query: {
                    ...queryPart,
                    ...(tab && { tab, ...(id && { id }) }),
                },
            });
    }, [onResetFilters, pathname, queryPart]);

    const { isOpen, openHandler, closeHandler } = usePopupHelper();

    const filtresDrawerProps = { isOpen, closeHandler, filtersSettings, setFiltersSettings, filters, filtersObject };

    return { filtersToShow, filtersSettings, resetFormHelper, openHandler, filtresDrawerProps };
};
