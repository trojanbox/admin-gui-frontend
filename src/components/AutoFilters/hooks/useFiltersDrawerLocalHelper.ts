import { FormikValues } from 'formik';
import { useCallback, useMemo, useState } from 'react';
import { DropResult } from 'react-beautiful-dnd';

import { MetaField } from '@api/common/types';

import { FiltersDrawerLocalHelper } from '../types';
import { useDrawerCSS } from './useDrawerCSS';

export const useFiltersDrawerLocalHelper = ({ filtersSettings, filters }: FiltersDrawerLocalHelper) => {
    const { formStyles, ulStyles, liStyles } = useDrawerCSS();

    const [filterOrder, setFilterOrder] = useState<string[]>(filtersSettings);

    /** react beautiful dnd callbacks */
    const reorderItems = useCallback(
        (startIndex: number, endIndex: number) => {
            const newFiltersOrder = filterOrder.slice();
            const [movedItem] = newFiltersOrder.splice(startIndex, 1);
            newFiltersOrder.splice(endIndex, 0, movedItem);
            setFilterOrder(newFiltersOrder);
        },
        [filterOrder]
    );

    const onDragEnd = useCallback(
        ({ source, destination }: DropResult) => {
            if (!destination || (destination.index === source.index && destination.droppableId === source.droppableId))
                return;
            reorderItems(source.index, destination.index);
        },
        [reorderItems]
    );

    const initialValues = useMemo(
        () =>
            filters?.reduce((acc, f) => {
                acc[f.code] = Boolean(filtersSettings?.includes(f.code));
                return acc;
            }, {} as FormikValues) || {},
        [filters, filtersSettings]
    );

    const filtersOrdered = useMemo(() => {
        if (!filters) return [];
        const [activeFilters, inactiveFilters] = filters.reduce(
            (acc, cur) => {
                if (filterOrder.includes(cur.code)) {
                    acc[0].push(cur);
                } else {
                    acc[1].push(cur);
                }
                return acc;
            },
            [[], []] as MetaField[][]
        );
        return [
            ...activeFilters.sort((a, b) => filterOrder.indexOf(a.code) - filterOrder.indexOf(b.code)),
            ...inactiveFilters,
        ];
    }, [filters, filterOrder]);

    return { initialValues, filterOrder, filtersOrdered, setFilterOrder, onDragEnd, formStyles, ulStyles, liStyles };
};
