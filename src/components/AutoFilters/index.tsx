import { FC } from 'react';

import { apiClient } from '@api/index';

import Block from '@components/Block';
import CalendarInput from '@components/controls/CalendarInput';
import CalendarRange from '@components/controls/CalendarRange';
import Form from '@components/controls/Form';
import Mask from '@components/controls/Mask';
import AutocompleteAsync from '@components/controls/future/AutocompleteAsync';
import Input from '@components/controls/future/Input';
import FormikSelect from '@components/controls/future/Select';
import SelectWithTags from '@components/controls/future/SelectWithTags';

import { Button, Layout, scale } from '@scripts/gds';
import { protectFieldName } from '@scripts/helpers';
import { useMedia } from '@scripts/hooks';
import { maskPhone } from '@scripts/mask';

import ResetIcon from '@icons/small/reset.svg';
import FilterIcon from '@icons/small/sliders.svg';

import FiltersDrawer from './components/FiltersDrawer';
import SkeletonBlock from './components/SkeletonBlock';
import { booleanOptions } from './helper';
import { useAutofiltersLocalHelper } from './hooks';
import { AutoFiltersTypes } from './types';

const AutoFilters: FC<AutoFiltersTypes> = ({
    initialValues,
    emptyInitialValues,
    onSubmit,
    onResetFilters,
    manualHiddenFilters,
    filtersActive,
    className,
    meta,
    queryPart,
    isLoading,
    filtersSettingsName,
}) => {
    const { md, xxl, xl } = useMedia();

    const { filtersToShow, filtersSettings, resetFormHelper, openHandler, filtresDrawerProps } =
        useAutofiltersLocalHelper({
            meta,
            manualHiddenFilters,
            filtersSettingsName,
            queryPart,
            onResetFilters,
        });

    if (isLoading) {
        return <SkeletonBlock className={className} />;
    }

    return (
        <>
            <Block className={className}>
                <Form initialValues={initialValues} onSubmit={onSubmit} enableReinitialize>
                    <Block.Body css={{ borderRadius: 0 }}>
                        <Layout cols={{ xxxl: 4, md: 3, sm: 2, xs: 1 }}>
                            {filtersToShow?.map(f => {
                                const {
                                    type,
                                    code,
                                    name,
                                    enum_info,
                                    filter_name,
                                    filter_key,
                                    filter,
                                    filter_range_key_from,
                                    filter_range_key_to,
                                } = f;

                                if (!filtersSettings.includes(code)) return null;

                                const filterLabel = filter_name || name;
                                const fieldName = protectFieldName(filter_key || code);

                                const items = enum_info?.values?.map(i => ({
                                    key: i.title,
                                    content: i.title,
                                    value: i.id,
                                }));

                                const asyncSearchFn = async (query: string) => {
                                    if (enum_info?.endpoint) {
                                        const res = await apiClient.post(enum_info?.endpoint?.split('v1/')[1], {
                                            data: { filter: { query } },
                                        });
                                        return {
                                            options: res.data.map((i: { title: string; id: string }) => ({
                                                key: i.title,
                                                value: i.id,
                                            })),
                                            hasMore: false,
                                        };
                                    }
                                    return {
                                        options: [],
                                        hasMore: false,
                                    };
                                };

                                const asyncOptionsByValuesFn = async (vals: string[]) => {
                                    if (enum_info?.endpoint) {
                                        const res = await apiClient.post(enum_info?.endpoint?.split('v1/')[1], {
                                            data: { filter: { query: '', id: vals } },
                                        });
                                        return res.data.map((i: { title: string; id: string }) => ({
                                            key: i.title,
                                            value: i.id,
                                        }));
                                    }
                                    return [];
                                };

                                if (filter === 'range' && filter_range_key_from && filter_range_key_to) {
                                    if (['date', 'datetime'].includes(type)) {
                                        return (
                                            <Layout.Item col={1} key={code} align="end">
                                                <CalendarRange
                                                    nameFrom={protectFieldName(filter_range_key_from)}
                                                    nameTo={protectFieldName(filter_range_key_to)}
                                                    label={filterLabel}
                                                />
                                            </Layout.Item>
                                        );
                                    }
                                    return (
                                        <Layout.Item col={1} key={code} align="end">
                                            <Layout cols={{ xxxl: 2, xxs: 1 }}>
                                                <Layout.Item col={1}>
                                                    <Form.FastField
                                                        name={protectFieldName(filter_range_key_from)}
                                                        label={filterLabel}
                                                    >
                                                        <Input type="number" placeholder="От" block />
                                                    </Form.FastField>
                                                </Layout.Item>
                                                <Layout.Item
                                                    col={1}
                                                    css={{
                                                        position: 'relative',
                                                        '&::before': {
                                                            content: "'–'",
                                                            position: 'absolute',
                                                            left: -15,
                                                            bottom: scale(1),
                                                            [xxl]: {
                                                                left: -scale(3, true),
                                                            },
                                                            [xl]: {
                                                                left: -15,
                                                            },
                                                            [md]: {
                                                                left: -scale(3, true),
                                                            },
                                                        },
                                                    }}
                                                    align="end"
                                                >
                                                    <Form.FastField name={protectFieldName(filter_range_key_to)}>
                                                        <Input type="number" placeholder="До" block />
                                                    </Form.FastField>
                                                </Layout.Item>
                                            </Layout>
                                        </Layout.Item>
                                    );
                                }

                                if (filter === 'many' && type === 'enum') {
                                    if (items) {
                                        return (
                                            <Layout.Item col={1} key={code} align="end">
                                                <Form.FastField name={fieldName}>
                                                    <SelectWithTags options={items} label={filterLabel} />
                                                </Form.FastField>
                                            </Layout.Item>
                                        );
                                    }
                                    if (enum_info?.endpoint) {
                                        return (
                                            <Layout.Item col={1} key={code} align="end">
                                                <Form.FastField name={fieldName} label={filterLabel}>
                                                    <AutocompleteAsync
                                                        asyncSearchFn={asyncSearchFn}
                                                        asyncOptionsByValuesFn={asyncOptionsByValuesFn}
                                                        multiple
                                                        collapseTagList
                                                    />
                                                </Form.FastField>
                                            </Layout.Item>
                                        );
                                    }
                                }

                                return (
                                    <Layout.Item col={1} key={code} align="end">
                                        {['date', 'datetime'].includes(type) && (
                                            <Form.FastField name={fieldName}>
                                                <CalendarInput label={filterLabel} />
                                            </Form.FastField>
                                        )}
                                        {type === 'enum' && items && (
                                            <Form.FastField name={fieldName}>
                                                <FormikSelect label={filterLabel} options={items} />
                                            </Form.FastField>
                                        )}
                                        {type === 'enum' && enum_info?.endpoint && (
                                            <Form.FastField name={fieldName} label={filterLabel}>
                                                <AutocompleteAsync
                                                    asyncSearchFn={asyncSearchFn}
                                                    asyncOptionsByValuesFn={asyncOptionsByValuesFn}
                                                    multiple
                                                    collapseTagList
                                                />
                                            </Form.FastField>
                                        )}
                                        {type === 'phone' && (
                                            <Form.FastField name={fieldName} label={filterLabel} type="tel">
                                                <Mask mask={maskPhone} />
                                            </Form.FastField>
                                        )}
                                        {type === 'bool' && (
                                            <Form.FastField name={fieldName}>
                                                <FormikSelect label={filterLabel} options={booleanOptions} />
                                            </Form.FastField>
                                        )}
                                        {type === 'string' && (
                                            <Form.FastField name={fieldName} label={filterLabel}>
                                                <Input block />
                                            </Form.FastField>
                                        )}
                                        {(type === 'url' || type === 'email') && (
                                            <Form.FastField name={fieldName} label={filterLabel}>
                                                <Input type={type} block />
                                            </Form.FastField>
                                        )}
                                        {type === 'int' && (
                                            <Form.TypedField
                                                name={fieldName}
                                                label={filterLabel}
                                                fieldType="positiveInt"
                                            />
                                        )}
                                        {['price', 'float'].includes(type) && (
                                            <Form.FastField name={fieldName} label={filterLabel}>
                                                <Input type="number" block />
                                            </Form.FastField>
                                        )}
                                    </Layout.Item>
                                );
                            })}
                        </Layout>
                    </Block.Body>

                    <Block.Footer>
                        <div>
                            <Button theme="secondary" Icon={FilterIcon} onClick={openHandler}>
                                Настройка фильтров
                            </Button>
                        </div>

                        <div css={{ display: 'flex' }}>
                            {filtersActive && (
                                <Form.Reset
                                    theme="fill"
                                    Icon={ResetIcon}
                                    type="button"
                                    initialValues={emptyInitialValues}
                                    onClick={resetFormHelper}
                                >
                                    Сбросить
                                </Form.Reset>
                            )}

                            <Button theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Footer>
                </Form>
            </Block>

            <FiltersDrawer {...filtresDrawerProps} />
        </>
    );
};

export default AutoFilters;
