import { CSSObject } from '@emotion/core';
import { FormikValues } from 'formik';

import { Meta, MetaField } from '@api/common/types';

export interface AutofiltersLocalHelper {
    manualHiddenFilters?: string[];
    meta: Meta | undefined;
    filtersSettingsName?: string;
    queryPart?: object;
    onResetFilters?: () => void;
}

export interface AutoFiltersTypes extends AutofiltersLocalHelper {
    initialValues: FormikValues;
    emptyInitialValues: FormikValues;
    onSubmit: (values: FormikValues) => void;
    filtersActive?: boolean;
    className?: string;
    isLoading?: boolean;
}

export interface FiltersDrawerLocalHelper {
    filtersSettings: string[];
    filters: MetaField[] | undefined;
}

export interface FiltresDrawer extends FiltersDrawerLocalHelper {
    isOpen: boolean;
    closeHandler: () => void;
    setFiltersSettings: (val: string[]) => void;
    filtersObject: Record<string, string>;
}

export interface DrawerStylesTypes {
    formStyles?: CSSObject;
    ulStyles?: CSSObject;
    liStyles?: CSSObject;
}
