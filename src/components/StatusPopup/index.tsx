import Popup, { PopupProps } from '@controls/future/Popup';

import { Layout, scale, typography, useTheme } from '@scripts/gds';

import CheckCircleIcon from '@icons/20/checkCircle.svg';
import DeleteIcon from '@icons/20/delete.svg';
import WarningIcon from '@icons/20/warning.svg';

enum StatusEnum {
    SUCCESS = 'success',
    WARNING = 'warning',
    DELETE = 'delete',
}

interface StatusPopupProps extends Omit<PopupProps, 'title'> {
    status?: string;
    title?: string;
}

const StatusPopup = ({ status = StatusEnum.SUCCESS, title, children, ...props }: StatusPopupProps) => {
    const { colors } = useTheme();
    return (
        <Popup hasCloser={false} {...props}>
            {/* TODO: Ставлю паддинги пока-что через css, если тенденция на них сохранится, то заменить в Body */}
            <Popup.Content>
                <Layout cols={[`${scale(5, true)}px`, 1]} gap={scale(2)}>
                    <Layout.Item>
                        {status === StatusEnum.SUCCESS && <CheckCircleIcon css={{ fill: colors?.success }} />}
                        {status === StatusEnum.WARNING && <WarningIcon css={{ fill: colors?.warning }} />}
                        {status === StatusEnum.DELETE && <DeleteIcon css={{ fill: colors?.danger }} />}
                    </Layout.Item>
                    <Layout.Item>
                        <p css={{ marginBottom: scale(1, true), ...typography('h3') }}>{title}</p>
                        {children}
                    </Layout.Item>
                </Layout>
            </Popup.Content>
        </Popup>
    );
};

export default StatusPopup;
