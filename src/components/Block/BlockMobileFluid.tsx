import { ReactNode } from 'react';

import { scale } from '@scripts/gds';
import { useMedia } from '@scripts/hooks/useMedia';

export interface BlockBodyProps {
    className?: string;
    children?: ReactNode;
}

/**
 * Через отрицательный margin заполняет содержимое блока во всю ширину.
 * Работает только на мобилках для таблиц.
 */
const BlockBodyFluid = ({ className, children }: BlockBodyProps) => {
    const { md } = useMedia();
    return (
        <div
            className={className}
            css={{
                [md]: {
                    marginLeft: -scale(2),
                    marginRight: -scale(2),
                },
            }}
        >
            {children}
        </div>
    );
};

export default BlockBodyFluid;
