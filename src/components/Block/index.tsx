import { jsx } from '@emotion/core';
import { ElementType, ReactNode, ReactNodeArray } from 'react';

import { scale, useTheme } from '@scripts/gds';

import BlockBody from './BlockBody';
import BlockMobileFluid from './BlockMobileFluid';
import BlockFooter from './BlockFooter';
import BlockHeader from './BlockHeader';

type BlockProps<P extends ElementType = 'section'> = {
    /** Use your own React component for render. */
    as?: P;
    children: ReactNode | ReactNodeArray;
    className?: string;
};

const Block = <T extends ElementType = 'section'>({ as, ...props }: BlockProps<T>) => {
    const { shadows, colors } = useTheme();

    return jsx(as || 'section', {
        css: { boxShadow: shadows?.big, width: '100%', backgroundColor: colors?.white, borderRadius: scale(1) },
        ...props,
    });
};

Block.Header = BlockHeader;
Block.Body = BlockBody;
Block.MobileFluid = BlockMobileFluid;
Block.Footer = BlockFooter;

export default Block;
