import { useCallback, useEffect, useState } from 'react';

interface RowData {
    id: number;
}

interface Payload<TRowData extends RowData, Cols extends string> {
    id: number;
    value: Cols extends keyof TRowData ? TRowData[Cols] : unknown;
    column: Cols;
}

export const useEditableTableRows = <
    T extends RowData,
    Cols extends string = keyof T extends string ? Exclude<keyof T, 'id'> : string
>(
    getInitialData: (id: number) => T,
    reduce: (old: T, payload: Payload<T, Cols>) => T
) => {
    const [extraRowsData, setExtraRowsData] = useState<Record<number, T>>({});

    useEffect(() => () => setExtraRowsData({}), []);

    const onRowChange = useCallback(
        <TCol extends string>(p: Payload<T, TCol>) => {
            if (!p.id) return;

            setExtraRowsData(old => {
                const newOld = { ...old };

                if (!(p.id in newOld)) newOld[p.id] = getInitialData(p.id);

                newOld[p.id] = reduce(newOld[p.id], p as never as Payload<T, Cols>);

                return newOld;
            });
        },
        [getInitialData, reduce]
    );

    return { extraRowsData, setExtraRowsData, onRowChange };
};
