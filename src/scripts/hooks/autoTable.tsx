import { cloneDeep, get, set } from 'lodash';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';
import { UseQueryResult, useQueries } from 'react-query';

import { CommonResponse, FetchError, FilterResponse, Meta, MetaEnumValue, MetaField } from '@api/common/types';
import { apiClient } from '@api/index';

import { Cell, ExtendedColumn } from '@components/Table';

import { cleanPhoneValue, fromRoubleToKopecks, isEmptyArray, toISOString } from '@scripts/helpers';
import { useFiltersHelper, useLinkCSS } from '@scripts/hooks';

import { useFiltersHelperClient } from './useFiltersHelperClient';

type MetaFieldObject = Record<string, Omit<MetaField, 'code'>>;

interface ParamsProps {
    isSaveToUrl?: boolean;
}

// из апи приходит url с v1, а наш apiClient принимает без v1
const getRightEndpoint = (url: string) => url.split('v1/')[1];

/**
 * Хук для получения
 */
export const useAutoFilters = (meta: Meta | undefined, params?: ParamsProps) => {
    const isSaveToUrl = useMemo(
        () => (typeof params?.isSaveToUrl === 'undefined' ? true : params.isSaveToUrl),
        [params?.isSaveToUrl]
    );

    // формируем объект полей, где ключом является code
    const metaField = useMemo(
        () =>
            meta?.fields.reduce((acc, cur) => {
                acc[cur?.sort_key || cur?.code] = cur;
                return acc;
            }, {} as MetaFieldObject) || undefined,
        [meta?.fields]
    );

    // формируем объект initialValues в зависимости от типов полей
    const emptyInitialValues = useMemo(
        () =>
            meta?.fields.reduce((acc, { filter, filter_key, code, filter_range_key_to, filter_range_key_from }) => {
                if (!filter) return acc;

                if (filter === 'range' && filter_range_key_from && filter_range_key_to) {
                    acc[filter_range_key_from] = '';
                    acc[filter_range_key_to] = '';
                    return acc;
                }

                acc[filter_key || code] = filter === 'many' ? [] : '';
                return acc;
            }, {} as Record<string, any>) || {},
        [meta?.fields]
    );

    const {
        initialValues: urlValues,
        URLHelper,
        filtersActive: urlFiltersActive,
    } = useFiltersHelper(emptyInitialValues);

    const {
        initialValues: valuesClient,
        pushValues: pushValuesClient,
        filtersActive: filtersActiveClient,
    } = useFiltersHelperClient(emptyInitialValues);

    const pushValues = isSaveToUrl ? URLHelper : pushValuesClient;
    const values = isSaveToUrl ? urlValues : valuesClient;
    const filtersActive = isSaveToUrl ? urlFiltersActive : filtersActiveClient;

    const reset = useCallback(() => {
        pushValues(emptyInitialValues);
    }, [emptyInitialValues, pushValues]);

    const searchRequestFilter = useMemo(() => {
        const keys = Object.keys(values) as string[];

        return keys.reduce((acc, key) => {
            if (key === 'tab') return acc;
            const val = values[key];
            const code = key.replace(/(_from)|(_to)|(_like)/, '');

            if (val && code && metaField && metaField[code]) {
                switch (metaField[code].type) {
                    case 'datetime':
                    case 'date': {
                        const time = key.includes('_to') ? '23:59:59.999' : '00:00:00.000';
                        acc[key] = `${toISOString(val)}T${time}Z`;
                        break;
                    }
                    case 'price':
                        acc[key] = fromRoubleToKopecks(val);
                        break;
                    case 'phone':
                        acc[key] = cleanPhoneValue(val);
                        break;
                    default:
                        acc[key] = val;
                        break;
                }
            } else if (val && !isEmptyArray(val)) {
                acc[key] = val;
            }
            return acc;
        }, {} as Record<string, any>);
    }, [values, metaField]);

    const searchRequestIncludes = useMemo(() => {
        if (!meta?.fields.length) return [];
        const includes: string[] = [];

        meta?.fields.forEach(field => {
            if (field.is_object && field.include && !includes.includes(field.include)) {
                includes.push(field.include);
            }
        });

        return includes;
    }, [meta?.fields]);

    const isLoading = !meta;

    return {
        isLoading,
        metaField,
        filtersActive,
        URLHelper: pushValues,
        reset,
        values,
        emptyInitialValues,
        searchRequestIncludes,
        searchRequestFilter,
    };
};

const nameKeys = ['product.name', 'name'];

/**
 * Хук для получения автосгенерированных столбцов
 */
export const useAutoColumns = (meta: Meta | undefined, canViewDetailPage = true, detailsPageName = '[id]') => {
    const { pathname } = useRouter();
    const listLink = pathname.split(detailsPageName)[0];
    const linkStyles = useLinkCSS();

    return useMemo(() => {
        if (!meta?.fields || meta?.fields.length === 0) return [];

        return meta?.fields?.reduce((acc, field) => {
            if (field.list && !acc.find(({ accessor }) => accessor === field.code)) {
                acc.push({
                    Header: field.name,
                    accessor: field.sort_key || field.code,
                    disableSortBy: !field.sort,
                    Cell: ({ value, row, ...props }) =>
                        nameKeys.includes(field.code) && canViewDetailPage ? (
                            <Link
                                href={`${row.original.customListLink || listLink}/${
                                    row.original.customIdResolver
                                        ? row.original.customIdResolver(row.original)
                                        : row.original.id
                                }`}
                                passHref
                            >
                                <a css={linkStyles}>{value}</a>
                            </Link>
                        ) : (
                            <Cell value={value} type={field.type} row={row} metaField={field} {...props} />
                        ),
                });
            }

            return acc;
        }, [] as ExtendedColumn[]);
    }, [meta?.fields, linkStyles, listLink, canViewDetailPage]);
};

/**
 * Хук автоматически дообогащает данными поля, в которых требовался дополнительный запрос в апи
 */
export function useAutoTableData<T extends Record<string, any>>(
    /**
     * данные, полученные от бэка. Массив строк таблицы
     * */
    dataToEnrich: Array<T> | undefined,
    /**
     * данные, полученные из хука useAutoFilters
     * */
    metaField: MetaFieldObject | undefined,
    /**
     * массив кодов из метода :meta, для которых не будут подставлять человекопонятный текст,
     * но вернется объект типа {title: string, id: string}
     * */
    codesExceptions?: string[]
): Array<T> {
    // Найдем поля, по которым нужно запрашивать данные. Объект типа {code: {endpoint: string, ids: string[]}}
    const dataForRequest = useMemo(
        () =>
            dataToEnrich?.reduce((acc, tableRow) => {
                Object.keys(tableRow).forEach(key => {
                    const isExisting = metaField && metaField[key];
                    if (!isExisting) return;

                    const value = tableRow[key];
                    const isAllowedType = ['number', 'string'].includes(typeof value);
                    if (!isAllowedType) return;

                    const { type, enum_info } = metaField[key];
                    if (type !== 'enum' || !enum_info?.endpoint) return;

                    if (!acc[key]) {
                        acc[key] = { endpoint: getRightEndpoint(enum_info.endpoint), ids: [value] };
                        return;
                    }

                    if (!acc[key].ids.includes(value)) {
                        acc[key].ids.push(value);
                    }
                });
                return acc;
            }, {} as Record<string, { endpoint: string; ids: string[] }>) || {},
        [dataToEnrich, metaField]
    );

    const dataForRequestKeys = useMemo(() => Object.keys(dataForRequest), [dataForRequest]);

    const enumsDataFromRequest = useQueries(
        dataForRequestKeys.map(key => ({
            queryKey: [key, dataForRequest[key]],
            queryFn: () =>
                apiClient.post(dataForRequest[key].endpoint, {
                    data: { filter: { id: dataForRequest[key].ids } },
                }),
        }))
    ) as unknown as UseQueryResult<CommonResponse<FilterResponse>, FetchError>[];

    const resultEnumsData = useMemo(
        () =>
            enumsDataFromRequest.reduce((acc, { data, isSuccess }, index) => {
                acc[dataForRequestKeys[index]] = isSuccess ? data?.data : [];
                return acc;
            }, {} as Record<string, any>),
        [dataForRequestKeys, enumsDataFromRequest]
    );

    // обогатим данные для таблицы данными, полученными
    // из запроса или из :meta enum_info.values
    const enrichedData = useMemo(() => {
        if (!dataToEnrich) return [];

        return dataToEnrich.map(tableRow => {
            if (!metaField) return tableRow;

            const enrichedRow = cloneDeep(tableRow);

            Object.keys(metaField).forEach(code => {
                const filter = metaField[code];
                if (!filter || filter.type !== 'enum') return;

                const saveFullObject = codesExceptions?.includes(code);
                const { enum_info } = filter;

                const value = get(enrichedRow, code);

                let foundMetaValue: MetaEnumValue | undefined;
                if (enum_info?.values) {
                    foundMetaValue = Array.isArray(value)
                        ? value.map((val: any) => enum_info!.values!.find(v => String(v.id) === String(val)))
                        : enum_info.values.find(v => String(v.id) === String(value));
                } else if (enum_info?.endpoint) {
                    foundMetaValue = resultEnumsData[code]?.find(
                        (list: { id: string; title: string }) => String(list.id) === String(value)
                    );
                }

                if (foundMetaValue) {
                    const valueToSave = saveFullObject ? foundMetaValue : foundMetaValue.title;
                    set(enrichedRow, `${code}_original`, foundMetaValue);
                    set(enrichedRow, code, valueToSave);
                }
            });

            return enrichedRow;
        });
    }, [dataToEnrich, metaField, resultEnumsData, codesExceptions]);

    return enrichedData;
}
