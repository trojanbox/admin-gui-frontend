import { useRouter } from 'next/router';
import { useEffect } from 'react';

import { usePrevious } from './usePrevious';

type PushParams = Parameters<ReturnType<typeof useRouter>['push']>;
export const useRedirectWhen = (to: PushParams[0], condition: boolean, options?: PushParams[2]) => {
    const prevCondition = usePrevious(condition);
    const { push } = useRouter();

    useEffect(() => {
        if (!prevCondition && condition) {
            push(to, undefined, options || {});
        }
    }, [prevCondition, condition, to, push, options]);
};
