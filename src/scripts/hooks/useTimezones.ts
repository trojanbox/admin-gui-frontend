import timezones, { Timezone } from 'countries-and-timezones';
import { useMemo } from 'react';

export const useTimezones = () => {
    const list: { [name: string]: Timezone } = timezones.getAllTimezones();

    // @TODO если аналитики решат получать таймзоны с бэка, то ликвидировать
    const options = Object.keys(list)
        ?.reduce((acc, cur: string) => {
            if (acc?.find(item => item?.utcOffset === item?.dstOffset && item?.utcOffset === list[cur]?.utcOffset))
                return acc;
            return [...acc, list[cur]];
        }, [] as Timezone[])
        ?.sort((a, b) => b?.utcOffset - a?.utcOffset)
        ?.map(({ name, utcOffsetStr }) => ({ value: name, label: `${name} ${utcOffsetStr}` }));

    return useMemo(() => options, [options]);
};
