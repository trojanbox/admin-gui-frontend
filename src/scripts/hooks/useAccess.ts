import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

enum BaseActionEnum {
    view = 'view',
    edit = 'edit',
    delete = 'delete',
}

type BaseAction = keyof typeof BaseActionEnum;
type Action = 'all' | BaseAction;
type CommonSections = 'LIST' | 'ID';

type BaseAccessUnit<T> = Record<BaseAction, T>;
type AccessUnit<T> = Record<Action, T>;

type AccessSection<T> = {
    [key: string]: AccessSection<T> | Partial<AccessUnit<T>>;
};

export type AccessMatrix = Record<CommonSections, AccessSection<number> | Partial<AccessUnit<number>>>;

type ReturnedMatrix<T> = 'all' extends keyof T
    ? Required<BaseAccessUnit<boolean>>
    : {
          [K in keyof T]: T[K] extends object ? ReturnedMatrix<T[K]> : boolean;
      };

const parseMatrix = <T extends Record<string, any>>(matrix: T, acesses: number[]) => {
    const keys = Object.keys(matrix);
    return keys.reduce((parsedMatrix, key: string) => {
        if (parsedMatrix[key] !== undefined) return parsedMatrix;

        const value = matrix[key];
        if (key === 'all' && typeof value === 'number') {
            const access = acesses.includes(value);
            Object.keys(BaseActionEnum).forEach(k => {
                parsedMatrix[k] = access;
            });
            return parsedMatrix;
        }

        if (typeof value === 'number') {
            parsedMatrix[key] = acesses.includes(value);
            return parsedMatrix;
        }

        if (typeof value === 'object') {
            parsedMatrix[key] = parseMatrix(value, acesses);
        }

        return parsedMatrix;
    }, {} as Record<string, any>);
};

export const useAccess = <T extends AccessMatrix>(accessMatrix: T) => {
    const { data: userData } = useCurrentUser();

    return useMemo(
        () => parseMatrix(accessMatrix, userData?.data.rights_access || []),
        [accessMatrix, userData]
    ) as ReturnedMatrix<T>;
};
