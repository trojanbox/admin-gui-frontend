import { useRouter } from 'next/router';
import { useCallback, useEffect, useId, useState } from 'react';

import type { TabsProps } from '@controls/future/Tabs';

/** useFutureTabs - хук, который возвращает пропгеттер для компонента future/Tabs
 * @param {string} name - используй в случае нескольких отдельных компонентов табов
 */
export const useFutureTabs = (tabName = 'tab', defaultTab: string | number = 0) => {
    const { push, query } = useRouter();

    const prefix = useId();
    const queryTab = query[tabName] as string;
    const tabIndex = queryTab || defaultTab;

    const [selectedId, setSelectedId] = useState(tabIndex);

    /** обновляем значение в useEffect, чтобы можно было пользоваться стрелками вперед/назад в браузере */
    useEffect(() => {
        if (tabIndex) setSelectedId(tabIndex);
    }, [prefix, tabIndex]);

    const getTabsProps = useCallback(
        (): Partial<TabsProps> => ({
            selectedId,
            onChange: (_, payload) => {
                const newQuery = JSON.parse(JSON.stringify(query));
                /** для того, чтобы ?tab=0 не показывалось, удалим из обоих объектов */
                if (payload.selectedId === 0) {
                    delete newQuery[tabName];
                } else {
                    // eslint-disable-next-line prefer-destructuring
                    newQuery[tabName] = `${payload.selectedId}`.split('_')[1];
                }

                push({ query: newQuery }, undefined, {
                    shallow: true,
                });
            },
        }),
        [selectedId, push, tabName, query]
    );

    return {
        prefix,
        selectedId,
        getTabsProps,
    };
};
