import { uniqueId } from 'lodash';
import {
    CSSProperties,
    HTMLProps,
    ReactNode,
    ReactNodeArray,
    createContext,
    useCallback,
    useContext,
    useEffect,
    useMemo,
} from 'react';
import { ToastPosition, toast, useToaster } from 'react-hot-toast';

import { FetchError } from '@api/index';

import Modal, { ModalProps } from '@components/controls/Modal';

import { Button, scale, useTheme } from '@scripts/gds';

type DefaultToast = Omit<ModalProps, 'onClose' | 'children'> & {
    promise?: never;
};

type PromiseToast = {
    promise: Promise<any>;
    loading: {
        title: string;
        message?: string;
    };
    success: {
        title: string;
        message?: string;
    };
    error: {
        title: string;
        message?: string;
    };
};

export type ToastProps = (DefaultToast | PromiseToast) & {
    duration?: number;
};

type ModalId = string;

export interface ModalContextProps {
    appendModal: (modal: ToastProps) => ModalId;
}

const ModalContext = createContext<ModalContextProps | null>(null);
ModalContext.displayName = 'ModalContext';

interface ToastWrapperProps {
    id: string;
    className?: string;
    style?: CSSProperties;
    onHeightUpdate: (id: string, height: number) => void;
    children?: ReactNode;
    ariaProps?: Partial<Omit<HTMLProps<HTMLDivElement>, 'children' | 'className' | 'ref' | 'style'>>;
}

const ToastWrapper = ({ id, className, style, onHeightUpdate, children, ariaProps }: ToastWrapperProps) => {
    const ref = useCallback(
        (el: HTMLElement | null) => {
            if (el) {
                const updateHeight = () => {
                    const { height } = el.getBoundingClientRect();
                    onHeightUpdate(id, height);
                };
                updateHeight();
                new MutationObserver(updateHeight).observe(el, {
                    subtree: true,
                    childList: true,
                    characterData: true,
                });
            }
        },
        [id, onHeightUpdate]
    );

    return (
        <div ref={ref} className={className} style={style} {...ariaProps}>
            {children}
        </div>
    );
};

const DEFAULT_DURATION = 5000;
const TOAST_SPACING = scale(1);

export const ModalProvider = ({ children }: { children: ReactNode | ReactNodeArray }) => {
    const { colors } = useTheme();

    const { toasts, handlers } = useToaster();
    const { startPause, endPause, calculateOffset } = handlers;

    const activeToasts = useMemo(() => toasts.filter(e => e.visible), [toasts]);
    const firstVisibleToast = activeToasts.length > 0 ? activeToasts[activeToasts.length - 1] : undefined;

    const offsetProps: {
        reverseOrder?: boolean;
        gutter?: number;
        defaultPosition?: ToastPosition;
    } = {
        reverseOrder: false,
        gutter: TOAST_SPACING,
        defaultPosition: 'top-right',
    };

    const closeAllBtnOffset = firstVisibleToast
        ? calculateOffset(firstVisibleToast, offsetProps) + (firstVisibleToast.height || 0) + (offsetProps.gutter || 0)
        : undefined;

    const removeAll = useCallback(() => {
        toasts.forEach(t => toast.remove(t.id));
    }, [toasts]);

    const memoizedValue = useMemo<ModalContextProps>(
        () => ({
            appendModal(modal) {
                const id = uniqueId();

                const duration = modal.duration || DEFAULT_DURATION;

                if (modal.promise) {
                    toast.promise(
                        modal.promise!,
                        {
                            loading: (
                                <Modal theme="dark" closeBtn onClose={() => toast.dismiss(id)} {...modal.loading} />
                            ),
                            success: () => (
                                <Modal theme="success" closeBtn onClose={() => toast.dismiss(id)} {...modal.success} />
                            ),
                            error: arg => {
                                if (typeof arg === 'object' && 'message' in arg) {
                                    return (
                                        <Modal
                                            theme="error"
                                            closeBtn
                                            {...modal.error}
                                            onClose={() => toast.dismiss(id)}
                                            message={arg.message}
                                        />
                                    );
                                }

                                return (
                                    <Modal theme="error" closeBtn onClose={() => toast.dismiss(id)} {...modal.error} />
                                );
                            },
                        },
                        {
                            id,
                            duration,
                        }
                    );
                } else {
                    toast.custom(<Modal {...modal} closeBtn onClose={() => toast.dismiss(id)} />, {
                        id,
                        duration,
                    });
                }

                return id;
            },
        }),
        []
    );

    return (
        <>
            <div
                onMouseEnter={startPause}
                onMouseLeave={endPause}
                css={{
                    position: 'fixed',
                    zIndex: 9999,
                    pointerEvents: 'none',
                    inset: 0,
                }}
            >
                {toasts.map(t => {
                    const offset = calculateOffset(t, offsetProps);

                    const isReactNode = t.message && typeof t.message === 'object' && '$$typeof' in t.message;

                    if (!isReactNode) {
                        console.error('Modal requires toast to be a React node ', t, 'is not a React node');
                        return;
                    }

                    return (
                        <ToastWrapper
                            id={t.id}
                            key={t.id}
                            onHeightUpdate={handlers.updateHeight}
                            css={{
                                right: scale(2),
                                top: scale(2),
                                display: 'flex',
                                position: 'absolute',
                                transition: 'all 230ms cubic-bezier(0.21, 1.02, 0.73, 1) 0s',
                                justifyContent: 'end',
                                maxWidth: scale(36),
                                pointerEvents: 'auto',
                            }}
                            style={{
                                transform: `translateY(${offset}px)`,
                                opacity: t.visible ? 1 : 0,
                                ...(!t.visible && {
                                    pointerEvents: 'none',
                                }),
                            }}
                            ariaProps={t.ariaProps}
                        >
                            {t.message}
                        </ToastWrapper>
                    );
                })}
                {activeToasts.length > 1 && firstVisibleToast && (
                    <div
                        css={{
                            position: 'absolute',
                            right: scale(2),
                            top: scale(2),
                            pointerEvents: 'auto',
                            transition: 'all 230ms cubic-bezier(0.21, 1.02, 0.73, 1) 0s',
                        }}
                        style={{
                            transform: `translateY(${closeAllBtnOffset}px)`,
                        }}
                    >
                        <Button
                            onClick={() => removeAll()}
                            css={{ backgroundColor: colors?.secondaryHover, color: colors?.white }}
                        >
                            Закрыть все
                        </Button>
                    </div>
                )}
            </div>
            <ModalContext.Provider value={memoizedValue}>{children}</ModalContext.Provider>
        </>
    );
};

export const useModalsContext = () => {
    const context = useContext(ModalContext);

    if (!context) {
        throw new Error(`Hook useModal must be used within ModalProvider`);
    }

    return context;
};

export const useError = (err?: FetchError | null) => {
    const { appendModal } = useModalsContext();

    useEffect(() => {
        if (err?.message || err?.code) appendModal({ title: err.code, message: err.message, theme: 'error' });
    }, [err?.message, appendModal, err?.code]);
};

export const useSuccess = (message: string | false) => {
    const { appendModal } = useModalsContext();

    useEffect(() => {
        if (message) appendModal({ message, theme: 'success' });
    }, [appendModal, message]);
};
