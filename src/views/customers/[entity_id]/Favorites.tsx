import { FormikValues } from 'formik';
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import * as Yup from 'yup';

import { useCreateFavorite, useCustomer, useDeleteFavorite, useFavorites } from '@api/customers';

import Block from '@components/Block';
import OldTable, { TableRowProps } from '@components/OldTable';
import Form from '@components/controls/Form';
import Popup from '@components/controls/future/Popup';

import { ErrorMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { usePopupState } from '@scripts/hooks';

import PlusIcon from '@icons/small/plus.svg';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Product ID',
        accessor: 'productId',
    },
];

type State = {
    id?: string;
    productId?: string;
    action?: ActionType;
    open?: boolean;
};

const Favorites = () => {
    const { query } = useRouter();
    const id = query.id?.toString() || '';

    const { data: customerData } = useCustomer(id);
    const customerId = customerData?.data?.id;
    const { data: favorites } = useFavorites({ filter: { customer_id: customerId }, enabled: !!customerId });

    const tableData = useMemo(
        () =>
            favorites?.data?.map(favorite => ({
                id: favorite.id,
                productId: favorite.product_id,
            })) || [],
        [favorites]
    );

    const initialValues = {
        id: '',
        productId: '',
    };
    const initialState = {
        ...initialValues,
        action: ActionType.Close,
        open: false,
    };

    const createFavorite = useCreateFavorite();
    const deleteFavorite = useDeleteFavorite();

    const [popupState, popupDispatch] = usePopupState<State>(initialState);

    const close = () => popupDispatch({ type: ActionType.Close });

    const onRowDelete = (row?: TableRowProps) => {
        if (row) {
            popupDispatch({
                type: ActionType.Delete,
                payload: {
                    id: row.id,
                    productId: row.productId,
                },
            });
        }
    };

    const onSubmit = async (vals: FormikValues) => {
        if (customerId) {
            if (popupState.id) {
                await createFavorite.mutateAsync({ product_id: +vals.productId, customer_id: customerId });
            }
            close();
        }
    };

    return (
        <Block>
            <Block.Header>
                <div>
                    <Button onClick={() => popupDispatch({ type: ActionType.Add })} Icon={PlusIcon}>
                        Добавить избранный товар
                    </Button>
                </div>
            </Block.Header>
            <Block.Body>
                {tableData.length ? (
                    <OldTable
                        columns={COLUMNS}
                        data={tableData}
                        needSettingsBtn={false}
                        needCheckboxesCol={false}
                        deleteRow={onRowDelete}
                    />
                ) : (
                    <p>Товаров в избранном не найдено</p>
                )}
            </Block.Body>
            <Popup
                open={Boolean(popupState?.open && popupState.action !== ActionType.Delete)}
                onClose={close}
                size="sm"
            >
                <Popup.Header title="Создание избранного" />
                <Popup.Content>
                    <Form
                        initialValues={popupState}
                        onSubmit={onSubmit}
                        validationSchema={Yup.object().shape({
                            productId: Yup.string().required(ErrorMessages.REQUIRED),
                        })}
                    >
                        <Layout cols={4}>
                            <Layout.Item col={4}>
                                <Form.FastField name="productId" label="Product ID" />
                            </Layout.Item>
                            <Layout.Item col={4} justify="end">
                                <Button theme="secondary" css={{ marginRight: scale(2) }} onClick={close}>
                                    Отменить
                                </Button>
                                <Button type="submit">Создать</Button>
                            </Layout.Item>
                        </Layout>
                    </Form>
                </Popup.Content>
            </Popup>
            <Popup
                open={Boolean(popupState?.open && popupState.action === ActionType.Delete)}
                onClose={close}
                size="sm"
            >
                <Popup.Header title="Вы уверены, что хотите удалить избранное?" />
                <Popup.Content>
                    <p css={{ marginBottom: scale(2) }}>
                        {popupState.id}# id товара:{popupState.productId}
                    </p>
                    <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Button theme="secondary" onClick={close} css={{ marginRight: scale(2) }}>
                            Отменить
                        </Button>
                        <Button
                            onClick={async () => {
                                if (popupState.productId && customerId) {
                                    await deleteFavorite.mutateAsync({
                                        customer_id: customerId,
                                        product_id: +popupState.productId,
                                    });
                                    close();
                                }
                            }}
                        >
                            Удалить
                        </Button>
                    </div>
                </Popup.Content>
            </Popup>
        </Block>
    );
};

export default Favorites;
