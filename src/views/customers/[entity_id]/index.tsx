import { CSSObject } from '@emotion/core';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useRef, useState } from 'react';
import { FieldValues, useFormContext } from 'react-hook-form';
import * as Yup from 'yup';

import {
    CustomerMutateWithId,
    useCustomer,
    useCustomerDeletePersonalData,
    useDeleteCustomer,
    useDeleteCustomerAvatar,
    useUpdateCustomer,
    useUploadCustomerAvatar,
} from '@api/customers';

import { useError, useSuccess } from '@context/modal';

import Textarea from '@controls/Textarea';
import Form from '@controls/future/Form';
import Popup from '@controls/future/Popup';
import Select from '@controls/future/Select';
import Tabs from '@controls/future/Tabs';

import Block from '@components/Block';
import CopyButton from '@components/CopyButton';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import { InfoListItemCommonType } from '@components/InfoList/types';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import { StatusSimplePopup } from '@components/StatusSimplePopup';
import Dropzone from '@components/controls/Dropzone';
import { downloadFile } from '@components/controls/Dropzone/utils';

import { CREATE_PARAM, ErrorMessages, FileSizes, ModalMessages } from '@scripts/constants';
import { ButtonNameEnum, CustomerGender, customerGenderValues, customerStatusValues } from '@scripts/enums';
import { Button, Layout, colors, scale } from '@scripts/gds';
import { useFutureTabs } from '@scripts/hooks/useFutureTabs';

import CheckIcon from '@icons/small/check.svg';

import Addresses from './Addresses';
import Orders from './Orders';

const Controls = ({
    isCreationPage,
    onDelete,
    onSubmit,
    onApply,
    canDeletePd,
    onDeletePd,
}: {
    isCreationPage: boolean;
    onDelete: () => void;
    onApply: () => void;
    onSubmit: () => void;
    canDeletePd: boolean;
    onDeletePd: () => void;
}) => {
    const {
        formState: { isDirty },
    } = useFormContext();
    return (
        <Layout type="flex">
            {!isCreationPage && (
                <Layout.Item>
                    <Button theme="dangerous" onClick={onDelete}>
                        Удалить
                    </Button>
                </Layout.Item>
            )}
            {canDeletePd && (
                <Layout.Item>
                    <Button theme="dangerous" onClick={onDeletePd}>
                        Удалить персональные данные
                    </Button>
                </Layout.Item>
            )}
            <Layout.Item>
                <Button theme="secondary" type="submit" disabled={!isDirty} onClick={onApply}>
                    Применить
                </Button>
            </Layout.Item>
            <Layout.Item>
                <Button type="submit" Icon={CheckIcon} iconAfter disabled={!isDirty} onClick={onSubmit}>
                    Сохранить
                </Button>
            </Layout.Item>
        </Layout>
    );
};

const trStyles: CSSObject = {
    ':not(:last-of-type)': { borderBottom: `1px solid ${colors.grey200}` },
};

const thStyles: CSSObject = {
    textAlign: 'left',
    padding: scale(1),
};

const tdStyles: CSSObject = {
    padding: `${scale(3, true)}px ${scale(1)}px`,
};

const Customer = () => {
    const { query, pathname, push } = useRouter();
    const listLink = pathname.split(`[entity_id]`)[0];

    const { getTabsProps } = useFutureTabs();
    const [isChangeStatusOpen, setIsChangeStatusOpen] = useState(false);
    const [isRemoveOpen, setIsRemoveOpen] = useState(false);

    const statuses = useMemo(
        () => Object.keys(customerStatusValues).map(k => ({ value: +k, key: customerStatusValues[k] })),
        []
    );
    const genderOptions = useMemo(
        () => Object.keys(customerGenderValues).map(k => ({ value: +k, key: customerGenderValues[k] })),
        []
    );

    const id = +(query.entity_id?.toString() || '');
    const isCreationPage = query.entity_id === CREATE_PARAM;

    const { data, isLoading, isIdle } = useCustomer(id);
    const customer = data?.data;
    const updateCustomer = useUpdateCustomer();
    const deleteCustomer = useDeleteCustomer();
    const deletePersonalData = useCustomerDeletePersonalData();
    const updateAvatar = useUploadCustomerAvatar();
    const deleteAvatar = useDeleteCustomerAvatar();

    useError(updateCustomer.error);
    useError(deleteCustomer.error);
    useError(deletePersonalData.error);

    useSuccess(updateCustomer.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const onStatusUpdate = async (vals: FieldValues) => {
        setIsChangeStatusOpen(false);

        if (customer) {
            const putData: CustomerMutateWithId = {
                ...customer,
                id: +id,
                comment_status: vals?.comment,
            };

            delete (putData as any).avatar;

            await updateCustomer.mutateAsync(putData);
        }
    };

    const buttonNameRef = useRef<ButtonNameEnum | null>(null);

    const clientName = customer?.is_deleted ? '(удален)' : customer?.first_name;
    const pageTitle = `Клиент ${clientName}`;

    const canDeletePd = !isLoading && !customer?.is_deleted;

    const [isDeletePersonalDataOpen, setDeletePersonalDataOpen] = useState(false);

    const [initialAvatarFile, setInitialAvatarFile] = useState<File[]>([]);

    useEffect(() => {
        if (!customer?.avatar) {
            setInitialAvatarFile([]);
            return;
        }
        downloadFile(customer.avatar).then(res => {
            if (res) setInitialAvatarFile([res]);
        });
    }, [customer?.avatar]);

    return (
        <>
            <PageWrapper
                title={pageTitle}
                isLoading={
                    isLoading ||
                    isIdle ||
                    updateCustomer.isLoading ||
                    deleteCustomer.isLoading ||
                    updateAvatar.isLoading ||
                    deleteAvatar.isLoading
                }
            >
                <Form
                    initialValues={{
                        name: customer?.first_name || '',
                        middleName: customer?.middle_name || '',
                        lastName: customer?.last_name || '',
                        gender: customer?.gender || CustomerGender.MALE,
                        comment: customer?.comment_status || '',
                        city: customer?.city || '',
                        file: initialAvatarFile,
                    }}
                    enableReinitialize
                    validationSchema={Yup.object().shape({
                        name: Yup.string().required(ErrorMessages.REQUIRED),
                        middleName: Yup.string().nullable(),
                        lastName: Yup.string().required(ErrorMessages.REQUIRED),
                        gender: Yup.number()
                            .transform(val => (Number.isNaN(val) ? undefined : val))
                            .required(ErrorMessages.REQUIRED),
                        comment: Yup.string().nullable(),
                        city: Yup.string().nullable(),
                    })}
                    css={{ width: '100%', marginBottom: scale(3) }}
                    onSubmit={async (values, { reset }) => {
                        if (!isCreationPage) {
                            const customerClone = { ...customer! };
                            delete customerClone.avatar;

                            await updateCustomer.mutateAsync({
                                ...customerClone,
                                first_name: values.name,
                                middle_name: values.middleName,
                                last_name: values.lastName,
                                gender: values.gender,
                                comment_status: values.comment,
                                city: values.city,
                            });

                            if (
                                values.file.length > 0 &&
                                values.file[0] &&
                                values.file[0]?.name !== initialAvatarFile[0]?.name
                            ) {
                                const formData = new FormData();
                                formData.append('file', values.file[0]);
                                await updateAvatar.mutateAsync({ id: customer!.id, file: formData });
                            } else if (!values.file.length && initialAvatarFile.length) {
                                await deleteAvatar.mutateAsync(customer!.id);
                            }

                            reset(values);

                            if (buttonNameRef.current === ButtonNameEnum.SAVE) push({ pathname: listLink });
                        }
                    }}
                >
                    <PageLeaveGuard />
                    <PageTemplate
                        h1={pageTitle}
                        backlink={{ text: 'К списку клиентов', href: '/customers' }}
                        customChildren
                        controls={
                            <Controls
                                isCreationPage={isCreationPage}
                                onDelete={() => setIsRemoveOpen(true)}
                                onApply={() => {
                                    buttonNameRef.current = ButtonNameEnum.APPLY;
                                }}
                                onSubmit={() => {
                                    buttonNameRef.current = ButtonNameEnum.SAVE;
                                }}
                                canDeletePd={canDeletePd}
                                onDeletePd={() => setDeletePersonalDataOpen(true)}
                            />
                        }
                        aside={
                            !isCreationPage && (
                                <>
                                    <div css={{ marginBottom: scale(1) }}>
                                        <span css={{ marginRight: scale(1, true), color: colors?.grey800 }}>ID:</span>
                                        <CopyButton>{`${id}`}</CopyButton>
                                    </div>
                                    <InfoList>
                                        {(
                                            [
                                                {
                                                    name: 'Активность',
                                                    value: customer?.active ? 'Активный' : `Неактивный`,
                                                },
                                                {
                                                    name: 'Удален?',
                                                    value: customer?.is_deleted,
                                                    type: 'boolean',
                                                },
                                                ...(customer?.is_deleted
                                                    ? [
                                                          {
                                                              name: 'Дата удаления',
                                                              value: customer.delete_request,
                                                              type: 'date',
                                                          },
                                                      ]
                                                    : []),
                                                ...getDefaultDates({ ...customer }),
                                                {
                                                    name: 'Дата последнего посещения клиента',
                                                    value: customer?.last_visit_date,
                                                    type: 'date',
                                                },
                                                {
                                                    name: 'Часовой пояс клиента',
                                                    value: customer?.timezone,
                                                },
                                            ] as InfoListItemCommonType[]
                                        ).map(item => (
                                            <InfoList.Item key={item.name} {...item} />
                                        ))}
                                    </InfoList>
                                </>
                            )
                        }
                    >
                        <Layout cols={1} css={{ flexGrow: 2 }}>
                            <Block>
                                <Block.Body>
                                    <table
                                        css={{
                                            width: '100%',
                                            borderCollapse: 'collapse',
                                            marginBottom: scale(4),
                                        }}
                                    >
                                        <thead>
                                            <tr css={{ borderBottom: `1px solid ${colors?.grey200}` }}>
                                                <th css={thStyles}>Инфопанель</th>
                                                <td
                                                    colSpan={3}
                                                    css={{
                                                        textAlign: 'right',
                                                        ...tdStyles,
                                                    }}
                                                >
                                                    {/* <Button onClick={() => setIsChangeStatusOpen(true)}>
                                                        Изменить статус
                                                    </Button> */}
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>Аватар</th>
                                                <td colSpan={3} css={tdStyles}>
                                                    <Form.Field name="file" css={{ marginTop: scale(1) }}>
                                                        <Dropzone
                                                            maxFiles={1}
                                                            accept={['image/jpeg', 'image/jpg', 'image/png']}
                                                            maxSize={FileSizes.MB1}
                                                        />
                                                    </Form.Field>
                                                </td>
                                            </tr>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>ФИО</th>
                                                <td css={tdStyles}>
                                                    <Form.Field name="lastName" placeholder="Фамилия" />
                                                </td>
                                                <td css={tdStyles}>
                                                    <Form.Field name="name" placeholder="Имя" />
                                                </td>
                                                <td css={tdStyles}>
                                                    <Form.Field name="middleName" placeholder="Отчество" />
                                                </td>
                                            </tr>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>Телефон</th>
                                                <td colSpan={3} css={tdStyles}>
                                                    {customer?.phone}
                                                </td>
                                            </tr>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>E-mail</th>
                                                <td colSpan={3} css={tdStyles}>
                                                    {customer?.email}
                                                </td>
                                            </tr>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>Пол</th>
                                                <td css={tdStyles}>
                                                    <Form.Field name="gender">
                                                        <Select options={genderOptions} hideClearButton />
                                                    </Form.Field>
                                                </td>
                                            </tr>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>Город</th>
                                                <td css={tdStyles}>
                                                    <Form.Field name="city" />
                                                </td>
                                            </tr>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>Комментарий</th>
                                                <td colSpan={3} css={tdStyles}>
                                                    <Form.Field name="comment">
                                                        <Textarea minRows={5} />
                                                    </Form.Field>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </Block.Body>
                            </Block>
                        </Layout>
                    </PageTemplate>
                </Form>
                <Tabs {...getTabsProps()}>
                    <Tabs.Tab title="Адреса" id="0">
                        <Addresses />
                    </Tabs.Tab>
                    <Tabs.Tab title="Заказы" id="1">
                        <Orders customerId={customer?.id!} />
                    </Tabs.Tab>
                </Tabs>
            </PageWrapper>
            <Popup open={isChangeStatusOpen} onClose={() => setIsChangeStatusOpen(false)} size="minSm">
                <Popup.Header title="Изменить статус" />
                <Popup.Content>
                    <Form
                        onSubmit={onStatusUpdate}
                        initialValues={{ status: '', comment: '' }}
                        validationSchema={Yup.object().shape({
                            status: Yup.number()
                                .transform(old => {
                                    if (!+old) return undefined;
                                    return +old;
                                })
                                .required(ErrorMessages.REQUIRED),
                            comment: Yup.string(),
                        })}
                    >
                        <Form.Field name="status" label="Статус проверки" css={{ marginBottom: scale(2) }}>
                            <Select options={statuses} />
                        </Form.Field>
                        <Form.Field name="comment" label="Комментарий к статусу" css={{ marginBottom: scale(4) }}>
                            <Textarea rows={3} />
                        </Form.Field>
                        <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Form.Reset
                                theme="outline"
                                onClick={() => setIsChangeStatusOpen(false)}
                                css={{ marginRight: scale(2) }}
                            >
                                Отменить
                            </Form.Reset>
                            <Button type="submit" theme="primary">
                                Сохранить
                            </Button>
                        </div>
                    </Form>
                </Popup.Content>
            </Popup>
            <Popup open={isDeletePersonalDataOpen} onClose={() => setDeletePersonalDataOpen(false)} size="sm">
                <Popup.Header title="Вы уверены, что хотите удалить персональные данные клиента?" />
                <Popup.Footer>
                    <Button type="button" theme="secondary" onClick={() => setDeletePersonalDataOpen(false)}>
                        Не удалять
                    </Button>
                    <Button
                        type="button"
                        theme="dangerous"
                        onClick={async () => {
                            setDeletePersonalDataOpen(false);
                            try {
                                await deletePersonalData.mutateAsync(+id);
                            } catch (err) {
                                console.error(err);
                            }
                        }}
                    >
                        Удалить
                    </Button>
                </Popup.Footer>
            </Popup>
            <StatusSimplePopup
                close={() => setIsRemoveOpen(false)}
                isOpen={isRemoveOpen}
                title={`Вы уверены, что хотите удалить клиента ${customer?.last_name}?`}
                text=""
                theme="delete"
                footer={
                    <>
                        <Button
                            theme="secondary"
                            onClick={() => {
                                setIsRemoveOpen(false);
                            }}
                        >
                            Отмена
                        </Button>
                        <Button
                            theme="dangerous"
                            onClick={async () => {
                                setIsRemoveOpen(false);
                                if (!id) return;

                                await deleteCustomer.mutateAsync(Number(id));

                                push({ pathname: listLink });
                            }}
                        >
                            Да
                        </Button>
                    </>
                }
            />
        </>
    );
};

export default Customer;
