import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { useCustomerOrdersMeta } from '@api/customers';
import { Order, useOrdersSearch } from '@api/orders';

import { useError } from '@context/modal';

import AutoFilters from '@components/AutoFilters';
import Block from '@components/Block';
import Table, {
    ExtendedRow,
    TableEmpty,
    TableFooter,
    TableHeader,
    TooltipContentProps,
    getSettingsColumn,
} from '@components/Table';
import LoadWrapper from '@components/controls/LoadWrapper';

import { scale } from '@scripts/gds';
import { declOfNum, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';

export interface OrdersProps {
    customerId: number;
}

const Orders = ({ customerId }: OrdersProps) => {
    const { push, pathname, query } = useRouter();

    const { data: metaData, error: metaError } = useCustomerOrdersMeta(!!customerId);
    const meta = metaData?.data;
    useError(metaError);

    const { sort, activePage, itemsPerPageCount, setItemsPerPageCount, setSort } = useTableList({
        defaultSort: meta?.default_sort,
    });

    const { metaField, values, filtersActive, URLHelper, searchRequestFilter, emptyInitialValues } =
        useAutoFilters(meta);

    const { data, isLoading, error } = useOrdersSearch(
        {
            sort: sort || meta?.default_sort,
            filter: {
                ...searchRequestFilter,
                customer_id: customerId,
            },
            include: [],
            pagination: { type: 'offset', limit: itemsPerPageCount, offset: (activePage - 1) * itemsPerPageCount },
        },
        Boolean(meta)
    );
    useError(error);

    const rows = useAutoTableData<Order>(data?.data, metaField);
    const canViewDetailPage = true;
    const autoGeneratedColumns = useAutoColumns(meta, canViewDetailPage);

    const convertedTableData = useMemo(
        () =>
            rows.map(e => ({
                ...e,
                customListLink: '/orders/list',
            })),
        [rows]
    );

    const columns = useMemo(
        () => [
            ...autoGeneratedColumns,
            getSettingsColumn({ columnsToDisable: [], defaultVisibleColumns: meta?.default_list }),
        ],
        [autoGeneratedColumns, meta?.default_list]
    );

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);
    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total });

    const goToDetailPage = useCallback(
        (originalRows: ExtendedRow['original'] | ExtendedRow['original'][] | undefined) => {
            if (!Array.isArray(originalRows) && canViewDetailPage) push(`/orders/list/${originalRows?.id}`);
        },
        [push, canViewDetailPage]
    );

    const tooltipContent: TooltipContentProps[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Перейти к заказу',
                action: goToDetailPage,
                onDisable: () => canViewDetailPage,
            },
        ],
        [canViewDetailPage, goToDetailPage]
    );

    const resetFilters = useCallback(() => {
        push(
            {
                pathname,
                query: {
                    tab: query.tab,
                    entity_id: query.entity_id,
                },
            },
            undefined,
            {
                scroll: false,
            }
        );
    }, [pathname, push, query.entity_id, query.tab]);

    const renderHeader = useCallback(
        () => (
            <TableHeader>
                <p>Найдено {`${total} ${declOfNum(total, ['заказ', 'заказа', 'заказов'])}`}</p>
            </TableHeader>
        ),
        [total]
    );

    return (
        <Block css={{ borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
            <Block.Body>
                <LoadWrapper isLoading={isLoading}>
                    <h2>Заказы</h2>
                    <AutoFilters
                        initialValues={values}
                        emptyInitialValues={emptyInitialValues}
                        onSubmit={URLHelper}
                        filtersActive={filtersActive}
                        css={{ marginBottom: scale(2), boxShadow: 'none', padding: 0 }}
                        meta={meta}
                        onResetFilters={resetFilters}
                        queryPart={{ tab: query.tab }}
                    />
                    <Table
                        columns={columns}
                        data={convertedTableData}
                        onSortingChange={setSort}
                        onRowClick={goToDetailPage}
                        renderHeader={renderHeader}
                        allowRowSelect={false}
                        tooltipContent={tooltipContent}
                    />

                    {convertedTableData.length === 0 && !isLoading ? (
                        <TableEmpty
                            filtersActive={filtersActive}
                            titleWithFilters="Заказы не найдены"
                            titleWithoutFilters="Заказов нет"
                            addItems={() => setItemsPerPageCount(10)}
                            onResetFilters={resetFilters}
                        />
                    ) : (
                        <TableFooter
                            pages={totalPages}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                        />
                    )}
                </LoadWrapper>
            </Block.Body>
        </Block>
    );
};

export default Orders;
