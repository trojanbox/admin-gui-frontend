import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useFormContext } from 'react-hook-form';
import * as Yup from 'yup';

import {
    useDeleteDiscount,
    useDiscount,
    useDiscountCreate,
    useDiscountEdit,
    useDiscountStatuses,
    useDiscountTypes,
    useDiscountValueTypes,
} from '@api/marketing';

import { useError, useSuccess } from '@context/modal';

import Select from '@controls/future/Select';

import CopyButton from '@components/CopyButton';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import CalendarRange from '@components/controls/CalendarRange';
import Checkbox from '@components/controls/Checkbox';
import LoadWrapper from '@components/controls/LoadWrapper';
import Popup from '@components/controls/Popup';
import Form from '@components/controls/future/Form';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, colors, scale } from '@scripts/gds';
import { fromKopecksToRouble, fromRoubleToKopecks, toISOString } from '@scripts/helpers';
import { useRedirectWhen } from '@scripts/hooks/useRedirectWhen';

import { prepareValuesForFutureSelect } from '../../helpers';
import { AddProductsPopup } from './AddProductsPopup';
import BindedProducts from './BindedProducts';
import { DiscountStatusEnum, DiscountTypeEnum, DiscountValueTypeEnum } from './types';

const formatDiscountFromKopecks = (valueType?: number, value?: number) => {
    if (valueType === undefined || value === undefined) return 0;
    if (valueType === DiscountValueTypeEnum.PERCENT) return value;
    return fromKopecksToRouble(value);
};

const formatDiscountFromRub = (valueType?: number, value?: number) => {
    if (valueType === undefined || value === undefined) return 0;
    if (valueType === DiscountValueTypeEnum.PERCENT) return value;
    return fromRoubleToKopecks(value);
};

const Controls = ({
    isCreationPage,
    onSubmit,
    onApply,
    onDelete,
}: {
    isCreationPage: boolean;
    onSubmit: () => void;
    onApply: () => void;
    onDelete: () => void;
}) => {
    const {
        formState: { isDirty },
    } = useFormContext();

    const { push } = useRouter();

    return (
        <>
            {!isCreationPage && (
                <Button theme="dangerous" type="button" onClick={onDelete}>
                    Удалить
                </Button>
            )}
            <Button
                theme="secondary"
                onClick={() =>
                    push({
                        pathname: '/marketing/discounts',
                    })
                }
            >
                Закрыть
            </Button>
            <Button theme="secondary" type="submit" disabled={!isDirty} onClick={onApply}>
                Применить
            </Button>
            <Button theme="primary" type="submit" disabled={!isDirty} onClick={onSubmit}>
                Сохранить
            </Button>
        </>
    );
};

const MarketingDiscounts = () => {
    const { query, push } = useRouter();

    const discountId = (query?.entity_id && +query?.entity_id) || 0;

    const {
        data: apiDiscount,
        isLoading,
        isIdle,
        error,
    } = useDiscount(discountId, ['products', 'products.product'].join(','));
    const discount = useMemo(() => apiDiscount?.data, [apiDiscount]);

    const { data: apiStatuses, error: errorStatuses } = useDiscountStatuses();
    const discountStatuses = useMemo(
        () => (apiStatuses?.data ? prepareValuesForFutureSelect(apiStatuses.data) : []),
        [apiStatuses]
    );

    const { data: apiTypes, error: errorTypes } = useDiscountTypes();
    const discountTypes = useMemo(
        () => (apiTypes?.data ? prepareValuesForFutureSelect(apiTypes.data) : []),
        [apiTypes]
    );

    const { data: apiValueTypes, error: errorValueTypes } = useDiscountValueTypes();
    const discountValueTypes = useMemo(
        () => (apiValueTypes?.data ? prepareValuesForFutureSelect(apiValueTypes.data) : []),
        [apiValueTypes]
    );

    const createDiscount = useDiscountCreate();
    const editDiscount = useDiscountEdit();

    useError(errorStatuses || errorTypes || errorValueTypes || createDiscount.error || editDiscount.error);
    useSuccess(createDiscount.isSuccess || editDiscount.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const isCreationPage = query?.entity_id === CREATE_PARAM;
    const isNotFound = !apiDiscount && !(isLoading || isIdle);

    useError(error);
    useRedirectWhen('/marketing/discounts', isNotFound);

    const initialValues = useMemo(
        () => ({
            type: discount ? discount?.type : '',
            name: discount?.name || '',
            value_type: discount ? discount.value_type : '',
            value: formatDiscountFromKopecks(discount?.value_type, discount?.value),
            status: discount ? discount.status : '',
            start_date: discount?.start_date ? new Date(discount.start_date).getTime() : null,
            end_date: discount?.end_date ? new Date(discount.end_date).getTime() : null,
            promo_code_only: discount ? discount.promo_code_only : false,
        }),
        [discount]
    );

    const [isOffer, setIsOffer] = useState(false);
    const [isAddingProducts, setAddingProducts] = useState(false);

    useEffect(() => {
        if (!discount) return;

        setIsOffer(discount.type === DiscountTypeEnum.OFFER);
    }, [discount]);

    const deleteDiscount = useDeleteDiscount();
    const [isDeleting, setDeleting] = useState(false);

    const onDeleteDiscount = useCallback(async () => {
        await deleteDiscount.mutateAsync(discount!.id);
        push('/marketing/discounts');
        setDeleting(false);
    }, [discount, deleteDiscount, push]);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const canEditStatus = (_: Pick<typeof initialValues, 'type'>) => true;

    const title = isCreationPage ? 'Создание скидки' : `Редактировать скидку ${discountId}`;
    const returnToListing = useRef(false);

    return (
        <PageWrapper title={title} isLoading={isLoading || createDiscount.isLoading || editDiscount.isLoading}>
            <Form
                enableReinitialize
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    type: Yup.number().required(ErrorMessages.REQUIRED),
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    value_type: Yup.number().required(ErrorMessages.REQUIRED),
                    value: Yup.number()
                        .when('value_type', {
                            is: (val: any) => Number(val) === Number(DiscountValueTypeEnum.PERCENT),
                            then: Yup.number()
                                .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                                .max(100, `${ErrorMessages.LESS_OR_EQUAL} 100`),
                            otherwise: Yup.number().moreThan(0, `${ErrorMessages.GREATER} 0`),
                        })
                        .required(ErrorMessages.REQUIRED),
                    status: Yup.number().when('type', {
                        is: (val: any) => canEditStatus({ type: val }),
                        then: Yup.number().required(ErrorMessages.REQUIRED),
                        otherwise: Yup.number().nullable(),
                    }),
                    start_date: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    end_date: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    promo_code_only: Yup.boolean().required(ErrorMessages.REQUIRED),
                })}
                onSubmit={async (values, helpers) => {
                    const data = {
                        ...values,
                        type: +values.type,
                        value_type: +values.value_type,
                        status: +values.status,
                        value: formatDiscountFromRub(+values.value_type, +values.value),
                        start_date: values.start_date ? toISOString(new Date(values.start_date)) : undefined,
                        end_date: values.end_date ? toISOString(new Date(values.end_date)) : undefined,
                        ...(!canEditStatus(values) && {
                            status: DiscountStatusEnum.SUSPENDED,
                        }),
                    };

                    if (isCreationPage) {
                        const result = await createDiscount.mutateAsync(data);
                        helpers.reset(values);

                        setTimeout(() => {
                            push(`/marketing/discounts/${result.data.id}`);
                        }, 0);
                    } else {
                        await editDiscount.mutateAsync({ ...data, id: discountId });
                        helpers.reset(values);

                        setTimeout(() => {
                            if (returnToListing.current) {
                                push('/marketing/discounts/');
                            }
                        }, 0);
                    }
                }}
            >
                {({ watch }) => (
                    <>
                        <PageLeaveGuard />
                        <PageTemplate
                            backlink={{
                                href: '/marketing/discounts',
                                text: 'Назад к списку скидок',
                            }}
                            h1={title}
                            controls={
                                <Controls
                                    onDelete={() => setDeleting(true)}
                                    isCreationPage={isCreationPage}
                                    onApply={() => {
                                        returnToListing.current = false;
                                    }}
                                    onSubmit={() => {
                                        returnToListing.current = true;
                                    }}
                                />
                            }
                            aside={
                                <>
                                    <div css={{ marginBottom: scale(1) }}>
                                        <span css={{ marginRight: scale(1, true), color: colors?.grey800 }}>ID:</span>
                                        <CopyButton>{`${discountId}`}</CopyButton>
                                    </div>
                                    <InfoList css={{ marginBottom: scale(5) }}>
                                        {getDefaultDates({ ...discount }).map(item => (
                                            <InfoList.Item {...item} key={item.name} />
                                        ))}
                                    </InfoList>
                                </>
                            }
                        >
                            <Layout cols={4}>
                                <Layout.Item col={4}>
                                    <Form.Field name="name" label="Название" />
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="type" label="Тип">
                                        <Select
                                            hideClearButton
                                            options={discountTypes}
                                            onChange={e => {
                                                setTimeout(() => {
                                                    setIsOffer(e?.selected?.value === DiscountTypeEnum.OFFER);
                                                }, 0);
                                            }}
                                        />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Layout cols={2}>
                                        <Layout.Item>
                                            <Form.Field name="value_type" label="Тип значения">
                                                <Select hideClearButton options={discountValueTypes} />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item>
                                            <Form.Field
                                                name="value"
                                                label={
                                                    watch('value_type') === DiscountValueTypeEnum.PERCENT
                                                        ? 'Значение в процентах'
                                                        : 'Значение в рублях'
                                                }
                                                type="number"
                                            />
                                            <p css={{ color: colors.grey600, marginTop: scale(1, true) }}>
                                                {watch('value_type') === DiscountValueTypeEnum.PERCENT
                                                    ? 'Значение от 0 до 100'
                                                    : 'Значение больше 0'}
                                            </p>
                                        </Layout.Item>
                                    </Layout>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <CalendarRange
                                        label="Период действия скидки"
                                        nameFrom="start_date"
                                        nameTo="end_date"
                                    />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.Field name="status" label="Статус">
                                        <Select hideClearButton options={discountStatuses} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Form.Field name="promo_code_only" css={{ width: 'fit-content' }}>
                                        <Checkbox>Скидка действительна только по промокоду</Checkbox>
                                    </Form.Field>
                                </Layout.Item>
                            </Layout>
                        </PageTemplate>
                    </>
                )}
            </Form>
            {!isCreationPage && (
                <div
                    css={{
                        ...(!isOffer
                            ? {
                                  visibility: 'hidden',
                                  pointerEvents: 'none',
                              }
                            : {
                                  marginTop: scale(2),
                              }),
                    }}
                >
                    <BindedProducts discountId={discountId} onAdd={() => setAddingProducts(true)} />
                </div>
            )}
            <Popup
                isOpen={isDeleting}
                onRequestClose={() => setDeleting(false)}
                title="Вы уверены, что хотите удалить скидку?"
            >
                <LoadWrapper
                    error={deleteDiscount.error ? JSON.stringify(deleteDiscount.error) : undefined}
                    isLoading={deleteDiscount.isLoading}
                >
                    <Popup.Footer>
                        <Button type="button" onClick={() => setDeleting(false)} theme="secondary">
                            Не удалять
                        </Button>
                        <Button type="button" onClick={() => onDeleteDiscount()} theme="dangerous">
                            Удалить
                        </Button>
                    </Popup.Footer>
                </LoadWrapper>
            </Popup>
            <AddProductsPopup
                discountId={discountId}
                isOpen={isAddingProducts}
                onClose={() => setAddingProducts(false)}
            />
        </PageWrapper>
    );
};

export default MarketingDiscounts;
