import { FormikValues } from 'formik';

import { SellerRowData } from '@api/units';

import Form from '@components/controls/Form';
import Select, { SelectItemProps } from '@components/controls/Select';
import Popup from '@components/controls/future/Popup';

import { Button, scale } from '@scripts/gds';

const ChangeStatusPopup = ({
    onSubmit,
    isOpen,
    close,
    selectedRows,
    statuses,
}: {
    onSubmit: (vals: FormikValues) => void;
    isOpen: boolean;
    close: () => void;
    selectedRows: SellerRowData[];
    statuses: SelectItemProps[];
}) => (
    <Popup open={isOpen} onClose={close} size="sm">
        <Popup.Header title={`Изменить статус продавц${selectedRows.length === 1 ? 'а' : 'ов'}`} />
        <Popup.Content>
            <Form onSubmit={onSubmit} initialValues={{ status: '' }}>
                <ol css={{ li: { listStyle: 'decimal inside' }, marginBottom: scale(2) }}>
                    {selectedRows.map(r => (
                        <li key={r.id}>{r.organizationName.name}</li>
                    ))}
                </ol>
                <Form.Field name="status" css={{ marginBottom: scale(2) }}>
                    <Select label="Сменить статус" items={statuses} />
                </Form.Field>
                <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button type="submit" theme="primary">
                        Сохранить
                    </Button>
                </div>
            </Form>
        </Popup.Content>
    </Popup>
);

export default ChangeStatusPopup;
