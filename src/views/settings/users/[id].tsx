import { FormikValues, useFormikContext } from 'formik';
import { useRouter } from 'next/router';
import { ReactNode, useMemo, useState } from 'react';
import * as Yup from 'yup';

import {
    useAddAdminUserRole,
    useAdminRoles,
    useAdminUser,
    useCreateAdminUser,
    useDeleteAdminUserRole,
    useUpdateAdminUser,
} from '@api/units';

import { useError, useSuccess } from '@context/modal';

import Form from '@controls/Form';
import Legend from '@controls/Legend';
import Mask from '@controls/Mask';
import Password from '@controls/Password';
import Switcher from '@controls/Switcher';

import CopyButton from '@components/CopyButton';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import Textarea from '@components/controls/Textarea';
import Input from '@components/controls/future/Input';
import SelectWithTags from '@components/controls/future/SelectWithTags';

import { CREATE_PARAM, DateFormaters, ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { cleanPhoneValue, formatDate, prepareTelValue } from '@scripts/helpers';
import { maskPhone } from '@scripts/mask';
import { regNameRu, regOneDigit, regOneLetter } from '@scripts/regex';

import CheckIcon from '@icons/small/check.svg';

import useUsersAccess from './useUsersAccess';

const SideBarItem = ({ name, children }: { name: string; children?: ReactNode }) => (
    <div css={{ ...typography('bodySm'), ':not(:last-of-type)': { marginBottom: scale(1) } }}>
        <span css={{ marginRight: scale(1, true) }}>{name}:</span>
        {children || '-'}
    </div>
);

const Controls = ({ setIsNeedRedirect }: { setIsNeedRedirect?: () => void }) => {
    const { dirty } = useFormikContext();
    const { push, pathname } = useRouter();
    const listLink = pathname.split(`[id]`)[0];

    return (
        <>
            <Button theme="secondary" onClick={() => push(listLink)}>
                Закрыть
            </Button>
            <Button theme="secondary" type="submit" disabled={!dirty}>
                Применить
            </Button>
            <Button Icon={CheckIcon} iconAfter type="submit" disabled={!dirty} onClick={setIsNeedRedirect}>
                Сохранить
            </Button>
        </>
    );
};

export default function RoleModelDetail() {
    const [isNeedRedirect, setIsNeedRedirect] = useState(false);
    const { query, push, pathname, replace } = useRouter();
    const listLink = pathname.split(`[id]`)[0];
    const id = Array.isArray(query.id) ? query.id[0] : query.id;
    const isCreatePage = id === CREATE_PARAM;

    const {
        canViewDetailPage,
        canEditUserWithoutActiveAndDelete,
        canEditUserActive,
        canEditPassword,
        canEditUserRoles,
    } = useUsersAccess();

    const {
        data: adminRoles,
        isLoading: isAdminRolesLoading,
        error: isAdminRolesError,
    } = useAdminRoles({
        filter: { active: true },
        pagination: { type: 'offset', limit: -1, offset: 0 },
    });

    useError(isAdminRolesError);

    const rolesOptions = useMemo(
        () => adminRoles?.data?.map(item => ({ value: item.id, key: item.title })) || [],
        [adminRoles?.data]
    );

    const {
        data: adminUserDetail,
        isLoading: isDetailLoading,
        error: isDetailError,
    } = useAdminUser(id, ['roles'], !!id && !isCreatePage);

    useError(isDetailError);

    const adminUserDetailData = useMemo(() => adminUserDetail?.data, [adminUserDetail]);

    const pageTitle = isCreatePage ? 'Новый пользователь' : `Пользователь: ${adminUserDetailData?.login}`;

    const {
        mutateAsync: onAddRole,
        isLoading: isRoleAddingLoading,
        error: addingRoleError,
        isSuccess: isAddingRolesSuccess,
    } = useAddAdminUserRole();
    useError(addingRoleError);

    const {
        mutateAsync: onDeleteRole,
        isLoading: isRoleDeleteLoading,
        error: deletingRoleError,
        isSuccess: isDeletingRoleSuccess,
    } = useDeleteAdminUserRole();
    useError(deletingRoleError);

    const {
        mutateAsync: createAdminUser,
        isLoading: isCreateLoading,
        error: isCreateError,
        isSuccess: isCreateSuccess,
    } = useCreateAdminUser();

    useError(isCreateError);
    // 1 уведомление об успешности создания для создания и добавления ролей
    useSuccess(isCreateSuccess || (isCreateSuccess && isAddingRolesSuccess) ? ModalMessages.SUCCESS_CREATE : '');

    const {
        mutateAsync: updateAdminUser,
        isLoading: isUpdateLoading,
        error: isUpdateError,
        isSuccess: isUpdateSuccess,
    } = useUpdateAdminUser();

    useError(isUpdateError);
    // 1 уведомление об успешности обновления и добавления/удаления ролей
    useSuccess(isUpdateSuccess && (isAddingRolesSuccess || isDeletingRoleSuccess) ? ModalMessages.SUCCESS_UPDATE : '');

    const formInitialValues: FormikValues = useMemo(
        () => ({
            active: adminUserDetailData?.active || false,
            cause_deactivation: '',
            email: adminUserDetailData?.email || '',
            first_name: adminUserDetailData?.first_name || '',
            last_name: adminUserDetailData?.last_name || '',
            login: adminUserDetailData?.login || '',
            middle_name: adminUserDetailData?.middle_name || '',
            password: '',
            phone: adminUserDetailData?.phone ? prepareTelValue(adminUserDetailData?.phone) : '',
            role_id: adminUserDetailData?.roles?.map(({ id: roleId }) => Number(roleId)) || [],
            // @TODO заглушка по причине несостыковок таймзон фронт/бек
            timezone: 'Europe/Moscow',
        }),
        [adminUserDetailData]
    );

    const formValidationSchema = useMemo(
        () => ({
            active: Yup.boolean(),

            cause_deactivation: formInitialValues?.active
                ? Yup.string().when('active', {
                      is: false,
                      then: Yup.string().required('Обязательное поле'),
                      otherwise: Yup.string(),
                  })
                : Yup.string(),
            email: Yup.string().email(ErrorMessages.EMAIL).required(ErrorMessages.REQUIRED),
            first_name: Yup.string()
                .matches(regNameRu, 'Используйте кириллицу для заполнения')
                .required(ErrorMessages.REQUIRED),
            last_name: Yup.string()
                .matches(regNameRu, 'Используйте кириллицу для заполнения')
                .required(ErrorMessages.REQUIRED),
            login: Yup.string().required(ErrorMessages.REQUIRED),
            middle_name: Yup.string()
                .matches(regNameRu, 'Используйте кириллицу для заполнения')
                .required(ErrorMessages.REQUIRED),
            password: Yup.string()
                .matches(regOneLetter, 'Пароль должен содержать хотя бы 1 латинскую букву')
                .matches(regOneDigit, 'Пароль должен содержать хотя бы 1 цифру')
                .min(8, 'Пароль должен быть не менее 8 символов'),
            ...(isCreatePage && {
                password: Yup.string()
                    .matches(regOneLetter, 'Пароль должен содержать хотя бы 1 латинскую букву')
                    .matches(regOneDigit, 'Пароль должен содержать хотя бы 1 цифру')
                    .min(8, 'Пароль должен быть не менее 8 символов')
                    .required('Было бы неплохо ввести пароль'),
            }),

            phone: Yup.string().required(ErrorMessages.REQUIRED),
            role_id: Yup.array().min(1, ErrorMessages.MIN_ITEMS(1)),
        }),
        [formInitialValues?.active, isCreatePage]
    );

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isDetailLoading ||
                isAdminRolesLoading ||
                isCreateLoading ||
                isUpdateLoading ||
                isRoleAddingLoading ||
                isRoleDeleteLoading
                // isDeleteLoading
            }
        >
            {canViewDetailPage ? (
                <Form
                    onSubmit={async (values: FormikValues, { resetForm }) => {
                        if (isCreatePage) {
                            const { data: createdAdminUserData } = await createAdminUser({
                                ...values,
                                phone: cleanPhoneValue(values?.phone),
                            });

                            await onAddRole({
                                id: Number(createdAdminUserData?.id),
                                roles: values?.role_id,
                                expires: null,
                            });

                            resetForm();

                            if (isNeedRedirect) push({ pathname: listLink });
                            else await replace(`${listLink}${createdAdminUserData?.id}`);
                        } else {
                            const changedValues: FormikValues = Object.keys(values).reduce((acc, cur) => {
                                if (values[cur] !== formInitialValues[cur]) return { ...acc, [cur]: values[cur] };
                                return acc;
                            }, {});

                            if (Object.keys(changedValues)?.length)
                                await updateAdminUser({
                                    id: Number(id),
                                    ...changedValues,
                                    ...(changedValues?.active === false && {
                                        cause_deactivation: changedValues?.cause_deactivation,
                                    }),
                                    ...(changedValues?.phone && {
                                        phone: cleanPhoneValue(changedValues?.phone),
                                    }),

                                    // инвалидировать только если нет ролей для изменения
                                    disableInvalidate: Boolean(changedValues?.role_id),
                                });

                            if (values?.role_id) {
                                const newRoles = values?.role_id?.filter(
                                    (roleItem: number) => !formInitialValues?.role_id?.includes(roleItem)
                                );

                                const deletedRoles = formInitialValues?.role_id?.filter(
                                    (roleItem: number) => !values?.role_id?.includes(roleItem)
                                );

                                if (newRoles?.length) {
                                    await onAddRole({
                                        id: Number(id),
                                        roles: newRoles,
                                        expires: null,
                                        // инвалидировать только если нет ролей на удаление
                                        disableInvalidate: Boolean(deletedRoles?.length),
                                    });
                                }

                                if (deletedRoles?.length) {
                                    await Promise.all(
                                        deletedRoles?.map((roleForDelete: number) =>
                                            onDeleteRole({
                                                id: Number(id),
                                                role_id: roleForDelete,
                                                // инвалидировать только в последнем запросе
                                                disableInvalidate: Boolean(
                                                    roleForDelete !== deletedRoles[deletedRoles?.length - 1]
                                                ),
                                            })
                                        )
                                    );
                                }
                            }

                            if (isNeedRedirect) {
                                resetForm();
                                push({ pathname: listLink });
                            }
                        }
                    }}
                    initialValues={formInitialValues}
                    enableReinitialize
                    css={{ height: '100%' }}
                    validationSchema={Yup.object().shape(formValidationSchema)}
                >
                    {({ values, error }: FormikValues) => (
                        <>
                            <PageLeaveGuard />

                            <PageTemplate
                                h1={pageTitle}
                                controls={<Controls setIsNeedRedirect={() => setIsNeedRedirect(true)} />}
                                backlink={{ text: 'Назад' }}
                                aside={
                                    <>
                                        <Form.FastField name="active" css={{ marginBottom: scale(3) }}>
                                            <Switcher disabled={!canEditUserActive}>
                                                {values?.active ? 'Активный' : 'Неактивный'}
                                            </Switcher>
                                        </Form.FastField>

                                        {!isCreatePage ? (
                                            <SideBarItem name="ID">
                                                {id ? <CopyButton>{String(id)}</CopyButton> : null}
                                            </SideBarItem>
                                        ) : null}

                                        <SideBarItem name="Изменено">
                                            {!isCreatePage && adminUserDetailData?.updated_at
                                                ? formatDate(
                                                      new Date(adminUserDetailData?.updated_at),
                                                      DateFormaters.DATE_AND_TIME
                                                  )
                                                : '-'}
                                        </SideBarItem>
                                        <SideBarItem name="Создано">
                                            {!isCreatePage && adminUserDetailData?.created_at
                                                ? formatDate(
                                                      new Date(adminUserDetailData?.created_at),
                                                      DateFormaters.DATE_AND_TIME
                                                  )
                                                : '-'}
                                        </SideBarItem>
                                    </>
                                }
                            >
                                <Layout
                                    cols={{
                                        xxxl: 2,
                                        xs: 1,
                                    }}
                                    css={{ marginBottom: scale(4) }}
                                >
                                    <Layout.Item  col={{ xxxl: 1, xxs: 2 }}>
                                        {JSON.stringify(error)}
                                        <Form.FastField
                                            name="last_name"
                                            label="Фамилия"
                                            disabled={!canEditUserWithoutActiveAndDelete}
                                        >
                                            <Input />
                                        </Form.FastField>
                                    </Layout.Item>

                                    <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                        <Form.FastField
                                            name="first_name"
                                            label="Имя"
                                            disabled={!canEditUserWithoutActiveAndDelete}
                                        >
                                            <Input />
                                        </Form.FastField>
                                    </Layout.Item>

                                    <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                        <Form.FastField
                                            name="middle_name"
                                            label="Отчество"
                                            disabled={!canEditUserWithoutActiveAndDelete}
                                        >
                                            <Input />
                                        </Form.FastField>
                                    </Layout.Item>

                                    <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                        <Form.FastField
                                            name="phone"
                                            label="Телефон"
                                            disabled={!canEditUserWithoutActiveAndDelete}
                                        >
                                            <Mask mask={maskPhone} />
                                        </Form.FastField>
                                    </Layout.Item>

                                    <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                        <Form.FastField
                                            name="login"
                                            label="Логин"
                                            autoComplete="off"
                                            disabled={!canEditUserWithoutActiveAndDelete}
                                        >
                                            <Input />
                                        </Form.FastField>
                                    </Layout.Item>

                                    <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                        <Form.FastField
                                            name="email"
                                            label="Email"
                                            autoComplete="off"
                                            disabled={!canEditUserWithoutActiveAndDelete}
                                        >
                                            <Input />
                                        </Form.FastField>
                                    </Layout.Item>

                                    <Layout.Item
                                        col={{
                                            xxxl: 2,
                                            xs: 1,
                                        }}
                                    >
                                        <hr />
                                    </Layout.Item>

                                    <Layout.Item
                                        col={{
                                            xxxl: 2,
                                            xs: 2,
                                        }}
                                    >
                                        <Form.Field name="password">
                                            <Legend
                                                hint="Пароль должен быть не менее 8 символов и содержать минимум 1 латинский символ"
                                                label="Пароль"
                                            />
                                            <Password autoComplete="off" disabled={!canEditPassword} />
                                        </Form.Field>
                                    </Layout.Item>

                                    <Layout.Item
                                        col={{
                                            xxxl: 2,
                                            xs: 2,
                                        }}
                                    >
                                        <Form.Field name="role_id">
                                            <SelectWithTags
                                                options={rolesOptions}
                                                label="Роли"
                                                disabled={!canEditUserRoles}
                                            />
                                        </Form.Field>
                                    </Layout.Item>

                                    {formInitialValues?.active && !values?.active ? (
                                        <Layout.Item
                                            col={{
                                                xxxl: 2,
                                                xs: 1,
                                            }}
                                        >
                                            <Form.Field
                                                name="cause_deactivation"
                                                label="Причина деактивации"
                                                disabled={!canEditUserActive}
                                            >
                                                <Textarea />
                                            </Form.Field>
                                        </Layout.Item>
                                    ) : null}
                                </Layout>
                            </PageTemplate>
                        </>
                    )}
                </Form>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра детальной страницы
                </h1>
            )}
        </PageWrapper>
    );
}
