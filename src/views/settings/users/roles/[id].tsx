import { FormikValues } from 'formik';
import { useRouter } from 'next/router';
import { ReactNode, useEffect, useMemo, useState } from 'react';
import * as Yup from 'yup';

import {
    useAdminRole,
    useCreateAdminRole,
    useDeleteAdminRole,
    useRolesRightAccess,
    useUpdateAdminRole,
} from '@api/units';

import { useError, useSuccess } from '@context/modal';

import Form from '@controls/Form';
import Switcher from '@controls/Switcher';

import CopyButton from '@components/CopyButton';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import { StatusSimplePopup } from '@components/StatusSimplePopup';
import Checkbox from '@components/controls/Checkbox';
import CheckboxGroup from '@components/controls/CheckboxGroup';
import Input from '@components/controls/future/Input';
import FormikSelect, { OptionShape } from '@components/controls/future/Select';

import { CREATE_PARAM, DateFormaters, ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { formatDate } from '@scripts/helpers';
import { onlyRussianLettersDigits } from '@scripts/regex';

import CheckIcon from '@icons/small/check.svg';

import useRolesAccess from './useRolesAccess';

const SideBarItem = ({ name, children }: { name: string; children?: ReactNode }) => (
    <div css={{ ...typography('bodySm'), ':not(:last-of-type)': { marginBottom: scale(1) } }}>
        <span css={{ marginRight: scale(1, true) }}>{name}:</span>
        {children || '-'}
    </div>
);

const Controls = ({
    isCreatePage,
    openDelete,
    setIsNeedRedirect,
    canEditDeleteRole,
    dirty,
}: {
    isCreatePage: boolean;
    openDelete?: () => void;
    setIsNeedRedirect?: () => void;
    canEditDeleteRole: boolean;
    dirty: boolean;
}) => {
    const { push, pathname } = useRouter();
    const listLink = pathname.split(`[id]`)[0];

    return (
        <>
            {!isCreatePage && canEditDeleteRole && (
                <Button theme="dangerous" onClick={openDelete}>
                    Удалить
                </Button>
            )}
            <Button theme="secondary" onClick={() => push(listLink)}>
                Закрыть
            </Button>
            <Button theme="secondary" type="submit" disabled={!dirty}>
                Применить
            </Button>
            <Button Icon={CheckIcon} iconAfter type="submit" disabled={!dirty} onClick={setIsNeedRedirect}>
                Сохранить
            </Button>
        </>
    );
};

const RoleModelDetail = () => {
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [isNeedRedirect, setIsNeedRedirect] = useState(false);
    const { query, push, pathname, replace } = useRouter();
    const listLink = pathname.split(`[id]`)[0];
    const id = Array.isArray(query.id) ? query.id[0] : query.id;
    const isCreatePage = id === CREATE_PARAM;

    const { canViewDetailPage, canEditRolesWithoutActiveAndDelete, canEditRoleActive, canEditDeleteRole } =
        useRolesAccess();

    const [activeGroups, setActiveGroups] = useState<OptionShape>({ value: '', key: '' });

    const {
        data: adminUserRoleDetail,
        isLoading: isDetailLoading,
        error: isDetailError,
    } = useAdminRole(id, !!id && !isCreatePage);

    useError(isDetailError);

    const adminUserRoleDetailData = useMemo(() => adminUserRoleDetail?.data, [adminUserRoleDetail]);

    const pageTitle = isCreatePage ? 'Новая роль' : `Роль: ${adminUserRoleDetailData?.title}`;

    const {
        mutateAsync: createAdminRole,
        isLoading: isCreateLoading,
        error: isCreateError,
        isSuccess: isCreateSuccess,
    } = useCreateAdminRole();

    useError(isCreateError);
    useSuccess(isCreateSuccess ? ModalMessages.SUCCESS_CREATE : '');

    const { data: rightAccessData, isLoading: isRightAccessLoading, error: isRightAccessError } = useRolesRightAccess();

    useError(isRightAccessError);

    const {
        mutateAsync: updateAdminRole,
        isLoading: isUpdateLoading,
        error: isUpdateError,
        isSuccess: isUpdateSuccess,
    } = useUpdateAdminRole(id);

    useError(isUpdateError);
    useSuccess(isUpdateSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const {
        mutateAsync: onAdminRoleDelete,
        isLoading: isDeleteLoading,
        error: isDeleteError,
        isSuccess: isDeleteSuccess,
    } = useDeleteAdminRole();

    useError(isDeleteError);
    useSuccess(isDeleteSuccess ? ModalMessages.SUCCESS_DELETE : '');

    const rightAccessSelectItems = useMemo(
        () =>
            rightAccessData?.data?.map(item => ({
                value: item?.section,
                key: item?.section,
            })) || [],
        [rightAccessData?.data]
    );

    useEffect(() => {
        setActiveGroups(rightAccessSelectItems[0]);
    }, [rightAccessSelectItems]);

    return (
        <PageWrapper
            title={canViewDetailPage ? pageTitle : ''}
            isLoading={isDetailLoading || isCreateLoading || isUpdateLoading || isDeleteLoading || isRightAccessLoading}
        >
            {canViewDetailPage ? (
                <Form
                    onSubmit={async (values: FormikValues, { resetForm }) => {
                        if (isCreatePage) {
                            const { data: createdAdminRoleData } = await createAdminRole({
                                title: values.title,
                                active: values.active,
                                rights_access: values.rights_access.map((el: string) => Number(el)),
                            });
                            resetForm();

                            if (isNeedRedirect) push({ pathname: listLink });
                            else await replace(`${listLink}${createdAdminRoleData?.id}`);
                        } else {
                            await updateAdminRole({
                                title: values.title,
                                active: values.active,
                                rights_access: values.rights_access.map((el: string) => Number(el)),
                            });
                            resetForm();

                            if (isNeedRedirect) push({ pathname: listLink });
                        }
                    }}
                    initialValues={{
                        title: adminUserRoleDetailData?.title || '',
                        active: adminUserRoleDetailData?.active || false,
                        groups: rightAccessData?.data[0].section || '',
                        rights_access: adminUserRoleDetailData?.rights_access.map(el => String(el)) || [],
                    }}
                    enableReinitialize
                    css={{ height: '100%' }}
                    validationSchema={Yup.object().shape({
                        title: Yup.string()
                            .matches(onlyRussianLettersDigits, 'Используйте кириллицу и цифры для заполнения')
                            .required(ErrorMessages.REQUIRED),
                    })}
                >
                    {({ values, resetForm, dirty }: FormikValues) => (
                        <>
                            <PageLeaveGuard />

                            <PageTemplate
                                h1={pageTitle}
                                controls={
                                    <Controls
                                        isCreatePage={isCreatePage}
                                        openDelete={() => setIsDeleteOpen(true)}
                                        setIsNeedRedirect={() => setIsNeedRedirect(true)}
                                        canEditDeleteRole={canEditDeleteRole}
                                        dirty={dirty}
                                    />
                                }
                                backlink={{ text: 'Назад' }}
                                aside={
                                    <>
                                        <Form.FastField name="active" css={{ marginBottom: scale(3) }}>
                                            <Switcher disabled={!canEditRoleActive}>
                                                {values?.active ? 'Активный' : 'Неактивный'}
                                            </Switcher>
                                        </Form.FastField>
                                        {!isCreatePage ? (
                                            <SideBarItem name="ID">
                                                {id ? <CopyButton>{String(id)}</CopyButton> : null}
                                            </SideBarItem>
                                        ) : null}

                                        <SideBarItem name="Изменено">
                                            {!isCreatePage && adminUserRoleDetailData?.updated_at
                                                ? formatDate(
                                                      new Date(adminUserRoleDetailData?.updated_at),
                                                      DateFormaters.DATE_AND_TIME
                                                  )
                                                : '-'}
                                        </SideBarItem>
                                        <SideBarItem name="Создано">
                                            {!isCreatePage && adminUserRoleDetailData?.created_at
                                                ? formatDate(
                                                      new Date(adminUserRoleDetailData?.created_at),
                                                      DateFormaters.DATE_AND_TIME
                                                  )
                                                : '-'}
                                        </SideBarItem>
                                    </>
                                }
                            >
                                <Layout cols={3} css={{ marginBottom: scale(4) }}>
                                    <Layout.Item col={3}>
                                        <Form.FastField
                                            name="title"
                                            label="Название"
                                            disabled={!canEditRolesWithoutActiveAndDelete}
                                        >
                                            <Input />
                                        </Form.FastField>
                                    </Layout.Item>

                                    <Layout.Item col={3}>
                                        <hr />
                                    </Layout.Item>

                                    <Layout.Item>
                                        {rightAccessSelectItems && (
                                            <FormikSelect
                                                options={rightAccessSelectItems}
                                                selected={activeGroups}
                                                hideClearButton
                                                onChange={value =>
                                                    setActiveGroups(value?.selected || { value: '', key: '' })
                                                }
                                            />
                                        )}
                                    </Layout.Item>

                                    <Layout.Item col={2}>
                                        {rightAccessData?.data?.map(item => (
                                            <div
                                                key={item?.section}
                                                css={{
                                                    ...(activeGroups?.value !== item?.section && { display: 'none' }),
                                                }}
                                            >
                                                <Form.Field name="rights_access" label="Функции/вкладки">
                                                    <CheckboxGroup>
                                                        {item?.items?.map(subItem => (
                                                            <Checkbox
                                                                key={`right_access_check_${subItem?.id}`}
                                                                value={String(subItem?.id)}
                                                                disabled={!canEditRolesWithoutActiveAndDelete}
                                                            >
                                                                {subItem?.title}
                                                            </Checkbox>
                                                        ))}
                                                    </CheckboxGroup>
                                                </Form.Field>
                                            </div>
                                        ))}
                                    </Layout.Item>
                                </Layout>
                            </PageTemplate>

                            {canEditDeleteRole && (
                                <StatusSimplePopup
                                    isOpen={isDeleteOpen}
                                    close={() => setIsDeleteOpen(false)}
                                    title="Удалить роль?"
                                    text="Роль будет удалена из системы безвозвратно."
                                    theme="delete"
                                    footer={
                                        <>
                                            <Button onClick={() => setIsDeleteOpen(false)} theme="secondary">
                                                Не удалять
                                            </Button>
                                            <Button
                                                theme="dangerous"
                                                onClick={async () => {
                                                    setIsDeleteOpen(false);
                                                    await onAdminRoleDelete(id);
                                                    resetForm();
                                                    replace({ pathname: listLink });
                                                }}
                                            >
                                                Удалить
                                            </Button>
                                        </>
                                    }
                                />
                            )}
                        </>
                    )}
                </Form>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра детальной страницы
                </h1>
            )}
        </PageWrapper>
    );
};

export default RoleModelDetail;
