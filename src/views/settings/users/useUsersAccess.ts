import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

const useUsersAccess = () => {
    const { data: userData } = useCurrentUser();

    const viewListingAndFilters = 701;
    const viewDetailPage = 702;
    const editUserWithoutActiveAndDelete = 703;
    const editUserActive = 704;
    const editPassword = 705;
    const editUserRoles = 706;
    const createNewUser = 707;

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canViewDetailPage = useMemo(
        () => !!userData?.data.rights_access.includes(viewDetailPage),
        [userData?.data.rights_access]
    );
    const canEditUserWithoutActiveAndDelete = useMemo(
        () => !!userData?.data.rights_access.includes(editUserWithoutActiveAndDelete),
        [userData?.data.rights_access]
    );
    const canEditUserActive = useMemo(
        () => !!userData?.data.rights_access.includes(editUserActive),
        [userData?.data.rights_access]
    );
    const canEditPassword = useMemo(
        () => !!userData?.data.rights_access.includes(editPassword),
        [userData?.data.rights_access]
    );
    const canEditUserRoles = useMemo(
        () => !!userData?.data.rights_access.includes(editUserRoles),
        [userData?.data.rights_access]
    );
    const canCreateNewUser = useMemo(
        () => !!userData?.data.rights_access.includes(createNewUser),
        [userData?.data.rights_access]
    );

    return {
        canViewListingAndFilters,
        canViewDetailPage,
        canEditUserWithoutActiveAndDelete,
        canEditUserActive,
        canEditPassword,
        canEditUserRoles,
        canCreateNewUser,
    };
};

export default useUsersAccess;
