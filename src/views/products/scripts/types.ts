export enum ATTR_TYPES {
    STRING = 'string',
    TEXT = 'text',
    INTEGER = 'integer',
    DOUBLE = 'double',
    BOOLEAN = 'boolean',
    DATETIME = 'datetime',
    IMAGE = 'image',
    FILE = 'file',
    COLOR = 'color',
    DIRECTORY = 'directory',
}

export enum ATTR_PROPS {
    COLOR = 'color',
    FILTER = 'filter',
    FEW_VALUES = 'fewValues',
    ACTIVITY = 'activity',
    PUBLIC = 'public',
    REQUIRED = 'required',
    DIRECTORY = 'directory',
    COMMON = 'common',
    SYSTEM = 'system',
}

export interface DirectoryValueItem {
    value?: string;
    name: string;
    code?: string;
    image?: File[] | string | undefined;
    file?: File[] | string | undefined;
    attrId?: number;
    date?: string | number;
    time?: string;
}
