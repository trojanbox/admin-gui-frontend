import * as Yup from 'yup';

import { CategoryTreeItem, Property } from '@api/catalog';

import { ErrorMessages } from '@scripts/constants';

import { ATTR_PROPS, ATTR_TYPES } from './types';

export const getEmptyValue = () => ({ value: '', code: '', name: '', image: '', file: '', date: '', time: '' });
export const generateEmptyValueForAttribute = () => Array.from({ length: 2 }, () => getEmptyValue());

export const dataToAttrProps = (data: Property) => {
    const attrsList: ATTR_PROPS[] = [];

    const {
        is_multiple: isMultiple,
        is_color: isColor,
        is_filterable: isFilterable,
        is_active: isActive,
        is_common: isCommon,
        is_public: isPublic,
        is_system: isSystem,
        has_directory: hasDirectory,
    } = data;

    const propsToAttrsMap = new Map([
        [ATTR_PROPS.FEW_VALUES, isMultiple],
        [ATTR_PROPS.COLOR, isColor],
        [ATTR_PROPS.FILTER, isFilterable],
        [ATTR_PROPS.ACTIVITY, isActive],
        [ATTR_PROPS.COMMON, isCommon],
        [ATTR_PROPS.PUBLIC, isPublic],
        [ATTR_PROPS.SYSTEM, isSystem],
        [ATTR_PROPS.DIRECTORY, hasDirectory],
    ]);

    propsToAttrsMap.forEach((attrActive: boolean | undefined, attr: ATTR_PROPS) => {
        if (attrActive) {
            attrsList.push(attr);
        }
    });

    return attrsList;
};

export const getValidationProperties = (type?: string) =>
    Yup.object().shape({
        productNameForPublic: Yup.string().required(ErrorMessages.REQUIRED),
        attrType: Yup.string().required(ErrorMessages.REQUIRED),
        additionalAttributes: Yup.array().when('attrProps', {
            is: (attrProps: string[]) => attrProps.includes(ATTR_PROPS.DIRECTORY),
            then: Yup.array()
                .of(
                    Yup.object().shape({
                        ...(type !== ATTR_TYPES.IMAGE &&
                            type !== ATTR_TYPES.FILE &&
                            type !== ATTR_TYPES.DATETIME && {
                                name: Yup.string().nullable(),
                                value: Yup.string().required(ErrorMessages.REQUIRED).nullable(),
                            }),
                        ...(type === ATTR_TYPES.IMAGE && {
                            image: Yup.array().min(1, ErrorMessages.REQUIRED).required(ErrorMessages.REQUIRED),
                        }),
                        ...(type === ATTR_TYPES.FILE && {
                            file: Yup.array().min(1, ErrorMessages.REQUIRED).required(ErrorMessages.REQUIRED),
                        }),
                        ...(type === ATTR_TYPES.DATETIME && {
                            date: Yup.string().required(ErrorMessages.REQUIRED).nullable(),
                            time: Yup.string().required(ErrorMessages.REQUIRED).nullable(),
                        }),
                        ...(type === ATTR_TYPES.COLOR && {
                            value: Yup.string().required(ErrorMessages.REQUIRED).nullable(),
                            code: Yup.string().required(ErrorMessages.REQUIRED).nullable(),
                        }),
                    })
                )
                .min(2, 'Введите минимум 2 атрибута')
                .required(ErrorMessages.REQUIRED),
            otherwise: Yup.array(),
        }),
    });

export const customFlat = (category: any, nesting?: string) => {
    if (category)
        return category.reduce((acc: CategoryTreeItem[], cur: CategoryTreeItem) => {
            const curNesting = nesting || '';
            if (cur.children.length > 0)
                return [
                    ...acc,
                    { value: cur.id, label: `${curNesting} ${cur.name}`, code: cur.code },
                    ...customFlat(cur.children, `${curNesting} -`),
                ];

            return [...acc, { value: cur.id, label: `${curNesting} ${cur.name}`, code: cur.code }];
        }, []);
    return [];
};

export const customFlatWithParents = (category: any, name = '', selfId?: number): { value: any; label: string }[] => {
    if (category)
        return category.reduce((acc: any[], cur: any) => {
            if (selfId === Number(cur?.id)) return acc;

            if (cur.children.length > 0)
                return [
                    ...acc,
                    { value: cur?.id, label: `${name?.length ? `${name} \u2192 ` : ''}${cur.name}` },
                    ...customFlatWithParents(
                        cur.children,
                        `${name?.length ? `${name} \u2192 ` : ''}${cur.name}`,
                        selfId
                    ),
                ];

            if (cur.children.length === 0)
                return [...acc, { value: cur.id, label: `${name?.length ? `${name} \u2192 ` : ''}${cur.name}` }];

            return acc;
        }, []);
    return [];
};
