import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import {
    useBrand,
    useBrandPreloadImage,
    useCreateBrand,
    useDeleteBrand,
    useEditBrand,
    useMutateBrandImage,
} from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import Dropzone from '@controls/Dropzone';
import { downloadFile } from '@controls/Dropzone/utils';
import Form from '@controls/Form';
import Switcher from '@controls/Switcher';

import CopyButton from '@components/CopyButton';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import { StatusSimplePopup } from '@components/StatusSimplePopup';
import Textarea from '@components/controls/Textarea';

import { CREATE_PARAM, ErrorMessages, FileSizes, ModalMessages } from '@scripts/constants';
import { ButtonNameEnum } from '@scripts/enums';
import { Button, Layout, colors, scale } from '@scripts/gds';

import CheckIcon from '@icons/small/check.svg';

const BrandDetail = () => {
    const { push, replace, pathname, query } = useRouter();
    const id = Array.isArray(query?.id) ? query.id[0] : query.id;
    const isCreationPage = useMemo(() => id === CREATE_PARAM, [id]);
    const listLink = pathname.split(`[id]`)[0];

    const buttonNameRef = useRef<ButtonNameEnum | null>(null);

    const [isRemoveOpen, setIsRemoveOpen] = useState(false);

    const {
        mutateAsync: onDeleteBrand,
        isLoading: isDeleteLoading,
        error: isDeleteError,
        isSuccess: isDeleteSuccess,
    } = useDeleteBrand();

    useError(isDeleteError);
    useSuccess(isDeleteSuccess ? ModalMessages.SUCCESS_DELETE : '');

    const {
        data: brand,
        isLoading: isDetailLoading,
        error: isDetailError,
    } = useBrand(Number(id), Boolean(id) && !isCreationPage && !isDeleteSuccess && !isDeleteLoading);

    useError(isDetailError);

    const brandData = useMemo(() => brand?.data, [brand]);

    const pageTitle = useMemo(
        () => (isCreationPage ? 'Новый бренд' : `Бренд: ${brandData?.name}`),
        [brandData?.name, isCreationPage]
    );

    const [imagesState, setImagesState] = useState<File[]>([]);

    const initialLogoName = useMemo(() => brandData?.image_url?.split('/')?.slice(-1)[0] || '', [brandData?.image_url]);

    const getLogo = useCallback(async () => {
        if (brandData?.image_url)
            downloadFile(brandData?.image_url, initialLogoName).then(res => {
                if (res) setImagesState([res]);
            });
    }, [brandData, initialLogoName]);

    useEffect(() => {
        getLogo();
    }, [getLogo]);

    const formInitialValues = useMemo(
        () => ({
            name: brandData?.name || '',
            code: brandData?.code || '',
            is_active: brandData ? brandData?.is_active : true,
            image_url: imagesState,
            description: brandData?.description || '',
        }),
        [brandData, imagesState]
    );

    const {
        mutateAsync: onCreateBrand,
        isLoading: isCreateLoading,
        error: isCreateError,
        isSuccess: isCreateSuccess,
    } = useCreateBrand();

    useError(isCreateError);
    useSuccess(isCreateSuccess ? ModalMessages.SUCCESS_SAVE : '');

    const {
        mutateAsync: onEditBrand,
        isLoading: isEditLoading,
        error: isEditError,
        isSuccess: isEditSuccess,
    } = useEditBrand();

    useError(isEditError);
    useSuccess(isEditSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const {
        mutateAsync: onBrandPreloadImage,
        isLoading: isPreloadImageLoading,
        error: isPreloadImageError,
    } = useBrandPreloadImage();

    useError(isPreloadImageError);

    const {
        mutateAsync: onMutateBrandImage,
        isLoading: isMutateImageLoading,
        error: isMutateImageError,
    } = useMutateBrandImage();

    useError(isMutateImageError);

    return (
        <>
            <PageWrapper
                title={pageTitle}
                isLoading={
                    isDetailLoading ||
                    isDeleteLoading ||
                    isCreateLoading ||
                    isPreloadImageLoading ||
                    isEditLoading ||
                    isMutateImageLoading
                }
            >
                <Form
                    initialValues={formInitialValues}
                    onSubmit={async (values, { resetForm }) => {
                        const { name, is_active, code, description, image_url } = values;
                        const formData = new FormData();
                        formData.append('file', image_url[0]);

                        if (isCreationPage) {
                            const imageData = await onBrandPreloadImage({ formData });

                            const { data: createdBrandData } = await onCreateBrand({
                                name,
                                is_active,
                                code,
                                description,
                                preload_file_id: imageData?.data?.preload_file_id,
                            });

                            if (buttonNameRef.current === ButtonNameEnum.APPLY) {
                                push(`${listLink}${createdBrandData.id}`);
                            }
                        } else {
                            if (image_url[0]?.name !== initialLogoName) {
                                await onMutateBrandImage({ id: Number(id), file: formData });
                            }

                            await onEditBrand({
                                id: Number(id),
                                ...(formInitialValues.name !== name && { name }),
                                ...(formInitialValues.is_active !== is_active && { is_active }),
                                ...(formInitialValues.code !== code && { code }),
                                ...(formInitialValues.description !== description && { description }),
                            });
                        }

                        resetForm({ values });

                        if (buttonNameRef.current === ButtonNameEnum.SAVE) {
                            push({ pathname: listLink });
                        }
                    }}
                    validationSchema={Yup.object().shape({
                        name: Yup.string().required(ErrorMessages.REQUIRED),
                        code: Yup.string().required(ErrorMessages.REQUIRED),
                        is_active: Yup.boolean().required(ErrorMessages.REQUIRED),
                        image_url: Yup.array().min(1, ErrorMessages.MIN_FILES(1)).max(1, ErrorMessages.MAX_FILES(1)),
                    })}
                    enableReinitialize
                >
                    {({ dirty }) => (
                        <>
                            <PageLeaveGuard />
                            <PageTemplate
                                h1={pageTitle}
                                backlink={{ text: 'Назад' }}
                                controls={
                                    <>
                                        {!isCreationPage && (
                                            <Button theme="dangerous" onClick={() => setIsRemoveOpen(true)}>
                                                Удалить
                                            </Button>
                                        )}
                                        <Button
                                            theme="secondary"
                                            onClick={() =>
                                                push({
                                                    pathname: listLink,
                                                })
                                            }
                                        >
                                            Закрыть
                                        </Button>
                                        <Button
                                            theme="secondary"
                                            type="submit"
                                            disabled={!dirty}
                                            onClick={() => {
                                                buttonNameRef.current = ButtonNameEnum.APPLY;
                                            }}
                                        >
                                            Применить
                                        </Button>
                                        <Button
                                            type="submit"
                                            Icon={CheckIcon}
                                            iconAfter
                                            disabled={!dirty}
                                            onClick={() => {
                                                buttonNameRef.current = ButtonNameEnum.SAVE;
                                            }}
                                        >
                                            Сохранить
                                        </Button>
                                    </>
                                }
                                aside={
                                    !isCreationPage && (
                                        <>
                                            <Form.FastField name="is_active" css={{ marginBottom: scale(3) }}>
                                                <Switcher>Активность</Switcher>
                                            </Form.FastField>
                                            <div css={{ marginBottom: scale(1) }}>
                                                <span css={{ marginRight: scale(1, true), color: colors?.grey800 }}>
                                                    ID:
                                                </span>
                                                <CopyButton>{`${id}`}</CopyButton>
                                            </div>
                                            <InfoList css={{ marginBottom: scale(5) }}>
                                                {getDefaultDates({ ...brandData }).map(item => (
                                                    <InfoList.Item {...item} key={item.name} />
                                                ))}
                                            </InfoList>
                                        </>
                                    )
                                }
                            >
                                <Layout cols={2} gap={scale(3)} align="end">
                                    <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                        <Form.FastField name="name" label="Наименование бренда*" />
                                    </Layout.Item>
                                    <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                        <Form.FastField name="code" label="Код бренда*" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.FastField name="description" label="Описание">
                                            <Textarea />
                                        </Form.FastField>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="image_url" label="Логотип*">
                                            <Dropzone
                                                accept={['image/jpeg', 'image/jpg', 'image/png']}
                                                maxFiles={1}
                                                maxSize={FileSizes.MB1}
                                                multiple={false}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </PageTemplate>
                        </>
                    )}
                </Form>
                <StatusSimplePopup
                    close={() => setIsRemoveOpen(false)}
                    isOpen={isRemoveOpen}
                    title={`Вы уверены, что хотите удалить бренд ${brandData?.name}?`}
                    text=""
                    theme="delete"
                    footer={
                        <>
                            <Button
                                theme="secondary"
                                onClick={() => {
                                    setIsRemoveOpen(false);
                                }}
                            >
                                Отмена
                            </Button>
                            <Button
                                theme="dangerous"
                                onClick={async () => {
                                    setIsRemoveOpen(false);
                                    if (!id) return;

                                    await onDeleteBrand(Number(id));

                                    replace({ pathname: listLink });
                                }}
                            >
                                Да
                            </Button>
                        </>
                    }
                />
            </PageWrapper>
        </>
    );
};

export default BrandDetail;
