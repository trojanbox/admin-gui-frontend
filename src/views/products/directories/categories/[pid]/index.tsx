import { FormikValues } from 'formik';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useState } from 'react';
import * as Yup from 'yup';

import {
    useCategoriesTree,
    useCategoryCreate,
    useCategoryDelete,
    useCategoryDetail,
    useCategoryUpdate,
} from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import { customFlatWithParents } from '@views/products/scripts';

import Checkbox from '@controls/Checkbox';
import Form from '@controls/Form';
import Select from '@controls/Select';
import Tabs from '@controls/Tabs';

import Block from '@components/Block';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import { InfoListItemCommonType } from '@components/InfoList/types';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import { StatusSimplePopup } from '@components/StatusSimplePopup';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { useMedia } from '@scripts/hooks';
import { onlyEnglishLettersDigits } from '@scripts/regex';

import CheckIcon from '@icons/small/check.svg';
import PlusIcon from '@icons/small/plus.svg';

import AddAttributePopup from '../components/AddAttributePopup';
import InheritedPropertiesTable from '../components/InheritedPropertiesTable';
import SelfPropertiesTable from '../components/SelfPropertiesTable';
import useCategoriesAccess from '../useCategoriesAccess';

const ProductCategory = () => {
    const { query, push, pathname, replace } = useRouter();
    const { sm, xs } = useMedia();
    const id = Array.isArray(query.pid) ? query.pid[0] : query.pid;
    const isCreatePage = id === CREATE_PARAM;
    const listLink = pathname.split(`[pid]`)[0];

    const [isNeedRedirect, setIsNeedRedirect] = useState(false);
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [isAddAttributePopupOpen, setAddAttributePopupOpen] = useState(false);

    const {
        canViewDetailPage,
        canEditAllCategoryData,
        canAddAndEditingProperties,
        canEditCategoryDataWithoutActive,
        canDeleteCategory,
        canEditCategoryActive,
    } = useCategoriesAccess();

    useEffect(() => {
        if (!isAddAttributePopupOpen && Object.keys(query)?.filter(i => i !== 'pid')?.length)
            push({
                pathname,
                query: { pid: id },
            });
    }, [id, isAddAttributePopupOpen, pathname, push, query]);

    const {
        data: category,
        isLoading: isDetailLoading,
        error: isDetailError,
    } = useCategoryDetail(Number(id), !!id && !isCreatePage && canViewDetailPage);
    useError(isDetailError);

    const categoryData = useMemo(() => category?.data, [category]);

    const pageTitle = useMemo(
        () => (isCreatePage ? 'Создать новую категорию' : `Категория: ${categoryData?.name}`),
        [categoryData?.name, isCreatePage]
    );

    const { data: categoriesTree, isLoading: categoriesTreeLoading, error: errorCategoriesTree } = useCategoriesTree();
    useError(errorCategoriesTree);

    const flattedCategoriesTree = customFlatWithParents(categoriesTree?.data, '', Number(id));

    const parentCategories = useMemo(
        () => [{ value: null, label: 'Нет' }, ...flattedCategoriesTree],
        [flattedCategoriesTree]
    );

    const createCategory = useCategoryCreate();
    useError(createCategory.error);
    useSuccess(createCategory.isSuccess ? ModalMessages.SUCCESS_SAVE : '');

    const updateCategory = useCategoryUpdate();
    useError(updateCategory.error);
    useSuccess(updateCategory.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const deleteCategory = useCategoryDelete();
    useError(deleteCategory.error);
    useSuccess(deleteCategory.isSuccess ? ModalMessages.SUCCESS_DELETE : '');

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isDetailLoading ||
                categoriesTreeLoading ||
                createCategory?.isLoading ||
                updateCategory?.isLoading ||
                deleteCategory?.isLoading
            }
        >
            {canViewDetailPage ? (
                <Form
                    onSubmit={async (values, { resetForm }) => {
                        if (isCreatePage) {
                            const {
                                data: { id: createdId },
                            } = await createCategory?.mutateAsync(values);

                            resetForm();

                            if (isNeedRedirect) push({ pathname: listLink });
                            else replace(`${listLink}${createdId}`);
                        } else {
                            await updateCategory?.mutateAsync({ id: Number(id), ...values });

                            if (isNeedRedirect) {
                                resetForm();
                                push({ pathname: listLink });
                            }
                        }
                    }}
                    initialValues={{
                        name: categoryData ? categoryData?.name : '',
                        code: categoryData ? categoryData?.code : '',
                        parent_id: categoryData ? categoryData?.parent_id : null,
                        is_inherits_properties: categoryData ? categoryData?.is_inherits_properties : false,
                        is_self_code: false,
                        is_active: categoryData ? categoryData?.is_active : false,
                    }}
                    validationSchema={Yup.object().shape({
                        name: Yup.string().required(ErrorMessages.REQUIRED).nullable(),
                        code: Yup.string().matches(onlyEnglishLettersDigits, ErrorMessages.WRONG_FORMAT),
                    })}
                    enableReinitialize
                >
                    {({ values: { is_self_code }, dirty, resetForm }): FormikValues => (
                        <>
                            <PageLeaveGuard />
                            <PageTemplate
                                h1={pageTitle}
                                backlink={{ text: 'Назад' }}
                                controls={
                                    <>
                                        {!isCreatePage && canDeleteCategory && (
                                            <Button theme="dangerous" onClick={() => setIsDeleteOpen(true)}>
                                                Удалить
                                            </Button>
                                        )}
                                        <Button theme="secondary" onClick={() => push(listLink)}>
                                            Закрыть
                                        </Button>
                                        <Button theme="secondary" type="submit" disabled={!dirty}>
                                            Применить
                                        </Button>
                                        <Button
                                            type="submit"
                                            Icon={CheckIcon}
                                            iconAfter
                                            disabled={!dirty}
                                            onClick={() => setIsNeedRedirect(true)}
                                        >
                                            Сохранить
                                        </Button>
                                    </>
                                }
                                aside={
                                    <>
                                        <InfoList css={{ marginBottom: scale(5) }}>
                                            {(
                                                [
                                                    { name: 'ID', value: categoryData?.id },
                                                    {
                                                        name: 'Код',
                                                        value: categoryData?.code,
                                                    },
                                                ] as InfoListItemCommonType[]
                                            ).map(item => (
                                                <InfoList.Item {...item} key={item.name} />
                                            ))}
                                        </InfoList>

                                        <InfoList css={{ marginBottom: scale(5) }}>
                                            {getDefaultDates({ ...categoryData }).map(item => (
                                                <InfoList.Item {...item} key={item.name} />
                                            ))}
                                        </InfoList>
                                    </>
                                }
                                customChildren
                            >
                                <Tabs css={{ flexGrow: 1 }}>
                                    <Tabs.List>
                                        <Tabs.Tab>Характеристики</Tabs.Tab>
                                        <Tabs.Tab disabled={isCreatePage || !categoryData}>Атрибуты</Tabs.Tab>
                                    </Tabs.List>
                                    <Tabs.Panel>
                                        <Block css={{ height: '100%', borderTopLeftRadius: 0 }}>
                                            <Block.Body>
                                                <Layout cols={3}>
                                                    <Layout.Item col={{ xxxl: 1, xs: 3 }}>
                                                        <Form.FastField
                                                            name="name"
                                                            label="Название категории"
                                                            disabled={
                                                                !(
                                                                    canEditAllCategoryData ||
                                                                    canEditCategoryDataWithoutActive
                                                                )
                                                            }
                                                        />
                                                    </Layout.Item>
                                                    <Layout.Item col={{ xxxl: 1, xs: 3 }}>
                                                        <Form.Field
                                                            name="code"
                                                            label="Символьный код"
                                                            disabled={!is_self_code}
                                                        />
                                                    </Layout.Item>
                                                    <Layout.Item
                                                        col={{ xxxl: 1, xs: 3 }}
                                                        align="end"
                                                        css={{ paddingBottom: scale(1), [sm]: { paddingBottom: 0 } }}
                                                    >
                                                        <Form.FastField
                                                            name="is_self_code"
                                                            disabled={
                                                                !(
                                                                    canEditAllCategoryData ||
                                                                    canEditCategoryDataWithoutActive
                                                                )
                                                            }
                                                        >
                                                            <Checkbox>Задать символьный код вручную</Checkbox>
                                                        </Form.FastField>
                                                    </Layout.Item>
                                                    <Layout.Item col={3}>
                                                        <Form.Field
                                                            name="parent_id"
                                                            label="Родительская категория"
                                                            disabled={
                                                                !(
                                                                    canEditAllCategoryData ||
                                                                    canEditCategoryDataWithoutActive
                                                                )
                                                            }
                                                        >
                                                            <Select items={parentCategories} />
                                                        </Form.Field>
                                                    </Layout.Item>

                                                    <Layout.Item col={3}>
                                                        <Form.FastField name="is_active">
                                                            <Checkbox
                                                                disabled={
                                                                    !(canEditCategoryActive || canEditAllCategoryData)
                                                                }
                                                            >
                                                                Активность категории
                                                            </Checkbox>
                                                        </Form.FastField>
                                                    </Layout.Item>
                                                    <Layout.Item col={3}>
                                                        <Form.FastField
                                                            name="is_inherits_properties"
                                                            disabled={
                                                                !(
                                                                    canEditAllCategoryData ||
                                                                    canEditCategoryDataWithoutActive
                                                                )
                                                            }
                                                        >
                                                            <Checkbox>
                                                                Категория наследует атрибуты родительской
                                                            </Checkbox>
                                                        </Form.FastField>
                                                    </Layout.Item>
                                                </Layout>
                                            </Block.Body>
                                        </Block>
                                    </Tabs.Panel>
                                    <Tabs.Panel>
                                        <Block css={{ height: '100%' }}>
                                            <Block.Header
                                                css={{
                                                    borderBottom: 'none',
                                                    [xs]: { flexDirection: 'column', alignItems: 'flex-start' },
                                                }}
                                            >
                                                <h2>Атрибуты категории</h2>
                                                <div css={{ button: { marginLeft: scale(2) } }}>
                                                    <Button
                                                        Icon={PlusIcon}
                                                        onClick={() => setAddAttributePopupOpen(true)}
                                                        disabled={
                                                            !(canAddAndEditingProperties || canEditAllCategoryData)
                                                        }
                                                        css={{ [xs]: { marginTop: scale(1) } }}
                                                    >
                                                        Добавить атрибут
                                                    </Button>
                                                </div>
                                            </Block.Header>
                                            <Block.Body css={{ padding: 0 }}>
                                                <SelfPropertiesTable
                                                    canEditCategoryData={
                                                        canAddAndEditingProperties || canEditAllCategoryData
                                                    }
                                                />
                                            </Block.Body>

                                            <Block.Header css={{ borderBottom: 'none', marginTop: scale(2) }}>
                                                <h2>Наследуемые атрибуты категории</h2>
                                            </Block.Header>
                                            <Block.Body css={{ padding: 0 }}>
                                                <InheritedPropertiesTable />
                                            </Block.Body>
                                        </Block>
                                    </Tabs.Panel>
                                </Tabs>
                            </PageTemplate>

                            <StatusSimplePopup
                                isOpen={isDeleteOpen}
                                close={() => setIsDeleteOpen(false)}
                                title="Вы уверены, что хотите удалить категорию?"
                                text=""
                                theme="delete"
                                footer={
                                    <>
                                        <Button onClick={() => setIsDeleteOpen(false)} theme="secondary">
                                            Не удалять
                                        </Button>
                                        <Button
                                            theme="dangerous"
                                            onClick={async () => {
                                                setIsDeleteOpen(false);
                                                await deleteCategory?.mutateAsync(Number(id));
                                                resetForm();
                                                push(listLink);
                                            }}
                                        >
                                            Удалить
                                        </Button>
                                    </>
                                }
                            />

                            <AddAttributePopup
                                isOpen={isAddAttributePopupOpen}
                                onRequestClose={() => setAddAttributePopupOpen(false)}
                            />
                        </>
                    )}
                </Form>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра категории
                </h1>
            )}
        </PageWrapper>
    );
};

export default ProductCategory;
