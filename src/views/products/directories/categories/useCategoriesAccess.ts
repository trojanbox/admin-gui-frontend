import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

const useCategoriesAccess = () => {
    const { data: userData } = useCurrentUser();

    const viewListingAndFilters = 501;
    const viewDetailPage = 502;
    const editAllCategoryData = 503;
    const createCategory = 504;
    const addAndEditingProperties = 505;
    const editCategoryDataWithoutActive = 506;
    const deleteCategory = 507;
    const editCategoryActive = 508;

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canViewDetailPage = useMemo(
        () => !!userData?.data.rights_access.includes(viewDetailPage),
        [userData?.data.rights_access]
    );
    const canEditAllCategoryData = useMemo(
        () => !!userData?.data.rights_access.includes(editAllCategoryData),
        [userData?.data.rights_access]
    );
    const canCreateCategory = useMemo(
        () => !!userData?.data.rights_access.includes(createCategory),
        [userData?.data.rights_access]
    );
    const canAddAndEditingProperties = useMemo(
        () => !!userData?.data.rights_access.includes(addAndEditingProperties),
        [userData?.data.rights_access]
    );
    const canEditCategoryDataWithoutActive = useMemo(
        () => !!userData?.data.rights_access.includes(editCategoryDataWithoutActive),
        [userData?.data.rights_access]
    );
    const canDeleteCategory = useMemo(
        () => !!userData?.data.rights_access.includes(deleteCategory),
        [userData?.data.rights_access]
    );
    const canEditCategoryActive = useMemo(
        () => !!userData?.data.rights_access.includes(editCategoryActive),
        [userData?.data.rights_access]
    );

    return {
        canViewListingAndFilters,
        canViewDetailPage,
        canEditAllCategoryData,
        canCreateCategory,
        canAddAndEditingProperties,
        canEditCategoryDataWithoutActive,
        canDeleteCategory,
        canEditCategoryActive,
    };
};

export default useCategoriesAccess;
