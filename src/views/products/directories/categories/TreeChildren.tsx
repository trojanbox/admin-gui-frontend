import { useCallback, useEffect, useState } from 'react';

import { CategoryTreeItem } from '@api/catalog';

import { scale } from '@scripts/gds';
import { useLineClampCSS, useLinkCSS } from '@scripts/hooks';

import ChevronDown from '@icons/small/chevronDown.svg';

const TreeChildren = ({
    childTree,
    onSelectCategory,
}: {
    childTree: CategoryTreeItem[] | undefined;
    onSelectCategory: ({ id, children }: { id: number; children: CategoryTreeItem[] }) => void;
}) => {
    const { lineClampStyles } = useLineClampCSS(2);
    const blueLinkStyles = useLinkCSS('blue');

    const [categoriesWithExpanded, setCategoriesWithExpanded] = useState<CategoryTreeItem[] | undefined>(
        childTree?.map(c => (c?.children.length > 0 ? { ...c, expanded: false } : c))
    );

    useEffect(() => {
        setCategoriesWithExpanded(childTree?.map(c => (c?.children.length > 0 ? { ...c, expanded: false } : c)));
    }, [childTree]);

    const toggleItem = useCallback(
        ({ id, expanded }: { id: number; expanded: boolean | undefined }) => {
            setCategoriesWithExpanded(
                categoriesWithExpanded?.map(c => (c.id === id ? { ...c, expanded: !expanded } : c))
            );
        },
        [categoriesWithExpanded]
    );

    return (
        <ul>
            {categoriesWithExpanded?.map(({ name, children, id, expanded }) => (
                <li
                    key={`${id}-${name}-${children.length}`}
                    css={{ padding: `${scale(1, true)}px ${scale(0)}px ${scale(1, true)}px ${scale(2)}px` }}
                >
                    <button
                        onClick={() => {
                            toggleItem({ id, expanded });
                            onSelectCategory({ id, children });
                        }}
                        type="button"
                        css={{ width: '100%', textAlign: 'left', display: 'inline-flex' }}
                    >
                        <span
                            css={{
                                ...blueLinkStyles,
                                maxWidth: `calc(100% - ${scale(2)}px)`,
                                ...lineClampStyles,
                            }}
                        >
                            {name}
                        </span>{' '}
                        {children.length > 0 && (
                            <ChevronDown
                                css={{
                                    verticalAlign: 'middle',
                                    transition: 'transform 0.25s ease-in-out',
                                    transform: expanded ? 'rotate(180deg)' : 'none',
                                }}
                            />
                        )}
                    </button>

                    {expanded && (
                        <TreeChildren key={`${id}-${name}`} childTree={children} onSelectCategory={onSelectCategory} />
                    )}
                </li>
            ))}
        </ul>
    );
};

export default TreeChildren;
