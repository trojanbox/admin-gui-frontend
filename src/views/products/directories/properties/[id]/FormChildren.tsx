import { FieldArray, useFormikContext } from 'formik';
import { useCallback, useEffect, useMemo, useState } from 'react';

import { usePropertiesTypes } from '@api/catalog';
import { download } from '@api/common';

import { useError } from '@context/modal';

import CalendarInput from '@controls/CalendarInput';
import Dropzone from '@controls/Dropzone';
import Input from '@controls/future/Input';

import Checkbox from '@components/controls/Checkbox';
import CheckboxGroup from '@components/controls/CheckboxGroup';
import Form from '@components/controls/Form';
import Select from '@components/controls/Select';

import { Button, Layout, scale, typography, useTheme } from '@scripts/gds';
import { debounce } from '@scripts/helpers';
import { usePrevious } from '@scripts/hooks';

import TrashIcon from '@icons/small/trash.svg';

import { ATTR_PROPS, ATTR_TYPES, DirectoryValueItem, getEmptyValue } from '../../../scripts';

interface AdditionalAttributeItemProps {
    attrType: string;
    attrId?: number;
    index: number;
    remove: (i: number) => void;
    disabled: boolean;
}

interface FormValuesTypes {
    attrType: string;
    attrProps: string[];
    productNameForAdmin: string;
    productNameForPublic: string;
    additionalAttributes: DirectoryValueItem[];
}

const AdditionalAttributeItem = ({ attrType, index, remove, attrId, disabled }: AdditionalAttributeItemProps) => {
    const {
        setFieldValue,
        values: { additionalAttributes },
    } = useFormikContext<FormValuesTypes>();

    const currentColorValue = additionalAttributes[index].code;
    const prevCurrentValue = usePrevious(currentColorValue);
    const [color, setColor] = useState('#000000');

    useEffect(() => {
        if (prevCurrentValue !== currentColorValue && currentColorValue) {
            setColor(currentColorValue);
        }
    }, [prevCurrentValue, currentColorValue]);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const changeColor = useCallback(
        debounce((e: any) => {
            const colorValue = e?.target?.value;
            setFieldValue(`additionalAttributes[${index}].code`, colorValue);
        }, 300),
        [index]
    );

    const isSpecialField = useMemo(
        () =>
            attrType === ATTR_TYPES.COLOR ||
            attrType === ATTR_TYPES.FILE ||
            attrType === ATTR_TYPES.IMAGE ||
            attrType === ATTR_TYPES.DATETIME,
        [attrType]
    );

    useEffect(() => {
        if (attrType === ATTR_TYPES.COLOR && !currentColorValue)
            setFieldValue(`additionalAttributes[${index}].code`, '#000000');
    }, [attrType, currentColorValue, index, setFieldValue]);

    return (
        <li css={{ marginBottom: scale(2) }}>
            <Layout cols={5}>
                {attrType === ATTR_TYPES.COLOR && (
                    <>
                        <Layout.Item col={2}>
                            <Form.FastField
                                label={`Возможное значение ${index + 1}`}
                                name={`additionalAttributes[${index}].value`}
                                disabled={disabled}
                            />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field
                                label="Код цвета"
                                name={`additionalAttributes[${index}].code`}
                                disabled={disabled}
                            />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <input
                                name={`additionalAttributes[${index}].code`}
                                type="color"
                                value={color}
                                onChange={e => {
                                    changeColor(e);
                                    setColor(e.target.value);
                                }}
                                css={{ width: '100%', height: scale(7, true), marginTop: scale(3) }}
                                disabled={disabled}
                            />
                        </Layout.Item>
                    </>
                )}

                {attrType === ATTR_TYPES.IMAGE && (
                    <>
                        <Layout.Item col={4}>
                            <Form.Field label="Изображение" name={`additionalAttributes[${index}].image`}>
                                <Dropzone accept={['image/jpeg', 'image/jpg', 'image/png']} disabled={disabled} />
                            </Form.Field>
                        </Layout.Item>
                    </>
                )}

                {attrType === ATTR_TYPES.FILE && (
                    <>
                        <Layout.Item col={4}>
                            <Form.Field label="Файл" name={`additionalAttributes[${index}].file`}>
                                <Dropzone
                                    onFileClick={f => {
                                        const url = URL.createObjectURL(f);
                                        download(url, f.name);
                                        URL.revokeObjectURL(url);
                                    }}
                                    enableFileClick={Boolean(attrId)}
                                    disabled={disabled}
                                />
                            </Form.Field>
                        </Layout.Item>
                    </>
                )}

                {attrType === ATTR_TYPES.DATETIME && (
                    <>
                        <Layout.Item col={4} css={{ display: 'flex' }}>
                            <Form.FastField
                                name={`additionalAttributes[${index}].date`}
                                css={{ marginRight: scale(1) }}
                            >
                                <CalendarInput placeholder="Дата" label="Дата" />
                            </Form.FastField>
                            <Form.FastField
                                name={`additionalAttributes[${index}].time`}
                                label="Время"
                                placeholder="Время"
                                type="time"
                            >
                                <Input />
                            </Form.FastField>
                        </Layout.Item>
                    </>
                )}

                {!isSpecialField && (
                    <Layout.Item col={3}>
                        <Form.Field
                            label={`Возможное значение ${index + 1}`}
                            name={`additionalAttributes[${index}].value`}
                            type={attrType === ATTR_TYPES.INTEGER || attrType === ATTR_TYPES.DOUBLE ? 'number' : 'text'}
                            disabled={disabled}
                        />
                    </Layout.Item>
                )}
                <Layout.Item col={1}>
                    <Button
                        theme="outline"
                        disabled={additionalAttributes.length < 3 || disabled}
                        title="Удалить атрибут"
                        onClick={() => remove(index)}
                        css={{ marginTop: scale(3) }}
                    >
                        <TrashIcon />
                    </Button>
                </Layout.Item>
            </Layout>
        </li>
    );
};

const FormChildren = ({
    onAttrTypeChange,
    canEditPropertiesData,
    canEditPropertyActive,
    canEditPropertyViewOnShowcase,
}: {
    onAttrTypeChange?: (e: any) => void;
    canEditPropertiesData: boolean;
    canEditPropertyActive: boolean;
    canEditPropertyViewOnShowcase: boolean;
}) => {
    const {
        values: { attrType, attrProps, additionalAttributes },
        errors,
        setFieldValue,
    } = useFormikContext<FormValuesTypes>();
    const { colors } = useTheme();

    useEffect(() => {
        if (attrType === ATTR_TYPES.BOOLEAN && attrProps.includes(ATTR_PROPS.DIRECTORY)) {
            setFieldValue(
                'attrProps',
                attrProps.filter(prop => prop !== ATTR_PROPS.DIRECTORY && prop !== ATTR_PROPS.FEW_VALUES)
            );
        }
    }, [attrType, setFieldValue, attrProps]);

    const { data: propertiesTypes, error: propertiesTypesError } = usePropertiesTypes();
    useError(propertiesTypesError);

    const propertiesTypesOptions = useMemo(
        () => propertiesTypes?.data?.map(item => ({ value: item.id, label: item.name })),
        [propertiesTypes]
    );

    return (
        <>
            <Layout cols={2}>
                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                    <Form.FastField
                        name="productNameForPublic"
                        label="Публичное название"
                        disabled={canEditPropertiesData}
                    />
                </Layout.Item>
                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                    <Form.FastField
                        name="productNameForAdmin"
                        label="Рабочее название"
                        disabled={canEditPropertiesData}
                    />
                </Layout.Item>
                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                    {propertiesTypesOptions && (
                        <Form.Field name="attrType" disabled={canEditPropertiesData}>
                            <Select label="Тип" items={propertiesTypesOptions} simple onChange={onAttrTypeChange} />
                        </Form.Field>
                    )}
                </Layout.Item>
                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                    <Form.Field name="attrProps">
                        <CheckboxGroup>
                            <Checkbox value={ATTR_PROPS.ACTIVITY} disabled={canEditPropertyActive}>
                                Активность атрибута
                            </Checkbox>
                            <Checkbox value={ATTR_PROPS.PUBLIC} disabled={canEditPropertyViewOnShowcase}>
                                Отображение на витрине
                            </Checkbox>
                            <Checkbox
                                value={ATTR_PROPS.FEW_VALUES}
                                disabled={canEditPropertiesData || attrType === ATTR_TYPES.BOOLEAN}
                            >
                                Атрибут хранит несколько значений
                            </Checkbox>
                            <Checkbox
                                value={ATTR_PROPS.DIRECTORY}
                                disabled={canEditPropertiesData || attrType === ATTR_TYPES.BOOLEAN}
                            >
                                Атрибут является справочником
                            </Checkbox>
                        </CheckboxGroup>
                    </Form.Field>
                </Layout.Item>
                {/* Актуальные категории убраны, подробнее в #77533 */}
                <Layout.Item col={2}>
                    <h2 css={{ ...typography('h3'), marginBottom: scale(2) }}>Атрибут может принимать значения:</h2>
                    {attrProps.includes(ATTR_PROPS.DIRECTORY) ? (
                        <>
                            <p
                                css={{
                                    marginBottom: scale(2),
                                    ...(errors?.additionalAttributes &&
                                        !Array.isArray(errors?.additionalAttributes) && { color: colors?.danger }),
                                }}
                            >
                                Укажите не менее двух возможных значений для атрибута:
                            </p>

                            <FieldArray
                                name="additionalAttributes"
                                render={({ push, remove }) => (
                                    <>
                                        <ul>
                                            {additionalAttributes.map((atr, index) => (
                                                <AdditionalAttributeItem
                                                    index={index}
                                                    remove={remove}
                                                    key={Number(index)}
                                                    attrType={attrType}
                                                    attrId={atr.attrId}
                                                    disabled={canEditPropertiesData}
                                                />
                                            ))}
                                        </ul>
                                        <Button onClick={() => push(getEmptyValue())} disabled={canEditPropertiesData}>
                                            Добавить значение
                                        </Button>
                                    </>
                                )}
                            />
                        </>
                    ) : (
                        <>
                            <p css={{ marginBottom: scale(2), color: colors?.success }}>
                                Атрибут может принимать любые значения
                            </p>
                            <p css={{ marginBottom: scale(2) }}>
                                Чтобы установить жестко заданные варианты, выберите тип &ldquo;Справочник&rdquo;
                            </p>
                        </>
                    )}
                </Layout.Item>
            </Layout>
        </>
    );
};

export default FormChildren;
