import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';

import {
    DirectoryData,
    useDirectoryPreloadFile,
    useDirectoryPreloadImage,
    useProperty,
    usePropertyChange,
    usePropertyCreate,
    usePropertyDirectoryChange,
    usePropertyDirectoryMassCreate,
    usePropertyDirectoryRemove,
    usePropertyRemove,
} from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import { InfoListItemCommonType } from '@components/InfoList/types';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import { downloadFile } from '@components/controls/Dropzone/utils';
import Form from '@components/controls/Form';
import LoadWrapper from '@components/controls/LoadWrapper';
import Popup from '@components/controls/future/Popup';

import { CREATE_PARAM, ModalMessages } from '@scripts/constants';
import { ButtonNameEnum } from '@scripts/enums';
import { Button, scale, typography } from '@scripts/gds';
import { formatISOTimeFromDate, prepareDateTime } from '@scripts/helpers';

import CheckIcon from '@icons/small/check.svg';

import {
    ATTR_PROPS,
    ATTR_TYPES,
    DirectoryValueItem,
    dataToAttrProps,
    generateEmptyValueForAttribute,
    getValidationProperties,
} from '../../../scripts';
import usePropertiesAccess from '../usePropertiesAccess';
import FormChildren from './FormChildren';

const ProductProperty = () => {
    const { query, push, pathname } = useRouter();
    const id = Array.isArray(query.id) ? query.id[0] : query.id;
    const isCreatePage = id === CREATE_PARAM;
    const listLink = pathname.split(`[id]`)[0];
    const [filesState, setFilesState] = useState<(File | undefined)[]>([]);
    const buttonNameRef = useRef<ButtonNameEnum | null>(null);

    const {
        canViewDetailPage,
        canEditPropertiesData,
        canDeleteProperty,
        canEditPropertyActive,
        canEditPropertyViewOnShowcase,
    } = usePropertiesAccess();

    const {
        mutateAsync: addProperty,
        isLoading: isLoadingCreate,
        error: createError,
        isSuccess: isCreateSuccess,
    } = usePropertyCreate();
    useError(createError);
    useSuccess(isCreateSuccess ? ModalMessages.SUCCESS_SAVE : '');

    const { mutateAsync: preloadDirectoryImage } = useDirectoryPreloadImage();
    const { mutateAsync: preloadDirectoryFile } = useDirectoryPreloadFile();

    const { data: apiData, isLoading: isDetailLoading, error: isDetailError } = useProperty(Number(id));
    useError(isDetailError);

    const { mutateAsync: changeProperty, isLoading: isLoadingChange, error: changeError } = usePropertyChange();
    useError(changeError);

    const { mutateAsync: removeProperty, isLoading: isLoadingRemove, error: removeError } = usePropertyRemove();
    useError(removeError);

    const {
        mutateAsync: massAddDirectories,
        isLoading: isLoadingMassAddDirectories,
        error: massAddDirectoriesError,
        isSuccess: isMassAddSuccess,
    } = usePropertyDirectoryMassCreate();
    useError(massAddDirectoriesError);
    useSuccess(isMassAddSuccess ? ModalMessages.SUCCESS_SAVE : '');

    const {
        mutateAsync: changeDirectory,
        isLoading: isLoadingDirectoryChange,
        error: changeDirectoryError,
        isSuccess: isChangeSuccess,
    } = usePropertyDirectoryChange();
    useError(changeDirectoryError);
    useSuccess(isChangeSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const {
        mutateAsync: removePropertyDirectory,
        isLoading: isLoadingDirectoryRemove,
        error: removeDirectoryError,
        isSuccess: isRemoveSuccess,
    } = usePropertyDirectoryRemove();
    useError(removeDirectoryError);
    useSuccess(isRemoveSuccess ? ModalMessages.SUCCESS_DELETE : '');

    const [isPopupOpen, setIsPopupOpen] = useState(false);

    const propertyData = useMemo(() => (apiData ? apiData.data : null), [apiData]);

    const existingFiles = useCallback(async () => {
        if (propertyData?.directory?.length) {
            await Promise.all(
                propertyData?.directory.map(item => (item.url ? downloadFile(item.url, item.name) : undefined))
            ).then(res => {
                setFilesState(res);
            });
        }
    }, [propertyData?.directory]);

    useEffect(() => {
        existingFiles();
    }, [existingFiles]);

    const preparedAdditionalAttributes = useMemo(
        () =>
            propertyData?.directory?.length
                ? propertyData.directory
                      ?.sort((a, b) => Number(a.id) - Number(b.id))
                      ?.map(({ name, code, type, url, value, id: attrId }, index) => ({
                          value: name,
                          name: code,
                          code,
                          attrId,
                          ...(type === ATTR_TYPES.IMAGE &&
                              url &&
                              filesState.length && {
                                  image: [filesState[index]],
                              }),
                          ...(type === ATTR_TYPES.FILE &&
                              url &&
                              filesState.length && {
                                  file: [filesState[index]],
                              }),
                          ...(type === ATTR_TYPES.DATETIME &&
                              value && {
                                  date: new Date(value).getTime(),
                                  time: formatISOTimeFromDate(value),
                              }),
                      }))
                : generateEmptyValueForAttribute(),
        [propertyData?.directory, filesState]
    );

    const [attrType, setAttrType] = useState(propertyData ? propertyData.type : '');

    useEffect(() => {
        if (!attrType && propertyData?.type) setAttrType(propertyData.type);
    }, [attrType, propertyData?.type]);

    const preparedValidationSchema = useMemo(() => getValidationProperties(attrType), [attrType]);

    const pageTitle = useMemo(
        () => (isCreatePage ? 'Создание атрибута' : 'Редактирование товарного атрибута'),
        [isCreatePage]
    );

    const formInitValues = useMemo(
        () => ({
            attrType: propertyData ? propertyData.type : '',
            attrProps: propertyData ? dataToAttrProps(propertyData) : [],
            productNameForAdmin: propertyData ? propertyData.name : '',
            productNameForPublic: propertyData ? propertyData.display_name : '',
            additionalAttributes: preparedAdditionalAttributes,
        }),
        [preparedAdditionalAttributes, propertyData]
    );

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isLoadingCreate ||
                isDetailLoading ||
                isLoadingChange ||
                isLoadingDirectoryChange ||
                isLoadingDirectoryRemove ||
                isLoadingMassAddDirectories
            }
        >
            {canViewDetailPage ? (
                <Form
                    onSubmit={async (values, { resetForm }) => {
                        const addPropertyValues = {
                            name: values.productNameForAdmin || values.productNameForPublic,
                            display_name: values.productNameForPublic,
                            type: values.attrType,
                            is_multiple: values.attrProps.includes(ATTR_PROPS.FEW_VALUES),
                            is_active: values.attrProps.includes(ATTR_PROPS.ACTIVITY),
                            is_public: values.attrProps.includes(ATTR_PROPS.PUBLIC),
                            has_directory: values.attrProps.includes(ATTR_PROPS.DIRECTORY),
                            ...(id && { id: Number(id) }),
                        };

                        const directoriesForAdd: DirectoryData[] = [];

                        if (isCreatePage) {
                            const { data: property } = await addProperty(addPropertyValues);

                            const directoriesPromise = new Promise(resolve => {
                                [...values.additionalAttributes]
                                    ?.filter(
                                        ({
                                            value,
                                            image,
                                            file,
                                            date,
                                            time,
                                            code,
                                        }: {
                                            value?: string;
                                            date?: number | string;
                                            time?: string;
                                            code?: string;
                                            image?: (File | undefined)[] | string;
                                            file?: (File | undefined)[] | string;
                                        }) => Boolean(value || image || file || date || time || code)
                                    )
                                    ?.forEach(
                                        async (
                                            {
                                                value,
                                                image,
                                                file,
                                                date,
                                                time,
                                                code,
                                            }: {
                                                value?: string;
                                                code?: string;
                                                date?: number | string;
                                                time?: string;
                                                image?: (File | undefined)[] | string;
                                                file?: (File | undefined)[] | string;
                                            },
                                            index
                                        ) => {
                                            let fileId;

                                            if (image?.length) {
                                                const formData = new FormData();
                                                if (image[0]) formData.append('file', image[0]);

                                                const { data: resData } = await preloadDirectoryImage({ formData });
                                                fileId = resData.preload_file_id;
                                            }

                                            if (file?.length) {
                                                const formData = new FormData();
                                                if (file[0]) formData.append('file', file[0]);

                                                const { data: resData } = await preloadDirectoryFile({ formData });
                                                fileId = resData.preload_file_id;
                                            }

                                            directoriesForAdd.push({
                                                name: `${value || (date && time && prepareDateTime(date, time))}`,
                                                value:
                                                    values.attrType === ATTR_TYPES.DATETIME && date && time
                                                        ? prepareDateTime(date, time)
                                                        : value,
                                                ...(code &&
                                                    values.attrType === ATTR_TYPES.COLOR && { code, value: code }),
                                                ...(fileId && { name: `${fileId}`, preload_file_id: fileId }),
                                            });

                                            if (index === values.additionalAttributes.length - 1) {
                                                resolve(null);
                                            }
                                        }
                                    );
                            });
                            directoriesPromise.then(async () => {
                                if (directoriesForAdd.length)
                                    await massAddDirectories({
                                        propertyId: property.id,
                                        items: directoriesForAdd,
                                    });
                            });

                            resetForm();
                            setTimeout(() => {
                                if (buttonNameRef.current === ButtonNameEnum.SAVE) {
                                    push(listLink);
                                } else push(`${listLink}${property.id}`);
                            }, 0);
                        } else if (propertyData) {
                            if (propertyData.type !== addPropertyValues.type)
                                await changeProperty({ id: Number(id), type: addPropertyValues.type });

                            const { additionalAttributes } = values;

                            const computedAdditionalAttributes = additionalAttributes?.map(item => ({
                                ...item,
                                ...(!item?.name && { name: item?.value }),
                                ...(!item?.code && { code: item?.value }),
                            }));

                            const directoryPromises: Promise<any>[] = [];

                            const additionalAttributesWithValues: DirectoryValueItem[] = (
                                computedAdditionalAttributes as any
                            ).filter(({ value, image, file, date }: DirectoryValueItem) =>
                                Boolean(value || image?.length || file?.length || date)
                            );

                            if (!propertyData.has_directory) {
                                await changeProperty(addPropertyValues);
                            }

                            if (additionalAttributesWithValues.length) {
                                if (addPropertyValues.has_directory) {
                                    const deletedValues =
                                        propertyData.directory?.filter(value => {
                                            if (value.type === ATTR_TYPES.FILE || value.type === ATTR_TYPES.IMAGE) {
                                                return (
                                                    (value.type === ATTR_TYPES.IMAGE &&
                                                        !additionalAttributesWithValues.find(
                                                            item => item.value === value.name
                                                        )?.image?.length) ||
                                                    (value.type === ATTR_TYPES.FILE &&
                                                        !additionalAttributesWithValues.find(
                                                            item => item.value === value.name
                                                        )?.file?.length)
                                                );
                                            }
                                            return !additionalAttributesWithValues?.find(
                                                ({ attrId }) => Number(attrId) === Number(value?.id)
                                            );
                                        }) || [];

                                    additionalAttributesWithValues.forEach(
                                        async (
                                            { value, attrId, code, image, file, date, time }: DirectoryValueItem,
                                            index
                                        ) => {
                                            const attr = propertyData.directory?.find(
                                                directoryItem => directoryItem?.id === attrId
                                            );

                                            let fileId;

                                            if (
                                                addPropertyValues.type === ATTR_TYPES.FILE ||
                                                addPropertyValues.type === ATTR_TYPES.IMAGE
                                            ) {
                                                if (image?.length) {
                                                    const formData = new FormData();
                                                    formData.append('file', image[0]);

                                                    const { data: resData } = await preloadDirectoryImage({ formData });
                                                    fileId = resData.preload_file_id;
                                                }

                                                if (file?.length) {
                                                    const formData = new FormData();
                                                    formData.append('file', file[0]);

                                                    const { data: resData } = await preloadDirectoryFile({ formData });
                                                    fileId = resData.preload_file_id;
                                                }
                                            }

                                            if (
                                                attr &&
                                                (attr.name !== value ||
                                                    attr.code !== code ||
                                                    propertyData.type !== addPropertyValues.type) &&
                                                !attr.url
                                            ) {
                                                let preparedValue;

                                                if (values.attrType === ATTR_TYPES.COLOR) {
                                                    preparedValue = code;
                                                } else if (values.attrType === ATTR_TYPES.DATETIME && date && time) {
                                                    preparedValue = prepareDateTime(date, time);
                                                } else {
                                                    preparedValue = value;
                                                }

                                                await directoryPromises.push(
                                                    changeDirectory({
                                                        directoryId: attr.id,
                                                        name: `${
                                                            value || (date && time && prepareDateTime(date, time))
                                                        }`,
                                                        value: preparedValue,
                                                        ...(code &&
                                                            values.attrType === ATTR_TYPES.COLOR && {
                                                                code,
                                                            }),
                                                    })
                                                );
                                            } else if (
                                                attr &&
                                                fileId &&
                                                !deletedValues.map(i => i.name).includes(attr.name)
                                            ) {
                                                await directoryPromises.push(
                                                    changeDirectory({
                                                        directoryId: attr.id,
                                                        name: value,
                                                        value: `${value}`,
                                                        ...(fileId && {
                                                            name: `${fileId}`,
                                                            preload_file_id: Number(fileId),
                                                        }),
                                                    })
                                                );
                                            } else if (!attr && (code || fileId || date)) {
                                                let preparedValue;

                                                if (values.attrType === ATTR_TYPES.COLOR) {
                                                    preparedValue = code;
                                                } else if (values.attrType === ATTR_TYPES.DATETIME && date && time) {
                                                    preparedValue = prepareDateTime(date, time);
                                                } else {
                                                    preparedValue = value;
                                                }

                                                directoriesForAdd.push({
                                                    name: `${value || (date && time && prepareDateTime(date, time))}`,
                                                    value: preparedValue,
                                                    ...(code && values.attrType === ATTR_TYPES.COLOR && { code }),
                                                    ...(fileId && { name: `${fileId}`, preload_file_id: fileId }),
                                                });
                                            }

                                            if (
                                                index === additionalAttributesWithValues.length - 1 &&
                                                directoriesForAdd.length
                                            ) {
                                                await massAddDirectories({
                                                    propertyId: Number(id),
                                                    items: directoriesForAdd,
                                                });
                                            }
                                        }
                                    );

                                    deletedValues.forEach(value => {
                                        directoryPromises.push(removePropertyDirectory(value.id));
                                    });
                                } else {
                                    additionalAttributesWithValues.forEach(value => {
                                        directoryPromises.push(removePropertyDirectory(Number(value.attrId)));
                                    });
                                }

                                if (propertyData.has_directory) {
                                    await Promise.all(directoryPromises);
                                    await changeProperty(addPropertyValues);
                                } else {
                                    await Promise.all(directoryPromises);
                                }
                            } else {
                                await changeProperty(addPropertyValues);
                            }

                            resetForm();

                            setTimeout(() => {
                                if (buttonNameRef.current === ButtonNameEnum.SAVE) {
                                    push(listLink);
                                }
                            }, 0);
                        }
                    }}
                    initialValues={formInitValues}
                    validationSchema={preparedValidationSchema}
                    enableReinitialize
                >
                    {({ dirty, resetForm }) => (
                        <>
                            <PageTemplate
                                h1={pageTitle}
                                backlink={{ text: 'Назад' }}
                                controls={
                                    <>
                                        {!isCreatePage && (
                                            <Button
                                                onClick={() => setIsPopupOpen(true)}
                                                theme="dangerous"
                                                disabled={!canDeleteProperty}
                                            >
                                                Удалить
                                            </Button>
                                        )}
                                        <Button theme="secondary" onClick={() => push(listLink)}>
                                            Закрыть
                                        </Button>
                                        <Button
                                            theme="secondary"
                                            type="submit"
                                            onClick={() => {
                                                buttonNameRef.current = ButtonNameEnum.APPLY;
                                            }}
                                            disabled={!dirty}
                                        >
                                            Применить
                                        </Button>
                                        <Button
                                            type="submit"
                                            Icon={CheckIcon}
                                            iconAfter
                                            disabled={!dirty}
                                            onClick={() => {
                                                buttonNameRef.current = ButtonNameEnum.SAVE;
                                            }}
                                        >
                                            Сохранить
                                        </Button>
                                    </>
                                }
                                aside={
                                    <>
                                        <InfoList css={{ marginBottom: scale(5) }}>
                                            {(
                                                [
                                                    { name: 'ID', value: propertyData?.id },
                                                    {
                                                        name: 'Код',
                                                        value: propertyData?.code,
                                                    },
                                                ] as InfoListItemCommonType[]
                                            ).map(item => (
                                                <InfoList.Item {...item} key={item.name} />
                                            ))}
                                        </InfoList>

                                        <InfoList css={{ marginBottom: scale(5) }}>
                                            {getDefaultDates({ ...propertyData }).map(item => (
                                                <InfoList.Item {...item} key={item.name} />
                                            ))}
                                        </InfoList>
                                    </>
                                }
                            >
                                <PageLeaveGuard />
                                <FormChildren
                                    onAttrTypeChange={e => {
                                        setAttrType(e?.selectedItem?.value);
                                    }}
                                    canEditPropertiesData={!canEditPropertiesData}
                                    canEditPropertyActive={!canEditPropertyActive}
                                    canEditPropertyViewOnShowcase={!canEditPropertyViewOnShowcase}
                                />
                            </PageTemplate>
                            <Popup open={isPopupOpen} onClose={() => setIsPopupOpen(false)} size="sm">
                                <Popup.Header title="Вы уверены, что хотите удалить атрибут?" />
                                <LoadWrapper isLoading={isLoadingRemove}>
                                    <Popup.Footer>
                                        <Button
                                            type="button"
                                            onClick={() => setIsPopupOpen(false)}
                                            css={{ marginRight: scale(2) }}
                                        >
                                            Отмена
                                        </Button>
                                        <Button
                                            type="button"
                                            onClick={async () => {
                                                if (id) {
                                                    await removeProperty(Number(id));
                                                    resetForm();
                                                    push(listLink);
                                                }
                                            }}
                                        >
                                            Да
                                        </Button>
                                    </Popup.Footer>
                                </LoadWrapper>
                            </Popup>
                        </>
                    )}
                </Form>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра атрибута
                </h1>
            )}
        </PageWrapper>
    );
};

export default ProductProperty;
