import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

const usePropertiesAccess = () => {
    const { data: userData } = useCurrentUser();

    const viewListingAndFilters = 601;
    const viewDetailPage = 602;
    const editPropertiesData = 603;
    const createProperty = 604;
    const deleteProperty = 605;
    const editPropertyActive = 606;
    const editPropertyViewOnShowcase = 607;

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canViewDetailPage = useMemo(
        () => !!userData?.data.rights_access.includes(viewDetailPage),
        [userData?.data.rights_access]
    );
    const canEditPropertiesData = useMemo(
        () => !!userData?.data.rights_access.includes(editPropertiesData),
        [userData?.data.rights_access]
    );
    const canCreateProperty = useMemo(
        () => !!userData?.data.rights_access.includes(createProperty),
        [userData?.data.rights_access]
    );
    const canDeleteProperty = useMemo(
        () => !!userData?.data.rights_access.includes(deleteProperty),
        [userData?.data.rights_access]
    );
    const canEditPropertyActive = useMemo(
        () => !!userData?.data.rights_access.includes(editPropertyActive),
        [userData?.data.rights_access]
    );
    const canEditPropertyViewOnShowcase = useMemo(
        () => !!userData?.data.rights_access.includes(editPropertyViewOnShowcase),
        [userData?.data.rights_access]
    );

    return {
        canViewListingAndFilters,
        canViewDetailPage,
        canEditPropertiesData,
        canCreateProperty,
        canDeleteProperty,
        canEditPropertyActive,
        canEditPropertyViewOnShowcase,
    };
};

export default usePropertiesAccess;
