import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useReducer, useRef, useState } from 'react';

import {
    Product,
    ProductMassPatchAttribute,
    ProductMassPatchFields,
    useMassPatchProducts,
    useMassPatchProductsByQuery,
    useProblemProducts,
    useProducts,
    useProductsCommonAttributes,
    useProductsMeta,
} from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import AutoFilters from '@components/AutoFilters';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Table, {
    Data,
    ExtendedRow,
    TableEmpty,
    TableFooter,
    TableHeader,
    TooltipContentProps,
    getSelectColumn,
    getSettingsColumn,
} from '@components/Table';
import LoadWrapper from '@components/controls/LoadWrapper';
import Tooltip, { ContentBtn, hideOnEsc } from '@components/controls/Tooltip';

import { ITEMS_PER_PRODUCTS_PAGE, ModalMessages } from '@scripts/constants';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { declOfNum, getTotal, getTotalPages } from '@scripts/helpers';
import { useDebounce, useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';

import PlusIcon from '@icons/plus.svg';
import KebabIcon from '@icons/small/kebab.svg';

import { mapAttributes, useCatalogAccess, useFileUpload } from './scripts/hooks';
import ConfirmPopup from './components/ConfirmPopup';
import MassEditPopup, { MassData, isMassAttribute } from './components/MassEditPopup';
import { WarningMessage } from './components/WarningMessage';

const TableWithPopups = ({
    filters,
    columns,
    products,
    setSort,
    total,
}: {
    filters: Record<string, any>;
    columns: any[];
    products: Product[];
    setSort: (sort: any) => void;
    total: number;
}) => {
    const { pathname, push, query } = useRouter();
    const { canCreateProduct, canViewDetailPage } = useCatalogAccess();

    const [selectedRowIndicies, setSelectedRowIndicies] = useState<number[]>([]);

    const page = +`${query?.page || 1}`;
    useEffect(() => {
        setSelectedRowIndicies([]);
    }, [page, filters]);

    const goToDetailPage = useCallback(
        (originalRows: ExtendedRow['original'] | ExtendedRow['original'][] | undefined) => {
            if (!Array.isArray(originalRows) && canViewDetailPage) push(`${pathname}/${originalRows?.id}`);
        },
        [pathname, push, canViewDetailPage]
    );

    const tooltipContentRow: TooltipContentProps[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать товар',
                action: goToDetailPage,
                onDisable: () => canViewDetailPage,
            },
        ],
        [canViewDetailPage, goToDetailPage]
    );

    interface MassEditingState extends MassData {
        popupOpen: boolean;
    }

    const popupReducerFunc = (state: MassEditingState, action: Partial<MassEditingState>) => ({ ...state, ...action });

    const [massData, setMassData] = useReducer(popupReducerFunc, {
        popupOpen: false,
        attributeOrField: '',
        value: '',
    } as MassEditingState);

    const [isOpenConfirmPopup, setIsOpenConfirmPopup] = useState(false);

    const massPatchFiltered = useMassPatchProductsByQuery();
    useError(massPatchFiltered.error);
    useSuccess(massPatchFiltered.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const massPatchSelected = useMassPatchProducts();
    useError(massPatchSelected.error);
    useSuccess(massPatchSelected.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const productIds = useMemo(() => products.map(e => e.id), [products]);
    const ids = useMemo(
        () => (selectedRowIndicies.length ? selectedRowIndicies.map(e => productIds[e]) : undefined),
        [productIds, selectedRowIndicies]
    );

    const { updateAttributesImages, updateAttributesFiles, isLoading: isFilesLoading } = useFileUpload();

    const onChangeSelectedRows = useCallback(
        (rows: any[]) => {
            if (!rows) {
                setSelectedRowIndicies([]);
                return;
            }
            setSelectedRowIndicies(rows.map(r => productIds.indexOf(r?.id)).filter(e => e >= 0));
        },
        [productIds]
    );

    const tooltipInstance = useRef<any>();

    const debouncedIds = useDebounce(ids, 300);

    const { data: commonAttributesData, error: commonAttributesError } = useProductsCommonAttributes({
        filter: selectedRowIndicies.length
            ? {
                  id: debouncedIds,
              }
            : filters,
        pagination: {
            limit: -1,
            offset: 0,
            type: 'offset',
        },
    });

    const commonAttributes = useMemo(
        () => commonAttributesData?.data.filter(e => e.is_active) || [],
        [commonAttributesData]
    );
    useError(commonAttributesError);

    const submitMassEdit = useCallback(async () => {
        setIsOpenConfirmPopup(false);

        const attrOrField = massData.attributeOrField;
        if (!attrOrField) return;

        let mappedAttributes: ProductMassPatchAttribute[] = [];
        let mappedFields: ProductMassPatchFields = {};

        if (isMassAttribute(attrOrField)) {
            if (Array.isArray(massData.value)) {
                mappedAttributes = mapAttributes(
                    massData.value.map(e => ({
                        attribute: attrOrField,
                        value: e,
                    }))
                );
            } else {
                mappedAttributes = mapAttributes([{ attribute: attrOrField, value: massData.value }]);
            }
        } else {
            // Field editing, not attribute
            mappedFields = {
                [attrOrField]: massData.value,
            };
        }

        const mediaAttributesMap = isMassAttribute(attrOrField)
            ? {
                  [attrOrField.id]: massData.value,
              }
            : {};

        // предзагрузка изображений справочников
        const imagesAttributesValues = await updateAttributesImages(mediaAttributesMap, commonAttributes);
        const imagesPreloadData = imagesAttributesValues.map<ProductMassPatchAttribute>(({ data, propertyId }) => ({
            property_id: +propertyId,
            preload_file_id: data?.preload_file_id,
        }));

        // предзагрузка файлов справочников

        const filesAttributesValues = await updateAttributesFiles(mediaAttributesMap, commonAttributes);
        const filesPreloadData = filesAttributesValues.map<ProductMassPatchAttribute>(({ data, propertyId }) => ({
            property_id: +propertyId!,
            preload_file_id: data?.preload_file_id,
        }));

        const totalAttributes = [...mappedAttributes, ...imagesPreloadData, ...filesPreloadData];

        if (ids?.length) {
            await massPatchSelected.mutateAsync({
                ids: ids!,
                attributes: totalAttributes,
                fields: mappedFields,
            });
        } else {
            await massPatchFiltered.mutateAsync({
                filter: filters,
                attributes: totalAttributes,
                fields: mappedFields,
            });
        }
    }, [
        commonAttributes,
        filters,
        ids,
        massData.attributeOrField,
        massData.value,
        massPatchFiltered,
        massPatchSelected,
        updateAttributesFiles,
        updateAttributesImages,
    ]);

    const renderHeader = useCallback(
        (selectedRows: Data[]) => {
            const count = selectedRows.length;
            const isSelected = count > 0;

            const tooltipContent: TooltipContentProps[] = [
                {
                    type: 'edit',
                    text: isSelected ? 'Изменить атрибуты выделенных' : 'Изменить атрибуты всех товаров',
                    action: () => {
                        setMassData({ popupOpen: true });
                        tooltipInstance.current.hide();
                    },
                },
            ];

            return (
                <TableHeader css={{ paddingLeft: 0, paddingRight: 0 }}>
                    <>
                        <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%', padding: 0 }}>
                            {isSelected ? (
                                <span css={typography('bodyMdBold')}>Выбрано {count}</span>
                            ) : (
                                <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                                    Найдено {`${total} ${declOfNum(total, ['товар', 'товара', 'товаров'])}`}
                                </p>
                            )}
                            <Layout type="flex">
                                {canCreateProduct && (
                                    <Layout.Item>
                                        <Button Icon={PlusIcon} theme="primary" onClick={() => push(`${pathname}/new`)}>
                                            Создать товар
                                        </Button>
                                    </Layout.Item>
                                )}
                                <Layout.Item>
                                    <Link href="/products/bulk-history" passHref>
                                        <Button as="a">История изменений</Button>
                                    </Link>
                                </Layout.Item>
                                <Layout.Item>
                                    <Tooltip
                                        hideOnClick
                                        content={
                                            <>
                                                {tooltipContent.map(t => (
                                                    <ContentBtn
                                                        key={t.text}
                                                        type={t.type}
                                                        onClick={() => {
                                                            t.action(selectedRows);
                                                        }}
                                                        disabled={t.onDisable ? t.onDisable([]) : false}
                                                    >
                                                        {t.text}
                                                    </ContentBtn>
                                                ))}
                                            </>
                                        }
                                        onMount={instance => {
                                            tooltipInstance.current = instance;
                                        }}
                                        plugins={[hideOnEsc]}
                                        trigger="click"
                                        arrow
                                        theme="light"
                                        placement="bottom"
                                        minWidth={scale(36)}
                                        disabled={tooltipContent.length === 0}
                                        appendTo={() => document.body}
                                    >
                                        <Button
                                            theme="outline"
                                            Icon={KebabIcon}
                                            css={{ marginLeft: scale(2) }}
                                            iconAfter
                                        >
                                            Действия
                                        </Button>
                                    </Tooltip>
                                </Layout.Item>
                            </Layout>
                        </div>
                    </>
                </TableHeader>
            );
        },
        [total, canCreateProduct, pathname, push]
    );

    const sortingHandler = useCallback(
        (sort: string | undefined) => {
            setSelectedRowIndicies([]);
            setSort(sort);
        },
        [setSort]
    );

    return (
        <LoadWrapper isLoading={massPatchSelected.isLoading || isFilesLoading}>
            <Table
                renderHeader={renderHeader}
                columns={columns}
                data={products}
                onSortingChange={sortingHandler}
                tooltipContent={tooltipContentRow}
                initialSelectedRows={selectedRowIndicies}
                onChangeSelectedRows={onChangeSelectedRows}
            />
            <MassEditPopup
                title={
                    selectedRowIndicies.length
                        ? `Изменение атрибутов выделенных (${selectedRowIndicies.length}) товаров`
                        : 'Изменение атрибутов всех товаров'
                }
                onSubmit={data => {
                    setMassData({ popupOpen: false, ...data });
                    setIsOpenConfirmPopup(true);
                }}
                isOpen={massData.popupOpen}
                close={() => setMassData({ popupOpen: false })}
                attributes={commonAttributes}
            />
            <ConfirmPopup
                isOpen={isOpenConfirmPopup}
                onClose={() => setIsOpenConfirmPopup(false)}
                onSave={submitMassEdit}
                isSelected={selectedRowIndicies.length > 0}
            />
        </LoadWrapper>
    );
};

const Catalog = () => {
    const { canViewListingAndFilters, canViewDetailPage } = useCatalogAccess();

    const { data: metaData, error: metaError } = useProductsMeta();
    const meta = useMemo(() => metaData?.data, [metaData]);

    const { sort, activePage, itemsPerPageCount, setItemsPerPageCount, setSort } = useTableList({
        defaultSort: meta?.default_sort,
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
    });

    const { metaField, values, filtersActive, URLHelper, searchRequestFilter, emptyInitialValues } =
        useAutoFilters(meta);

    const { data, isLoading, error } = useProducts(
        {
            sort: sort || meta?.default_sort,
            filter: searchRequestFilter,
            pagination: { type: 'offset', limit: itemsPerPageCount, offset: (activePage - 1) * itemsPerPageCount },
        },
        Boolean(meta)
    );

    const {
        data: problemProducts,
        isLoading: isLoadingProblemProducts,
        error: problemProductsError,
    } = useProblemProducts({
        sort: meta?.default_sort,
        filter: { has_no_filled_required_attributes: true },
        pagination: { type: 'offset', limit: -1, offset: 0 },
    });

    const products = useAutoTableData<Product>(data?.data, metaField);
    const autoGeneratedColumns = useAutoColumns(meta, canViewDetailPage);

    const columns = useMemo(
        () => [
            getSelectColumn(),
            ...autoGeneratedColumns,
            getSettingsColumn({ columnsToDisable: [], defaultVisibleColumns: meta?.default_list }),
        ],
        [autoGeneratedColumns, meta?.default_list]
    );

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);
    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total });

    useError(error);
    useError(metaError);
    useError(problemProductsError);

    return (
        <PageWrapper h1={canViewListingAndFilters ? 'Товары' : ''} isLoading={isLoading || isLoadingProblemProducts}>
            {canViewListingAndFilters ? (
                <>
                    <AutoFilters
                        initialValues={values}
                        emptyInitialValues={emptyInitialValues}
                        onSubmit={URLHelper}
                        filtersActive={filtersActive}
                        css={{ marginBottom: scale(2) }}
                        meta={meta}
                    />
                    {problemProducts?.data && !!problemProducts.data.length && (
                        <WarningMessage
                            warningProducts={problemProducts?.data}
                            text="У следующих товаров не заполнены все обязательные атрибуты: "
                        />
                    )}
                    <Block>
                        <Block.Body>
                            <TableWithPopups
                                filters={searchRequestFilter}
                                products={products}
                                columns={columns}
                                total={total}
                                setSort={setSort}
                            />
                            {products.length === 0 ? (
                                <TableEmpty
                                    filtersActive={filtersActive}
                                    titleWithFilters="Товары с данными фильтрами не найдены"
                                    titleWithoutFilters="Товары отсутствуют"
                                />
                            ) : (
                                <TableFooter
                                    pages={totalPages}
                                    itemsPerPageCount={itemsPerPageCount}
                                    setItemsPerPageCount={setItemsPerPageCount}
                                />
                            )}
                        </Block.Body>
                    </Block>
                </>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра витрины
                </h1>
            )}
        </PageWrapper>
    );
};

export default Catalog;
