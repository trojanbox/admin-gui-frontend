import { useCallback, useEffect, useState } from 'react';

import {
    DirectoryPreloadFile,
    DirectoryPreloadImage,
    ProductDetail,
    Property,
    useDirectoryPreloadFile,
    useDirectoryPreloadImage,
    useProductPreloadImage,
} from '@api/catalog';
import { CommonResponse } from '@api/common/types';

import { useError } from '@context/modal';

import { ATTR_TYPES } from '@views/products/scripts';

import { downloadFile } from '@controls/Dropzone/utils';

import { useProductForm } from './useProductForm';

type AttributesType = ReturnType<typeof useProductForm>['initialValues']['attributes'];

export interface ImgData {
    id?: number;
    preload_file_id?: number;
    name: string;
    url?: string;
}

export const useFileUpload = (attributes?: ProductDetail['attributes'], productImages?: ProductDetail['images']) => {
    const [imageFiles, setImageFiles] = useState<File[]>([]);
    const [attributesFiles, setAttributesFiles] = useState<File[]>([]);

    const [removedImages, setRemovedImages] = useState<string[]>([]);
    const [attributesImagesFiles, setAttributesImagesFiles] = useState<File[]>([]);

    const getImages = useCallback(async () => {
        if (productImages?.length) {
            const res = await Promise.all(productImages.map(img => downloadFile(`${img.url}`, img.name)));

            const filteredFiles = res.reduce((acc, file) => (file ? [...acc, file] : acc), [] as File[]);
            setImageFiles(filteredFiles);
        }
    }, [productImages, setImageFiles]);

    useEffect(() => {
        getImages();
    }, [getImages]);

    const getAttributesImages = useCallback(async () => {
        if (attributes?.length) {
            await Promise.all(
                attributes
                    // directory_value_id есть только у справочников изображений
                    .filter(({ type, directory_value_id }) => type === ATTR_TYPES.IMAGE && !directory_value_id)
                    .map(img => downloadFile(String(img?.url), String(img.value)))
            ).then(res => {
                const filtredFiles = res.reduce((acc, file) => (file ? [...acc, file] : acc), [] as File[]);
                setAttributesImagesFiles(filtredFiles);
            });
        }
    }, [attributes]);

    useEffect(() => {
        getAttributesImages();
    }, [getAttributesImages]);

    const existingFiles = useCallback(async () => {
        if (attributes?.length) {
            await Promise.all(
                attributes
                    // directory_value_id есть только у справочников изображений
                    .filter(({ type, directory_value_id }) => type === ATTR_TYPES.FILE && !directory_value_id)
                    .map(file => downloadFile(String(file?.url), String(file.value)))
            ).then(res => {
                const filtredFiles = res.reduce((acc, file) => (file ? [...acc, file] : acc), [] as File[]);
                setAttributesFiles(filtredFiles);
            });
        }
    }, [attributes]);

    useEffect(() => {
        existingFiles();
    }, [existingFiles]);

    const loadImagesOnServer = useProductPreloadImage();
    const productAttributeImageAdd = useDirectoryPreloadImage();
    const productAttributeFileAdd = useDirectoryPreloadFile();

    useError(loadImagesOnServer.error);
    useError(productAttributeImageAdd.error);
    useError(productAttributeFileAdd.error);

    const uploadImageFile = async (img: File) => {
        const formData = new FormData();
        formData.append('file', img);
        return loadImagesOnServer.mutateAsync({ formData });
    };

    const uploadImageFiles = async (imgs: File[]) => {
        const uploadResponses = await Promise.all(imgs.map(img => uploadImageFile(img)));

        const temp = uploadResponses.reduce((acc, imgFile, index) => {
            const img = imgs.find(i => i.name === imgs[index].name);
            if (img) {
                acc.push({
                    preload_file_id: imgFile.data.preload_file_id,
                    name: img.name,
                });
            }
            return acc;
        }, [] as ImgData[]);

        return temp;
    };

    const updateAttributesImages = async (attributesValuesMap: AttributesType, categoryProperties?: Property[]) => {
        // атрибуты типа IMAGE с дропзоной
        const imageProperties = categoryProperties
            ?.filter(({ type, has_directory }) => type === ATTR_TYPES.IMAGE && !has_directory)
            ?.map(({ property_id, id }) => property_id || id);

        if (!imageProperties?.length) return [];

        const promises = Object.keys(attributesValuesMap)
            .filter(key => imageProperties.includes(Number(key)))
            .flatMap(async key => {
                const attrValue = attributesValuesMap[key];

                // если атрибут изображения НЕ множественный и файл лежит в values по ключу атрибута
                if (attrValue?.length === 1 && attrValue[0] instanceof File) {
                    if (attrValue[0]) {
                        const formData = new FormData();
                        formData.append('file', attrValue[0]);

                        return productAttributeImageAdd.mutateAsync({ formData, propertyId: key });
                    }
                }

                // если атрибут изображения множественный и массив с файлами лежит в values по ключу атрибута
                if (attrValue?.length) {
                    return Promise.all(
                        attrValue.map(async (fileArr: File[]) => {
                            if (fileArr?.[0]) {
                                const formData = new FormData();
                                formData.append('file', fileArr[0]);
                                return productAttributeImageAdd.mutateAsync({ formData, propertyId: key });
                            }
                        })
                    );
                }
            });

        return (await Promise.all(promises))
            .flatMap(e => e)
            .filter(Boolean) as (CommonResponse<DirectoryPreloadImage> & {
            propertyId: string;
        })[];
    };

    const updateAttributesFiles = async (attributesValuesMap: AttributesType, categoryProperties?: Property[]) => {
        // атрибуты типа FILE с дропзоной
        const fileProperties = categoryProperties
            ?.filter(({ type, has_directory }) => type === ATTR_TYPES.FILE && !has_directory)
            ?.map(({ property_id, id }) => property_id || id);

        if (!fileProperties) return [];

        const promises = Object.keys(attributesValuesMap)
            .filter(key => fileProperties.includes(Number(key)))
            .flatMap(async key => {
                const attrValue = attributesValuesMap[key];

                // если атрибут изображения НЕ множественный и файл лежит в values по ключу атрибута
                if (attrValue?.length === 1 && attrValue[0] instanceof File) {
                    const formData = new FormData();
                    formData.append('file', attrValue[0]);
                    return productAttributeFileAdd.mutateAsync({ formData, propertyId: key });
                }

                // если атрибут изображения множественный и массив с файлами лежит в values по ключу атрибута
                if (attrValue?.length) {
                    return Promise.all(
                        attrValue.map(async (fileArr: File[]) => {
                            if (fileArr?.[0] instanceof File) {
                                const formData = new FormData();
                                formData.append('file', fileArr[0]);
                                return productAttributeFileAdd.mutateAsync({ formData, propertyId: key });
                            }
                        })
                    );
                }

                return null;
            });

        const result = (await Promise.all(promises)).flatMap(e => e);

        return result.filter(Boolean) as (CommonResponse<DirectoryPreloadFile> & {
            propertyId: string;
        })[];
    };

    return {
        imagesState: imageFiles,
        setImagesState: setImageFiles,
        removedImages,
        setRemovedImages,
        attributesImagesState: attributesImagesFiles,
        filesState: attributesFiles,
        uploadImageFile,
        uploadImageFiles,
        updateAttributesImages,
        updateAttributesFiles,
        isLoading: productAttributeImageAdd.isLoading || productAttributeFileAdd.isLoading,
    };
};
