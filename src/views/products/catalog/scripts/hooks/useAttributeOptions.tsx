import Image from 'next/image';
import { useCallback, useMemo } from 'react';

import { Property, PropertyDirectoryItem } from '@api/catalog';

import { ATTR_TYPES } from '@views/products/scripts';

import { scale } from '@scripts/gds';

export const useAttributeOptions = ({
    properties,
    directoriesCollection,
}: {
    properties?: Property[];
    directoriesCollection?: PropertyDirectoryItem[];
}) => {
    const optionStyles = useCallback(
        (backgroundColor: string) => ({
            display: 'flex',
            width: scale(10, true),
            height: scale(3, true),
            borderRadius: '50vh',
            background: `${backgroundColor}`,
            marginRight: scale(1),
        }),
        []
    );
    const propertiesOptions = useMemo(
        () =>
            properties?.map(a => ({
                key: a.name!,
                value: a,
            })) || [],
        [properties]
    );

    // фильтруем значения справочников для опций селектов
    const getDirectoryItems = useCallback(
        (property_id: number) =>
            [
                ...(properties?.find(e => e.id === property_id)?.directory || []),
                ...(directoriesCollection?.filter(
                    ({ property_id: directoryPropertyId }) => directoryPropertyId === property_id
                ) || []),
            ].map(item => ({
                ...item,
                label: item?.name,

                ...(item?.type === ATTR_TYPES.COLOR && {
                    label: (
                        <div css={{ display: 'inline-flex', alignItems: 'center', gap: scale(1) }}>
                            <span style={optionStyles(`${item?.value}`)} />
                            <strong>{item?.name}</strong>
                        </div>
                    ),
                }),

                ...(item?.type === ATTR_TYPES.IMAGE && {
                    label: (
                        <div css={{ display: 'inline-flex', alignItems: 'center', gap: scale(1) }}>
                            <div css={{ width: scale(6), height: scale(6), borderRadius: scale(1, true) }}>
                                <Image
                                    width={scale(6)}
                                    height={scale(6)}
                                    unoptimized
                                    src={item?.url || '/noimage.png'}
                                    alt=""
                                />
                            </div>
                            <strong>{item?.name}</strong>
                        </div>
                    ),
                }),
                value: item?.id,
                dictionary_value: item?.value,
            })),
        [directoriesCollection, optionStyles, properties]
    );
    return {
        propertiesOptions,
        getDirectoryItems,
    };
};
