import { useMemo } from 'react';
import * as Yup from 'yup';

import { Property } from '@api/catalog';
import { ProductDetail } from '@api/catalog/types/products';
import { useSearchNameplates } from '@api/content/nameplates';

import { ATTR_TYPES } from '@views/products/scripts';

import { ErrorMessages } from '@scripts/constants';
import { formatISOTimeFromDate, fromKopecksToRouble, fromRoubleToKopecks, prepareDateTime } from '@scripts/helpers';

export const MAIN_DATA_VALIDATION = {
    name: Yup.string().required(ErrorMessages.REQUIRED),
    vendor_code: Yup.string().required(ErrorMessages.REQUIRED),
    barcode: Yup.mixed()
        .nullable()
        .required(ErrorMessages.REQUIRED)
        .test('barcode-range', 'Числовое значения должно быть 8 или 13 символов', anyVal => {
            const val = Number(anyVal);
            if (val) {
                const value = val.toString();
                return value.length === 8 || value.length === 13;
            }
            return false;
        }),
    base_price: Yup.number()
        .transform(value => (Number.isNaN(value) ? undefined : value))
        .required(ErrorMessages.REQUIRED),
    type: Yup.number().nullable().required(ErrorMessages.REQUIRED),
    category_id: Yup.number().nullable().required(ErrorMessages.REQUIRED),
};

export const mapAttributes = (
    entries: {
        attribute: Property;
        value: any;
    }[]
) =>
    entries
        .flatMap(({ attribute, value }) => {
            const { has_directory, is_multiple, type, id } = attribute;

            const typesOnlyWithDirectory: string[] = [ATTR_TYPES.IMAGE, ATTR_TYPES.FILE];
            if (typesOnlyWithDirectory.includes(type!) && !has_directory) return [];

            if (Array.isArray(value)) {
                const result = value.map(element => {
                    const common = {
                        property_id: id,
                        ...(has_directory && is_multiple && { directory_value_id: element }),
                        ...(!has_directory && is_multiple && { value: element }),
                    };

                    if (!has_directory && type === ATTR_TYPES.COLOR) {
                        if (!element?.value) return null;

                        return {
                            ...common,
                            value: element?.value,
                            name: element?.name || '',
                        };
                    }

                    if (!has_directory && type === ATTR_TYPES.DATETIME) {
                        if (!element?.date) return null;

                        return {
                            ...common,
                            name: element?.date && element?.time && prepareDateTime(+element?.date, element?.time),
                            value: element?.date && element?.time && prepareDateTime(+element?.date, element?.time),
                        };
                    }

                    if (!has_directory && is_multiple && (element === undefined || element === null)) {
                        return null;
                    }

                    return common;
                });

                return result.filter(Boolean) as Exclude<typeof result[number], null>[];
            }
            if (value === undefined || value === null) return [];

            return [
                {
                    property_id: id,
                    ...(has_directory && { directory_value_id: value }),
                    ...(!has_directory && type !== ATTR_TYPES.COLOR && { value }),
                    ...(!has_directory &&
                        type === ATTR_TYPES.COLOR && {
                            value: value?.value,
                            name: value?.name || '',
                        }),
                    ...(!has_directory &&
                        type === ATTR_TYPES.DATETIME && {
                            name: prepareDateTime(value?.date, value?.time),
                            value: prepareDateTime(value?.date, value?.time),
                        }),
                },
            ];
        })
        .filter(Boolean);

export interface PreloadData {
    property_id?: string;
    preload_file_id: number;
}

export const useProductForm = ({
    productData,
    images,
    attributeImages,
    attributeFiles,
}: {
    productData?: ProductDetail;
    images?: File[];
    /** "не привязанный" массив изображений, вхождение искать по полю name === value */
    attributeImages?: File[];
    /** "не привязанный" массив файлов, вхождение искать по полю name === value */
    attributeFiles?: File[];
}) => {
    const categoryPropertiesWithProductValues: Property[] = useMemo(
        () =>
            productData?.category?.properties?.map(categoryProp => {
                const multiplyValues = productData?.attributes?.filter(
                    ({ property_id }) => property_id === categoryProp?.property_id
                );

                return {
                    ...categoryProp,
                    attributeValue: multiplyValues,
                };
            }) || [],
        [productData]
    );

    const { data: apiNameplates } = useSearchNameplates(
        {
            filter: {
                product_id: productData?.id,
            },
        },
        !!productData?.id
    );

    const productNameplates = useMemo(() => apiNameplates?.data.map(e => e.id) || [], [apiNameplates?.data]);

    const attributesInitValues = useMemo(
        () =>
            categoryPropertiesWithProductValues.reduce(
                (acc, cur) => ({
                    ...acc,

                    // пустой простой атрибут
                    ...(!cur?.attributeValue?.length &&
                        !cur?.is_multiple &&
                        !cur?.has_directory && {
                            [cur?.property_id]:
                                // eslint-disable-next-line no-nested-ternary
                                cur?.type === ATTR_TYPES.COLOR
                                    ? [
                                          {
                                              name: '',
                                              value: '#000000',
                                          },
                                      ]
                                    : cur?.type === ATTR_TYPES.DATETIME
                                    ? [
                                          {
                                              date: '',
                                              time: '',
                                          },
                                      ]
                                    : '',
                        }),

                    // простое значение - ни справочник - ни множественное
                    ...(cur?.attributeValue?.length === 1 &&
                        !cur?.is_multiple &&
                        !cur?.has_directory && {
                            [cur?.property_id]: cur?.attributeValue[0]?.value,
                            ...(cur?.type === ATTR_TYPES.IMAGE && {
                                [cur?.property_id]: attributeImages?.length
                                    ? [attributeImages?.find(file => file?.name === cur?.attributeValue[0]?.value)]
                                    : '',
                            }),
                            ...(cur?.type === ATTR_TYPES.FILE && {
                                [cur?.property_id]: attributeFiles?.length
                                    ? [attributeFiles?.find(file => file?.name === cur?.attributeValue[0]?.value)]
                                    : '',
                            }),
                            ...(cur?.type === ATTR_TYPES.COLOR && {
                                [cur?.property_id]: [
                                    {
                                        name: cur?.attributeValue[0]?.name,
                                        value: cur?.attributeValue[0]?.value,
                                    },
                                ],
                            }),
                            ...(cur?.type === ATTR_TYPES.DATETIME && {
                                [cur?.property_id]: [
                                    {
                                        name: cur?.attributeValue[0]?.name,
                                        value: cur?.attributeValue[0]?.value,
                                        date: new Date(cur?.attributeValue[0]?.value).getTime(),
                                        time: formatISOTimeFromDate(cur?.attributeValue[0]?.value),
                                    },
                                ],
                            }),
                        }),

                    // пустой справочник НЕмножественный
                    ...(!cur?.attributeValue?.length &&
                        !cur?.is_multiple &&
                        cur?.has_directory && { [cur?.property_id]: null }),

                    // пустой НЕсправочник множественный
                    ...(!cur?.attributeValue?.length &&
                        cur?.is_multiple &&
                        !cur?.has_directory && {
                            [cur?.property_id]: [''],
                            ...(cur?.type === ATTR_TYPES.COLOR && {
                                [cur?.property_id]: [{ name: '', value: '' }],
                            }),
                            ...(cur?.type === ATTR_TYPES.DATETIME && {
                                [cur?.property_id]: [{ date: '', time: '' }],
                            }),
                        }),

                    // НЕсправочник множественное
                    ...(cur?.attributeValue?.length &&
                        cur?.is_multiple &&
                        !cur?.has_directory && {
                            [cur?.property_id]: cur?.attributeValue?.map((item: any) => {
                                if (cur?.type === ATTR_TYPES.IMAGE)
                                    return attributeImages?.length
                                        ? [attributeImages?.find(file => file?.name === item?.value)]
                                        : [];
                                if (cur?.type === ATTR_TYPES.FILE)
                                    return attributeFiles?.length
                                        ? [attributeFiles?.find(file => file?.name === item?.value)]
                                        : [];
                                if (cur?.type === ATTR_TYPES.COLOR) return { name: item?.name, value: item?.value };
                                if (cur?.type === ATTR_TYPES.DATETIME)
                                    return {
                                        name: item?.name,
                                        value: item?.value,
                                        date: new Date(item?.value).getTime(),
                                        time: formatISOTimeFromDate(item?.value),
                                    };
                                return item?.value;
                            }),
                        }),

                    // НЕмножественный справочник
                    ...(cur?.attributeValue?.length &&
                        !cur?.is_multiple &&
                        cur?.has_directory && { [cur?.property_id]: cur?.attributeValue[0]?.directory_value_id }),

                    // пустой множественный справочник
                    ...(!cur?.attributeValue?.length &&
                        cur?.is_multiple &&
                        cur?.has_directory && { [cur?.property_id]: [] }),

                    // множественный справочник
                    ...(cur?.attributeValue?.length >= 1 &&
                        cur?.is_multiple &&
                        cur?.has_directory &&
                        cur?.attributeValue?.some((item: any) => item?.directory_value_id !== undefined) && {
                            [cur?.property_id]: cur?.attributeValue?.map((item: any) => item?.directory_value_id),
                        }),

                    ...(cur?.type === ATTR_TYPES.BOOLEAN && {
                        [cur?.property_id]: cur?.attributeValue[0]?.value || false,
                    }),
                }),
                {} as Record<`${number}` | string, any>
            ),
        [attributeFiles, attributeImages, categoryPropertiesWithProductValues]
    );

    const initialValues = useMemo(
        () => ({
            allow_publish: productData?.allow_publish || false,

            attributes: attributesInitValues,
            nameplates: productNameplates,

            barcode: productData?.barcode || '',
            base_price: productData?.base_price ? fromKopecksToRouble(productData?.base_price || 0, 2) : '',
            brand_id: productData?.brand_id || '',
            category_id: productData?.category_id || null,
            code: productData?.code || '',
            description: productData?.description || '',
            external_id: productData?.external_id || '',
            height: productData?.height || '',
            images: images || [],
            is_adult: productData?.is_adult || false,
            length: productData?.length || '',
            name: productData?.name || '',
            type: productData?.type || null,
            vendor_code: productData?.vendor_code || '',
            weight: productData?.weight || '',
            weight_gross: productData?.weight_gross || '',
            width: productData?.width || '',
            created_at: productData?.created_at || '',
            updated_at: productData?.updated_at || '',
        }),
        [
            productData?.allow_publish,
            productData?.barcode,
            productData?.base_price,
            productData?.brand_id,
            productData?.category_id,
            productData?.code,
            productData?.description,
            productData?.external_id,
            productData?.height,
            productData?.is_adult,
            productData?.length,
            productData?.name,
            productData?.type,
            productData?.vendor_code,
            productData?.weight,
            productData?.weight_gross,
            productData?.width,
            productData?.created_at,
            productData?.updated_at,
            attributesInitValues,
            productNameplates,
            images,
        ]
    );

    const getFormValues = (
        values: typeof initialValues,
        imagesPreloadData: PreloadData[] = [],
        filesPreloadData: PreloadData[] = []
    ) => {
        const { attributes } = values;

        const fields = {
            external_id: values.external_id,
            name: values.name,
            code: values.code,
            category_id: values.category_id,
            brand_id: Number(values.brand_id) || undefined,
            description: values.description,
            type: Number(values.type),
            allow_publish: values.allow_publish,
            vendor_code: values.vendor_code,
            barcode: String(values.barcode),
            weight: Number(values.weight) || undefined,
            weight_gross: Number(values.weight_gross) || undefined,
            length: Number(values.length) || undefined,
            width: Number(values.width) || undefined,
            height: Number(values.height) || undefined,
            is_adult: values.is_adult,
            base_price: fromRoubleToKopecks(Number(values.base_price) || 0) || undefined,
        };

        // при смене категории нужно не учитывать атрибуты, ведь они меняются
        if (values?.category_id !== initialValues?.category_id) {
            return fields;
        }

        const attributeEntries = Object.keys(attributes).map(e => {
            const attribute = categoryPropertiesWithProductValues.find(({ property_id }) => property_id === Number(e))!;

            return {
                attribute: {
                    ...attribute,
                    property_id: Number(e),
                    id: Number(e),
                },
                value: attributes[e],
            };
        });

        return {
            ...fields,
            attributes: [...mapAttributes(attributeEntries), ...imagesPreloadData, ...filesPreloadData],
        };
    };

    const numberValidationWithLength = useMemo(
        () =>
            Yup.mixed()
                .transform(a => (a === '' ? undefined : Number(a)))
                .test(`number-length`, 'Превышено кол-во символов', strVal => {
                    const val = strVal as never as number;
                    if (val === undefined) return true;
                    if (val && val?.toString()?.length) {
                        return val?.toLocaleString('fullwide', { useGrouping: false })?.length < 50;
                    }
                    return false;
                })
                .nullable(),
        []
    );

    const shapeWithDynamicFields = useMemo(
        () =>
            categoryPropertiesWithProductValues.reduce(
                (acc: { [x: string]: any }, { property_id, is_required, type, has_directory, is_multiple }: any) => {
                    if (has_directory && is_multiple) {
                        acc[property_id] = is_required
                            ? Yup.array().nullable().min(1, ErrorMessages.REQUIRED)
                            : Yup.array().nullable();
                    }

                    if (has_directory && !is_multiple) {
                        acc[property_id] = is_required
                            ? Yup.number().nullable().required(ErrorMessages.REQUIRED)
                            : Yup.number().nullable();
                    }

                    if (type === ATTR_TYPES.COLOR && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        name: Yup.string()
                                            .nullable()
                                            // валидируем на обязательность только первый элемент из всего списка
                                            .test('name', ErrorMessages.REQUIRED, (item, context) => {
                                                const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                                return !(currentIndex === 0 && !item);
                                            }),

                                        value: Yup.string()
                                            .nullable()
                                            .test('value', ErrorMessages.REQUIRED, (item, context) => {
                                                const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                                return !(currentIndex === 0 && !item);
                                            }),
                                    })
                                )
                                .min(1, ErrorMessages.MIN_ITEMS(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array().of(
                                Yup.object().shape({
                                    name: Yup.string().nullable(),
                                    value: Yup.string().nullable(),
                                })
                            );
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        name: Yup.string().nullable().required(ErrorMessages.REQUIRED),
                                        value: Yup.string().nullable().required(ErrorMessages.REQUIRED),
                                    })
                                )
                                .required(ErrorMessages.REQUIRED);
                        } else
                            acc[property_id] = Yup.array().of(
                                Yup.object().shape({
                                    name: Yup.string().nullable(),
                                    value: Yup.string().nullable(),
                                })
                            );
                    }

                    if (type === ATTR_TYPES.DATETIME && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        date: Yup.number()
                                            .transform(value => (Number.isNaN(+value) ? null : value))
                                            .nullable()
                                            .test('date', ErrorMessages.REQUIRED, (item, context) => {
                                                const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                                return !(
                                                    currentIndex === 0 &&
                                                    (!item ||
                                                        (typeof item === 'string' && item === 'NaN') ||
                                                        Number.isNaN(item))
                                                );
                                            }),
                                        time: Yup.string()
                                            .nullable()
                                            .test('time', ErrorMessages.REQUIRED, (item, context) => {
                                                const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                                return !(currentIndex === 0 && !item);
                                            }),
                                    })
                                )
                                .min(1, ErrorMessages.MIN_ITEMS(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array().of(
                                Yup.object().shape({
                                    date: Yup.number()
                                        .transform(value => (Number.isNaN(+value) ? null : value))
                                        .nullable(),
                                    time: Yup.string().nullable(),
                                })
                            );
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        date: Yup.number()
                                            .transform(value => (Number.isNaN(+value) ? null : value))
                                            .nullable()
                                            .test('date', ErrorMessages.REQUIRED, val => {
                                                if (Number.isNaN(val) || (typeof val === 'string' && val === 'NaN'))
                                                    return false;
                                                return !!val;
                                            }),
                                        time: Yup.string().nullable().required(ErrorMessages.REQUIRED),
                                    })
                                )
                                .required(ErrorMessages.REQUIRED);
                        } else
                            acc[property_id] = Yup.array().of(
                                Yup.object().shape({
                                    date: Yup.number()
                                        .transform(value => (Number.isNaN(+value) ? null : value))
                                        .nullable(),
                                    time: Yup.string().nullable(),
                                })
                            );
                    }

                    if ((type === ATTR_TYPES.INTEGER || type === ATTR_TYPES.DOUBLE) && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    numberValidationWithLength.test(
                                        'number-required',
                                        ErrorMessages.REQUIRED,
                                        (item, context) => {
                                            const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                            return !(currentIndex === 0 && !item);
                                        }
                                    )
                                )
                                .min(1, ErrorMessages.MIN_ITEMS(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array().of(numberValidationWithLength);
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = numberValidationWithLength.required(ErrorMessages.REQUIRED);
                        } else acc[property_id] = numberValidationWithLength;
                    }

                    if ((type === ATTR_TYPES.TEXT || type === ATTR_TYPES.STRING) && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.string()
                                        .nullable()
                                        .max(200, 'Превышено кол-во символов')
                                        .test('string-required', ErrorMessages.REQUIRED, (item, context) => {
                                            const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                            return !(currentIndex === 0 && !item);
                                        })
                                )
                                .min(1, ErrorMessages.MIN_ITEMS(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array().of(
                                Yup.string().nullable().max(200, 'Превышено кол-во символов (200)')
                            );
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = Yup.string()
                                .nullable()
                                .max(200, 'Превышено кол-во символов')
                                .required(ErrorMessages.REQUIRED);
                        } else {
                            acc[property_id] = Yup.string().nullable().max(200, 'Превышено кол-во символов');
                        }
                    }

                    if ((type === ATTR_TYPES.IMAGE || type === ATTR_TYPES.FILE) && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.array()
                                        .max(1, ErrorMessages.MAX_FILES(1))
                                        .test('file-required', ErrorMessages.REQUIRED, (item, context) => {
                                            const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                            return !(currentIndex === 0 && !item?.length);
                                        })
                                )
                                .min(1, ErrorMessages.MIN_FILES(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array().of(
                                Yup.array().max(1, ErrorMessages.MAX_FILES(1)).nullable()
                            );
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = Yup.array()
                                .transform(e => {
                                    if (e === '' || e === undefined || e === null) return [];
                                    return e;
                                })
                                .min(1, ErrorMessages.MIN_FILES(1))
                                .max(1, ErrorMessages.MAX_FILES(1))
                                .required(ErrorMessages.REQUIRED);
                        } else {
                            acc[property_id] = Yup.array().max(1, ErrorMessages.MAX_FILES(1)).nullable();
                        }
                    }

                    return acc;
                },
                {}
            ),
        [categoryPropertiesWithProductValues, numberValidationWithLength]
    );

    const validationSchema = useMemo(
        () =>
            Yup.object().shape({
                ...MAIN_DATA_VALIDATION,
                // отменяем валидацию атрибутов при смене категории
                attributes: Yup.object().when('category_id', {
                    is: (category_id: number) => category_id === initialValues.category_id,
                    then: Yup.object().shape(shapeWithDynamicFields).nullable(),
                    otherwise: Yup.object(),
                }),
            }),
        [initialValues.category_id, shapeWithDynamicFields]
    );

    return { initialValues, validationSchema, getFormValues, categoryPropertiesWithProductValues };
};
