import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

export const useCatalogAccess = () => {
    const { data: userData } = useCurrentUser();

    const viewListingAndFilters = 401;
    const viewDetailPage = 402;
    const createProduct = 403;
    const editAllProductData = 404;
    const viewMainTab = 405;
    const viewContentTab = 406;
    const viewPropertyTab = 407;
    const editPropertyActive = 408;
    const editDataContentTab = 409;
    const editDataPropertiesTab = 410;
    const editDataMainTab = 411;
    const editOnlyPriceMainTab = 412;
    const deleteProducts = 413;

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canViewDetailPage = useMemo(
        () => !!userData?.data.rights_access.includes(viewDetailPage),
        [userData?.data.rights_access]
    );
    const canCreateProduct = useMemo(
        () => !!userData?.data.rights_access.includes(createProduct),
        [userData?.data.rights_access]
    );
    const canEditAllProductData = useMemo(
        () => !!userData?.data.rights_access.includes(editAllProductData),
        [userData?.data.rights_access]
    );
    const canViewMainTab = useMemo(
        () => !!userData?.data.rights_access.includes(viewMainTab),
        [userData?.data.rights_access]
    );
    const canViewContentTab = useMemo(
        () => !!userData?.data.rights_access.includes(viewContentTab),
        [userData?.data.rights_access]
    );
    const canViewPropertyTab = useMemo(
        () => !!userData?.data.rights_access.includes(viewPropertyTab),
        [userData?.data.rights_access]
    );
    const canEditPropertyActive = useMemo(
        () => !!userData?.data.rights_access.includes(editPropertyActive),
        [userData?.data.rights_access]
    );
    const canEditDataContentTab = useMemo(
        () => !!userData?.data.rights_access.includes(editDataContentTab),
        [userData?.data.rights_access]
    );
    const canEditDataPropertiesTab = useMemo(
        () => !!userData?.data.rights_access.includes(editDataPropertiesTab),
        [userData?.data.rights_access]
    );
    const canEditDataMainTab = useMemo(
        () => !!userData?.data.rights_access.includes(editDataMainTab),
        [userData?.data.rights_access]
    );
    const canEditOnlyPriceMainTab = useMemo(
        () => !!userData?.data.rights_access.includes(editOnlyPriceMainTab),
        [userData?.data.rights_access]
    );
    const canDeleteProducts = useMemo(
        () => !!userData?.data.rights_access.includes(deleteProducts),
        [userData?.data.rights_access]
    );

    return {
        canViewListingAndFilters,
        canViewDetailPage,
        canCreateProduct,
        canEditAllProductData,
        canViewMainTab,
        canViewContentTab,
        canViewPropertyTab,
        canEditPropertyActive,
        canEditDataContentTab,
        canEditDataPropertiesTab,
        canEditDataMainTab,
        canEditOnlyPriceMainTab,
        canDeleteProducts,
    };
};
