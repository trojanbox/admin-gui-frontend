import { ProductStatePopup } from '@scripts/enums';

import { PopupDataItem } from '../types';

export const POPUP_DATA: PopupDataItem[] = [
    {
        action: ProductStatePopup.DELETE_SUCCESS,
        title: 'Успех',
        description: 'Товар удален из системы',
        status: 'success',
    },
    {
        action: ProductStatePopup.DELETE,
        title: 'Удаление',
        description: 'Товар будет удален из системы безвозвратно.',
        status: 'delete',
    },
    {
        action: ProductStatePopup.LEAVE,
        title: 'Покинуть форму редактирования?',
        description: 'Внесенные изменения не будут сохранены.',
        status: 'warning',
    },
    {
        action: ProductStatePopup.SAVED,
        title: 'Товар создан',
        status: 'success',
    },
];

export const INCLUDE_LIST = [
    'brand',
    'brand_count',
    'category',
    'category_count',
    'images',
    'images_count',
    'attributes',
    'category.properties',
    'category.hidden_properties',
    'product_groups.products',
];

export const MASS_EDITABLE_PRODUCT_FIELDS = [
    'name',
    'vendor_code',
    'base_price',
    'type',
    'category_id',
    'brand_id',
    'is_adult',
    'width',
    'height',
    'length',
    'weight_gross',
    'weight',
    'nameplates',
    'description',
    'allow_publish',
] as const;

export type MassEditableProductField = typeof MASS_EDITABLE_PRODUCT_FIELDS[number];

export const MAIN_DATA_FIELD_NAMES: Record<MassEditableProductField, string> = {
    base_price: 'Стоимость',
    brand_id: 'Бренд',
    category_id: 'Категория',
    height: 'Высота, мм',
    weight_gross: 'Масса, кг',
    is_adult: 'Товар 18+',
    length: 'Длина, мм',
    name: 'Название товара',
    type: 'Тип',
    vendor_code: 'Артикул',
    weight: 'Масса нетто, кг',
    width: 'Ширина, мм',
    nameplates: 'Шильдики',
    description: 'Описание',
    allow_publish: 'Публикация разрешена',
};
