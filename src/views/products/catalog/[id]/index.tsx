import { useRouter } from 'next/router';
import { useMemo, useReducer, useState } from 'react';

import { ProductImage } from '@api/catalog';
import {
    useProductDeleteImage,
    useProductDetail,
    useProductDetailCreate,
    useProductDetailDelete,
    useProductDetailUpdate,
    useProductUpdateAllImages,
    useProductUpdateSeveralImages,
    useProductUploadImage,
} from '@api/catalog/products';
import { useProductAddNameplates, useProductDeleteNameplates } from '@api/content/products';

import { useError, useSuccess } from '@context/modal';

import Form from '@controls/future/Form';

import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { ModalMessages } from '@scripts/constants';
import { ProductStatePopup } from '@scripts/enums';
import { typography } from '@scripts/gds';
import { calcOptionsDifference } from '@scripts/helpers';

import { useCatalogAccess, useFileUpload, useProductForm } from '../scripts/hooks';
import Controls from './components/Controls';
import ProductStatusPopup from './components/ProductStatusPopup';
import SideBar from './components/SideBar';
import TabsContent from './components/TabsContent';
import { INCLUDE_LIST } from './scripts/settings';

const Product = () => {
    const { query, push, replace, pathname } = useRouter();

    const listLink = pathname.split(`[id]`)[0];

    const [newID, setNewID] = useState(0);
    const [isNeedRedirect, setIsNeedRedirect] = useState(false);

    const { canViewDetailPage, canViewMainTab, canViewContentTab, canViewPropertyTab, canDeleteProducts } =
        useCatalogAccess();

    const isVisibleAnyOnPage = useMemo(
        () => canViewDetailPage || canViewMainTab || canViewContentTab || canViewPropertyTab,
        [canViewDetailPage, canViewMainTab, canViewContentTab, canViewPropertyTab]
    );

    const popupReducerFunc = (
        state: { open: boolean; popupAction?: ProductStatePopup },
        action: { open?: boolean; popupAction?: ProductStatePopup }
    ) => ({ ...state, ...action });

    const [popupReducer, setPopupReducer] = useReducer(popupReducerFunc, {
        open: false,
    });

    const { id: queryId } = query;

    const id = useMemo(() => Number(queryId), [queryId]);

    const isNewProduct = useMemo(() => queryId === 'new', [queryId]);

    const productUpdate = useProductDetailUpdate();
    const productCreate = useProductDetailCreate();
    const productDelete = useProductDetailDelete();

    const productImagesAdd = useProductUpdateAllImages();
    const productImageDelete = useProductDeleteImage();
    const productImageUpload = useProductUploadImage();
    const productImagesUpdate = useProductUpdateSeveralImages();

    const {
        data: product,
        isLoading: isDetailLoading,
        error: isDetailError,
    } = useProductDetail(
        {
            id: String(id),
            include: INCLUDE_LIST,
        },
        Boolean(id) && !isNewProduct
    );

    const productData = useMemo(() => product?.data, [product]);

    const pageTitle = useMemo(
        () => (isNewProduct ? 'Новый товар' : `Товар: ${productData?.name}`),
        [productData?.name, isNewProduct]
    );

    const {
        filesState,
        imagesState,
        removedImages,
        attributesImagesState,
        setImagesState,
        setRemovedImages,
        uploadImageFiles,
        updateAttributesImages,
        updateAttributesFiles,
        isLoading: isFilesLoading,
    } = useFileUpload(productData?.attributes, productData?.images);

    const { initialValues, validationSchema, getFormValues } = useProductForm({
        productData,
        images: imagesState,
        attributeImages: attributesImagesState,
        attributeFiles: filesState,
    });

    useError(isDetailError);
    useError(productUpdate?.error);
    useError(productCreate?.error);
    useError(productDelete?.error);
    useError(productImageDelete?.error);
    useError(productImageUpload?.error);

    useSuccess(productUpdate.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(productCreate.status === 'success' ? ModalMessages.SUCCESS_SAVE : '');
    useSuccess(productDelete.status === 'success' ? ModalMessages.SUCCESS_DELETE : '');

    const addProductImgs = async (values: typeof initialValues, images: File[], idParam: number) => {
        const imgs = await uploadImageFiles(images);
        const startSortValue = values.images.length - imgs.length;
        const parsedImages = imgs.map((img, index) => ({ ...img, sort: startSortValue + index }));
        await productImagesUpdate.mutateAsync({ id: idParam, imgs: parsedImages });
    };

    const updateProductImgs = async (values: typeof initialValues, productId: number = id) => {
        if (!values.images.length) {
            await productImagesAdd.mutateAsync({ id: productId, imgs: [] });
            setRemovedImages([]);
            return;
        }

        // Картинки, которые были изначально
        const remainingImages = productData?.images?.filter(img => {
            if (img.name) {
                return removedImages.indexOf(img.name) === -1;
            }
            return true;
        });

        // Новые картинки
        const newImages = values.images.filter(
            img => remainingImages?.findIndex(remainingImg => remainingImg.name === img.name) === -1
        );

        const isSortingSaved = values.images.every((img, index) => {
            const findedIndex = productData?.images?.findIndex(productImg => productImg.name === img.name);
            if (findedIndex === -1) {
                return true;
            }
            return findedIndex === index;
        });

        if (removedImages.length || !isSortingSaved) {
            const imgs = await uploadImageFiles(newImages);
            const allImg = remainingImages ? [...imgs, ...remainingImages] : imgs;
            const removedImagesIds = productData?.images
                ?.filter(img => img.name && removedImages.includes(img.name) && img.id)
                ?.map(img => img.id);

            await Promise.all(
                removedImagesIds?.map(removedId =>
                    productImageDelete.mutateAsync({ id: productId, file_id: Number(removedId) })
                ) || []
            );

            const parsedImages = values.images.reduce((acc, imgFile) => {
                if (allImg.length) {
                    const img = allImg.find(rImg => rImg.name === imgFile.name);
                    if (img) {
                        acc.push({
                            id: img.id,
                            preload_file_id: img.preload_file_id,
                            name: img.name,
                            sort: acc.length,
                            url: img.url,
                        });
                    }
                }
                return acc;
            }, [] as ProductImage[]);

            await Promise.all(
                parsedImages?.map(async img => {
                    if (!img.id) await productImageUpload.mutateAsync({ id: productId, data: img });
                })
            );
            setRemovedImages([]);
            return;
        }

        if (
            (productData?.images && values.images.length && values.images.length > productData.images.length) ||
            !productData?.images?.length
        ) {
            await addProductImgs(values, newImages, productId);
        }
    };

    const createProduct = async (values: typeof initialValues) => {
        const createdProduct = await productCreate.mutateAsync(getFormValues(values));
        const newId = createdProduct.data.id;
        await addProductImgs(values, values.images, newId);
        setNewID(newId);
        setPopupReducer({ open: true, popupAction: ProductStatePopup.SAVED });
    };

    const addNameplates = useProductAddNameplates(id);
    const deleteNameplates = useProductDeleteNameplates(id);

    const updateProduct = async (values: typeof initialValues) => {
        await updateProductImgs(values);
        // Сбрасываем список картинок, чтобы была возможность перезаписать картинки с другой сортировкой

        // предзагрузка изображений справочников
        const imagesAttributesValues = await updateAttributesImages(
            values.attributes,
            productData?.category?.properties
        );
        const imagesPreloadData = imagesAttributesValues.map(({ data, propertyId }) => ({
            property_id: propertyId,
            preload_file_id: data?.preload_file_id,
        }));

        // предзагрузка файлов справочников

        const filesAttributesValues = await updateAttributesFiles(values.attributes, productData?.category?.properties);
        const filesPreloadData = filesAttributesValues.map(({ data, propertyId }) => ({
            property_id: propertyId,
            preload_file_id: data?.preload_file_id,
        }));

        setImagesState([]);

        await productUpdate.mutateAsync({
            id,
            data: getFormValues(values, imagesPreloadData, filesPreloadData),
        });

        // Перезапись шильдиков
        const { deleted, added } = calcOptionsDifference(initialValues.nameplates, values.nameplates);

        if (added.length) {
            await addNameplates.mutateAsync({
                ids: added,
            });
        }

        if (deleted.length) {
            await deleteNameplates.mutateAsync({
                ids: deleted,
            });
        }
    };

    return (
        <>
            <PageWrapper
                title={pageTitle}
                isLoading={
                    isDetailLoading ||
                    productUpdate?.isLoading ||
                    productCreate?.isLoading ||
                    productDelete?.isLoading ||
                    productImagesAdd?.isLoading ||
                    isFilesLoading
                }
            >
                {isVisibleAnyOnPage ? (
                    <Form
                        initialValues={initialValues}
                        enableReinitialize
                        validationSchema={validationSchema}
                        onSubmit={async (values, { reset }) => {
                            let postAttributes = {};
                            // Отсеиваем незаполненные значения атрибутов
                            if (values.attributes)
                                postAttributes = Object.entries(values.attributes).reduce(
                                    (acc: any, [key, value]: any) => {
                                        acc[key] =
                                            !Array.isArray(value) || (Array.isArray(value) && !value.length)
                                                ? value
                                                : value.filter(v => {
                                                      if (typeof v === 'string' && !v) return false;
                                                      if (
                                                          typeof v === 'object' &&
                                                          (((v?.name !== undefined || v?.value !== undefined) &&
                                                              (!v?.name || !v.value)) ||
                                                              ((v?.date !== undefined || v?.time !== undefined) &&
                                                                  (!v?.date || !v?.time)))
                                                      )
                                                          return false;
                                                      return true;
                                                  });
                                        return acc;
                                    },
                                    {}
                                );

                            const postValues = { ...values, attributes: postAttributes };

                            if (isNewProduct) {
                                await createProduct(postValues);
                                return;
                            }

                            try {
                                await updateProduct(values);
                                reset(values);

                                if (isNeedRedirect) {
                                    push({ pathname: listLink });
                                }
                            } catch (e) {
                                console.error(e);
                            }
                        }}
                    >
                        {({ reset }) => (
                            <>
                                <PageLeaveGuard<typeof initialValues>
                                    onSubmit={async values => {
                                        if (isNewProduct) {
                                            await createProduct(values);
                                            return;
                                        }

                                        await updateProduct(values);

                                        push({ pathname: listLink });
                                    }}
                                />

                                <PageTemplate
                                    h1={pageTitle}
                                    backlink={{ text: 'Назад', href: '/products/catalog' }}
                                    controls={
                                        <Controls
                                            onDelete={() =>
                                                setPopupReducer({
                                                    open: true,
                                                    popupAction: ProductStatePopup.DELETE,
                                                })
                                            }
                                            isNewProduct={isNewProduct}
                                            canDeleteProducts={canDeleteProducts}
                                            onCancel={() => push(listLink)}
                                            onSave={() => setIsNeedRedirect(true)}
                                        />
                                    }
                                    aside={<SideBar id={id} initialValues={initialValues} />}
                                    customChildren
                                >
                                    <TabsContent
                                        id={id}
                                        isNewProduct={isNewProduct}
                                        productGroups={productData?.product_groups}
                                        removedImages={removedImages}
                                        setRemovedImages={setRemovedImages}
                                    />
                                </PageTemplate>

                                <ProductStatusPopup
                                    popupReducer={popupReducer}
                                    setPopupReducer={setPopupReducer}
                                    newID={newID}
                                    onDelete={async () => {
                                        setPopupReducer({ open: false });
                                        await productDelete.mutateAsync(Number(id), {
                                            onSuccess: () => {
                                                setPopupReducer({
                                                    open: true,
                                                    popupAction: ProductStatePopup.DELETE_SUCCESS,
                                                });
                                                reset();
                                                setTimeout(() => {
                                                    replace(listLink);
                                                }, 1000);
                                            },
                                        });
                                    }}
                                />
                            </>
                        )}
                    </Form>
                ) : (
                    <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                        У вас недостаточно прав для просмотра деталей о товаре
                    </h1>
                )}
            </PageWrapper>
        </>
    );
};

export default Product;
