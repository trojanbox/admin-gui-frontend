import { Product, ProductGroupsData } from '@api/catalog';
import Checkbox from '@components/controls/Checkbox';
import Table, { ExtendedColumn, TableEmpty, TableHeader } from '@components/Table';
import { Layout } from '@scripts/gds';
import { useLinkCSS } from '@scripts/hooks';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMemo } from 'react';

interface ProductGroupsProps {
    productGroups?: ProductGroupsData[];
}

const ProductGroups = ({ productGroups }: ProductGroupsProps) => {
    const { query } = useRouter();
    const productId = Array.isArray(query.id) ? query.id[0] : query.id || '';

    const linkStyles = useLinkCSS();

    const productGroupData = useMemo(() => productGroups || [], [productGroups]);

    const convertProductGroupData = productGroupData?.map((group: ProductGroupsData) => ({
        ...group,
        products: group?.products?.map((product: Product) => ({
            ...product,
            main_product: !!(group.main_product_id === product.id),
        })),
    }));

    const columns: ExtendedColumn[] = [
        {
            Header: 'Названия товаров',
            accessor: 'name',
            disableSortBy: true,
            Cell: ({
                value,
                row: {
                    original: { id },
                },
            }) =>
                +productId === +id ? (
                    <p>{value}</p>
                ) : (
                    <Link href={`/products/catalog/${id}`} passHref>
                        <a css={{ ...linkStyles }}>{value}</a>
                    </Link>
                ),
        },
        {
            Header: 'Главный товар',
            accessor: 'main_product',
            disableSortBy: true,
            Cell: ({ value }) => <Checkbox checked={value} readOnly />,
        },
    ];

    return (
        <>
            <Layout cols={1}>
                {convertProductGroupData?.map((group: ProductGroupsData) => (
                    <Layout.Item>
                        <Table
                            renderHeader={() => (
                                <TableHeader>
                                    <Link href={`/products/product-groups/${group?.id}`} passHref>
                                        <a css={linkStyles}>{group?.name}</a>
                                    </Link>
                                </TableHeader>
                            )}
                            columns={columns}
                            data={group?.products || []}
                        />
                    </Layout.Item>
                ))}
                {convertProductGroupData.length === 0 ? (
                    <Layout.Item>
                        <TableEmpty
                            filtersActive={false}
                            titleWithFilters="Склейки не найдены"
                            titleWithoutFilters="Склеек нет"
                        />
                    </Layout.Item>
                ) : null}
            </Layout>
        </>
    );
};

export default ProductGroups;
