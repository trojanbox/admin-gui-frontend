import type { CSSObject } from '@emotion/core';
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import { useFormContext } from 'react-hook-form';

import StatusPopup from '@components/StatusPopup';

import { ProductStatePopup } from '@scripts/enums';
import { Button, scale, typography } from '@scripts/gds';

import { POPUP_DATA } from '../scripts/settings';

const ProductStatusPopup = ({
    popupReducer,
    setPopupReducer,
    newID,
    onDelete,
}: {
    popupReducer: { open: boolean; popupAction?: ProductStatePopup };
    setPopupReducer: (reducer: { open?: boolean; popupAction?: ProductStatePopup }) => void;
    newID: any;
    onDelete: () => void;
}) => {
    const { reset } = useFormContext();
    const { push, pathname } = useRouter();

    const listLink = pathname.split(`[id]`)[0];

    const popupData = useMemo(
        () => popupReducer.popupAction && POPUP_DATA.find(d => d.action === popupReducer.popupAction),
        [popupReducer.popupAction]
    );

    const statePopupFooterCSS: CSSObject = {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: scale(3),
        button: {
            ':not(first-child)': {
                marginLeft: scale(1),
            },
        },
    };
    return (
        <>
            <StatusPopup
                open={popupReducer.open}
                onClose={() => {
                    setPopupReducer({ open: false });
                    push(listLink);
                }}
                id="product-state-popup"
                title={popupData?.title}
                status={popupData?.status}
                onUnmount={() => {
                    setPopupReducer({ popupAction: undefined });
                }}
                size="sm"
            >
                <>
                    {popupData?.description && (
                        <p css={{ ...typography('bodyMd'), marginTop: scale(1) }}>{popupData.description}</p>
                    )}

                    {popupReducer.popupAction === ProductStatePopup.SAVED && (
                        <div css={statePopupFooterCSS}>
                            <Button
                                onClick={() => {
                                    reset();
                                    push(listLink);
                                    setPopupReducer({ open: false });
                                }}
                                theme="secondary"
                            >
                                Перейти к списку
                            </Button>
                            <Button
                                theme="primary"
                                onClick={() => {
                                    reset();
                                    push(`${listLink}${newID}`);
                                    setPopupReducer({ open: false });
                                }}
                            >
                                Перейти к товару
                            </Button>
                        </div>
                    )}
                    {popupReducer.popupAction === ProductStatePopup.DELETE && (
                        <div css={statePopupFooterCSS}>
                            <Button onClick={() => setPopupReducer({ open: false })} theme="secondary">
                                Отменить
                            </Button>
                            <Button
                                onClick={onDelete}
                                theme="dangerous"
                            >
                                Удалить
                            </Button>
                        </div>
                    )}
                </>
            </StatusPopup>
        </>
    );
};
export default ProductStatusPopup;
