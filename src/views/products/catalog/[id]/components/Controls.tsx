import { useFormContext } from 'react-hook-form';
import { Button } from '@scripts/gds';
import CheckIcon from '@icons/small/check.svg';


const Controls = ({
    canDeleteProducts,
    isNewProduct,
    onDelete,
    onCancel,
    onSave,
}: {
    canDeleteProducts: boolean;
    isNewProduct: boolean;
    onDelete: () => void;
    onCancel: () => void;
    onSave: () => void;
}) => {

    const {
        formState: { isDirty },
    } = useFormContext();
    return (
        <>
            {!isNewProduct && (
                <Button theme="dangerous" onClick={onDelete} disabled={!canDeleteProducts}>
                    Удалить
                </Button>
            )}
            <Button theme="secondary" onClick={onCancel}>
                Закрыть
            </Button>
            <Button theme="secondary" type="submit" disabled={!isDirty}>
                Применить
            </Button>
            <Button type="submit" Icon={CheckIcon} iconAfter disabled={!isDirty} onClick={onSave}>
                Сохранить
            </Button>
        </>
    );
};
export default Controls;
