import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import { FormikValues } from 'formik';
import { ReactNode } from 'react';
import { useFormContext } from 'react-hook-form';

import Form from '@controls/future/Form';

import CopyButton from '@components/CopyButton';
import Switcher from '@components/controls/Switcher';

import { Layout, scale, typography } from '@scripts/gds';

import { useCatalogAccess } from '../../scripts/hooks';

const SideBarItem = ({ name, children }: { name: string; children?: ReactNode }) => (
    <div css={{ ...typography('bodySm'), ':not(:last-of-type)': { marginBottom: scale(1) } }}>
        <span css={{ marginRight: scale(1, true) }}>{name}:</span>
        {children || '-'}
    </div>
);

const SideBar = ({ id, initialValues }: { id: number; initialValues: FormikValues }) => {
    const { canEditAllProductData, canEditPropertyActive } = useCatalogAccess();

    const { watch } = useFormContext();
    return (
        <>
            <Layout cols={1} gap={scale(3)}>
                <Layout.Item>
                    <Form.Field name="allow_publish">
                        <Switcher disabled={!(canEditPropertyActive || canEditAllProductData)}>
                            {watch('allow_publish') ? 'Активный' : 'Неактивный'}
                        </Switcher>
                    </Form.Field>
                </Layout.Item>
                <Layout.Item>
                    <SideBarItem name="ID">{id ? <CopyButton>{String(id)}</CopyButton> : null}</SideBarItem>
                    <SideBarItem name="Код">
                        {initialValues.code ? <CopyButton>{initialValues.code}</CopyButton> : null}
                    </SideBarItem>
                </Layout.Item>
                <Layout.Item>
                    <SideBarItem name="Изменен">
                        {initialValues.updated_at
                            ? format(new Date(initialValues.updated_at), 'dd.MM.yyyy p', {
                                  locale: ru,
                              })
                            : null}
                    </SideBarItem>
                    <SideBarItem name="Создан">
                        {initialValues.created_at
                            ? format(new Date(initialValues.created_at), 'dd.MM.yyyy p', {
                                  locale: ru,
                              })
                            : null}
                    </SideBarItem>
                </Layout.Item>
            </Layout>
        </>
    );
};

export default SideBar;
