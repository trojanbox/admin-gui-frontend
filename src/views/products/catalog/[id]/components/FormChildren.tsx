import { useCallback, useEffect, useState } from 'react';
import { useFormContext } from 'react-hook-form';

import Form from '@controls/future/Form';

import Block from '@components/Block';
import Checkbox from '@components/controls/Checkbox';
import CheckboxGroup from '@components/controls/CheckboxGroup';
import Select from '@components/controls/Select';

import { Button, Layout, scale, typography, useTheme } from '@scripts/gds';
import { debounce } from '@scripts/helpers';
import { usePrevious } from '@scripts/hooks';

import { ATTR_PROPS, ATTR_TYPES, DirectoryValueItem, getEmptyValue } from '../../../scripts';

const attrTypes = [
    { label: 'Строка', value: ATTR_TYPES.STRING },
    { label: 'Целое число', value: ATTR_TYPES.INTEGER },
    { label: 'Вещественное число', value: ATTR_TYPES.DOUBLE },
    { label: 'Дата и время', value: ATTR_TYPES.DATETIME },
    { label: 'Ссылка', value: ATTR_TYPES.DATETIME },
    { label: 'Справочник', value: ATTR_TYPES.DIRECTORY },
];

interface AdditionalAttributeItemProps {
    isColorField: boolean;
    name: string;
    index: number;
}

interface FormValuesTypes {
    attrType: string;
    attrProps: string[];
    productNameForAdmin: string;
    productNameForPublic: string;
    additionalAttributes: DirectoryValueItem[];
}

const AdditionalAttributeItem = ({ isColorField, name, index }: AdditionalAttributeItemProps) => {
    const { setValue: setFieldValue, watch } = useFormContext<FormValuesTypes>();

    const currentColorValue = watch(name as any) as string;
    const prevCurrentValue = usePrevious(currentColorValue);
    const [color, setColor] = useState('#000000');

    useEffect(() => {
        if (prevCurrentValue !== currentColorValue && currentColorValue) {
            setColor(currentColorValue);
        }
    }, [prevCurrentValue, currentColorValue]);

    const changeColor = useCallback(
        debounce((e: any) => {
            const colorValue = e?.target?.value;
            setFieldValue(`${name}.code` as any, colorValue);
        }, 300),
        [name]
    );

    return (
        <li css={{ marginBottom: scale(2) }}>
            <Layout cols={5}>
                {isColorField ? (
                    <>
                        <Layout.Item col={2}>
                            <Form.Field
                                label={`Возможное значение ${index + 1}`}
                                name={`additionalAttributes[${index}].value`}
                            />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field label="Код цвета" name={`additionalAttributes[${index}].code`} />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <input
                                name={`additionalAttributes[${index}].code`}
                                type="color"
                                value={color}
                                onChange={e => {
                                    changeColor(e);
                                    setColor(e.target.value);
                                }}
                                css={{ width: '100%', height: scale(7, true), marginTop: scale(3) }}
                            />
                        </Layout.Item>
                    </>
                ) : (
                    <Layout.Item col={3}>
                        <Form.Field
                            label={`Возможное значение ${index + 1}`}
                            name={`additionalAttributes[${index}].value`}
                        />
                    </Layout.Item>
                )}
            </Layout>
        </li>
    );
};

const FormChildren = ({ forCreate, removeAttribute }: { forCreate?: boolean; removeAttribute?: () => void }) => {
    const {
        formState: { isDirty: dirty },
        setValue: setFieldValue,
        watch,
    } = useFormContext<FormValuesTypes>();

    const [attrType, attrProps] = watch(['attrType', 'attrProps']);

    const { colors } = useTheme();
    const isColorField = attrProps ? attrProps.some(i => i === 'color') : false;
    useEffect(() => {
        if (attrType !== ATTR_TYPES.DIRECTORY && isColorField) {
            setFieldValue(
                'attrProps',
                attrProps.filter(i => i !== 'color')
            );
        }
    }, [attrProps, attrType, isColorField, setFieldValue]);

    return (
        <Block>
            <Block.Body>
                <h2 css={{ ...typography('h3'), marginBottom: scale(2) }}>Общие параметры товарного атрибута: </h2>
                <Layout cols={2}>
                    <Layout.Item>
                        <Form.Field name="productNameForPublic" label="Название атрибута для публичной части сайта" />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field
                            name="productNameForAdmin"
                            label="Название атрибута для административного раздела"
                        />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field name="attrType">
                            <Select label="Тип атрибута" items={attrTypes} />
                        </Form.Field>
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field name="attrProps">
                            <CheckboxGroup>
                                <Checkbox
                                    value={ATTR_PROPS.COLOR}
                                    checked={attrProps && attrProps.some(prop => prop === ATTR_PROPS.COLOR)}
                                    disabled={attrType !== ATTR_TYPES.DIRECTORY}
                                >
                                    Атрибут хранит цвет
                                </Checkbox>
                                <Checkbox value={ATTR_PROPS.FILTER}>Выводить атрибут в фильтр товаров</Checkbox>
                                <Checkbox value={ATTR_PROPS.FEW_VALUES}>Атрибут хранит несколько значений</Checkbox>
                            </CheckboxGroup>
                        </Form.Field>
                    </Layout.Item>
                    {/* Актуальные категории убраны, подробнее в #77533 */}
                    <Layout.Item>
                        <h2 css={{ ...typography('h3'), marginBottom: scale(2) }}>Атрибут может принимать значения:</h2>
                        {attrType === ATTR_TYPES.DIRECTORY ? (
                            <>
                                <p css={{ marginBottom: scale(2) }}>
                                    Укажите не менее двух возможных значений для атрибута:
                                </p>
                                <Form.FieldArray name="additionalAttributes" initialValue={getEmptyValue()}>
                                    {({ name, index }) => (
                                        <AdditionalAttributeItem
                                            index={index}
                                            name={name}
                                            isColorField={isColorField}
                                        />
                                    )}
                                </Form.FieldArray>
                            </>
                        ) : (
                            <>
                                <p css={{ marginBottom: scale(2), color: colors?.success }}>
                                    Атрибут может принимать любые значения
                                </p>
                                <p css={{ marginBottom: scale(2) }}>
                                    Чтобы установить жестко заданные варианты, выберите тип &ldquo;Справочник&rdquo;
                                </p>
                            </>
                        )}
                    </Layout.Item>
                </Layout>
            </Block.Body>
            <Block.Footer css={{ justifyContent: 'flex-end' }}>
                {removeAttribute && (
                    <Button type="button" css={{ marginRight: scale(2) }} onClick={removeAttribute}>
                        Удалить товарный атрибут
                    </Button>
                )}
                <Button type="submit" disabled={!dirty}>
                    {forCreate ? 'Создать' : 'Изменить'} товарный атрибут
                </Button>
            </Block.Footer>
        </Block>
    );
};

export default FormChildren;
