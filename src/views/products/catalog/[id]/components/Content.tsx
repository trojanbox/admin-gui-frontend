import Dropzone from '@controls/Dropzone';
import Form from '@controls/future/Form';
import Textarea from '@controls/Textarea';

import { FileSizes } from '@scripts/constants';
import { Layout, scale, typography, useTheme } from '@scripts/gds';

interface ContentProps {
    removedImages: string[];
    setRemovedImages: (urls: string[]) => void;
    canEditAllProductData: boolean;
    canEditDataContentTab: boolean;
}

const Content = ({ removedImages, setRemovedImages, canEditAllProductData, canEditDataContentTab }: ContentProps) => {
    const { colors } = useTheme();

    return (
        <>
            <Form.Field name="description" label="Описание">
                <Textarea disabled={!canEditDataContentTab && !canEditAllProductData} />
            </Form.Field>
            <div
                css={{
                    marginTop: scale(2),
                    marginBottom: scale(2),
                    height: '1px',
                    background: colors?.grey300,
                }}
            />
            <Layout cols={12}>
                <Layout.Item col={{ xxxl: 9, xxs: 12 }}>
                    <Form.Field
                        label="Изображения"
                        name="images"
                        disabled={!canEditDataContentTab && !canEditAllProductData}
                    >
                        <Dropzone
                            accept={['image/jpeg', 'image/jpg', 'image/png']}
                            maxSize={FileSizes.MB1}
                            onFileRemove={(_, file) => {
                                if (removedImages.indexOf(file.name) === -1) {
                                    setRemovedImages([...removedImages, file.name]);
                                }
                            }}
                        />
                    </Form.Field>
                </Layout.Item>
                <Layout.Item col={{ xxxl: 3, xxs: 12 }} css={{ minWidth: '225px' }}>
                    <h5
                        css={{
                            ...typography('bodySmBold'),
                            color: colors?.grey700,
                            marginTop: scale(2),
                            marginBottom: scale(1),
                        }}
                    >
                        Требования к файлу:
                    </h5>
                    <ul
                        css={{
                            ...typography('bodySm'),
                            color: colors?.grey800,
                        }}
                    >
                        <li>1. Максимальное кол-во &#8212; 10шт;</li>
                        <li>2. Формат &#8212; JPEG или PNG;</li>
                        <li>3. Размер &#8212; не больше 1 МБ;</li>
                    </ul>
                </Layout.Item>
            </Layout>
        </>
    );
};
export default Content;
