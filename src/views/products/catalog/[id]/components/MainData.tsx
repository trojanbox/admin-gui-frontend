import Form from '@controls/future/Form';

import { Layout, colors, typography } from '@scripts/gds';

import ProductFields from '../../components/ProductFields';

const MainData = ({
    canEditAllProductData,
    canEditDataMainTab,
}: {
    canEditAllProductData: boolean;
    canEditDataMainTab: boolean;
}) => (
    <>
        <Layout cols={{ xxxl: 6 }}>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <Form.Field
                    disabled={!canEditAllProductData && !canEditDataMainTab}
                    name="name"
                    label="Название товара*"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <Form.Field
                    disabled={!canEditAllProductData && !canEditDataMainTab}
                    name="vendor_code"
                    label="Артикул*"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <Form.TypedField
                    disabled={!canEditAllProductData && !canEditDataMainTab}
                    name="barcode"
                    label="Штрихкод*"
                    fieldType="positiveInt"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.BasePrice />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.Type />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.Category />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.Nameplates />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.Brand />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3 }}>
                <ProductFields.Adult />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 6 }}>
                <hr />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 6 }}>
                <h4
                    css={{
                        ...typography('bodyMdBold'),
                        color: colors?.grey700,
                    }}
                >
                    Габариты
                </h4>
            </Layout.Item>
            <Layout.Item col={{ xxxl: 2, xxs: 6 }}>
                <Form.TypedField
                    disabled={!canEditAllProductData && !canEditDataMainTab}
                    name="width"
                    label="Ширина, мм"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 2, xxs: 6 }}>
                <Form.TypedField
                    disabled={!canEditAllProductData && !canEditDataMainTab}
                    name="height"
                    label="Высота, мм"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 2, xxs: 6 }}>
                <Form.TypedField
                    disabled={!canEditAllProductData && !canEditDataMainTab}
                    name="length"
                    label="Длина, мм"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <Form.TypedField
                    disabled={!canEditAllProductData && !canEditDataMainTab}
                    name="weight_gross"
                    label="Масса, кг"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <Form.TypedField
                    disabled={!canEditAllProductData && !canEditDataMainTab}
                    name="weight"
                    label="Масса нетто, кг"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
        </Layout>
    </>
);

export default MainData;
