import { Dispatch, SetStateAction } from 'react';
import { useFormContext } from 'react-hook-form';

import { ProductGroupsData } from '@api/catalog';

import Tabs from '@controls/Tabs';

import Block from '@components/Block';
import Circle from '@components/Circle';

import { scale, useTheme } from '@scripts/gds';

import { ProductAttributesList } from '../../components/ProductAttributeList';
import { useCatalogAccess } from '../../scripts/hooks';
import { MASS_EDITABLE_PRODUCT_FIELDS } from '../scripts/settings';
import Content from './Content';
import MainData from './MainData';
import ProductGroups from './ProductGroups';

const hasErrors = (attrErrors: any) => Array.isArray(attrErrors) && attrErrors.some(e => e !== null);

const hasErrorsByKeys = (errors: any, keys: string[]) => {
    if (keys.some(e => !!errors[e])) return true;

    return false;
};

const TabsContent = ({
    id,
    isNewProduct,
    productGroups,
    removedImages,
    setRemovedImages,
}: {
    id: number;
    isNewProduct: boolean;
    productGroups?: ProductGroupsData[];
    removedImages: string[];
    setRemovedImages: Dispatch<SetStateAction<string[]>>;
}) => {
    const { colors } = useTheme();

    const {
        formState: { errors },
    } = useFormContext();

    const {
        canViewDetailPage,
        canEditAllProductData,
        canViewMainTab,
        canViewContentTab,
        canViewPropertyTab,
        canEditDataContentTab,
        canEditDataMainTab,
    } = useCatalogAccess();

    return (
        <Tabs css={{ flexGrow: 1 }}>
            <Tabs.List>
                {(canViewMainTab || canViewDetailPage) && (
                    <Tabs.Tab>
                        {hasErrorsByKeys(errors, MASS_EDITABLE_PRODUCT_FIELDS as never as string[]) && (
                            <Circle css={{ background: colors?.danger, marginRight: scale(1) }} />
                        )}
                        Основные данные
                    </Tabs.Tab>
                )}
                {(canViewContentTab || canViewDetailPage) && <Tabs.Tab>Контент</Tabs.Tab>}
                {(canViewPropertyTab || canViewDetailPage) && (
                    <Tabs.Tab disabled={isNewProduct}>
                        {hasErrors(errors?.attributes) && (
                            <Circle css={{ background: colors?.danger, marginRight: scale(1) }} />
                        )}
                        Характеристики
                    </Tabs.Tab>
                )}
                <Tabs.Tab>Товарные скейки</Tabs.Tab>
            </Tabs.List>
            <Block css={{ padding: scale(3), borderTopLeftRadius: 0 }}>
                {(canViewMainTab || canViewDetailPage) && (
                    <Tabs.Panel>
                        <MainData
                            canEditAllProductData={canEditAllProductData}
                            canEditDataMainTab={canEditDataMainTab}
                        />
                    </Tabs.Panel>
                )}
                {(canViewContentTab || canViewDetailPage) && (
                    <Tabs.Panel>
                        <Content
                            removedImages={removedImages}
                            setRemovedImages={setRemovedImages}
                            canEditAllProductData={canEditAllProductData}
                            canEditDataContentTab={canEditDataContentTab}
                        />
                    </Tabs.Panel>
                )}
                {(canViewPropertyTab || canViewDetailPage) && (
                    <Tabs.Panel>
                        <ProductAttributesList id={id} />
                    </Tabs.Panel>
                )}
                <Tabs.Panel>
                    <ProductGroups productGroups={productGroups} />
                </Tabs.Panel>
            </Block>
        </Tabs>
    );
};

export default TabsContent;
