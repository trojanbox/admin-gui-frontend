import { ProductStatePopup } from '@scripts/enums';

export interface PopupDataItem {
    action: ProductStatePopup;
    title: string;
    description?: string;
    status: string;
}
