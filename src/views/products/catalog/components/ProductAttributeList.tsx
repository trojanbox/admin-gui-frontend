import Image from 'next/image';
import { useCallback, useMemo } from 'react';

import { useProductDetail, usePropertyDirectories } from '@api/catalog';

import { useError } from '@context/modal';

import { ATTR_TYPES } from '@views/products/scripts';

import LoadWrapper from '@controls/LoadWrapper';

import { CREATE_PARAM } from '@scripts/constants';
import { Layout, scale } from '@scripts/gds';

import { useProductForm } from '../scripts/hooks';
import { BooleanField } from './AttributeField/Boolean';
import { ColorField } from './AttributeField/Color';
import { DateTimeField } from './AttributeField/DateTime';
import { FileField } from './AttributeField/File';
import { ImageField } from './AttributeField/Image';
import { NumberField } from './AttributeField/Number';
import { StringField } from './AttributeField/String';
import { TextField } from './AttributeField/Text';

/**
 * Компонент который отвечает за рендер списка атрибутов по id товара
 */
export const ProductAttributesList = ({ id }: { id: typeof CREATE_PARAM | number }) => {
    const isNewProduct = id === CREATE_PARAM;

    // деталка товара
    const { data: product } = useProductDetail(
        {
            id: String(id),
            include: [
                'brand',
                'brand_count',
                'category',
                'category_count',
                'images',
                'images_count',
                'attributes',
                'category.properties',
                'category.hidden_properties',
                'product_groups.products',
            ],
        },
        Boolean(id) && !isNewProduct
    );

    // добавляем значения атрибутов товара в список атрибутов родительской категории товара
    const { categoryPropertiesWithProductValues } = useProductForm({ productData: product?.data });

    // получаем коллекцию всех значений справочников всех атрибутов-справочников
    const {
        data: directories,
        isLoading: isLoadingDirectories,
        error: directoriesError,
    } = usePropertyDirectories(
        {
            sort: [],
            include: [],
            filter: {
                property_id: categoryPropertiesWithProductValues
                    ?.filter(({ has_directory }) => has_directory)
                    ?.map(({ property_id }) => property_id),
            },
            pagination: { type: 'offset', limit: -1, offset: 0 },
        },
        Boolean(product)
    );
    useError(directoriesError);

    const directoriesCollection = useMemo(() => directories?.data, [directories]);

    const optionStyles = useCallback(
        (backgroundColor: string) => ({
            display: 'flex',
            width: scale(10, true),
            height: scale(3, true),
            borderRadius: '50vh',
            background: `${backgroundColor}`,
            marginRight: scale(1),
        }),
        []
    );

    // фильтруем значения справочников для опций селектов
    const getDirectoryItems = useCallback(
        (property_id: number) =>
            directoriesCollection
                ?.filter(({ property_id: directoryPropertyId }) => directoryPropertyId === property_id)
                ?.map(item => ({
                    ...item,
                    label: item?.name,

                    ...(item?.type === ATTR_TYPES.COLOR && {
                        label: (
                            <div css={{ display: 'inline-flex', alignItems: 'center', gap: scale(1) }}>
                                <span style={optionStyles(`${item?.value}`)} />
                                <strong>{item?.name}</strong>
                            </div>
                        ),
                    }),

                    ...(item?.type === ATTR_TYPES.IMAGE && {
                        label: (
                            <div css={{ display: 'inline-flex', alignItems: 'center', gap: scale(1) }}>
                                <div css={{ width: scale(6), height: scale(6), borderRadius: scale(1, true) }}>
                                    <Image
                                        width={scale(6)}
                                        height={scale(6)}
                                        unoptimized
                                        src={item?.url || '/noimage.png'}
                                        alt=""
                                    />
                                </div>
                                <strong>{item?.name}</strong>
                            </div>
                        ),
                    }),
                    value: item?.id,
                    dictionary_value: item?.value,
                })) || [],
        [directoriesCollection, optionStyles]
    );

    return (
        <LoadWrapper isLoading={isLoadingDirectories}>
            <Layout cols={2} gap={scale(2)} css={{ marginBottom: scale(2) }}>
                {categoryPropertiesWithProductValues?.map(property => {
                    const { type, property_id } = property;
                    const commonProps = {
                        fieldName: `attributes[${property_id}]`,
                        directoryItems: getDirectoryItems(property_id),
                        property,
                    };
                    switch (type) {
                        case ATTR_TYPES.STRING:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <StringField {...commonProps} />
                                </Layout.Item>
                            );
                        case ATTR_TYPES.TEXT:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <TextField {...commonProps} />
                                </Layout.Item>
                            );

                        case ATTR_TYPES.INTEGER:
                        case ATTR_TYPES.DOUBLE:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <NumberField {...commonProps} />
                                </Layout.Item>
                            );

                        case ATTR_TYPES.COLOR:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <ColorField {...commonProps} />
                                </Layout.Item>
                            );

                        case ATTR_TYPES.IMAGE:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <ImageField {...commonProps} />
                                </Layout.Item>
                            );

                        case ATTR_TYPES.FILE:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <FileField {...commonProps} />
                                </Layout.Item>
                            );

                        case ATTR_TYPES.DATETIME:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <DateTimeField {...commonProps} />
                                </Layout.Item>
                            );
                        case ATTR_TYPES.BOOLEAN:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <BooleanField {...commonProps} />
                                </Layout.Item>
                            );
                        default:
                            return null;
                    }
                })}
            </Layout>
        </LoadWrapper>
    );
};
