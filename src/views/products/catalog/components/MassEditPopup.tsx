import { useEffect, useMemo, useState } from 'react';
import * as Yup from 'yup';

import { Property } from '@api/catalog';

import { ATTR_TYPES } from '@views/products/scripts';

import Select, { OptionShape } from '@controls/future/Select';

import Form from '@components/controls/future/Form';
import Popup from '@components/controls/future/Popup';

import { ErrorMessages } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';

import { MAIN_DATA_FIELD_NAMES, MASS_EDITABLE_PRODUCT_FIELDS } from '../[id]/scripts/settings';
import { MAIN_DATA_VALIDATION, useAttributeOptions } from '../scripts/hooks';
import AttributeField from './AttributeField';
import { MainDataField } from './MainDataField';

type FieldName = string;
type AttrOrField = Property | FieldName;

export interface MassData {
    attributeOrField: AttrOrField;
    value: any[] | string | boolean | number;
}

export function isMassAttribute(attributeOrField?: AttrOrField): attributeOrField is Property {
    if (!attributeOrField) return false;
    return typeof attributeOrField === 'object' && attributeOrField && 'type' in attributeOrField;
}

export function isMassField(attributeOrField?: AttrOrField): attributeOrField is FieldName {
    if (!attributeOrField) return false;
    return typeof attributeOrField === 'string';
}

const MassEditPopup = ({
    title,
    onSubmit,
    isOpen,
    close,
    attributes: properties,
}: {
    title: string;
    onSubmit: (vals: MassData) => Promise<void> | void;
    isOpen: boolean;
    close: () => void;
    attributes: Property[];
}) => {
    const [selectedOption, setSelectedOption] = useState<Omit<OptionShape, 'value'> & { value: AttrOrField }>();
    const selectedAttrOrField = selectedOption?.value;

    useEffect(() => {
        if (!isOpen) setSelectedOption(undefined);
    }, [isOpen]);

    const { propertiesOptions } = useAttributeOptions({
        properties,
    });

    const initialValue = useMemo(() => {
        if (isMassField(selectedAttrOrField) && selectedAttrOrField) {
            const boolKeys = ['is_adult', 'allow_publish'];
            if (boolKeys.includes(selectedAttrOrField)) return false;

            return '';
        }

        if (!selectedAttrOrField || !isMassAttribute(selectedAttrOrField)) return '';

        // пустой простой атрибут
        if (!selectedAttrOrField?.is_multiple && !selectedAttrOrField?.has_directory) {
            if (selectedAttrOrField?.type === ATTR_TYPES.COLOR)
                return [
                    {
                        name: '',
                        value: '#000000',
                    },
                ];

            if (selectedAttrOrField?.type === ATTR_TYPES.DATETIME)
                return [
                    {
                        date: '',
                        time: '',
                    },
                ];
        }

        // пустой справочник НЕмножественный
        if (!selectedAttrOrField?.is_multiple && selectedAttrOrField?.has_directory) return '';
        // пустой НЕсправочник множественный
        if (selectedAttrOrField?.is_multiple && !selectedAttrOrField?.has_directory)
            // eslint-disable-next-line no-nested-ternary
            return selectedAttrOrField?.type === ATTR_TYPES.COLOR
                ? [{ name: '', value: '' }]
                : selectedAttrOrField?.type === ATTR_TYPES.DATETIME
                ? [{ date: '', time: '' }]
                : [''];
        // пустой множественный справочник
        if (selectedAttrOrField?.is_multiple && selectedAttrOrField?.has_directory) return [];
        if (selectedAttrOrField?.type === ATTR_TYPES.BOOLEAN) return false;
        return '';
    }, [selectedAttrOrField]);

    const numberValidationWithLength = useMemo(
        () =>
            Yup.number()
                .transform(value => (Number.isNaN(+value) ? undefined : value))
                .test(`number-length`, 'Превышено кол-во символов', val => {
                    if (val === undefined) return true;
                    if (val && val?.toString()?.length) {
                        return val?.toLocaleString('fullwide', { useGrouping: false })?.length < 50;
                    }
                    return false;
                })
                .nullable(),
        []
    );

    const valueValidation = useMemo(() => {
        if (selectedAttrOrField && isMassAttribute(selectedAttrOrField)) {
            const { is_required, type, has_directory, is_multiple } = selectedAttrOrField;

            if (type === ATTR_TYPES.COLOR && !has_directory) {
                if (is_required && is_multiple)
                    return Yup.array()
                        .of(
                            Yup.object().shape({
                                name: Yup.string()
                                    .nullable()
                                    // валидируем на обязательность только первый элемент из всего списка
                                    .test('name', ErrorMessages.REQUIRED, (item, context) => {
                                        const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                        return !(currentIndex === 0 && !item);
                                    }),

                                value: Yup.string()
                                    .nullable()
                                    .test('value', ErrorMessages.REQUIRED, (item, context) => {
                                        const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                        return !(currentIndex === 0 && !item);
                                    }),
                            })
                        )
                        .min(1, ErrorMessages.MIN_ITEMS(1))
                        .required(ErrorMessages.REQUIRED);
                if (!is_required && is_multiple) {
                    return Yup.array().of(
                        Yup.object().shape({
                            name: Yup.string().nullable(),
                            value: Yup.string().nullable(),
                        })
                    );
                }
                if (is_required && !is_multiple) {
                    return Yup.array()
                        .of(
                            Yup.object().shape({
                                name: Yup.string().nullable().required(ErrorMessages.REQUIRED),
                                value: Yup.string().nullable().required(ErrorMessages.REQUIRED),
                            })
                        )
                        .required(ErrorMessages.REQUIRED);
                }
                if (!is_required && !is_multiple) {
                    return Yup.array().of(
                        Yup.object().shape({
                            name: Yup.string().nullable(),
                            value: Yup.string().nullable(),
                        })
                    );
                }
            }
            if (type === ATTR_TYPES.DATETIME && !has_directory) {
                if (is_required && is_multiple) {
                    return Yup.array()
                        .of(
                            Yup.object().shape({
                                date: Yup.number()
                                    .transform(value => (Number.isNaN(+value) ? null : value))
                                    .nullable()
                                    .test('date', ErrorMessages.REQUIRED, (item, context) => {
                                        const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                        return !(
                                            currentIndex === 0 &&
                                            (!item ||
                                                (typeof item === 'string' && item === 'NaN') ||
                                                Number.isNaN(item))
                                        );
                                    }),
                                time: Yup.string()
                                    .nullable()
                                    .test('time', ErrorMessages.REQUIRED, (item, context) => {
                                        const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                        return !(currentIndex === 0 && !item);
                                    }),
                            })
                        )
                        .min(1, ErrorMessages.MIN_ITEMS(1))
                        .required(ErrorMessages.REQUIRED);
                }
                if (!is_required && is_multiple) {
                    return Yup.array().of(
                        Yup.object().shape({
                            date: Yup.number()
                                .transform(value => (Number.isNaN(+value) ? null : value))
                                .nullable(),
                            time: Yup.string().nullable(),
                        })
                    );
                }
                if (is_required && !is_multiple) {
                    return Yup.array()
                        .of(
                            Yup.object().shape({
                                date: Yup.number()
                                    .transform(value => (Number.isNaN(+value) ? null : value))
                                    .nullable()
                                    .test('date', ErrorMessages.REQUIRED, val => {
                                        if (Number.isNaN(val) || (typeof val === 'string' && val === 'NaN'))
                                            return false;
                                        return !!val;
                                    }),
                                time: Yup.string().nullable().required(ErrorMessages.REQUIRED),
                            })
                        )
                        .required(ErrorMessages.REQUIRED);
                }

                return Yup.array().of(
                    Yup.object().shape({
                        date: Yup.number()
                            .transform(value => (Number.isNaN(+value) ? null : value))
                            .nullable(),
                        time: Yup.string().nullable(),
                    })
                );
            }
            if ((type === ATTR_TYPES.INTEGER || type === ATTR_TYPES.DOUBLE) && !has_directory) {
                if (is_required && is_multiple) {
                    return Yup.array()
                        .of(
                            numberValidationWithLength.test(
                                'number-required',
                                ErrorMessages.REQUIRED,
                                (item, context) => {
                                    const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                    return !(currentIndex === 0 && !item);
                                }
                            )
                        )
                        .min(1, ErrorMessages.MIN_ITEMS(1))
                        .required(ErrorMessages.REQUIRED);
                }
                if (!is_required && is_multiple) {
                    return Yup.array().of(numberValidationWithLength);
                }
                if (is_required && !is_multiple) {
                    return numberValidationWithLength.required(ErrorMessages.REQUIRED);
                }
                return numberValidationWithLength;
            }
            if ((type === ATTR_TYPES.TEXT || type === ATTR_TYPES.STRING) && !has_directory) {
                if (is_required && is_multiple) {
                    return Yup.array()
                        .of(
                            Yup.string()
                                .nullable()
                                .max(200, 'Превышено кол-во символов')
                                .test('string-required', ErrorMessages.REQUIRED, (item, context) => {
                                    const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                    return !(currentIndex === 0 && !item);
                                })
                        )
                        .min(1, ErrorMessages.MIN_ITEMS(1))
                        .required(ErrorMessages.REQUIRED);
                }
                if (!is_required && is_multiple) {
                    return Yup.array().of(Yup.string().nullable().max(200, 'Превышено кол-во символов (200)'));
                }
                if (is_required && !is_multiple) {
                    return Yup.string()
                        .nullable()
                        .max(200, 'Превышено кол-во символов')
                        .required(ErrorMessages.REQUIRED);
                }
                return Yup.string().nullable().max(200, 'Превышено кол-во символов');
            }
            if ((type === ATTR_TYPES.IMAGE || type === ATTR_TYPES.FILE) && !has_directory) {
                if (is_required && is_multiple) {
                    return Yup.array()
                        .of(
                            Yup.array()
                                .max(1, ErrorMessages.MAX_FILES(1))
                                .test('file-required', ErrorMessages.REQUIRED, (item, context) => {
                                    const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                    return !(currentIndex === 0 && !item?.length);
                                })
                        )
                        .min(1, ErrorMessages.MIN_FILES(1))
                        .required(ErrorMessages.REQUIRED);
                }
                if (!is_required && is_multiple) {
                    return Yup.array().of(Yup.array().max(1, ErrorMessages.MAX_FILES(1)).nullable());
                }
                if (is_required && !is_multiple) {
                    return Yup.array()
                        .min(1, ErrorMessages.MIN_FILES(1))
                        .max(1, ErrorMessages.MAX_FILES(1))
                        .required(ErrorMessages.REQUIRED);
                }
                if (!is_required && !is_multiple) {
                    return Yup.array().max(1, ErrorMessages.MAX_FILES(1)).nullable();
                }
            }

            if (has_directory && is_multiple) {
                return is_required ? Yup.array().nullable().min(1, ErrorMessages.REQUIRED) : Yup.array().nullable();
            }
            if (has_directory && !is_multiple) {
                return is_required ? Yup.number().nullable().required(ErrorMessages.REQUIRED) : Yup.number().nullable();
            }
        }

        if (selectedAttrOrField && isMassField(selectedAttrOrField) && selectedAttrOrField in MAIN_DATA_VALIDATION) {
            return MAIN_DATA_VALIDATION[selectedAttrOrField as keyof typeof MAIN_DATA_VALIDATION];
        }

        return Yup.mixed();
    }, [selectedAttrOrField, numberValidationWithLength]);

    const validationSchema = useMemo(
        () =>
            Yup.object().shape({
                attributeOrField: Yup.mixed().required(ErrorMessages.REQUIRED),
                value: valueValidation,
            }),
        [valueValidation]
    );

    const combinedOptions = useMemo(() => {
        const options = MASS_EDITABLE_PRODUCT_FIELDS.map(e => ({
            key: MAIN_DATA_FIELD_NAMES[e],
            value: e,
        }));

        return [...options, ...propertiesOptions];
    }, [propertiesOptions]);

    const initialValues = useMemo(
        () => ({
            attributeOrField: selectedAttrOrField,
            value: initialValue,
        }),
        [initialValue, selectedAttrOrField]
    );

    return (
        <Popup open={isOpen} onClose={close} size="minMd">
            <Popup.Header title={title} />
            <Form
                initialValues={initialValues}
                enableReinitialize
                validationSchema={validationSchema}
                onSubmit={async vals => {
                    onSubmit({
                        attributeOrField: vals.attributeOrField!,
                        value: vals.value,
                    });
                }}
            >
                <Popup.Content>
                    {combinedOptions.length ? (
                        <>
                            <Form.Field name="atribute" label="Атрибут или свойство" css={{ marginBottom: scale(3) }}>
                                <Select
                                    hideClearButton
                                    options={combinedOptions}
                                    onChange={({ selected }) => {
                                        setSelectedOption(selected as any);
                                    }}
                                    selected={selectedOption}
                                    visibleOptions={7}
                                />
                            </Form.Field>
                            {selectedAttrOrField && isMassAttribute(selectedAttrOrField) && (
                                <AttributeField property={selectedAttrOrField} />
                            )}
                            {isMassField(selectedAttrOrField) && (
                                <MainDataField fieldName={selectedAttrOrField as any} />
                            )}
                        </>
                    ) : (
                        <>У данных товаров нет пересечений в свойствах</>
                    )}
                </Popup.Content>
                <Popup.Footer>
                    <Form.Reset theme="outline" onClick={close}>
                        Не сохранять
                    </Form.Reset>
                    <Button type="submit" disabled={!combinedOptions.length}>
                        Сохранить
                    </Button>
                </Popup.Footer>
            </Form>
        </Popup>
    );
};

export default MassEditPopup;
