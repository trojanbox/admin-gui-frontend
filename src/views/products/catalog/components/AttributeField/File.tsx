import { Property } from '@api/catalog';

import Dropzone from '@components/controls/Dropzone';
import MultiSelect from '@components/controls/MultiSelect';
import Select from '@components/controls/Select';
import Form from '@components/controls/future/Form';

import { FileSizes } from '@scripts/constants';
import { scale } from '@scripts/gds';

import { useCatalogAccess } from '../../scripts/hooks';
import { ArrayAddButton } from '../ArrayAddButton';
import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import { directoryItemsType } from './types';

export interface FileFieldProps {
    fieldName: string;
    property: Property;
    directoryItems: directoryItemsType;
}

export const FileField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryItems,
}: FileFieldProps) => {
    const { canEditAllProductData, canEditDataPropertiesTab } = useCatalogAccess();

    return (
        <>
            {!has_directory && !is_multiple ? (
                <>
                    <LegendAndErrorOfBlock
                        name={`${name}`}
                        isRequired={Boolean(is_required)}
                        propertyId={property_id}
                        css={{ marginBottom: scale(1) }}
                    />
                    <Form.Field name={fieldName}>
                        <Dropzone
                            maxSize={FileSizes.MB1}
                            maxFiles={1}
                            multiple={false}
                            disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                        />
                    </Form.Field>
                </>
            ) : null}
            {!has_directory && is_multiple ? (
                <>
                    <LegendAndErrorOfBlock
                        name={`${name}`}
                        isRequired={Boolean(is_required)}
                        propertyId={property_id}
                        css={{ marginBottom: scale(1) }}
                    />
                    <Form.FieldArray name={fieldName} AddButton={ArrayAddButton} initialValue={['']}>
                        {({ name }) => (
                            <Form.Field name={name}>
                                <Dropzone
                                    maxSize={FileSizes.MB1}
                                    maxFiles={1}
                                    multiple={false}
                                    disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                                />
                            </Form.Field>
                        )}
                    </Form.FieldArray>
                </>
            ) : null}
            {has_directory && !is_multiple ? (
                <Form.Field name={fieldName} label={`${name} ${is_required ? '* ' : ''}`}>
                    <Select
                        items={directoryItems}
                        simple
                        disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                    />
                </Form.Field>
            ) : null}
            {has_directory && is_multiple ? (
                <Form.Field name={fieldName}>
                    <MultiSelect
                        items={directoryItems}
                        label={`${name} ${is_required ? '* ' : ''}`}
                        disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                    />
                </Form.Field>
            ) : null}
        </>
    );
};
