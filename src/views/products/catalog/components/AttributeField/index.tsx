import { useMemo } from 'react';

import { Property } from '@api/catalog';

import { ATTR_TYPES } from '@views/products/scripts';

import { useAttributeOptions } from '../../scripts/hooks';
import { BooleanField } from './Boolean';
import { ColorField } from './Color';
import { DateTimeField } from './DateTime';
import { FileField } from './File';
import { ImageField } from './Image';
import { NumberField } from './Number';
import { StringField } from './String';
import { TextField } from './Text';

/**
 * Компонент который отвечает за рендер одного атрибута по переданному property
 * @param param0
 * @returns
 */
const AttributeField = ({ property }: { property: Property }) => {
    const { getDirectoryItems } = useAttributeOptions({
        properties: [property],
    });
    const { type } = property;

    const commonProps = useMemo(
        () => ({
            property,
            fieldName: 'value',
            directoryItems: getDirectoryItems(property.id),
        }),
        [getDirectoryItems, property]
    );

    switch (type) {
        case ATTR_TYPES.STRING:
            return <StringField {...commonProps} />;
        case ATTR_TYPES.TEXT:
            return <TextField {...commonProps} />;

        case ATTR_TYPES.INTEGER:
        case ATTR_TYPES.DOUBLE:
            return <NumberField {...commonProps} />;

        case ATTR_TYPES.COLOR:
            return <ColorField {...commonProps} />;

        case ATTR_TYPES.IMAGE:
            return <ImageField {...commonProps} />;

        case ATTR_TYPES.FILE:
            return <FileField {...commonProps} />;

        case ATTR_TYPES.DATETIME:
            return <DateTimeField {...commonProps} />;

        case ATTR_TYPES.BOOLEAN:
            return <BooleanField {...commonProps} />;

        default:
            return null;
    }
};

export default AttributeField;
