import { ReactNode } from "react";

export type directoryItemsType = {
    value: number;
    dictionary_value: string | undefined;
    label: string | ReactNode | undefined;
    id: number;
    name?: string | undefined;
    code: string;
    property_id: number;
    url?: string | undefined;
    type?: string | undefined;
    preload_file_id?: string | undefined;
}[];
