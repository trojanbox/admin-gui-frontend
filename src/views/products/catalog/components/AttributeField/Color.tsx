import { Property } from '@api/catalog';

import MultiSelect from '@components/controls/MultiSelect';
import Select from '@components/controls/Select';
import Form from '@components/controls/future/Form';

import { Layout, scale } from '@scripts/gds';

import { useCatalogAccess } from '../../scripts/hooks';
import { ArrayAddButton } from '../ArrayAddButton';
import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import { directoryItemsType } from './types';

export interface ColorFieldProps {
    fieldName: string;
    property: Property;
    directoryItems: directoryItemsType;
}

export const ColorField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryItems,
}: ColorFieldProps) => {
    const { canEditAllProductData, canEditDataPropertiesTab } = useCatalogAccess();

    return (
        <>
            {!has_directory && !is_multiple ? (
                <Layout cols={3} gap="8px 16px">
                    <Layout.Item col={3}>
                        <LegendAndErrorOfBlock
                            name={`${name}`}
                            isRequired={Boolean(is_required)}
                            propertyId={property_id}
                        />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field
                            label="Название цвета"
                            name={`${fieldName}[0].name`}
                            disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                        />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field
                            label="Код цвета"
                            name={`${fieldName}[0].value`}
                            disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                        />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field
                            name={`${fieldName}[0].value`}
                            label="Палитра"
                            type="color"
                            disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                        />
                    </Layout.Item>
                </Layout>
            ) : null}

            {!has_directory && is_multiple ? (
                <>
                    <LegendAndErrorOfBlock
                        name={`${name}`}
                        isRequired={Boolean(is_required)}
                        propertyId={property_id}
                        css={{ marginBottom: scale(1) }}
                    />
                    <Form.FieldArray name={fieldName} initialValue={{ name: '', value: '' }} AddButton={ArrayAddButton}>
                        {({ name: ithName }) => (
                            <Layout
                                cols={4}
                                css={{
                                    gridTemplateColumns: '1fr 1fr 1fr auto !important',
                                    alignItems: 'end',
                                    marginTop: scale(2),
                                }}
                            >
                                <Layout.Item>
                                    <Form.Field
                                        label="Название цвета"
                                        name={`${ithName}.name`}
                                        disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                                    />
                                </Layout.Item>
                                <Layout.Item>
                                    <Form.Field
                                        label="Код цвета"
                                        name={`${ithName}.value`}
                                        disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                                    />
                                </Layout.Item>
                                <Layout.Item>
                                    <Form.Field
                                        name={`${ithName}.value`}
                                        label="Палитра"
                                        type="color"
                                        disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                                    />
                                </Layout.Item>
                            </Layout>
                        )}
                    </Form.FieldArray>
                </>
            ) : null}

            {has_directory && !is_multiple ? (
                <Form.Field name={fieldName} label={`${name} ${is_required ? '* ' : ''}`}>
                    <Select
                        items={directoryItems}
                        simple
                        disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                    />
                </Form.Field>
            ) : null}

            {has_directory && is_multiple ? (
                <Form.Field name={fieldName}>
                    <MultiSelect
                        items={directoryItems}
                        label={`${name} ${is_required ? '* ' : ''}`}
                        disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                    />
                </Form.Field>
            ) : null}
        </>
    );
};
