import { Property } from '@api/catalog';

import Checkbox from '@components/controls/Checkbox';
import Form from '@components/controls/future/Form';

import { useCatalogAccess } from '../../scripts/hooks';
import { directoryItemsType } from './types';

export interface BooleanFieldProps {
    fieldName: string;
    property: Property;
    directoryItems?: directoryItemsType;
}

export const BooleanField = ({
    fieldName,
    property: { is_required, name },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    directoryItems,
}: BooleanFieldProps) => {
    const { canEditAllProductData, canEditDataPropertiesTab } = useCatalogAccess();

    return (
        <>
            <Form.Field name={fieldName} disabled={!canEditAllProductData && !canEditDataPropertiesTab}>
                <Checkbox>{`${name} ${is_required ? '* ' : ''}`}</Checkbox>
            </Form.Field>
        </>
    );
};
