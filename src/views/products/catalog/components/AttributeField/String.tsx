import { Property } from '@api/catalog';

import MultiSelect from '@components/controls/MultiSelect';
import Select from '@components/controls/Select';
import Form from '@components/controls/future/Form';

import { scale } from '@scripts/gds';

import { useCatalogAccess } from '../../scripts/hooks';
import { ArrayAddButton } from '../ArrayAddButton';
import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import { directoryItemsType } from './types';

export interface StringFieldProps {
    fieldName: string;
    property: Property;
    directoryItems: directoryItemsType;
}

export const StringField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryItems,
}: StringFieldProps) => {
    const { canEditAllProductData, canEditDataPropertiesTab } = useCatalogAccess();

    return (
        <>
            {!has_directory && !is_multiple ? (
                <Form.Field
                    name={fieldName}
                    label={`${name} ${is_required ? '* ' : ''}`}
                    maxLength={50}
                    disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                />
            ) : null}

            {!has_directory && is_multiple ? (
                <>
                    <LegendAndErrorOfBlock
                        name={`${name}`}
                        isRequired={Boolean(is_required)}
                        propertyId={property_id}
                        css={{ marginBottom: scale(1) }}
                    />
                    <Form.FieldArray name={fieldName} AddButton={ArrayAddButton}>
                        {({ name: ithName }) => (
                            <Form.Field
                                name={ithName}
                                maxLength={50}
                                disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                            />
                        )}
                    </Form.FieldArray>
                </>
            ) : null}

            {has_directory && !is_multiple ? (
                <Form.Field
                    name={fieldName}
                    label={`${name} ${is_required ? '* ' : ''}`}
                    disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                >
                    <Select items={directoryItems} />
                </Form.Field>
            ) : null}

            {has_directory && is_multiple ? (
                <Form.Field name={fieldName}>
                    <MultiSelect
                        items={directoryItems}
                        label={`${name} ${is_required ? '* ' : ''}`}
                        disabled={!canEditAllProductData && !canEditDataPropertiesTab}
                    />
                </Form.Field>
            ) : null}
        </>
    );
};
