import { Property } from '@api/catalog';

import CalendarInput from '@components/controls/CalendarInput';
import MultiSelect from '@components/controls/MultiSelect';
import Select from '@components/controls/Select';
import Form from '@components/controls/future/Form';
import Input from '@components/controls/future/Input';

import { scale } from '@scripts/gds';

import { ArrayAddButton } from '../ArrayAddButton';
import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import { directoryItemsType } from './types';

export interface DateTimeFieldProps {
    fieldName: string;
    property: Property;
    directoryItems: directoryItemsType;
}

export const DateTimeField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryItems,
}: DateTimeFieldProps) => (
    <>
        <LegendAndErrorOfBlock
            name={`${name}`}
            isRequired={Boolean(is_required)}
            propertyId={property_id}
            css={{ marginBottom: scale(1) }}
        />

        {!has_directory && !is_multiple ? (
            <div css={{ display: 'flex', alignItems: 'center' }}>
                <Form.Field name={`${fieldName}[0].date`} css={{ marginRight: scale(1) }} label="Дата">
                    <CalendarInput placeholder="Дата" />
                </Form.Field>
                <Form.Field name={`${fieldName}[0].time`} label="Время" placeholder="Время">
                    <Input type="time" />
                </Form.Field>
            </div>
        ) : null}

        {!has_directory && is_multiple ? (
            <Form.FieldArray name={fieldName} AddButton={ArrayAddButton}>
                {({ name }) => (
                    <div
                        css={{
                            display: 'flex',
                            gap: scale(2),
                            alignItems: 'center',
                        }}
                    >
                        <Form.Field name={`${name}.date`} css={{ marginRight: scale(1) }}>
                            <CalendarInput placeholder="Дата" label="Дата" />
                        </Form.Field>
                        <Form.Field name={`${name}.time`} label="Время" placeholder="Время">
                            <Input />
                        </Form.Field>
                    </div>
                )}
            </Form.FieldArray>
        ) : null}

        {has_directory && !is_multiple ? (
            <Form.Field name={fieldName} label={`${name} ${is_required ? '* ' : ''}`}>
                <Select items={directoryItems} />
            </Form.Field>
        ) : null}

        {has_directory && is_multiple ? (
            <Form.Field name={fieldName}>
                <MultiSelect items={directoryItems} label={`${name} ${is_required ? '* ' : ''}`} />
            </Form.Field>
        ) : null}
    </>
);
