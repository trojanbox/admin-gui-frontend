import type { FieldArrayAddProps } from '@components/controls/future/Form/FieldArray';

import { Button } from '@scripts/gds';

import IconPlus from '@icons/plus.svg';

export const ArrayAddButton = (props: FieldArrayAddProps) => (
    <Button Icon={IconPlus} {...props}>
        Добавить значение атрибута
    </Button>
);
