import Checkbox from '@components/controls/Checkbox';
import Textarea from '@components/controls/Textarea';
import Form from '@components/controls/future/Form';

import { MAIN_DATA_FIELD_NAMES, MassEditableProductField as MainDataFieldType } from '../[id]/scripts/settings';
import ProductFields from './ProductFields';

export const MainDataField = ({ fieldName }: { fieldName: MainDataFieldType }) => {
    switch (fieldName) {
        case 'type':
            return <ProductFields.Type name="value" isRequired={false} />;
        case 'category_id':
            return <ProductFields.Category name="value" isRequired={false} />;
        case 'brand_id':
            return <ProductFields.Brand name="value" />;
        case 'base_price':
            return <ProductFields.BasePrice name="value" isRequired={false} />;
        case 'nameplates':
            return <ProductFields.Nameplates name="value" isRequired={false} />;
        case 'description':
            return (
                <Form.Field name="value" label={MAIN_DATA_FIELD_NAMES[fieldName]}>
                    <Textarea />
                </Form.Field>
            );
        case 'allow_publish':
        case 'is_adult':
            return (
                <Form.Field name="value" label={MAIN_DATA_FIELD_NAMES[fieldName]}>
                    <Checkbox>Да</Checkbox>
                </Form.Field>
            );
        default:
            return <Form.Field name="value" label={MAIN_DATA_FIELD_NAMES[fieldName]} />;
    }
};
