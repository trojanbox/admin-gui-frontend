import { useFormContext } from 'react-hook-form';

import Legend from '@components/controls/Legend';
import Form from '@components/controls/future/Form';

import { scale } from '@scripts/gds';

export const LegendAndErrorOfBlock = ({
    name,
    isRequired,
    propertyId,
    className,
}: {
    name: string;
    isRequired: boolean;
    propertyId: string | number;
    className?: string;
}) => {
    const {
        formState: { errors },
    } = useFormContext();
    return (
        <>
            <Legend label={`${name} ${isRequired ? '* ' : ''}`} css={{ paddingBottom: 0 }} className={className} />

            {(errors?.attributes as Record<any, any>)?.[propertyId] &&
            !Array.isArray((errors?.attributes as Record<any, any>)?.[propertyId]) ? (
                <Form.Message
                    message={(errors?.attributes as Record<any, any>)[propertyId]?.message}
                    css={{ marginTop: scale(1, true) }}
                />
            ) : null}
        </>
    );
};
