import { useFormContext } from 'react-hook-form';

import Form from '@components/controls/future/Form';

import { useCatalogAccess } from '../../scripts/hooks';

export const BasePriceField = ({ name = 'base_price', isRequired = true }: { name?: string; isRequired?: boolean }) => {
    const { canEditAllProductData, canEditDataMainTab, canEditOnlyPriceMainTab } = useCatalogAccess();
    const { getValues, setValue } = useFormContext();

    return (
        <Form.TypedField
            disabled={!canEditAllProductData && !canEditDataMainTab && !canEditOnlyPriceMainTab}
            name={name}
            label={`Стоимость${isRequired ? '*' : ''}`}
            fieldType="positiveFloat"
            onBlur={() => {
                const currentValue = getValues(name);
                if (currentValue) setValue(name, (+getValues(name)).toFixed(2));
            }}
        />
    );
};
