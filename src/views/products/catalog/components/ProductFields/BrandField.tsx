import { useMemo } from 'react';

import { useBrands } from '@api/catalog';

import { useError } from '@context/modal';

import Form from '@components/controls/future/Form';
import Select from '@components/controls/future/Select';

export const BrandField = ({ name = 'brand_id' }: { name?: string }) => {
    const { data: apiDataBrands, error: apiErrorBrands } = useBrands({
        filter: {
            is_active: true,
        },
        pagination: {
            limit: -1,
            offset: 0,
            type: 'offset',
        },
    });
    useError(apiErrorBrands);

    const brandsOptions = useMemo(
        () =>
            apiDataBrands?.data
                ? [
                      { key: 'Нет', value: null },
                      ...apiDataBrands?.data.map(({ id, name }) => ({ key: name, value: id })),
                  ]
                : [],
        [apiDataBrands]
    );

    return (
        <Form.Field name={name} label="Бренд">
            <Select options={brandsOptions} />
        </Form.Field>
    );
};
