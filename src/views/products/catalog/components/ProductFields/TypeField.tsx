import { useMemo } from 'react';

import { useProductsTypes } from '@api/catalog';

import { useError } from '@context/modal';

import Form from '@controls/future/Form';
import Select from '@controls/future/Select';

import { useCatalogAccess } from '../../scripts/hooks';

export const TypeField = ({ name = 'type', isRequired = true }: { name?: string; isRequired?: boolean }) => {
    const { data: apiTypes, error: apiTypesError } = useProductsTypes();
    useError(apiTypesError);

    const productTypes = useMemo(
        () => (apiTypes ? apiTypes.data.map(el => ({ value: el.id, key: el.name })) : []),
        [apiTypes]
    );

    const { canEditAllProductData, canEditDataMainTab } = useCatalogAccess();

    return (
        <Form.Field name={name} label={`Тип товара${isRequired ? '*' : ''}`}>
            <Select
                disabled={!canEditAllProductData && !canEditDataMainTab}
                options={productTypes}
                hideClearButton
                optionsListWidth="field"
            />
        </Form.Field>
    );
};
