import Checkbox from '@components/controls/Checkbox';
import Form from '@components/controls/future/Form';

import { useCatalogAccess } from '../../scripts/hooks';

export const AdultField = ({ name = 'is_adult' }: { name?: string }) => {
    const { canEditAllProductData, canEditDataMainTab } = useCatalogAccess();

    return (
        <Form.Field name={name}>
            <Checkbox disabled={!canEditAllProductData && !canEditDataMainTab}>Товар 18+</Checkbox>
        </Form.Field>
    );
};
