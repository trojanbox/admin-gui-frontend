import { ReactNode } from 'react';

import { AdultField } from './AdultField';
import { BrandField } from './BrandField';
import { CategoryField } from './CategoryField';
import { TypeField } from './TypeField';
import { BasePriceField } from './BasePriceField';
import { NameplatesField } from './NameplatesField';

export type ProductFieldsProps = {
    Type: typeof TypeField;
    Category: typeof CategoryField;
    Brand: typeof BrandField;
    Adult: typeof AdultField;
    BasePrice: typeof BasePriceField;
    Nameplates: typeof NameplatesField;
    children?: ReactNode | ReactNode[];
};
