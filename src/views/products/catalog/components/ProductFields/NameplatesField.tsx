import { fetchSearchNameplates } from '@api/content/nameplates';

import AutocompleteAsync from '@controls/future/AutocompleteAsync';
import Form from '@controls/future/Form';

import { useCatalogAccess } from '../../scripts/hooks';

export const NameplatesField = ({
    name = 'nameplates',
    isRequired = false,
}: {
    name?: string;
    isRequired?: boolean;
}) => {
    const asyncSearchFn = async (query: string) => {
        const res = await fetchSearchNameplates({
            filter: {
                name_like: query,
                is_active: true,
            },
        });
        return {
            options: res.data.map(e => ({
                key: e.name,
                value: e.id,
            })),
            hasMore: false,
        };
    };

    const asyncOptionsByValuesFn = async (vals: string[]) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number((v as any).value);
            return Number(v);
        });
        const res = await fetchSearchNameplates({
            filter: { id: actualValues },
        });
        return res.data.map(e => ({
            key: e.name,
            value: e.id,
            disabled: !e.is_active,
        }));
    };

    const { canEditAllProductData, canEditDataMainTab } = useCatalogAccess();

    return (
        <Form.Field name={name} label={`Шильдики${isRequired ? '*' : ''}`}>
            <AutocompleteAsync
                moveInputToNewLine={false}
                multiple
                asyncOptionsByValuesFn={asyncOptionsByValuesFn}
                asyncSearchFn={asyncSearchFn}
                disabled={!canEditAllProductData && !canEditDataMainTab}
            />
        </Form.Field>
    );
};
