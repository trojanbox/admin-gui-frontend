import { useMemo } from 'react';

import { useCategoriesTree } from '@api/catalog';

import { useError } from '@context/modal';

import { customFlatWithParents } from '@views/products/scripts';

import Autocomplete from '@controls/future/Autocomplete';
import Form from '@controls/future/Form';

import { useCatalogAccess } from '../../scripts/hooks';

export const CategoryField = ({ name = 'category_id', isRequired = true }: { name?: string; isRequired?: boolean }) => {
    const { data: categoriesTree, error: errorCategoriesTree } = useCategoriesTree();
    useError(errorCategoriesTree);

    const categoriesOptions = useMemo(
        () =>
            customFlatWithParents(categoriesTree?.data).map(e => ({
                value: e.value,
                key: e.label,
                content: e.label,
            })),
        [categoriesTree?.data]
    );

    const { canEditAllProductData, canEditDataMainTab } = useCatalogAccess();

    return (
        <Form.Field name={name}>
            <Autocomplete
                disabled={!canEditAllProductData && !canEditDataMainTab}
                label={`Категория товара${isRequired ? '*' : ''}`}
                options={categoriesOptions}
                allowUnselect={false}
                visibleOptions={7}
                hideClearButton
                optionsListWidth="field"
            />
        </Form.Field>
    );
};
