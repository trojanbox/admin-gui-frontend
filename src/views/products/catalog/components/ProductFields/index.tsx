import { AdultField } from './AdultField';
import { BasePriceField } from './BasePriceField';
import { BrandField } from './BrandField';
import { CategoryField } from './CategoryField';
import { NameplatesField } from './NameplatesField';
import { TypeField } from './TypeField';
import { ProductFieldsProps } from './types';

const ProductFields = ({ children }: ProductFieldsProps) => <>{children}</>;

ProductFields.Type = TypeField;
ProductFields.Category = CategoryField;
ProductFields.Brand = BrandField;
ProductFields.Adult = AdultField;
ProductFields.BasePrice = BasePriceField;
ProductFields.Nameplates = NameplatesField;

export default ProductFields;
