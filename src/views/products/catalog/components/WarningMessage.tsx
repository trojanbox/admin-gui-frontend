import { useState } from 'react';

import { Product } from '@api/catalog';

import Modal from '@components/controls/Modal';

import { scale } from '@scripts/gds';
import { useLinkCSS } from '@scripts/hooks';

const MAX_TO_SHOW = 20;

const Link = ({ index, total, productId }: { index: number; total: number; productId: number | string }) => {
    const linkStyles = useLinkCSS();

    return (
        <>
            <a
                key={productId}
                href={`/products/catalog/${productId}`}
                css={linkStyles}
                target="_blank"
                rel="noreferrer"
            >
                {productId}
            </a>
            {index === total - 1 ? '' : ', '}
        </>
    );
};

export const WarningMessage = ({ warningProducts, text }: { text: string; warningProducts: Product[] }) => {
    const linkStyles = useLinkCSS();
    const [open, setOpen] = useState(false);
    const total = warningProducts.length;

    return (
        <Modal theme="warning" title="Внимание!" icon css={{ marginBottom: scale(2) }}>
            {text}
            {warningProducts.slice(0, MAX_TO_SHOW).map((product, index) => (
                <Link key={product.id} productId={product.id} index={index} total={total} />
            ))}
            {open &&
                warningProducts
                    .slice(MAX_TO_SHOW)
                    .map((product, index) => (
                        <Link productId={product.id} key={product.id} index={index + MAX_TO_SHOW} total={total} />
                    ))}
            {total > MAX_TO_SHOW && (
                <>
                    <br />
                    <button onClick={() => setOpen(!open)} type="button" css={linkStyles}>
                        {open ? 'Скрыть' : 'Показать все'}
                    </button>
                </>
            )}
        </Modal>
    );
};
