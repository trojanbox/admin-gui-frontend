import { FC } from 'react';

import Popup from '@components/controls/future/Popup';

import { Button, Layout, scale } from '@scripts/gds';

interface ConfirmPopupProps {
    isOpen: boolean;
    onClose: () => void;
    onSave: () => void;
    isSelected: boolean;
}
const ConfirmPopup: FC<ConfirmPopupProps> = ({ isOpen, onClose, onSave, isSelected }) => (
    <Popup open={isOpen} onClose={onClose} size="lg">
        <Popup.Header
            title={isSelected ? 'Внести изменения во все выбранные товары?' : 'Внести изменения во все товары?'}
        />
        <Popup.Footer css={{ border: 'none' }} type="flex" wrap={false} gap={scale(2)}>
            <Layout.Item>
                <Button onClick={onClose} theme="outline">
                    Нет
                </Button>
            </Layout.Item>
            <Layout.Item>
                <Button onClick={onSave}>Да</Button>
            </Layout.Item>
        </Popup.Footer>
    </Popup>
);

export default ConfirmPopup;
