import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

const useProductGroupsAccess = () => {
    const { data: userData } = useCurrentUser();

    const viewListingAndFilters = 1101;
    const viewDetailPage = 1102;
    const editProductGroup = 1103;
    const activityChange = 1104;
    const createProductGroup = 1105;
    const deleteProductGroup = 1106;

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canViewDetailPage = useMemo(
        () => !!userData?.data.rights_access.includes(viewDetailPage),
        [userData?.data.rights_access]
    );
    const canEditProductGroup = useMemo(
        () => !!userData?.data.rights_access.includes(editProductGroup),
        [userData?.data.rights_access]
    );
    const canChangeProductGroupActivity = useMemo(
        () => !!userData?.data.rights_access.includes(activityChange),
        [userData?.data.rights_access]
    );
    const canCreateProductGroup = useMemo(
        () => !!userData?.data.rights_access.includes(createProductGroup),
        [userData?.data.rights_access]
    );
    const canDeleteProductGroup = useMemo(
        () => !!userData?.data.rights_access.includes(deleteProductGroup),
        [userData?.data.rights_access]
    );

    return {
        canViewListingAndFilters,
        canViewDetailPage,
        canEditProductGroup,
        canChangeProductGroupActivity,
        canCreateProductGroup,
        canDeleteProductGroup,
    };
};

export default useProductGroupsAccess;
