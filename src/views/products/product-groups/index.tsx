import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { ProductGroupsData, useProductsGroups, useProductsGroupsMeta } from '@api/catalog';

import { useError } from '@context/modal';

import AutoFilters from '@components/AutoFilters';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Table, {
    ExtendedRow,
    TableEmpty,
    TableFooter,
    TableHeader,
    TooltipContentProps,
    getSettingsColumn,
} from '@components/Table';

import { CREATE_PARAM } from '@scripts/constants';
import { Button, scale, typography } from '@scripts/gds';
import { declOfNum, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';

import PlusIcon from '@icons/plus.svg';

import useProductGroupsAccess from './useProductGroupsAccess';

export default function ProductGroups() {
    const { pathname, push } = useRouter();

    const { canViewListingAndFilters, canCreateProductGroup, canViewDetailPage } = useProductGroupsAccess();

    const { data: metaData, error: metaError, isLoading } = useProductsGroupsMeta();

    const meta = useMemo(() => metaData?.data, [metaData]);

    const { metaField, values, filtersActive, URLHelper, searchRequestFilter, emptyInitialValues } =
        useAutoFilters(meta);

    const { sort, activePage, itemsPerPageCount, setItemsPerPageCount, setSort } = useTableList({
        defaultSort: meta?.default_sort,
    });

    const {
        data,
        isLoading: isLoadingData,
        isIdle,
        error,
    } = useProductsGroups(
        {
            sort: sort || meta?.default_sort,
            filter: searchRequestFilter,
            pagination: { type: 'offset', limit: itemsPerPageCount, offset: (activePage - 1) * itemsPerPageCount },
        },
        Boolean(meta)
    );

    const productGroup = useAutoTableData<ProductGroupsData>(data?.data, metaField);
    const autoGeneratedColumns = useAutoColumns(meta, canViewDetailPage);

    const columns = useMemo(
        () => [
            ...autoGeneratedColumns,
            getSettingsColumn({ columnsToDisable: [], defaultVisibleColumns: meta?.default_list }),
        ],
        [autoGeneratedColumns, meta?.default_list]
    );

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);
    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total });

    useError(error);
    useError(metaError);

    const renderHeader = useCallback(
        () => (
            <TableHeader>
                <p>
                    Найдено{' '}
                    {`${total} ${declOfNum(total, ['товарная склейка', 'товарных склейки', 'товарных склеек'])}`}
                </p>
                {canCreateProductGroup && (
                    <div css={{ display: 'flex', alignItems: 'center', marginLeft: 'auto' }}>
                        <Link href={`${pathname}/${CREATE_PARAM}`} passHref>
                            <Button as="a" Icon={PlusIcon}>
                                Создать товарную склейку
                            </Button>
                        </Link>
                    </div>
                )}
            </TableHeader>
        ),
        [total, pathname, canCreateProductGroup]
    );

    const goToDetailPage = useCallback(
        (originalRows: ExtendedRow['original'] | ExtendedRow['original'][] | undefined) => {
            if (!Array.isArray(originalRows)) push(`${pathname}/${originalRows?.id}`);
        },
        [pathname, push]
    );

    const tooltipContent: TooltipContentProps[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать склейку',
                action: goToDetailPage,
                onDisable: () => canViewDetailPage,
            },
        ],
        [goToDetailPage, canViewDetailPage]
    );

    return (
        <PageWrapper
            h1={canViewListingAndFilters ? 'Товарные склейки' : ''}
            isLoading={isLoading || isLoadingData || isIdle}
        >
            {canViewListingAndFilters ? (
                <>
                    <AutoFilters
                        initialValues={values}
                        emptyInitialValues={emptyInitialValues}
                        onSubmit={URLHelper}
                        filtersActive={filtersActive}
                        css={{ marginBottom: scale(2) }}
                        meta={meta}
                    />

                    <Block>
                        <Block.Body>
                            <Table
                                columns={columns}
                                data={productGroup}
                                onSortingChange={setSort}
                                renderHeader={renderHeader}
                                tooltipContent={tooltipContent}
                                allowRowSelect={false}
                            />

                            {productGroup?.length === 0 && !isLoading ? (
                                <TableEmpty
                                    filtersActive={filtersActive}
                                    titleWithFilters="Склейки не найдены"
                                    titleWithoutFilters="Склеек нет"
                                />
                            ) : (
                                <TableFooter
                                    pages={totalPages}
                                    itemsPerPageCount={itemsPerPageCount}
                                    setItemsPerPageCount={setItemsPerPageCount}
                                />
                            )}
                        </Block.Body>
                    </Block>
                </>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра товарных склеек
                </h1>
            )}
        </PageWrapper>
    );
}
