import { useFormikContext } from 'formik';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useState } from 'react';

import { ProductGroupsData, useProductsGroupsUpdateProducts } from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import Table, {
    Data,
    ExtendedRow,
    TableEmpty,
    TableHeader,
    TooltipContentProps,
    getSettingsColumn,
} from '@components/Table';
import Checkbox from '@components/controls/Checkbox';
import LoadWrapper from '@components/controls/LoadWrapper';

import { ModalMessages } from '@scripts/constants';
import { Button, typography } from '@scripts/gds';
import { declOfNum } from '@scripts/helpers';
import { useLinkCSS } from '@scripts/hooks';

import PlusIcon from '@icons/plus.svg';

import useProductGroupsAccess from '../useProductGroupsAccess';
import AddProductGroupItemsPopup from './AddProductGroupItemsPopup';

export const ProductGroupItems = ({
    isCreatePage,
    errorProducts,
}: {
    isCreatePage: boolean;
    errorProducts: number[];
}) => {
    const { query } = useRouter();
    const groupId: string = query?.pid?.toString() || '';

    const [isAddingTableOpen, setIsAddingTableOpen] = useState(false);

    const { canEditProductGroup } = useProductGroupsAccess();

    const {
        values: { main_product, products },
        setFieldValue,
    } =
        useFormikContext<{
            main_product: any;
            products: any[];
        }>();

    const convertProductGroupData = useMemo(
        () =>
            products
                ?.filter((p: ProductGroupsData) => !errorProducts.includes(p.id))
                ?.map((product: ProductGroupsData) => ({
                    id: product.id,
                    name: product.name,
                    main_product: !!(product.id === main_product?.id),
                })),
        [main_product, products, errorProducts]
    );

    const linkStyles = useLinkCSS();

    const pGUpdateProducts = useProductsGroupsUpdateProducts();

    useSuccess(pGUpdateProducts.status === 'success' ? ModalMessages.SUCCESS_SAVE : '');
    useError(pGUpdateProducts.error);

    useEffect(() => {
        if (main_product && isCreatePage) {
            setFieldValue('products', []);
        }
    }, [isCreatePage, main_product, setFieldValue]);

    const tableColumns = useMemo(
        () =>
            isCreatePage
                ? [
                      {
                          Header: 'Название товара',
                          accessor: 'name',
                          disableSortBy: true,
                          Cell: ({ row }) => (
                              <Link href={`/products/catalog/${row.original.id}`} passHref>
                                  <a css={linkStyles}>{row.original.name}</a>
                              </Link>
                          ),
                      },
                      getSettingsColumn(),
                  ]
                : [
                      {
                          Header: 'Главный продукт',
                          accessor: 'main_product',
                          Cell: ({ value, row }) => (
                              <Checkbox
                                  id={`${row.original.id}`}
                                  name={`main-product-${row.original.id}`}
                                  checked={value}
                                  onClick={() => {
                                      setFieldValue(
                                          'main_product',
                                          convertProductGroupData.filter(item => item.id === row.original.id)[0]
                                      );
                                  }}
                                  disabled={!canEditProductGroup}
                              />
                          ),
                          disableSortBy: true,
                      },
                      {
                          Header: 'Название товара',
                          accessor: 'name',
                          disableSortBy: true,
                          Cell: ({ row }) => (
                              <Link href={`/products/catalog/${row.original.id}`} passHref>
                                  <a css={linkStyles}>{row.original.name}</a>
                              </Link>
                          ),
                      },
                      getSettingsColumn(),
                  ],
        [isCreatePage, convertProductGroupData, setFieldValue, linkStyles, canEditProductGroup]
    );

    const renderHeader = useCallback(
        () => (
            <TableHeader css={{ paddingLeft: 0, paddingRight: 0 }}>
                <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%', padding: 0 }}>
                    <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                        {`${convertProductGroupData?.length} ${declOfNum(convertProductGroupData?.length, [
                            'товар',
                            'товара',
                            'товаров',
                        ])}`}
                    </p>

                    {canEditProductGroup && (
                        <Button
                            Icon={PlusIcon}
                            onClick={() => setIsAddingTableOpen(true)}
                            disabled={!main_product || isCreatePage}
                        >
                            Добавить товар
                        </Button>
                    )}
                </div>
            </TableHeader>
        ),
        [main_product, convertProductGroupData?.length, canEditProductGroup, isCreatePage]
    );

    const deleteProduct = useCallback(
        async (originalRows: ExtendedRow['original'] | ExtendedRow['original'][] | undefined) => {
            if (originalRows) {
                const id = Array.isArray(originalRows)
                    ? originalRows.map(r => Number(r.id))[0]
                    : Number(originalRows?.id);

                await pGUpdateProducts?.mutateAsync({
                    id: Number(groupId),
                    replace: true,
                    products: convertProductGroupData
                        .filter(item => item.id !== id)
                        .map((el: any) => ({
                            id: el.id,
                        })),
                });
            }
        },
        [groupId, pGUpdateProducts, convertProductGroupData]
    );

    const tooltipContent: TooltipContentProps[] = useMemo(
        () => [
            {
                type: 'delete',
                text: 'Удалить из склейки',
                action: deleteProduct,
                onDisable: (row: Data | Data[]) => {
                    if (!Array.isArray(row) && row.id) {
                        return row.id !== main_product?.id && canEditProductGroup;
                    }
                },
                onDisableHint: canEditProductGroup ? 'Главный товар удалить нельзя' : '',
            },
        ],
        [deleteProduct, main_product, canEditProductGroup]
    );

    return (
        <LoadWrapper isLoading={false}>
            <Table
                renderHeader={renderHeader}
                tooltipContent={tooltipContent}
                columns={tableColumns}
                data={convertProductGroupData}
            />

            {convertProductGroupData?.length === 0 ? (
                <TableEmpty
                    filtersActive={false}
                    titleWithFilters="Товары с данными фильтрами не найдены"
                    titleWithoutFilters="Товары отсутствуют"
                />
            ) : null}

            <AddProductGroupItemsPopup isOpen={isAddingTableOpen} onRequestClose={() => setIsAddingTableOpen(false)} />
        </LoadWrapper>
    );
};
