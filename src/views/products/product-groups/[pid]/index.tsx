import { FormikValues } from 'formik';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useState } from 'react';
import * as Yup from 'yup';

import { Product } from '@api/catalog';
import {
    useProductsGroupDetail,
    useProductsGroupsCreate,
    useProductsGroupsDelete,
    useProductsGroupsUpdate,
    useProductsGroupsUpdateProducts,
} from '@api/catalog/product-groups';
import { CommonResponse } from '@api/common/types';
import { apiClient } from '@api/index';

import { useError, useModalsContext, useSuccess } from '@context/modal';

import Switcher from '@controls/Switcher';

import Block from '@components/Block';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import { InfoListItemCommonType } from '@components/InfoList/types';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import { StatusSimplePopup } from '@components/StatusSimplePopup';
import Form from '@components/controls/Form';
import AutocompleteAsync from '@components/controls/future/AutocompleteAsync';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { useLinkCSS } from '@scripts/hooks';

import CheckIcon from '@icons/small/check.svg';

import useProductGroupsAccess from '../useProductGroupsAccess';
import AddProductGroupItemsPopup from './AddProductGroupItemsPopup';
import { ProductGroupItems } from './ProductGroupItems';

const SaleProductsList = ({ product }: { product: Product }) => {
    const linkStyles = useLinkCSS();
    return (
        <>
            <div css={{ display: 'flex', alignItems: 'end', gap: scale(2) }}>
                <div css={{ position: 'relative', width: scale(8), height: scale(8) }}>
                    <Image
                        src={product?.main_image_file || product?.main_image_url || '/noimage.png'}
                        unoptimized
                        layout="fill"
                        objectFit="contain"
                        objectPosition="bottom center"
                    />
                </div>

                <Link href={`/products/catalog/${product?.id}`} passHref>
                    <a css={linkStyles} target="_blank">
                        {product?.name}
                    </a>
                </Link>
            </div>
        </>
    );
};
export default function ProductGroup() {
    const { query, push, pathname, replace } = useRouter();
    const { appendModal } = useModalsContext();
    const groupId: string = query?.pid?.toString() || '';

    const isCreatePage = groupId === CREATE_PARAM;
    const listLink = pathname.split(`[pid]`)[0];

    const [isNeedRedirect, setIsNeedRedirect] = useState(false);
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [isSpecifyTableOpen, setIsSpecifyTableOpen] = useState(false);
    const [errorProducts, setErrorProducts] = useState<number[]>([]);

    const { canViewDetailPage, canEditProductGroup, canDeleteProductGroup, canChangeProductGroupActivity } =
        useProductGroupsAccess();

    const {
        data: productsGroup,
        isLoading: isDetailLoading,
        error: isDetailError,
    } = useProductsGroupDetail(Number(groupId), !isCreatePage && !!groupId);

    useError(isDetailError);

    const productsGroupData = useMemo(() => productsGroup?.data, [productsGroup]);

    const pageTitle = useMemo(
        () => (isCreatePage ? 'Создание склейки товаров' : `Склейка товаров: ${productsGroupData?.id}`),
        [isCreatePage, productsGroupData?.id]
    );

    const {
        mutateAsync: productGroupsCreate,
        isLoading: isProductGroupsCreationLoading,
        error: isProductGroupsCreationError,
        isSuccess: isProductGroupsCreationSuccess,
    } = useProductsGroupsCreate();

    useSuccess(isProductGroupsCreationSuccess ? ModalMessages.SUCCESS_SAVE : '');
    useError(isProductGroupsCreationError);

    const {
        mutateAsync: productGroupsUpdate,
        isLoading: isProductGroupsUpdateLoading,
        error: isProductGroupsUpdateError,
        isSuccess: isProductGroupsUpdateSuccess,
    } = useProductsGroupsUpdate();

    useSuccess(isProductGroupsUpdateSuccess ? ModalMessages.SUCCESS_UPDATE : '');
    useError(isProductGroupsUpdateError);

    const {
        mutateAsync: pGUpdateProducts,
        isLoading: isPGUpdateProductsLoading,
        error: isPGUpdateProductsError,
        isSuccess: isPGUpdateProductsSuccess,
    } = useProductsGroupsUpdateProducts();

    useSuccess(isPGUpdateProductsSuccess ? ModalMessages.SUCCESS_UPDATE : '');
    useError(isPGUpdateProductsError);

    const {
        mutateAsync: productGroupsDelete,
        isLoading: isProductGroupsDeleteLoading,
        error: isProductGroupsDeleteError,
        isSuccess: isProductGroupsDeleteSuccess,
    } = useProductsGroupsDelete();

    useSuccess(isProductGroupsDeleteSuccess ? ModalMessages.SUCCESS_DELETE : '');
    useError(isProductGroupsDeleteError);

    const asyncLoadProducts = async (value: string) => {
        const filter = {
            name: value || '',
            has_product_groups: false,
            category_has_is_gluing: true,
        };

        const res: CommonResponse<Product[]> = await apiClient.post('catalog/products:search', {
            data: { filter },
        });

        return {
            options: res?.data?.map(p => ({
                key: p.name,
                value: p,
            })),
            hasMore: false,
        };
    };

    const asyncOptionsByValuesFn = async (vals: Product[]) => {
        if (!vals?.[0]) return [];

        return [
            {
                key: vals[0].name,
                value: vals[0],
            },
        ];
    };

    const formInitValues: FormikValues = useMemo(
        () => ({
            name: productsGroupData?.name || '',
            main_product: productsGroupData?.main_product || null,
            products: productsGroupData?.products || [],
            is_active: productsGroupData?.is_active || false,
        }),
        [productsGroupData]
    );

    useEffect(() => {
        if (errorProducts.length) {
            appendModal({
                theme: 'error',
                title: `Не удалось добавить товары [${errorProducts.map(
                    id => id
                )}], т.к. у них не заполнен атрибут(ы) склеивания`,
            });
            setErrorProducts([]);
        }
    }, [errorProducts, appendModal]);

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isDetailLoading ||
                isProductGroupsCreationLoading ||
                isProductGroupsUpdateLoading ||
                isPGUpdateProductsLoading ||
                isProductGroupsDeleteLoading
            }
        >
            {canViewDetailPage ? (
                <Form
                    initialValues={formInitValues}
                    validationSchema={Yup.object().shape({
                        name: Yup.string().required(ErrorMessages.REQUIRED),
                        main_product: Yup.object().nullable().required(ErrorMessages.REQUIRED),
                    })}
                    enableReinitialize
                    onSubmit={async (values, { resetForm }) => {
                        if (isCreatePage) {
                            const {
                                data: { id: createdId },
                            } = await productGroupsCreate({
                                category_id: values.main_product.category_id,
                                main_product_id: values.main_product.id,
                                name: values.name,
                                is_active: values.is_active,
                            });

                            if (values?.products?.length > 0 && createdId) {
                                const products = values?.products?.map((el: any) => ({
                                    id: el.id,
                                }));

                                if (products.filter((item: any) => item.id === values.main_product.id).length === 0) {
                                    products.push({ id: values.main_product.id });
                                }

                                const { data } = await pGUpdateProducts({
                                    id: createdId,
                                    replace: true,
                                    products,
                                });
                                if (data?.product_errors?.length) setErrorProducts(data.product_errors);
                            }

                            resetForm();

                            setTimeout(() => {
                                if (isNeedRedirect) push({ pathname: listLink });
                                else replace(`${listLink}${createdId}`);
                            }, 0);
                        } else {
                            await productGroupsUpdate({
                                id: Number(groupId),
                                data: {
                                    category_id: values.main_product.category_id,
                                    main_product_id: values.main_product?.id || 0,
                                    name: values.name,
                                    is_active: values.is_active,
                                },
                            });

                            if (values?.products?.length > 0) {
                                const { data } = await pGUpdateProducts({
                                    id: Number(groupId),
                                    replace: isCreatePage,
                                    products: values?.products?.map((el: any) => ({
                                        id: el.id,
                                    })),
                                });
                                if (data?.product_errors?.length) setErrorProducts(data.product_errors);
                            }
                            resetForm();

                            if (isNeedRedirect) {
                                resetForm();
                                push({ pathname: listLink });
                            }
                        }
                    }}
                >
                    {({ values: { main_product }, dirty, resetForm }) => (
                        <>
                            <PageLeaveGuard isTrackParameters />
                            <PageTemplate
                                h1={pageTitle}
                                backlink={{ text: 'Назад' }}
                                controls={
                                    <>
                                        {!isCreatePage && canDeleteProductGroup && (
                                            <Button theme="dangerous" onClick={() => setIsDeleteOpen(true)}>
                                                Удалить
                                            </Button>
                                        )}
                                        <Button theme="secondary" onClick={() => push(listLink)}>
                                            Закрыть
                                        </Button>
                                        <Button theme="secondary" type="submit" disabled={!dirty}>
                                            Применить
                                        </Button>
                                        <Button
                                            type="submit"
                                            Icon={CheckIcon}
                                            iconAfter
                                            disabled={!dirty}
                                            onClick={() => setIsNeedRedirect(true)}
                                        >
                                            Сохранить
                                        </Button>
                                    </>
                                }
                                aside={
                                    <>
                                        <Form.FastField
                                            name="is_active"
                                            css={{ marginBottom: scale(3) }}
                                            disabled={!canChangeProductGroupActivity}
                                        >
                                            <Switcher>Активность</Switcher>
                                        </Form.FastField>
                                        <InfoList css={{ marginBottom: scale(5) }}>
                                            {(
                                                [
                                                    {
                                                        name: 'Кол-во характеристик:',
                                                        value: productsGroupData?.main_product?.attributes?.length,
                                                    },
                                                    {
                                                        name: 'Кол-во товаров:',
                                                        value: productsGroupData?.products?.length,
                                                    },
                                                    ...getDefaultDates({ ...productsGroupData }),
                                                ] as InfoListItemCommonType[]
                                            ).map(item => (
                                                <InfoList.Item {...item} key={item.name} />
                                            ))}
                                        </InfoList>
                                    </>
                                }
                                customChildren
                            >
                                <Block>
                                    <Block.Body>
                                        <Layout cols={2}>
                                            <Layout.Item>
                                                <Form.FastField
                                                    name="name"
                                                    label="Название"
                                                    disabled={!canEditProductGroup}
                                                />
                                            </Layout.Item>

                                            <Layout.Item> </Layout.Item>

                                            <Layout.Item
                                                css={{
                                                    display: isCreatePage ? 'flex' : 'none',
                                                    alignItems: 'end',
                                                    gap: scale(2),
                                                }}
                                            >
                                                <Form.Field
                                                    name="main_product"
                                                    label="Главный товар"
                                                    hint="Начните вводить название товара"
                                                    disabled={!canEditProductGroup}
                                                >
                                                    <AutocompleteAsync
                                                        asyncSearchFn={asyncLoadProducts}
                                                        asyncOptionsByValuesFn={asyncOptionsByValuesFn}
                                                    />
                                                </Form.Field>

                                                <Button onClick={() => setIsSpecifyTableOpen(true)}>Уточнить</Button>
                                            </Layout.Item>

                                            <Layout.Item
                                                css={{
                                                    display: isCreatePage ? 'flex' : 'none',
                                                }}
                                                align="end"
                                            >
                                                {main_product && <SaleProductsList product={main_product} />}
                                            </Layout.Item>

                                            <Layout.Item col={2}>
                                                <ProductGroupItems
                                                    isCreatePage={isCreatePage}
                                                    errorProducts={errorProducts}
                                                />
                                            </Layout.Item>
                                        </Layout>
                                    </Block.Body>
                                </Block>
                            </PageTemplate>

                            <StatusSimplePopup
                                isOpen={isDeleteOpen}
                                close={() => setIsDeleteOpen(false)}
                                title="Вы уверены, что хотите удалить товарную склейку?"
                                text=""
                                theme="delete"
                                footer={
                                    <>
                                        <Button onClick={() => setIsDeleteOpen(false)} theme="secondary">
                                            Не удалять
                                        </Button>
                                        <Button
                                            theme="dangerous"
                                            onClick={async () => {
                                                setIsDeleteOpen(false);
                                                await productGroupsDelete(Number(groupId));
                                                resetForm();
                                                push(listLink);
                                            }}
                                        >
                                            Удалить
                                        </Button>
                                    </>
                                }
                            />
                            <AddProductGroupItemsPopup
                                isOpen={isSpecifyTableOpen}
                                onRequestClose={() => setIsSpecifyTableOpen(false)}
                                maxRowSelect={1}
                                isMainProduct
                            />
                        </>
                    )}
                </Form>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра деталей о товарной склейке
                </h1>
            )}
        </PageWrapper>
    );
}
