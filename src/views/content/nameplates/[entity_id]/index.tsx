import { useRouter } from 'next/router';
import { useEffect, useMemo, useRef, useState } from 'react';
import { useFormContext } from 'react-hook-form';
import * as Yup from 'yup';

import { useCreateNameplate, useDeleteNameplate, useNameplate, useUpdateNameplate } from '@api/content/nameplates';

import { useError, useSuccess } from '@context/modal';

import Form from '@controls/future/Form';

import CopyButton from '@components/CopyButton';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import LoadingSkeleton from '@components/controls/LoadingSkeleton';
import Switcher from '@components/controls/Switcher';
import Input from '@components/controls/future/Input';
import Popup from '@components/controls/future/Popup';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, colors, scale } from '@scripts/gds';

import CheckIcon from '@icons/small/check.svg';

import { AddProductsPopup } from './AddProductsPopup';
import BindedProducts from './BindedProducts';

const Controls = ({
    isCreationPage,
    onSave,
    onApply,
    onDelete,
}: {
    isCreationPage: boolean;
    onDelete: () => void;
    onSave: () => void;
    onApply: () => void;
}) => {
    const {
        formState: { isDirty },
    } = useFormContext();
    const { push, pathname } = useRouter();
    const listLink = pathname.split(`[entity_id]`)[0];

    return (
        <>
            {!isCreationPage && (
                <Button theme="dangerous" onClick={onDelete}>
                    Удалить
                </Button>
            )}
            <Button
                theme="secondary"
                onClick={() =>
                    push({
                        pathname: listLink,
                    })
                }
            >
                Закрыть
            </Button>
            <Button theme="secondary" type="submit" disabled={!isDirty} onClick={onApply}>
                Применить
            </Button>
            <Button Icon={CheckIcon} iconAfter type="submit" disabled={!isDirty} onClick={onSave}>
                Сохранить
            </Button>
        </>
    );
};

const NameplateInner = ({ isLoading }: { isLoading: boolean }) => {
    const [deferredIsLoading, setDeferredIsLoading] = useState(isLoading);

    useEffect(() => {
        if (!isLoading) {
            setTimeout(() => {
                setDeferredIsLoading(false);
            }, 150);
        }
    }, [isLoading]);

    if (deferredIsLoading) {
        return (
            <LoadingSkeleton
                count={5}
                css={{
                    marginBottom: scale(2),
                }}
                height={scale(6)}
            />
        );
    }

    return (
        <>
            <Layout cols={8} gap={scale(2)} css={{ marginBottom: scale(2) }}>
                <Layout.Item col={8}>
                    <Form.Field name="name" label="Название*" />
                </Layout.Item>
                <Layout.Item col={8}>
                    <Form.Field name="code" label="Код*" />
                </Layout.Item>
                <Layout.Item col={8}>
                    <Form.Field name="backgroundColor" label="Цвет фона*">
                        <Input type="color" />
                    </Form.Field>
                </Layout.Item>
                <Layout.Item col={8}>
                    <Form.Field name="textColor" label="Цвет текста*">
                        <Input type="color" />
                    </Form.Field>
                </Layout.Item>
                {/* Пока что не требуется */}
                {/* <Layout.Item col={6}>
                    <Form.Field name="productIds" label="Товары">
                        <AutocompleteAsync
                            multiple
                            asyncOptionsByValuesFn={async () => []}
                            asyncSearchFn={async () => ({
                                hasMore: false,
                                options: [],
                            })}
                        />
                    </Form.Field>
                </Layout.Item> */}
                {/* <Layout.Item col={2} align="end">
                    <Button type="button" onClick={() => setAddingProducts(true)}>
                        Добавить товары-склейки
                    </Button>
                </Layout.Item> */}
            </Layout>
        </>
    );
};

const ContentNameplate = () => {
    const {
        query: { entity_id },
        push,
    } = useRouter();

    const parsedId = +`${entity_id}`;
    const isCreation = entity_id === CREATE_PARAM;

    const { data: apiData, error, isLoading } = useNameplate(parsedId);
    useError(error);
    const nameplate = apiData?.data;

    const updateNameplate = useUpdateNameplate();
    const deleteNameplate = useDeleteNameplate();
    const createNameplate = useCreateNameplate();

    useSuccess(updateNameplate.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useSuccess(deleteNameplate.isSuccess && ModalMessages.SUCCESS_DELETE);
    useSuccess(createNameplate.isSuccess && ModalMessages.SUCCESS_CREATE);

    const initialValues = useMemo(
        () =>
            isCreation
                ? {
                      name: '',
                      code: '',
                      backgroundColor: '',
                      textColor: '',
                      active: false,
                  }
                : {
                      name: nameplate?.name || '',
                      code: nameplate?.code || '',
                      backgroundColor: nameplate?.background_color || '',
                      textColor: nameplate?.text_color || '',
                      active: nameplate?.is_active || false,
                  },
        [nameplate, isCreation]
    );

    const [idDeleteOpen, setIsDeleteOpen] = useState(false);
    const pageTitle = isCreation ? 'Создать шильдик' : `Редактировать шильдик ${parsedId}`;

    const shouldReturnBack = useRef(false);
    const [isAddingProducts, setAddingProducts] = useState(false);

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={isLoading || updateNameplate.isLoading || createNameplate.isLoading || deleteNameplate.isLoading}
        >
            <Form
                onSubmit={async (values, { reset }) => {
                    if (isCreation) {
                        const result = await createNameplate.mutateAsync({
                            background_color: values.backgroundColor!,
                            code: values.code!,
                            is_active: values.active!,
                            name: values.name!,
                            text_color: values.textColor!,
                        });

                        push(`/content/nameplates/${result.data.id}`);
                    } else {
                        await updateNameplate.mutateAsync({
                            id: parsedId,
                            background_color: values.backgroundColor!,
                            code: values.code!,
                            is_active: values.active!,
                            name: values.name!,
                            text_color: values.textColor!,
                        });

                        if (shouldReturnBack.current) {
                            reset(values);
                            setTimeout(() => push(`/content/nameplates`), 0);
                        }
                    }
                }}
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    code: Yup.string().required(ErrorMessages.REQUIRED),
                    backgroundColor: Yup.string().min(1, ErrorMessages.REQUIRED),
                    textColor: Yup.string().min(1, ErrorMessages.REQUIRED),
                    // productIds: Yup.array().of(Yup.mixed()).min(1, ErrorMessages.MIN_ITEMS(1)),
                })}
                enableReinitialize
            >
                <PageLeaveGuard />
                <PageTemplate
                    h1={pageTitle}
                    controls={
                        <Controls
                            onDelete={() => setIsDeleteOpen(true)}
                            isCreationPage={isCreation}
                            onApply={() => {
                                shouldReturnBack.current = false;
                            }}
                            onSave={() => {
                                shouldReturnBack.current = true;
                            }}
                        />
                    }
                    backlink={{ text: 'К списку шильдиков', href: '/content/nameplates' }}
                    aside={
                        <>
                            <Form.Field name="active" label="Активность">
                                <Switcher>Активен</Switcher>
                            </Form.Field>

                            <div css={{ marginBottom: scale(1), marginTop: scale(3) }}>
                                <span css={{ marginRight: scale(1, true), color: colors?.grey800 }}>ID:</span>
                                <CopyButton>{`${entity_id}`}</CopyButton>
                            </div>

                            <InfoList css={{ marginBottom: scale(5) }}>
                                {getDefaultDates({ ...nameplate }).map(item => (
                                    <InfoList.Item {...item} key={item.name} />
                                ))}
                            </InfoList>
                        </>
                    }
                >
                    <NameplateInner isLoading={isLoading} />
                </PageTemplate>
            </Form>
            {!!parsedId && (
                <BindedProducts
                    nameplateId={parsedId}
                    css={{ marginTop: scale(2) }}
                    onAdd={() => setAddingProducts(true)}
                />
            )}
            <AddProductsPopup
                nameplateId={parsedId}
                isOpen={isAddingProducts}
                onClose={() => setAddingProducts(false)}
            />
            <Popup open={idDeleteOpen} onClose={() => setIsDeleteOpen(false)} size="sm">
                <Popup.Header title="Вы уверены, что хотите удалить шильдик?" />
                <Popup.Footer>
                    <Button
                        type="button"
                        onClick={async () => {
                            await deleteNameplate.mutateAsync({
                                id: parsedId,
                            });

                            setIsDeleteOpen(false);

                            push(`/content/nameplates`);
                        }}
                    >
                        Удалить
                    </Button>
                    <Button theme="secondary" type="button" onClick={() => setIsDeleteOpen(false)}>
                        Не удалять
                    </Button>
                </Popup.Footer>
            </Popup>
        </PageWrapper>
    );
};

export default ContentNameplate;
