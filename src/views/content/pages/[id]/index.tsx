import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';
import * as Yup from 'yup';

import { useCreatePage, usePageDelete, usePageDetail, usePageUpdate } from '@api/content';
import { PageMutate } from '@api/content/types/pages';

import { useError, useSuccess } from '@context/modal';

import CalendarInput from '@controls/CalendarInput';
import Form from '@controls/Form';
import Switcher from '@controls/Switcher';

import CopyButton from '@components/CopyButton';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import { StatusSimplePopup } from '@components/StatusSimplePopup';
import TextEditor from '@components/TextEditor';
import Input from '@components/controls/future/Input';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, colors, scale, typography } from '@scripts/gds';
import { toISOStringWithoutZone } from '@scripts/helpers';

import CheckIcon from '@icons/small/check.svg';

import useContentPagesAccess from '../useContentPagesAccess';

const PageDetail = () => {
    const { push, pathname, query, replace } = useRouter();
    const id = Array.isArray(query?.id) ? query.id[0] : query.id;
    const isCreationPage = id === CREATE_PARAM;
    const listLink = pathname.split(`[id]`)[0];

    const { canViewDetailPage, canEditContentPage, canEditContentPageActive, canDeleteContentPage } =
        useContentPagesAccess();

    const [isRemoveOpen, setIsRemoveOpen] = useState(false);

    const [isNeedRedirect, setIsNeedRedirect] = useState(false);

    const { data, isLoading, error } = usePageDetail(!isCreationPage ? id : undefined);
    const page = useMemo(() => data?.data, [data?.data]);

    const createPage = useCreatePage();
    const updatePage = usePageUpdate();
    const deletePage = usePageDelete();

    useError(error);
    useError(createPage.error);
    useError(updatePage.error);
    useError(deletePage.error);

    useSuccess(createPage.status === 'success' ? ModalMessages.SUCCESS_SAVE : '');
    useSuccess(updatePage.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(deletePage.status === 'success' ? ModalMessages.SUCCESS_DELETE : '');

    const initialValues = useMemo<PageMutate>(
        () => ({
            name: page?.name || '',
            is_active: typeof page?.is_active === 'boolean' ? page.is_active : true,
            slug: page?.slug || '',
            active_from: page?.active_from ? new Date(page.active_from) : '',
            active_to: page?.active_to ? new Date(page.active_to) : '',
            content: page?.content || '',
        }),
        [page]
    );

    return (
        <PageWrapper isLoading={isLoading || createPage.isLoading || updatePage.isLoading || deletePage.isLoading}>
            {canViewDetailPage ? (
                <>
                    <Form
                        initialValues={initialValues}
                        onSubmit={async (values, { resetForm }) => {
                            const newPageData = { ...values };
                            if (!newPageData.active_from) delete newPageData.active_from;
                            else newPageData.active_from = toISOStringWithoutZone(newPageData.active_from);
                            if (!newPageData.active_to) delete newPageData.active_to;
                            else newPageData.active_to = toISOStringWithoutZone(newPageData.active_to);

                            if (isCreationPage) {
                                const {
                                    data: { id: createdId },
                                } = await createPage.mutateAsync(newPageData);

                                resetForm();

                                if (isNeedRedirect) push({ pathname: listLink });
                                else replace(`${listLink}${createdId}`);
                            } else {
                                if (!id) return;
                                const updatedPageData = Object.fromEntries(
                                    Object.entries(newPageData).filter(([key, value]) =>
                                        key === 'active_from' || key === 'active_to'
                                            ? toISOStringWithoutZone(initialValues[key]) !== value
                                            : initialValues[key as keyof PageMutate] !== value
                                    )
                                );
                                await updatePage.mutateAsync({ ...updatedPageData, id: +id });

                                resetForm();

                                if (isNeedRedirect) push({ pathname: listLink });
                            }
                        }}
                        validationSchema={Yup.object().shape({
                            name: Yup.string().required(ErrorMessages.REQUIRED),
                            is_active: Yup.boolean().required(ErrorMessages.REQUIRED),
                            slug: Yup.string().required(ErrorMessages.REQUIRED),
                            active_from: Yup.string(),
                            active_to: Yup.string(),
                            content: Yup.string().required(ErrorMessages.REQUIRED),
                        })}
                        enableReinitialize
                    >
                        {({ dirty }) => (
                            <>
                                <PageLeaveGuard />
                                <PageTemplate
                                    h1={isCreationPage ? 'Новая страница' : page?.name}
                                    backlink={{ text: 'Назад' }}
                                    controls={
                                        <>
                                            {!isCreationPage && (
                                                <Button
                                                    theme="dangerous"
                                                    onClick={() => setIsRemoveOpen(true)}
                                                    disabled={!canDeleteContentPage}
                                                >
                                                    Удалить
                                                </Button>
                                            )}
                                            <Button
                                                theme="secondary"
                                                onClick={() =>
                                                    push({
                                                        pathname: listLink,
                                                    })
                                                }
                                            >
                                                Закрыть
                                            </Button>
                                            <Button theme="secondary" type="submit" disabled={!dirty}>
                                                Применить
                                            </Button>
                                            <Button
                                                type="submit"
                                                Icon={CheckIcon}
                                                iconAfter
                                                disabled={!dirty}
                                                onClick={() => setIsNeedRedirect(true)}
                                            >
                                                Сохранить
                                            </Button>
                                        </>
                                    }
                                    aside={
                                        !isCreationPage && (
                                            <>
                                                <Form.FastField
                                                    name="is_active"
                                                    disabled={!canEditContentPageActive}
                                                    css={{ marginBottom: scale(3) }}
                                                >
                                                    <Switcher>Активность</Switcher>
                                                </Form.FastField>

                                                <div css={{ marginBottom: scale(1) }}>
                                                    <span css={{ marginRight: scale(1, true), color: colors?.grey800 }}>
                                                        ID:
                                                    </span>
                                                    <CopyButton>{`${id}`}</CopyButton>
                                                </div>

                                                <InfoList css={{ marginBottom: scale(5) }}>
                                                    {getDefaultDates({ ...page }).map(item => (
                                                        <InfoList.Item {...item} key={item.name} />
                                                    ))}
                                                </InfoList>
                                            </>
                                        )
                                    }
                                >
                                    <Layout cols={2} gap={scale(3)} align="end">
                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <Form.FastField
                                                name="name"
                                                label="Наименование"
                                                disabled={!canEditContentPage}
                                            >
                                                <Input />
                                            </Form.FastField>
                                        </Layout.Item>
                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <Form.FastField name="slug" label="Ссылка" disabled={!canEditContentPage} />
                                        </Layout.Item>
                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <Form.Field name="active_from" disabled={!canEditContentPage}>
                                                <CalendarInput label="Начало публикации" />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <Form.Field name="active_to" disabled={!canEditContentPage}>
                                                <CalendarInput label="Окончание публикации" />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.FastField
                                                label="Контент"
                                                name="content"
                                                disabled={!canEditContentPage}
                                                css={{ marginBottom: scale(2) }}
                                            >
                                                <TextEditor />
                                            </Form.FastField>
                                        </Layout.Item>
                                    </Layout>
                                </PageTemplate>
                            </>
                        )}
                    </Form>
                    <StatusSimplePopup
                        close={() => setIsRemoveOpen(false)}
                        isOpen={isRemoveOpen}
                        title={`Вы уверены, что хотите удалить страницу ${page?.name}?`}
                        text=""
                        theme="delete"
                        footer={
                            <>
                                <Button
                                    theme="secondary"
                                    onClick={() => {
                                        setIsRemoveOpen(false);
                                    }}
                                >
                                    Нет
                                </Button>
                                <Button
                                    theme="dangerous"
                                    onClick={async () => {
                                        setIsRemoveOpen(false);
                                        if (!id) return;
                                        await deletePage.mutateAsync(+id);
                                        push({ pathname: listLink });
                                    }}
                                >
                                    Да
                                </Button>
                            </>
                        }
                    />
                </>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра страницы
                </h1>
            )}
        </PageWrapper>
    );
};

export default PageDetail;
