import { FormikValues } from 'formik';
import * as Yup from 'yup';

import Form from '@components/controls/Form';

import { ErrorMessages } from '@scripts/constants';
import { Button, Layout, scale } from '@scripts/gds';

const CommonSettings = ({
    initialValues,
    onSubmit,
}: {
    initialValues: FormikValues;
    onSubmit: (values: FormikValues) => void;
}) => (
    <Form
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={Yup.object().shape({
            rtg: Yup.number()
                .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                .integer(ErrorMessages.INTEGER)
                .required(ErrorMessages.REQUIRED),
            ct: Yup.number()
                .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                .integer(ErrorMessages.INTEGER)
                .required(ErrorMessages.REQUIRED),
            ppt: Yup.number()
                .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                .integer(ErrorMessages.INTEGER)
                .required(ErrorMessages.REQUIRED),
        })}
        enableReinitialize
    >
        {({ dirty }) => (
            <Layout
                cols={{
                    xxxl: 3,
                    sm: 1,
                }}
            >
                <Layout.Item
                    col={{
                        xxxl: 2,
                        sm: 1,
                    }}
                >
                    <Form.FastField
                        label="RTG (мин)"
                        hint="Ready-To-Go time - среднее время подтверждения заказа"
                        name="rtg"
                        type="number"
                        min={0}
                    />
                </Layout.Item>
                <Layout.Item
                    col={{
                        xxxl: 2,
                        sm: 1,
                    }}
                >
                    <Form.FastField
                        label="CT (мин)"
                        hint="Confirmation Time - среднее время начала сборки с момента создания заказа"
                        name="ct"
                        type="number"
                        min={0}
                    />
                </Layout.Item>
                <Layout.Item
                    col={{
                        xxxl: 2,
                        sm: 1,
                    }}
                >
                    <Form.FastField
                        label="PPT (мин)"
                        hint="Planned Processing Time - среднее время сборки заказа"
                        name="ppt"
                        type="number"
                        min={0}
                    />
                </Layout.Item>
                <Layout.Item
                    col={{
                        xxxl: 3,
                        sm: 1,
                    }}
                >
                    <Form.Reset theme="fill" css={{ marginRight: scale(2) }} disabled={!dirty}>
                        Сбросить
                    </Form.Reset>
                    <Button type="submit" disabled={!dirty}>
                        Сохранить
                    </Button>
                </Layout.Item>
            </Layout>
        )}
    </Form>
);

export default CommonSettings;
