import { useMemo } from 'react';

import {
    useDeliveryKpi,
    useDeliveryKpiChange,
    useDeliveryKpiCtCreate,
    useDeliveryKpiCtDelete,
    useDeliveryKpiCtEdit,
    useDeliveryKpiPptCreate,
    useDeliveryKpiPptDelete,
    useDeliveryKpiPptEdit,
    useDeliveryServiceChange,
} from '@api/logistic';

import { useError, useSuccess } from '@context/modal';

import Tabs from '@controls/future/Tabs';

import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

import { ModalMessages } from '@scripts/constants';
import { useFutureTabs } from '@scripts/hooks/useFutureTabs';

import CommonSettings from './CommonSettings';

const LogisticKPI = () => {
    const { getTabsProps } = useFutureTabs();

    const { data: apiCommon, isLoading: isKpiLoading, error } = useDeliveryKpi();

    const kpiCommon = useMemo(
        () =>
            apiCommon?.data || {
                rtg: '',
                ct: '',
                ppt: '',
            },
        [apiCommon]
    );

    // const { data: apiSellersCt, refetch: refetchApiSellerCt } = useDeliveryKpiCt();

    // const kpiCt = useMemo(() => apiSellersCt?.data || [], [apiSellersCt]);

    // const { data: apiUnitsSellers } = useGetSellers({
    //     pagination: getPagination(1, -1),
    // });

    // const sellers = useMemo(
    //     () => apiUnitsSellers?.data.map(item => ({ value: `${item.id}`, label: `${item.legal_name}` })) || [],
    //     [apiUnitsSellers]
    // );

    // const preparedCtSellers = useMemo(
    //     () =>
    //         kpiCt?.map(item => ({
    //             name: sellers.find(i => +i.value === item.seller_id)?.label || `id:${item.seller_id}`,
    //             value: item.ct,
    //             id: item.seller_id,
    //         })) || [],
    //     [sellers, kpiCt]
    // );

    const changeDeliveryKpi = useDeliveryKpiChange();
    const createDeliveryKpiCt = useDeliveryKpiCtCreate();
    const editDeliveryKpiCt = useDeliveryKpiCtEdit();
    const deleteDeliveryKpiCt = useDeliveryKpiCtDelete();

    const createDeliveryKpiPpt = useDeliveryKpiPptCreate();
    const editDeliveryKpiPpt = useDeliveryKpiPptEdit();
    const deleteDeliveryKpiPpt = useDeliveryKpiPptDelete();

    const editDeliveryService = useDeliveryServiceChange();

    useSuccess(createDeliveryKpiCt.isSuccess || createDeliveryKpiPpt.isSuccess ? ModalMessages.SUCCESS_CREATE : '');
    useSuccess(
        changeDeliveryKpi.isSuccess ||
            editDeliveryKpiCt.isSuccess ||
            editDeliveryKpiPpt.isSuccess ||
            editDeliveryService.isSuccess
            ? ModalMessages.SUCCESS_UPDATE
            : ''
    );
    useSuccess(deleteDeliveryKpiPpt.isSuccess || deleteDeliveryKpiCt.isSuccess ? ModalMessages.SUCCESS_DELETE : '');

    useError(
        changeDeliveryKpi.error ||
            editDeliveryKpiCt.error ||
            editDeliveryKpiPpt.error ||
            editDeliveryService.error ||
            createDeliveryKpiCt.error ||
            createDeliveryKpiPpt.error ||
            deleteDeliveryKpiPpt.error ||
            deleteDeliveryKpiCt.error
    );

    // const { data: apiSellersPpt, refetch: refetchApiSellersPpt } = useDeliveryKpiPpt();

    // const kpiPpt = useMemo(() => apiSellersPpt?.data || [], [apiSellersPpt]);

    // const preparedPptSellers = useMemo(
    //     () =>
    //         kpiPpt?.map(item => ({
    //             name: sellers.find(i => +i.value === item.seller_id)?.label || `id:${item.seller_id}`,
    //             value: item.ppt,
    //             id: item.seller_id,
    //         })) || [],
    //     [sellers, kpiPpt]
    // );

    // const { data: apiServices, refetch: refetchApiServices } = useDeliveryServices({
    //     pagination: { type: 'offset', limit: -1, offset: 0 },
    // });
    // const deliveryServices = useMemo(
    //     () => apiServices?.data.map(item => ({ value: `${item.id}`, label: `${item.name}` })) || [],
    //     [apiServices?.data]
    // );
    // const preparedDeliveryServices = useMemo(
    //     () =>
    //         apiServices?.data
    //             .filter(item => item.pct && item.pct > 0)
    //             .map(item => ({
    //                 name: `${item.name}`,
    //                 value: item.pct || 0,
    //                 id: item.id,
    //             })) || [],
    //     [apiServices?.data]
    // );

    const isLoading =
        isKpiLoading ||
        changeDeliveryKpi.isLoading ||
        createDeliveryKpiCt.isLoading ||
        editDeliveryKpiCt.isLoading ||
        deleteDeliveryKpiCt.isLoading ||
        createDeliveryKpiPpt.isLoading ||
        editDeliveryKpiPpt.isLoading ||
        deleteDeliveryKpiPpt.isLoading ||
        editDeliveryService.isLoading;

    // const getItems = useCallback((values: FormMakerFormValues, prevValues: FormMakerFormValues) => {
    //     const itemsToCreate = values.list.filter((item: any) => !prevValues.list.find(i => i.name === item.name));

    //     const itemsToEdit = values.list.filter((item: any) => {
    //         const existingItem = prevValues.list.find(i => i.name === item.name);
    //         return existingItem && existingItem.value !== item.value;
    //     });

    //     const itemsToDelete = prevValues.list.filter(item => !values.list.find((i: any) => i.name === item.name));

    //     return { itemsToCreate, itemsToEdit, itemsToDelete };
    // }, []);
    return (
        <PageWrapper h1="Параметры доставки" isLoading={isLoading} error={error ? JSON.stringify(error) : undefined}>
            <Tabs {...getTabsProps()}>
                <Tabs.Tab title="Общие настройки" id="0">
                    <Block css={{ borderRadius: 0 }}>
                        <Block.Body>
                            <CommonSettings
                                initialValues={kpiCommon}
                                onSubmit={values => {
                                    changeDeliveryKpi.mutate({
                                        rtg: values.rtg,
                                        ct: values.ct,
                                        ppt: values.ppt,
                                    });
                                }}
                            />
                        </Block.Body>
                    </Block>
                </Tabs.Tab>
            </Tabs>
            {/* <Block>
                    <Block.Body>
                        <Tabs.Panel></Tabs.Panel>
                        <Tabs.Panel>
                            <FormMaker
                                name="Продавец"
                                value="CT (мин)"
                                tooltipText="Confirmation Time - время перехода Отправления из статуса “Ожидает подтверждения” в статус “На комплектации”"
                                loadSuggestions={(inputValue, ids) => loadSellers(inputValue, ids)}
                                onSubmit={async values => {
                                    const { itemsToCreate, itemsToEdit, itemsToDelete } = getItems(values, {
                                        list: preparedCtSellers,
                                    });

                                    await Promise.all([
                                        ...itemsToCreate.map((item: any) =>
                                            createDeliveryKpiCt.mutate({
                                                seller_id: item.id,
                                                ct: item.value,
                                            })
                                        ),
                                        ...itemsToEdit.map((item: any) =>
                                            editDeliveryKpiCt.mutate({
                                                seller_id: item.id,
                                                ct: item.value,
                                            })
                                        ),
                                        ...itemsToDelete.map((item: any) => deleteDeliveryKpiCt.mutate(+item.id)),
                                    ]);

                                    await refetchApiSellerCt();
                                }}
                                initialValues={{
                                    list: preparedCtSellers,
                                }}
                            />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <FormMaker
                                name="Продавец"
                                value="PPT (мин)"
                                tooltipText="Planned Processing Time - плановое время для прохождения Отправлением статусов от “На комплектации” до “Готов к передаче ЛО”"
                                loadSuggestions={loadSellers}
                                initialValues={{
                                    list: preparedPptSellers,
                                }}
                                onSubmit={async values => {
                                    const { itemsToCreate, itemsToEdit, itemsToDelete } = getItems(values, {
                                        list: preparedPptSellers,
                                    });

                                    await Promise.all([
                                        ...itemsToCreate.map((item: any) =>
                                            createDeliveryKpiPpt.mutate({
                                                seller_id: item.id,
                                                ppt: item.value,
                                            })
                                        ),
                                        ...itemsToEdit.map((item: any) =>
                                            editDeliveryKpiPpt.mutate({
                                                seller_id: item.id,
                                                ppt: item.value,
                                            })
                                        ),
                                        ...itemsToDelete.map((item: any) => deleteDeliveryKpiPpt.mutate(+item.id)),
                                    ]);

                                    await refetchApiSellersPpt();
                                }}
                            />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <FormMaker
                                name="Служба доставки"
                                value="PCT (мин)"
                                tooltipText="Planned Сonsolidation Time - плановое время доставки заказа от склада продавца до логистического хаба ЛО и обработки заказа в сортировочном центре или хабе на стороне ЛО"
                                selectProps={{ items: deliveryServices }}
                                initialValues={{
                                    list: preparedDeliveryServices,
                                }}
                                onSubmit={async values => {
                                    const { itemsToCreate, itemsToEdit, itemsToDelete } = getItems(values, {
                                        list: preparedDeliveryServices,
                                    });

                                    await Promise.all([
                                        ...[...itemsToCreate, ...itemsToEdit].map(
                                            (item: { name: string; value: number; id: number }) =>
                                                editDeliveryService.mutateAsync({
                                                    id: item.id,
                                                    pct: item.value,
                                                })
                                        ),
                                        ...itemsToDelete.map((item: any) =>
                                            editDeliveryService.mutateAsync({
                                                id: item.id,
                                                pct: 0,
                                            })
                                        ),
                                    ]);
                                    await refetchApiServices();
                                }}
                            />
                        </Tabs.Panel>
                    </Block.Body>
                </Block> */}
        </PageWrapper>
    );
};

export default LogisticKPI;
