import { FieldArray, useFormikContext } from 'formik';
import { FC, Fragment, useCallback, useMemo, useRef } from 'react';
import * as Yup from 'yup';

import { Seller } from '@api/units';

import Form, { FormProps } from '@components/controls/Form';
import Select, { SelectProps, getLabel } from '@components/controls/Select';
import Tooltip from '@components/controls/Tooltip';
import AutocompleteAsync from '@components/controls/future/AutocompleteAsync';
import { AutocompleteAsyncProps } from '@components/controls/future/AutocompleteAsync/types';
import { OptionShape } from '@components/controls/future/Select';

import { ErrorMessages } from '@scripts/constants';
import { Button, Layout, scale, typography } from '@scripts/gds';

import PlusIcon from '@icons/small/plus.svg';
import TipIcon from '@icons/small/status/tip.svg';
import TrashIcon from '@icons/small/trash.svg';

export interface FormMakerFormValues {
    /** select value */
    select?: number;
    /** field value */
    field?: number;
    /** list array values */
    list: {
        /** list array item name */
        name: string;
        /** list array item value */
        value: number;
        id: number;
    }[];
}

interface FormChildrenProps {
    selectProps?: SelectProps;
    loadSuggestions?: (queryString: string, ids: number[]) => ReturnType<AutocompleteAsyncProps['asyncSearchFn']>;
}

const FormChildren: FC<FormChildrenProps> = ({ selectProps, loadSuggestions }) => {
    const {
        values: { list, select, field },
        setFieldValue,
        dirty,
    } = useFormikContext<FormMakerFormValues>();

    const asyncOptionsByValuesFn = async (vals: Seller[]) => {
        if (vals[0]) return [{ value: vals[0], key: vals[0].legal_name || '' }];
        return [];
    };

    const selectedIds = useMemo(() => list.map(({ id }) => id), [list]);
    const selectedSellers = useRef<OptionShape[]>([]);

    const getSelectData = useCallback(() => {
        if (select) {
            const name =
                (selectProps?.items
                    ? getLabel(selectProps.items.find(i => i.value === select))
                    : selectedSellers.current.find(seller => seller.value === select)?.key) || '';
            return {
                name,
                value: field,
                id: +select,
            };
        }
    }, [field, select, selectProps?.items]);

    return (
        <>
            <FieldArray
                name="list"
                render={({ remove, push }) => (
                    <>
                        {list.map((item, index) => (
                            <Fragment key={item.name}>
                                <Layout.Item col={1}>{item.name}</Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.Field
                                        type="number"
                                        name={`list[${index}].value`}
                                        min={0}
                                        css={{ marginRight: scale(1) }}
                                    />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Button theme="fill" Icon={TrashIcon} onClick={() => remove(index)}>
                                        Удалить
                                    </Button>
                                </Layout.Item>
                            </Fragment>
                        ))}
                        <Layout.Item col={1}>
                            <Form.Field name="select">
                                {selectProps && (
                                    <Select
                                        {...selectProps}
                                        items={selectProps.items.filter(
                                            item => !selectedIds.includes(Number(item.value))
                                        )}
                                    />
                                )}
                                {loadSuggestions && (
                                    <AutocompleteAsync
                                        asyncSearchFn={query => loadSuggestions(query, selectedIds)}
                                        asyncOptionsByValuesFn={asyncOptionsByValuesFn}
                                        onChange={({ selectedMultiple }) => {
                                            const selected = selectedMultiple[0];
                                            if (typeof selected !== 'string') selectedSellers.current.push(selected);
                                        }}
                                    />
                                )}
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="field" type="number" min={0} css={{ marginRight: scale(2) }} />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Button
                                Icon={PlusIcon}
                                onClick={() => {
                                    const data = getSelectData();
                                    if (data) {
                                        push(data);
                                        setFieldValue('select', null);
                                        setFieldValue('field', 0);
                                        selectedSellers.current = [];
                                    }
                                }}
                                disabled={!select}
                            >
                                Добавить
                            </Button>
                        </Layout.Item>
                    </>
                )}
            />
            <Layout.Item col={1}>
                <Button type="submit" theme="primary" disabled={!dirty}>
                    Сохранить
                </Button>
            </Layout.Item>
        </>
    );
};

interface FormMakerProps extends FormChildrenProps, Pick<FormProps<FormMakerFormValues>, 'onSubmit'> {
    /** left column name */
    name: string;
    /** right column name */
    value: string;
    /** tooltip text */
    tooltipText?: string;
    /** initial values of form */
    initialValues?: FormMakerFormValues;
}

export const DEFAULT_INITIAL_VALUES: FormMakerFormValues = {
    select: undefined,
    field: 0,
    list: [],
};

const FormMaker = ({
    name,
    value,
    tooltipText,
    selectProps,
    initialValues,
    onSubmit,
    loadSuggestions,
}: FormMakerProps) => (
    <Form
        initialValues={{ ...DEFAULT_INITIAL_VALUES, ...initialValues }}
        onSubmit={onSubmit}
        validationSchema={Yup.object().shape({
            list: Yup.array().of(
                Yup.object().shape({
                    value: Yup.number()
                        .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                        .required(`${ErrorMessages.REQUIRED}`),
                })
            ),
        })}
        enableReinitialize
    >
        <Layout cols={3}>
            <Layout.Item col={1}>
                <p css={typography('bodyMdBold')}>{name}</p>
            </Layout.Item>
            <Layout.Item col={2}>
                <p css={typography('bodyMdBold')}>
                    {value}{' '}
                    {tooltipText ? (
                        <Tooltip content={tooltipText} arrow maxWidth={scale(30)}>
                            <button type="button" css={{ verticalAlign: 'middle' }}>
                                <TipIcon />
                            </button>
                        </Tooltip>
                    ) : null}
                </p>
            </Layout.Item>
            <FormChildren selectProps={selectProps} loadSuggestions={loadSuggestions} />
        </Layout>
    </Form>
);

export default FormMaker;
