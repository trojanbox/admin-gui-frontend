import { useMemo } from 'react';

import { useAccess } from '@scripts/hooks';

const ACCESS_MATRIX = {
    LIST: {
        all: 101,
    },
    ID: {
        view: 102,
        edit: 109,

        ACTION: {
            EDIT_STATUS: {
                edit: 117,
            },
            REFUND: {
                edit: 118,
            },
        },

        MAIN_TAB: {
            view: 103,
        },
        CLEINT_TAB: {
            view: 104,
            edit: 114,
        },
        PRODUCTS_TAB: {
            view: 105,
        },
        DELIVERY_TAB: {
            view: 106,
            DELIVERY: {
                edit: 112,
            },
            COMMENTS: {
                edit: 113,
            },
        },
        COMMENT_TAB: {
            view: 107,
            COMMENTS: {
                edit: 110,
            },
            PROBLEMS: {
                edit: 111,
            },
        },
        ATTACHMENTS_TAB: {
            view: 108,
            ATTACHMENTS: {
                edit: 115,
                delete: 116,
            },
        },
    },
};

const useOrdersAccess = () => {
    const access = useAccess(ACCESS_MATRIX);

    const canViewListingAndFilters = useMemo(
        () => access.LIST.view && access.LIST.edit && access.LIST.delete,
        [access.LIST.delete, access.LIST.edit, access.LIST.view]
    );
    const canViewDetailPage = useMemo(() => access.ID.view, [access.ID.view]);
    const canViewMainTab = useMemo(() => access.ID.MAIN_TAB.view, [access.ID.MAIN_TAB.view]);
    const canViewClientTab = useMemo(() => access.ID.CLEINT_TAB.view, [access.ID.CLEINT_TAB.view]);
    const canViewProductsTab = useMemo(() => access.ID.PRODUCTS_TAB.view, [access.ID.PRODUCTS_TAB.view]);
    const canViewDeliveryTab = useMemo(() => access.ID.DELIVERY_TAB.view, [access.ID.DELIVERY_TAB.view]);
    const canViewCommentsTab = useMemo(() => access.ID.COMMENT_TAB.view, [access.ID.COMMENT_TAB.view]);
    const canViewAttachmentsTab = useMemo(() => access.ID.ATTACHMENTS_TAB.view, [access.ID.ATTACHMENTS_TAB.view]);
    const canEditOrderData = useMemo(() => access.ID.edit, [access.ID.edit]);
    const canEditCommentariesInCommentTab = useMemo(
        () => access.ID.COMMENT_TAB.COMMENTS.edit,
        [access.ID.COMMENT_TAB.COMMENTS.edit]
    );
    const canAddDeleteOrderProblem = useMemo(
        () => access.ID.COMMENT_TAB.PROBLEMS.edit,
        [access.ID.COMMENT_TAB.PROBLEMS.edit]
    );
    const canEditDeliveryMethod = useMemo(
        () => access.ID.DELIVERY_TAB.DELIVERY.edit,
        [access.ID.DELIVERY_TAB.DELIVERY.edit]
    );
    const canEditCommentariesInDeliveryTab = useMemo(
        () => access.ID.DELIVERY_TAB.COMMENTS.edit,
        [access.ID.DELIVERY_TAB.COMMENTS.edit]
    );
    const canEditDataInClientTab = useMemo(() => access.ID.CLEINT_TAB.edit, [access.ID.CLEINT_TAB.edit]);
    const canLoadAttchmentsData = useMemo(
        () => access.ID.ATTACHMENTS_TAB.ATTACHMENTS.edit,
        [access.ID.ATTACHMENTS_TAB.ATTACHMENTS.edit]
    );
    const canDeleteAttachmentsData = useMemo(
        () => access.ID.ATTACHMENTS_TAB.ATTACHMENTS.delete,
        [access.ID.ATTACHMENTS_TAB.ATTACHMENTS.delete]
    );
    const canEditStatusOrder = useMemo(() => access.ID.ACTION.EDIT_STATUS.edit, [access.ID.ACTION.EDIT_STATUS.edit]);
    const canCreateRequestForRefund = useMemo(() => access.ID.ACTION.REFUND.edit, [access.ID.ACTION.REFUND.edit]);

    return {
        canViewListingAndFilters,
        canViewDetailPage,
        canViewMainTab,
        canViewClientTab,
        canViewProductsTab,
        canViewDeliveryTab,
        canViewCommentsTab,
        canViewAttachmentsTab,
        canEditOrderData,
        canEditCommentariesInCommentTab,
        canAddDeleteOrderProblem,
        canEditDeliveryMethod,
        canEditCommentariesInDeliveryTab,
        canEditDataInClientTab,
        canLoadAttchmentsData,
        canDeleteAttachmentsData,
        canEditStatusOrder,
        canCreateRequestForRefund,
    };
};

export default useOrdersAccess;
