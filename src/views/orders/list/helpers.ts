
import { OrderStatus } from '@api/orders';

export const getStatusReplacements = (status: OrderStatus | undefined) => {
    switch (status) {
        case OrderStatus.WAITING:
            return [OrderStatus.NEW, OrderStatus.CANCELED];
        case OrderStatus.NEW:
            return [OrderStatus.CONFIRMED, OrderStatus.CANCELED ];
        case OrderStatus.CONFIRMED:
            return [OrderStatus.FINISHED, OrderStatus.CANCELED];
        default:
            return [];
    }
};
