import Block from '@components/Block';
import Form from '@components/controls/Form';
import Mask from '@components/controls/Mask';
import Input from '@components/controls/future/Input';

import { Layout, scale } from '@scripts/gds';
import { maskPhone } from '@scripts/mask';

export const Customer = ({ canEditDataInClientTab }: { canEditDataInClientTab: boolean }) => (
    <Block css={{ padding: scale(3) }}>
        <Layout cols={1}>
            <Layout.Item col={1}>
                <Form.FastField name="receiver_name" label="ФИО" disabled={!canEditDataInClientTab}>
                    <Input />
                </Form.FastField>
            </Layout.Item>
            <Layout.Item col={1}>
                <Form.FastField name="receiver_phone" label="Телефон" type="phone" disabled={!canEditDataInClientTab}>
                    <Mask mask={maskPhone} />
                </Form.FastField>
            </Layout.Item>
            <Layout.Item col={1}>
                <Form.FastField name="receiver_email" label="E-mail" type="email" disabled={!canEditDataInClientTab}>
                    <Input />
                </Form.FastField>
            </Layout.Item>
        </Layout>
    </Block>
);
