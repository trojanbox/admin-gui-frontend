import { CSSObject } from '@emotion/core';
import Link from 'next/link';
import { useMemo, useState } from 'react';

import { Order, OrderShipment, useDeliveryStatuses, useShipmentStatuses } from '@api/orders';
import { useShipmentChange } from '@api/orders/shipments';

import { useError, useSuccess } from '@context/modal';

import Block from '@components/Block';
import Table, { Cell, ExtendedColumn } from '@components/Table';
import Tooltip, { ContentBtn } from '@components/controls/Tooltip';

import { DateFormaters, ModalMessages } from '@scripts/constants';
import { Layout, scale, typography, useTheme } from '@scripts/gds';
import { formatDate, formatPrice, fromKopecksToRouble, getOptionName } from '@scripts/helpers';
import { useLinkCSS } from '@scripts/hooks';

const itemCSS: CSSObject = {
    display: 'flex',
    ':not(:last-of-type)': {
        marginBottom: scale(1, true),
    },
};

const itemTitleCSS: CSSObject = {
    ...typography('bodySmBold'),
    marginRight: scale(1, true),
};

export const columns: ExtendedColumn[] = [
    {
        accessor: 'id',
        Header: 'ID',
    },
    {
        accessor: 'photo',
        Header: 'Фото',
        Cell: ({ value }) => <Cell value={value} type="photo" />,
    },
    {
        accessor: 'name',
        Header: 'Название',
        Cell: ({ value }) => {
            const linkStyles = useLinkCSS();
            return (
                <>
                    {value?.link ? (
                        <p css={{ marginBottom: scale(1) }}>
                            <Link passHref href={value.link}>
                                <a css={linkStyles}>{value.name}</a>
                            </Link>
                        </p>
                    ) : (
                        <p css={{ marginBottom: scale(1) }}>{value.name}</p>
                    )}
                </>
            );
        },
    },
    { accessor: 'vendor_code', Header: 'Артикул' },
    {
        accessor: 'price',
        Header: 'Цена и цена без скидки,  ₽',
        Cell: ({ value }: { value: number[] }) => {
            const { colors } = useTheme();
            return value.map((v, index) => (
                <div key={Number(index)} css={index > 0 && { color: colors?.grey700 }}>
                    <Cell value={v} type="price" />
                </div>
            ));
        },
    },
    {
        accessor: 'quantity',
        Header: 'Количество',
    },

    {
        accessor: 'cost',
        Header: 'Стоимость,  ₽',
        Cell: ({ value }) => <Cell value={value} type="price" />,
    },
];

export const Shipment = ({
    shipment,
    canEditOrderData,
    onChangeStatus,
}: {
    shipment: OrderShipment;
    canEditOrderData: boolean;
    onChangeStatus: (status: number) => void;
}) => {
    const [tippyInstance, setTippyInstance] = useState<any>();
    const linkStyles = useLinkCSS();
    const data = useMemo(
        () =>
            shipment.order_items.map(item => ({
                id: item.id,
                photo: item.product?.main_image_url || item.product?.main_image_file,
                name: {
                    name: item.name,
                    code: item.product?.vendor_code,
                    link: item?.product?.id ? `/products/catalog/${item?.product?.id}` : null,
                },
                vendor_code: item.product?.vendor_code,
                price: [item.price_per_one, item.cost_per_one],
                quantity: item.qty,
                cost: item.price,
            })),
        [shipment.order_items]
    );

    const { data: apiShipmentStatuses } = useShipmentStatuses();
    const shipmentStatus = useMemo(
        () => getOptionName(apiShipmentStatuses?.data, shipment.status),
        [apiShipmentStatuses?.data, shipment.status]
    );

    return (
        <div>
            <ul css={{ marginBottom: scale(2) }}>
                <li css={itemCSS}>
                    <p css={itemTitleCSS}>Номер отгрузки:</p>
                    {shipment.number}
                </li>
                <li css={itemCSS}>
                    <p css={itemTitleCSS}>Идентификатор склада:</p>
                    {shipment.store_id}
                </li>
                <li css={itemCSS}>
                    <p css={itemTitleCSS}>Статус отгрузки:</p>
                    {canEditOrderData ? (
                        <Tooltip
                            trigger="click"
                            theme="light"
                            arrow
                            placement="bottom-start"
                            minWidth={scale(15)}
                            onCreate={inst => setTippyInstance(inst)}
                            content={
                                <ul>
                                    {apiShipmentStatuses?.data.map(status => (
                                        <li key={status.id}>
                                            <ContentBtn
                                                onClick={() => {
                                                    tippyInstance?.hide();
                                                    onChangeStatus(status.id);
                                                }}
                                            >
                                                {status.name}
                                            </ContentBtn>
                                        </li>
                                    ))}
                                </ul>
                            }
                        >
                            <button
                                type="button"
                                css={linkStyles}
                                aria-label="Изменить статус отгрузки"
                                title="Изменить статус отгрузки"
                            >
                                {shipmentStatus}
                            </button>
                        </Tooltip>
                    ) : (
                        shipmentStatus
                    )}
                </li>
                <li css={itemCSS}>
                    <p css={itemTitleCSS}>Дата/время изменения статуса:</p>
                    {shipment.status_at
                        ? formatDate(new Date(shipment.status_at), DateFormaters.DATE_AND_TIME, true)
                        : '-'}
                </li>
            </ul>

            <Table columns={columns} data={data} allowRowSelect={false} allowColumnOrder={false} disableSortBy />
        </div>
    );
};

export const Products = ({
    order,
    refetch,
    canEditOrderData,
}: {
    order: Order | undefined;
    refetch: () => void;
    canEditOrderData: boolean;
}) => {
    const { data: apiDeliveryStatuses } = useDeliveryStatuses();

    const changeShipmentStatus = useShipmentChange();

    useSuccess(changeShipmentStatus.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useError(changeShipmentStatus.error);

    return (
        <Layout cols={1} gap={scale(2)}>
            {order?.deliveries?.map(delivery => (
                <>
                    <Layout.Item>
                        <Block css={{ padding: scale(3) }}>
                            <ul css={{ marginBottom: scale(4) }}>
                                <li css={itemCSS}>
                                    <p css={itemTitleCSS}>Номер отправления:</p>
                                    {delivery.number}
                                </li>
                                <li css={itemCSS}>
                                    <p css={itemTitleCSS}>Дата доставки отправления:</p>
                                    {delivery.date
                                        ? formatDate(new Date(delivery.date), 'dd MMMM yyyy')
                                        : 'Дата не выбрана'}
                                </li>
                                <li css={itemCSS}>
                                    <p css={itemTitleCSS}>Статус отправления:</p>
                                    {getOptionName(apiDeliveryStatuses?.data, delivery?.status)}
                                </li>
                                <li css={itemCSS}>
                                    <p css={itemTitleCSS}>Дата/время изменения статуса: </p>
                                    {delivery.status_at
                                        ? formatDate(new Date(delivery.status_at), DateFormaters.DATE_AND_TIME, true)
                                        : '-'}
                                </li>
                            </ul>
                            <ul>
                                {delivery.shipments.map((shipment, j) => (
                                    <>
                                        {j > 0 && <hr css={{ margin: `${scale(2)}px 0` }} />}
                                        <li key={shipment.id} css={j > 0 && { marginTop: scale(3) }}>
                                            <Shipment
                                                shipment={shipment}
                                                canEditOrderData={canEditOrderData}
                                                onChangeStatus={async (status: number) => {
                                                    try {
                                                        await changeShipmentStatus.mutateAsync(
                                                            {
                                                                id: shipment.id,
                                                                status,
                                                            },
                                                            {
                                                                onSuccess: () => {
                                                                    refetch();
                                                                },
                                                            }
                                                        );
                                                    } catch (err) {
                                                        console.error(err);
                                                    }
                                                }}
                                            />
                                            <div css={{ marginTop: scale(2) }}>
                                                <p css={itemCSS}>
                                                    <span css={itemTitleCSS}>Количество позиций:</span>
                                                    {shipment.order_items.length || 0}
                                                </p>
                                                <p css={itemCSS}>
                                                    <span css={itemTitleCSS}>Итого:</span>
                                                    {formatPrice(fromKopecksToRouble(+shipment.cost))} ₽
                                                </p>
                                            </div>
                                        </li>
                                    </>
                                ))}
                            </ul>
                        </Block>
                    </Layout.Item>
                </>
            ))}
        </Layout>
    );
};
