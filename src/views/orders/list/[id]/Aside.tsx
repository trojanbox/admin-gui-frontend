import { FC } from 'react';

import { Order, OrderStatus, useOrderStatuses } from '@api/orders';

import Block from '@components/Block';
import Circle from '@components/Circle';
import CopyButton from '@components/CopyButton';

import { scale } from '@scripts/gds';
import { formatDate, getOptionName } from '@scripts/helpers';
import { useMedia } from '@scripts/hooks/useMedia';

import { getStatusColor } from './helpers';

export const Aside: FC<{ order: Order | undefined; className?: string }> = ({ order, className }) => {
    const { data: statuses } = useOrderStatuses();
    const { md } = useMedia();
    return (
        <Block
            css={{
                padding: scale(3),
                width: scale(35),
                [md]: {
                    width: '100%',
                },
                flexShrink: 0,
                height: 'fit-content',
            }}
            className={className}
        >
            <ul css={{ li: { marginBottom: scale(1) } }}>
                <li>
                    Статус:
                    <Circle
                        css={{
                            marginRight: scale(1, true),
                            marginLeft: scale(1),
                            background: getStatusColor(order?.status as OrderStatus),
                        }}
                    />
                    {getOptionName(statuses?.data, order?.status)}
                </li>
                <li>
                    ID: <CopyButton>{`${order?.id}`}</CopyButton>
                </li>
                <li>
                    Номер заказа: <CopyButton>{`${order?.number}`}</CopyButton>
                </li>
                <li>
                    Создано: {order?.created_at && formatDate(new Date(order?.created_at), 'dd MMMM yyyy, в HH:mm')}
                </li>
                <li>
                    Изменено: {order?.updated_at && formatDate(new Date(order?.updated_at), 'dd MMMM yyyy, в HH:mm')}
                </li>
            </ul>
        </Block>
    );
};
