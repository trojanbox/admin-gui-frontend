import { FormikProps, FormikValues } from 'formik';
import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';
import * as Yup from 'yup';

import {
    useOrderChange,
    useOrderDeliveryChange,
    useOrderDetail,
    useOrdersAddFile,
    useOrdersDeleteFiles,
} from '@api/orders';

import { useError, useSuccess } from '@context/modal';

import PageLeaveGuard from '@components/PageLeaveGuard';
import PageWrapper from '@components/PageWrapper';
import { FileType } from '@components/controls/Dropzone/DropzoneFile';
import Form from '@components/controls/Form';
import Tooltip, { ContentBtn } from '@components/controls/Tooltip';
import Tabs from '@components/controls/future/Tabs';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, scale, typography, useTheme } from '@scripts/gds';
import { cleanPhoneValue, isNotEmptyObject } from '@scripts/helpers';
import { useFutureTabs } from '@scripts/hooks/useFutureTabs';
import { useMedia } from '@scripts/hooks/useMedia';

import CheckIcon from '@icons/small/check.svg';
import KebabIcon from '@icons/small/kebab.svg';

import ChangeStatusPopup from '../components/ChangeStatusPopup';
import useOrdersAccess from '../useOrdersAccess';
import { Aside } from './Aside';
import { Comments } from './Comments';
import { Customer } from './Customer';
import { Delivery } from './Delivery';
import { Files } from './Files';
import { Main } from './Main';
import { Products } from './Products';

const OrderPage = () => {
    const { colors } = useTheme();
    const { md, mdMin } = useMedia();
    const { query, push, pathname } = useRouter();
    const { getTabsProps } = useFutureTabs('tab', 'main');
    const orderId = (query && query.id && +query.id) || 0;

    const {
        canViewDetailPage,
        canViewMainTab,
        canViewClientTab,
        canViewProductsTab,
        canViewDeliveryTab,
        canViewCommentsTab,
        canViewAttachmentsTab,
        canEditOrderData,
        canEditCommentariesInCommentTab,
        canAddDeleteOrderProblem,
        canEditDeliveryMethod,
        canEditCommentariesInDeliveryTab,
        canEditDataInClientTab,
        canLoadAttchmentsData,
        canDeleteAttachmentsData,
        canEditStatusOrder,
        canCreateRequestForRefund,
    } = useOrdersAccess();

    const isVisibleAnyOnPage = useMemo(
        () =>
            canViewDetailPage ||
            canViewMainTab ||
            canViewClientTab ||
            canViewProductsTab ||
            canViewDeliveryTab ||
            canViewCommentsTab ||
            canViewAttachmentsTab,
        [
            canViewAttachmentsTab,
            canViewMainTab,
            canViewClientTab,
            canViewCommentsTab,
            canViewDeliveryTab,
            canViewDetailPage,
            canViewProductsTab,
        ]
    );

    const [isStatusOpen, setIsStatusOpen] = useState(false);
    const [tippyInstance, setTippyInstance] = useState<any>();

    const changeOrder = useOrderChange();
    const changeOrderDelivery = useOrderDeliveryChange();

    const { data, isIdle, isLoading, error, refetch } = useOrderDetail(orderId, [
        'deliveries',
        'deliveries.shipments',
        'deliveries.shipments.orderItems',
        'customer',
        'customer.user',
        'responsible',
        'files',
        'orderItems.product',
    ]);

    const order = useMemo(() => data?.data, [data?.data]);

    const addFile = useOrdersAddFile();
    const deleteFiles = useOrdersDeleteFiles();
    useError(addFile.error);
    useError(deleteFiles.error);
    useSuccess(addFile.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(deleteFiles.status === 'success' ? ModalMessages.SUCCESS_DELETE : '');

    useError(error);

    useError(changeOrder.error);
    useError(changeOrderDelivery.error);
    useSuccess(changeOrder.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(changeOrderDelivery.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');

    const initialValues = useMemo(
        () => ({
            responsible_id: { label: order?.responsible?.full_name || '', value: order?.responsible?.id || null },

            receiver_name: order?.receiver_name || '',
            receiver_phone: order?.receiver_phone || '',
            receiver_email: order?.receiver_email || '',

            delivery_method: order?.delivery_method || '',
            delivery_address: order?.delivery_address || null,
            porch: order?.delivery_address?.porch || '',
            floor: order?.delivery_address?.floor || '',
            flat: order?.delivery_address?.flat || '',
            intercom: order?.delivery_address?.intercom || '',
            delivery_comment: order?.delivery_comment || '',
            delivery:
                order?.deliveries?.map(d => ({
                    date: d?.date ? new Date(d?.date).getTime() : NaN,
                    timeslot: !d?.timeslot?.from || !d?.timeslot?.to ? '' : `${d?.timeslot?.from} - ${d?.timeslot?.to}`,
                })) || [],
            delivery_point_id: { label: order?.delivery_point_id, value: order?.delivery_point_id },

            client_comment: order?.client_comment || '',
            is_problem: order?.is_problem || false,
            problem_comment: order?.problem_comment || '',

            files: (order?.files?.map(f => ({ name: f.original_name, id: f.id, file: f.file })) || []) as FileType[],
            files_to_delete: [],
        }),
        [
            order?.client_comment,
            order?.deliveries,
            order?.delivery_address,
            order?.delivery_comment,
            order?.delivery_method,
            order?.delivery_point_id,
            order?.is_problem,
            order?.problem_comment,
            order?.receiver_email,
            order?.receiver_name,
            order?.receiver_phone,
            order?.responsible?.full_name,
            order?.responsible?.id,
            order?.files,
        ]
    );

    return (
        <PageWrapper h1={isVisibleAnyOnPage ? `Заказ №${order?.number || ''}` : ''} isLoading={isIdle || isLoading}>
            {isVisibleAnyOnPage ? (
                <>
                    <Form
                        initialValues={initialValues}
                        enableReinitialize
                        onSubmit={vals => {
                            if (order) {
                                const changeOrderData = {
                                    receiver_name:
                                        initialValues.receiver_name !== vals.receiver_name
                                            ? vals.receiver_name
                                            : undefined,
                                    receiver_email:
                                        initialValues.receiver_email !== vals.receiver_email
                                            ? vals.receiver_email
                                            : undefined,
                                    receiver_phone:
                                        initialValues.receiver_phone !== cleanPhoneValue(vals.receiver_phone)
                                            ? cleanPhoneValue(vals.receiver_phone)
                                            : undefined,
                                    responsible_id:
                                        initialValues?.responsible_id.value !== vals.responsible_id.value &&
                                        vals.responsible_id.value
                                            ? +vals.responsible_id.value
                                            : undefined,
                                    client_comment:
                                        initialValues.client_comment !== vals.client_comment
                                            ? vals.client_comment
                                            : undefined,
                                    is_problem:
                                        initialValues.is_problem !== vals.is_problem ? vals.is_problem : undefined,
                                    problem_comment:
                                        vals.is_problem && initialValues.problem_comment !== vals.problem_comment
                                            ? vals.problem_comment
                                            : undefined,
                                };

                                if (isNotEmptyObject(changeOrderData)) {
                                    changeOrder.mutate({
                                        id: orderId,
                                        ...changeOrderData,
                                    });
                                }

                                const unchangedData = {
                                    id: orderId,
                                    delivery_service: order?.delivery_service,
                                    delivery_cost: order?.delivery_cost,
                                    delivery_price: order?.delivery_price,
                                    delivery_tariff_id: order?.delivery_tariff_id,
                                };
                                const changeOrderDeliveryData = {
                                    delivery_method: +vals.delivery_method,
                                    delivery_point_id: vals.delivery_point_id.value || null,
                                    delivery_address: {
                                        ...vals.delivery_address,
                                        address_string: vals?.delivery_address?.address_string,
                                        porch: vals.porch,
                                        floor: vals.floor,
                                        flat: vals.flat,
                                        intercom: vals.intercom,
                                    },
                                    delivery_comment: vals.delivery_comment,
                                };

                                // если данные поменялись
                                if (
                                    initialValues.delivery_method !== changeOrderDeliveryData.delivery_method ||
                                    order.delivery_point_id !== changeOrderDeliveryData.delivery_point_id ||
                                    order.delivery_comment !== changeOrderDeliveryData.delivery_comment ||
                                    initialValues.porch !== vals.porch ||
                                    initialValues.floor !== vals.floor ||
                                    initialValues.flat !== vals.flat ||
                                    initialValues.intercom !== vals.intercom ||
                                    initialValues?.delivery_address?.address_string !==
                                        vals?.delivery_address?.address_string
                                )
                                    // @ts-ignore
                                    changeOrderDelivery.mutate({
                                        ...unchangedData,
                                        ...changeOrderDeliveryData,
                                    });

                                if (vals.files_to_delete.length > 0) {
                                    deleteFiles.mutate({ id: orderId, file_ids: vals.files_to_delete });
                                }

                                if (vals.files.length > 0) {
                                    vals.files.forEach(file => {
                                        // Если файл еще не был загружен
                                        if (!file.id) {
                                            addFile.mutate({ id: orderId, file });
                                        }
                                    });
                                }
                            }
                        }}
                        validationSchema={Yup.object().shape({
                            receiver_name: Yup.string().min(3).required(),
                            receiver_email: Yup.string().email(ErrorMessages.EMAIL),
                            problem_comment: Yup.string().when('is_problem', {
                                is: true,
                                then: Yup.string().required(ErrorMessages.REQUIRED),
                                otherwise: Yup.string(),
                            }),
                        })}
                        css={{ position: 'relative' }}
                    >
                        {({ dirty }: FormikProps<FormikValues>) => (
                            <>
                                <PageLeaveGuard />
                                <div
                                    css={{
                                        display: 'flex',
                                        gap: scale(1),
                                        [md]: {
                                            flexWrap: 'wrap',
                                            marginBottom: scale(2),
                                        },
                                        [mdMin]: {
                                            position: 'absolute',
                                            top: -scale(5),
                                            right: 0,
                                        },
                                        background: colors?.white,
                                        padding: scale(1),
                                    }}
                                >
                                    <Tooltip
                                        trigger="click"
                                        theme="light"
                                        arrow
                                        placement="bottom-end"
                                        minWidth={scale(36)}
                                        onCreate={inst => setTippyInstance(inst)}
                                        content={
                                            <ul>
                                                <li>
                                                    <ContentBtn
                                                        type="edit"
                                                        onClick={() => {
                                                            setIsStatusOpen(true);
                                                            tippyInstance?.hide();
                                                        }}
                                                        disabled={!canEditStatusOrder}
                                                    >
                                                        Изменить статус заказа
                                                    </ContentBtn>
                                                </li>
                                                <li>
                                                    <ContentBtn
                                                        type="edit"
                                                        onClick={() => {
                                                            push({
                                                                pathname: `${pathname}/create-refund`,
                                                                query: { id: orderId },
                                                            });
                                                        }}
                                                        disabled={!canCreateRequestForRefund}
                                                    >
                                                        Создать заявку на возврат
                                                    </ContentBtn>
                                                </li>
                                            </ul>
                                        }
                                    >
                                        <Button
                                            theme="secondary"
                                            Icon={KebabIcon}
                                            iconAfter
                                            disabled={
                                                !(canEditOrderData || canEditStatusOrder || canCreateRequestForRefund)
                                            }
                                        >
                                            Действия
                                        </Button>
                                    </Tooltip>
                                    {dirty && (
                                        <Form.Reset
                                            theme="dangerous"
                                            css={{
                                                [md]: {
                                                    order: 3,
                                                },
                                            }}
                                        >
                                            Отменить
                                        </Form.Reset>
                                    )}
                                    <Button type="submit" Icon={CheckIcon} iconAfter disabled={!dirty}>
                                        Сохранить
                                    </Button>
                                </div>
                                <div css={{ display: 'flex', gap: scale(2), flexWrap: 'wrap' }}>
                                    <div css={{ flexGrow: 1, flexShrink: 1, maxWidth: '100%' }}>
                                        <Tabs {...getTabsProps()}>
                                            <Tabs.Tab
                                                title="Главное"
                                                id="main"
                                                hidden={!(canViewDetailPage || canViewMainTab)}
                                            >
                                                <Main order={order} />
                                            </Tabs.Tab>
                                            <Tabs.Tab
                                                title="Клиент"
                                                id="client"
                                                hidden={!(canViewDetailPage || canViewClientTab)}
                                            >
                                                <Customer
                                                    canEditDataInClientTab={canEditOrderData || canEditDataInClientTab}
                                                />
                                            </Tabs.Tab>
                                            <Tabs.Tab
                                                title="Товары"
                                                id="products"
                                                hidden={!(canViewDetailPage || canViewProductsTab)}
                                            >
                                                <Products order={order} canEditOrderData={canEditOrderData} refetch={refetch}/>
                                            </Tabs.Tab>

                                            <Tabs.Tab
                                                title="Доставка"
                                                id="delivery"
                                                hidden={!(canViewDetailPage || canViewDeliveryTab)}
                                            >
                                                <Delivery
                                                    order={order}
                                                    canEditCommentariesInDeliveryTab={
                                                        canEditOrderData || canEditCommentariesInDeliveryTab
                                                    }
                                                    canEditDeliveryMethod={canEditOrderData || canEditDeliveryMethod}
                                                    canEditOrderData={canEditOrderData}
                                                />
                                            </Tabs.Tab>
                                            <Tabs.Tab
                                                title="Комментарии"
                                                id="comments"
                                                hidden={!(canViewDetailPage || canViewCommentsTab)}
                                            >
                                                <Comments
                                                    canEditCommentariesInCommentTab={
                                                        canEditOrderData || canEditCommentariesInCommentTab
                                                    }
                                                    canAddDeleteOrderProblem={
                                                        canEditOrderData || canAddDeleteOrderProblem
                                                    }
                                                />
                                            </Tabs.Tab>
                                            <Tabs.Tab
                                                title="Вложения"
                                                id="attachments"
                                                hidden={!(canViewDetailPage || canViewAttachmentsTab)}
                                            >
                                                <Files
                                                    canLoadAttchmentsData={canEditOrderData || canLoadAttchmentsData}
                                                    canDeleteAttachmentsData={
                                                        canEditOrderData || canDeleteAttachmentsData
                                                    }
                                                />
                                            </Tabs.Tab>
                                        </Tabs>
                                    </div>
                                    <Aside
                                        order={order}
                                        css={{
                                            marginTop: scale(6),
                                            [md]: {
                                                marginTop: 0,
                                            },
                                        }}
                                    />
                                </div>
                            </>
                        )}
                    </Form>
                    <ChangeStatusPopup
                        isOpen={isStatusOpen}
                        setIsOpen={setIsStatusOpen}
                        orderId={orderId}
                        oldStatus={order?.status}
                    />
                </>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра деталей заказа
                </h1>
            )}
        </PageWrapper>
    );
};

export default OrderPage;
