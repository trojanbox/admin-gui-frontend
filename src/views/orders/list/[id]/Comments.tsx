import Block from '@components/Block';
import Checkbox from '@components/controls/Checkbox';
import Form from '@components/controls/Form';
import Textarea from '@components/controls/Textarea';

import { scale, Layout } from '@scripts/gds';
import { useFormikContext } from 'formik';
import { useEffect, useState } from 'react';

export const Comments = ({
    canEditCommentariesInCommentTab,
    canAddDeleteOrderProblem,
}: {
    canEditCommentariesInCommentTab: boolean;
    canAddDeleteOrderProblem: boolean;
}) => {
    const { values, setFieldValue } = useFormikContext<{ is_problem: boolean }>();
    const [isProblem, setIsProblem] = useState(values.is_problem);

    useEffect(() => {
        setIsProblem(values.is_problem);
    }, [setFieldValue, values.is_problem]);

    return (
        <Block css={{ padding: scale(3) }}>
            <Layout cols={1}>
                <Layout.Item col={1}>
                    <Form.FastField label="Комментарий клиента" name="client_comment">
                        <Textarea minRows={5} disabled={!canEditCommentariesInCommentTab} />
                    </Form.FastField>
                </Layout.Item>
                <Layout.Item col={1}>
                    <Form.FastField name="is_problem" disabled={!canAddDeleteOrderProblem}>
                        <Checkbox>Есть проблема</Checkbox>
                    </Form.FastField>
                </Layout.Item>
                {isProblem && (
                    <Layout.Item col={1}>
                        <Form.FastField label="Проблемы" name="problem_comment" disabled={!canAddDeleteOrderProblem}>
                            <Textarea minRows={5} />
                        </Form.FastField>
                    </Layout.Item>
                )}
            </Layout>
        </Block>
    );
};
