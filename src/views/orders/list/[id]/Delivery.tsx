import { FieldArray, useFormikContext } from 'formik';
import { Fragment, useMemo } from 'react';

import { loadAddressesSearchFn } from '@api/dadata';
import { useDeliveryMethods } from '@api/logistic';
import { Address, Order } from '@api/orders';

import { useError } from '@context/modal';

import Block from '@components/Block';
import CalendarInput from '@components/controls/CalendarInput';
import Form from '@components/controls/Form';
import Textarea from '@components/controls/Textarea';
import AutocompleteAsync from '@components/controls/future/AutocompleteAsync';
import Input from '@components/controls/future/Input';
import FormikSelect from '@components/controls/future/Select';

import { DeliveryMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { toFutureSelectItems } from '@scripts/helpers';

export const Delivery = ({
    order,
    canEditCommentariesInDeliveryTab,
    canEditDeliveryMethod,
    canEditOrderData,
}: {
    order: Order | undefined;
    canEditCommentariesInDeliveryTab: boolean;
    canEditDeliveryMethod: boolean;
    canEditOrderData: boolean;
}) => {
    const { data, error } = useDeliveryMethods();
    useError(error);
    const items = useMemo(() => toFutureSelectItems(data?.data), [data?.data]);
    const { values } =
        useFormikContext<{ delivery_method: DeliveryMethods; delivery: { date: number; timeslot: string }[] }>();

    const asyncOptionsByValuesFn = async (vals: Address[]) => {
        if (vals[0]) return [{ value: vals[0], key: vals[0].address_string || '' }];
        return [];
    };

    return (
        <Block css={{ padding: scale(3) }}>
            <Layout cols={4}>
                <Layout.Item col={4}>
                    <Form.Field label="Способ доставки" name="delivery_method">
                        <FormikSelect options={items} disabled={!canEditDeliveryMethod} />
                    </Form.Field>
                </Layout.Item>
                {values.delivery_method === DeliveryMethods.DELIVERY && (
                    <>
                        <Layout.Item col={4}>
                            <Form.Field
                                label="Адрес"
                                name="delivery_address"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            >
                                <AutocompleteAsync
                                    asyncSearchFn={loadAddressesSearchFn}
                                    asyncOptionsByValuesFn={asyncOptionsByValuesFn}
                                />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <Form.FastField
                                label="Подъезд"
                                name="porch"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            >
                                <Input />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <Form.FastField
                                label="Этаж"
                                name="floor"
                                type="number"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            >
                                <Input />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <Form.FastField
                                label="Кв/Офис"
                                name="flat"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            >
                                <Input />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <Form.FastField
                                label="Домофон"
                                name="intercom"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            >
                                <Input />
                            </Form.FastField>
                        </Layout.Item>
                        <FieldArray
                            name="delivery"
                            render={() =>
                                values.delivery.map((d, index) => (
                                    <Fragment key={Number(index)}>
                                        <Layout.Item col={4}>
                                            <p>
                                                Дата и время доставки для №
                                                {order?.deliveries && order.deliveries[index]?.number}
                                            </p>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.FastField
                                                name={`delivery.${index}.date`}
                                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                                            >
                                                <CalendarInput label="Дата доставки" />
                                            </Form.FastField>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.FastField
                                                label="Время доставки"
                                                name={`delivery.${index}.timeslot`}
                                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                                            />
                                        </Layout.Item>
                                    </Fragment>
                                ))
                            }
                        />
                    </>
                )}

                <Layout.Item col={4}>
                    <Form.FastField label="Комментарий к доставке" name="delivery_comment">
                        <Textarea minRows={5} disabled={!canEditCommentariesInDeliveryTab} />
                    </Form.FastField>
                </Layout.Item>
            </Layout>
        </Block>
    );
};
