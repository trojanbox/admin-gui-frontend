import { Order, useOrderSources, usePaymentMethods, usePaymentStatuses } from '@api/orders';

import { useError } from '@context/modal';

import Block from '@components/Block';
import Label from '@components/Label';

import { Layout, scale } from '@scripts/gds';
import { formatPrice, fromKopecksToRouble, getOptionName } from '@scripts/helpers';

const getFeature = (order: Order | undefined) => {
    const result: string[] = [];
    if (order) {
        const { is_expired, is_partial_return, is_problem, is_return } = order;
        if (is_expired) result.push('Просрочен');
        if (is_partial_return) result.push('Частичный возврат');
        if (is_problem) result.push('Проблемный');
        if (is_return) result.push('Возвратный');
        if (!is_expired && !is_partial_return && !is_problem && !is_return) result.push('Отсутствует');
    } else {
        result.push('Отсутствует');
    }
    return result;
};

export const Main = ({ order }: { order: Order | undefined }) => {
    const { data: orderSourses, error: orderSourcesError } = useOrderSources();
    const { data: paymentMethods, error: paymentMethodsError } = usePaymentMethods();
    const { data: paymentStatuses, error: paymentStatusesError } = usePaymentStatuses();

    useError(orderSourcesError);
    useError(paymentMethodsError);
    useError(paymentStatusesError);

    return (
        <Block css={{ borderTopLeftRadius: 0, padding: scale(3) }}>
            <Layout cols={2}>
                <Layout.Item>
                    <Label>Источник</Label>
                    {getOptionName(orderSourses?.data, order?.source)}
                </Layout.Item>

                <Layout.Item>
                    <Label>Сумма</Label>
                    {formatPrice(fromKopecksToRouble(order?.price || 0))}
                </Layout.Item>

                <Layout.Item>
                    <Label>Признак</Label>
                    {new Intl.ListFormat('ru').format(getFeature(order))}
                </Layout.Item>

                <Layout.Item>
                    <Label>Способ оплаты</Label>
                    {getOptionName(paymentMethods?.data, order?.payment_method)}
                </Layout.Item>

                <Layout.Item>
                    <Label>Клиент</Label>
                    {order?.customer?.user?.login}
                </Layout.Item>

                <Layout.Item>
                    <Label>Статус оплаты</Label>
                    {getOptionName(paymentStatuses?.data, order?.payment_status)}
                </Layout.Item>
            </Layout>
        </Block>
    );
};
