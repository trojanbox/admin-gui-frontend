import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

const useSettingsAccess = () => {
    const { data: userData } = useCurrentUser();

    const viewListingAndFilters = 301;
    const editPropertyCart = 302;
    const editOrderPaymentTime = 303;
    const editOrderRegistrationTime = 304;
    const editProcessingTime = 305;

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canEditPropertyCart = useMemo(
        () => !!userData?.data.rights_access.includes(editPropertyCart),
        [userData?.data.rights_access]
    );
    const canEditOrderPaymentTime = useMemo(
        () => !!userData?.data.rights_access.includes(editOrderPaymentTime),
        [userData?.data.rights_access]
    );
    const canEditOrderRegistrationTime = useMemo(
        () => !!userData?.data.rights_access.includes(editOrderRegistrationTime),
        [userData?.data.rights_access]
    );
    const canEditProcessingTime = useMemo(
        () => !!userData?.data.rights_access.includes(editProcessingTime),
        [userData?.data.rights_access]
    );

    return {
        canViewListingAndFilters,
        canEditPropertyCart,
        canEditOrderPaymentTime,
        canEditOrderRegistrationTime,
        canEditProcessingTime,
    };
};

export default useSettingsAccess;
