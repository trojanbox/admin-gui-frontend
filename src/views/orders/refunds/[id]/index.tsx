import { FormikProps, FormikValues } from 'formik';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useState } from 'react';
import * as Yup from 'yup';

import { downloadFile } from '@api/common';
import { useCustomer } from '@api/customers';
import {
    RefundStatuses,
    useOrderSources,
    usePatchRefund,
    useRefund,
    useRefundAddFile,
    useRefundDeleteFiles,
    useRefundStatuses,
} from '@api/orders';
import { adminUsersOptionsByValuesFn, adminUsersSearchFn, getUserById, useAdminUser } from '@api/units';

import { useError, useSuccess } from '@context/modal';

import Block from '@components/Block';
import Label from '@components/Label';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import Table, { Cell, ExtendedColumn } from '@components/Table';
import Dropzone from '@components/controls/Dropzone';
import { FileType } from '@components/controls/Dropzone/DropzoneFile';
import Form from '@components/controls/Form';
import Select from '@components/controls/Select';
import Tabs from '@components/controls/Tabs';
import Textarea from '@components/controls/Textarea';
import Tooltip, { ContentBtn } from '@components/controls/Tooltip';
import AutocompleteAsync from '@components/controls/future/AutocompleteAsync';
import Popup from '@components/controls/future/Popup';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, scale, typography, useTheme } from '@scripts/gds';
import { formatPrice, fromKopecksToRouble, getOptionName, prepareTelValue, toSelectItems } from '@scripts/helpers';
import { useLinkCSS, useMedia, useTabs } from '@scripts/hooks';

import CheckIcon from '@icons/small/check.svg';
import KebabIcon from '@icons/small/kebab.svg';

import useRefundsAccess from '../useRefundsAccess';
import { Aside } from './Aside';

const columns: ExtendedColumn[] = [
    {
        accessor: 'id',
        Header: 'ID',
    },
    {
        accessor: 'photo',
        Header: 'Фото',
        Cell: ({ value }) => <Cell value={value} type="photo" />,
    },
    {
        accessor: 'name',
        Header: 'Название',
        Cell: ({ value }) => {
            const linkStyles = useLinkCSS();
            return (
                <>
                    {value?.link ? (
                        <p css={{ marginBottom: scale(1) }}>
                            <Link passHref href={value.link}>
                                <a css={linkStyles}>{value.name}</a>
                            </Link>
                        </p>
                    ) : (
                        <p css={{ marginBottom: scale(1) }}>{value.name}</p>
                    )}
                </>
            );
        },
    },
    {
        accessor: 'price',
        Header: 'Цена и цена без скидки,  ₽',
        Cell: ({ value }: { value: number[] }) => {
            const { colors } = useTheme();
            return value.map((v, index) => (
                <div key={index} css={index > 0 && { color: colors?.grey700 }}>
                    <Cell value={v} type="price" />
                </div>
            ));
        },
    },
    {
        accessor: 'quantity',
        Header: 'Количество',
    },
    {
        accessor: 'cost',
        Header: 'Стоимость,  ₽',
        Cell: ({ value }) => <Cell value={value} type="price" />,
    },
    {
        accessor: 'refund_qty',
        Header: 'Количество к возврату',
    },
];

export default function RefundsPage() {
    const { pathname, push, query } = useRouter();
    const { sm } = useMedia();
    const refundId = (query && query.id && +query.id) || 0;

    const { getTabsProps } = useTabs();

    const listLink = pathname.split(`[id]`)[0];

    const {
        canViewDetailPage,
        canViewMainTab,
        canViewProductsTab,
        canViewAttachmentsTab,
        canEditResponsibleForRefund,
        canEditOnlyRefundsData,
        canLoadAttachmentsData,
        canDeleteAttachmentsData,
        canEditRefund,
    } = useRefundsAccess();

    const [isStatusOpen, setIsStatusOpen] = useState(false);
    const [responsibleUserData, setResponsibleUserData] = useState<{ id: number; title: string } | null>(null);
    const closeStatusPopup = useCallback(() => setIsStatusOpen(false), []);

    const { data, isLoading, error } = useRefund(
        refundId,
        ['files', 'items', 'items.product', 'order', 'reasons'],
        canViewDetailPage || canViewMainTab || canViewProductsTab || canViewAttachmentsTab
    );
    const { data: sourcesData } = useOrderSources(canViewDetailPage || canViewProductsTab || canViewMainTab);

    const refund = useMemo(() => data?.data, [data?.data]);
    const { data: customerData } = useCustomer(refund?.order.customer_id, canViewDetailPage || canViewMainTab);

    const disabledEditResponsible = !(canEditResponsibleForRefund || canEditOnlyRefundsData);

    const { data: author } = useAdminUser(
        String(refund?.manager_id),
        undefined,
        (canViewDetailPage || canViewMainTab) && !!refund?.manager_id
    );

    const patchRefund = usePatchRefund();
    const addFile = useRefundAddFile();
    const deleteFiles = useRefundDeleteFiles();
    const { data: statusData } = useRefundStatuses();

    useError(error);
    useError(patchRefund.error);
    useSuccess(patchRefund.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useError(addFile.error);
    useSuccess(addFile.status === 'success' ? ModalMessages.SUCCESS_SAVE : '');
    useError(deleteFiles.error);
    useSuccess(deleteFiles.status === 'success' ? ModalMessages.SUCCESS_DELETE : '');

    const pageTitle = useMemo(
        () =>
            canViewDetailPage || canViewMainTab || canViewProductsTab || canViewAttachmentsTab
                ? `Заявка на возврат ${refund?.id || ''}`
                : '',
        [canViewDetailPage, canViewMainTab, canViewProductsTab, canViewAttachmentsTab, refund?.id]
    );

    const initialValues = useMemo(
        () => ({
            files_to_delete: [] as number[],
            files: (refund?.files?.map(f => ({ id: f.id, name: f.original_name, file: f.file })) || []) as FileType[],
            responsible_id: responsibleUserData ? responsibleUserData.id : refund?.responsible_id,
        }),
        [refund?.files, refund?.responsible_id, responsibleUserData]
    );

    const products = useMemo(
        () =>
            refund?.items?.map(item => ({
                id: item.id,
                photo: item.product?.main_image_file,
                name: {
                    name: item.name,
                    code: item.product_data?.barcode,
                    link: item?.product?.id ? `/products/catalog/${item?.product?.id}` : null,
                },
                price: [item.price_per_one, item.cost_per_one],
                quantity: item.qty,
                cost: item.price,
                refund_qty: item.refund_qty,
            })) || [],
        [refund?.items]
    );

    useEffect(() => {
        const fetchuser = async (id: number) => {
            const res = await getUserById(id);
            setResponsibleUserData(res[0]);
        };

        if (disabledEditResponsible && refund?.responsible_id && !responsibleUserData)
            fetchuser(refund?.responsible_id);
    }, [disabledEditResponsible, refund?.responsible_id, responsibleUserData]);

    return (
        <PageWrapper isLoading={isLoading}>
            {canViewDetailPage || canViewMainTab || canViewProductsTab || canViewAttachmentsTab ? (
                <>
                    <Form
                        initialValues={initialValues}
                        enableReinitialize
                        onSubmit={vals => {
                            if (initialValues.responsible_id !== vals.responsible_id) {
                                patchRefund.mutate({
                                    id: refundId,
                                    responsible_id: vals.responsible_id,
                                });
                            }
                            if (vals.files_to_delete.length > 0) {
                                deleteFiles.mutate({
                                    id: refundId,
                                    file_ids: vals.files_to_delete,
                                });
                            }
                            vals.files.forEach((file: FileType) => {
                                if (!file.id) {
                                    addFile.mutate({ id: refundId, file });
                                }
                            });
                        }}
                        css={{ position: 'relative' }}
                    >
                        {({ dirty, values, setFieldValue }: FormikProps<FormikValues>) => (
                            <>
                                <PageLeaveGuard />
                                <PageTemplate
                                    h1={pageTitle}
                                    backlink={{ text: 'Назад' }}
                                    controls={
                                        <>
                                            {refund?.status === RefundStatuses.NEW && (
                                                <Tooltip
                                                    trigger="click"
                                                    theme="light"
                                                    arrow
                                                    placement="bottom-end"
                                                    minWidth={scale(36)}
                                                    content={
                                                        <ul>
                                                            <li>
                                                                <ContentBtn
                                                                    type="edit"
                                                                    onClick={() => setIsStatusOpen(true)}
                                                                >
                                                                    Изменить статус заявки
                                                                </ContentBtn>
                                                            </li>
                                                        </ul>
                                                    }
                                                >
                                                    <Button
                                                        theme="secondary"
                                                        Icon={KebabIcon}
                                                        iconAfter
                                                        css={{ marginRight: scale(1) }}
                                                    >
                                                        Действия
                                                    </Button>
                                                </Tooltip>
                                            )}
                                            <Button
                                                theme="secondary"
                                                onClick={() =>
                                                    push({
                                                        pathname: listLink,
                                                    })
                                                }
                                            >
                                                Закрыть
                                            </Button>
                                            {dirty && <Form.Reset theme="dangerous">Отменить</Form.Reset>}

                                            <Button theme="secondary" type="submit" disabled={!dirty}>
                                                Применить
                                            </Button>

                                            <Button type="submit" Icon={CheckIcon} iconAfter disabled={!dirty}>
                                                Сохранить
                                            </Button>
                                        </>
                                    }
                                    aside={
                                        <Aside refund={refund} css={{ marginTop: scale(6), [sm]: { marginTop: 0 } }} />
                                    }
                                    customChildren
                                >
                                    <Tabs {...getTabsProps()}  css={{ flexGrow: 1 }}>
                                        <Tabs.List>
                                            {(canViewMainTab || canViewDetailPage) && <Tabs.Tab>Главное</Tabs.Tab>}
                                            {(canViewProductsTab || canViewDetailPage) && <Tabs.Tab>Товары</Tabs.Tab>}
                                            {(canViewAttachmentsTab || canViewDetailPage) && (
                                                <Tabs.Tab>Вложения</Tabs.Tab>
                                            )}
                                        </Tabs.List>
                                        {(canViewMainTab || canViewDetailPage) && (
                                            <Tabs.Panel>
                                                <Block css={{ borderTopLeftRadius: 0 }}>
                                                    <Block.Body>
                                                        <Layout cols={2}>
                                                            <Layout.Item col={2}>
                                                                <Form.Field label="Ответственный" name="responsible_id">
                                                                    <AutocompleteAsync
                                                                        asyncSearchFn={adminUsersSearchFn}
                                                                        asyncOptionsByValuesFn={
                                                                            adminUsersOptionsByValuesFn
                                                                        }
                                                                        disabled={disabledEditResponsible}
                                                                    />
                                                                </Form.Field>
                                                            </Layout.Item>
                                                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                                <Label>Клиент</Label>
                                                                <p>
                                                                    {`${customerData?.data.last_name} ${customerData?.data.first_name} ${customerData?.data.middle_name}`}
                                                                </p>

                                                                <p>
                                                                    <a href={`tel:${customerData?.data.phone}`}>
                                                                        {prepareTelValue(
                                                                            customerData?.data.phone || ''
                                                                        )}
                                                                    </a>
                                                                </p>
                                                                <p>
                                                                    <a href={`mailto:${customerData?.data.email}`}>
                                                                        {customerData?.data.email}
                                                                    </a>
                                                                </p>
                                                            </Layout.Item>

                                                            {refund?.manager_id && (
                                                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                                    <Label>Автор</Label>
                                                                    {author?.data.full_name}
                                                                </Layout.Item>
                                                            )}
                                                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                                <Label>Канал</Label>
                                                                {getOptionName(sourcesData?.data, refund?.source)}
                                                            </Layout.Item>

                                                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                                <Label>Сумма возврата</Label>
                                                                {formatPrice(fromKopecksToRouble(refund?.price || 0))} ₽
                                                            </Layout.Item>

                                                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                                <Label>Причина возврата</Label>
                                                                {new Intl.ListFormat('ru').format(
                                                                    refund?.reasons.map(r => r.name) || []
                                                                )}
                                                            </Layout.Item>
                                                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                                <Label>Комментарий</Label>
                                                                {refund?.user_comment}
                                                            </Layout.Item>
                                                            {refund?.rejection_comment && (
                                                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                                    <Label>Причина отклонения</Label>
                                                                    {refund?.rejection_comment}
                                                                </Layout.Item>
                                                            )}
                                                            {/* Способ возврата Не реализовано на бэкенде */}
                                                            {/* Иноформация о возврате Не реализовано на бэкенде */}
                                                        </Layout>
                                                    </Block.Body>
                                                </Block>
                                            </Tabs.Panel>
                                        )}
                                        {(canViewProductsTab || canViewDetailPage) && (
                                            <Tabs.Panel>
                                                <Block>
                                                    <Block.Body>
                                                        <Table
                                                            columns={columns}
                                                            data={products}
                                                            allowRowSelect={false}
                                                            disableSortBy
                                                        />
                                                    </Block.Body>
                                                </Block>
                                            </Tabs.Panel>
                                        )}
                                        {(canViewAttachmentsTab || canViewDetailPage) && (
                                            <Tabs.Panel>
                                                <Block>
                                                    <Block.Body>
                                                        <Form.Field label="Прикрепите вложения" name="files">
                                                            <Dropzone
                                                                onFileRemove={(index, file) => {
                                                                    if (file?.id) {
                                                                        setFieldValue('files_to_delete', [
                                                                            ...values.files_to_delete,
                                                                            file?.id,
                                                                        ]);
                                                                    }
                                                                }}
                                                                onFileClick={f => downloadFile(f.file, f.name)}
                                                                isDragDisabled={
                                                                    !(canLoadAttachmentsData || canEditOnlyRefundsData)
                                                                }
                                                                isDisableRemove={
                                                                    !(
                                                                        canDeleteAttachmentsData ||
                                                                        canEditOnlyRefundsData
                                                                    )
                                                                }
                                                            />
                                                        </Form.Field>
                                                    </Block.Body>
                                                </Block>
                                            </Tabs.Panel>
                                        )}
                                    </Tabs>
                                </PageTemplate>
                            </>
                        )}
                    </Form>
                    <Popup open={isStatusOpen} onClose={closeStatusPopup} size="sm">
                        <Popup.Header title="Изменить статус заявки" />

                        <Form
                            initialValues={{ status: '', rejection_comment: '' }}
                            onSubmit={vals => {
                                patchRefund.mutate({
                                    id: refundId,
                                    status: +vals.status,
                                    rejection_comment: vals.rejection_comment || undefined,
                                });
                                closeStatusPopup();
                            }}
                            validationSchema={Yup.object().shape({
                                status: Yup.number().required(ErrorMessages.REQUIRED),
                                rejection_comment: Yup.string().when('status', {
                                    is: (status: number) => status === RefundStatuses.REJECTED,
                                    then: Yup.string().required(ErrorMessages.REQUIRED),
                                    otherwise: Yup.string(),
                                }),
                            })}
                        >
                            {({ values }: FormikProps<FormikValues>) => (
                                <>
                                    <Popup.Content>
                                        <Form.Field
                                            label="Выберите статус"
                                            name="status"
                                            css={{ marginBottom: scale(2) }}
                                        >
                                            <Select
                                                simple
                                                items={toSelectItems(
                                                    statusData?.data.filter(
                                                        s =>
                                                            s.id === RefundStatuses.CONFIRMED ||
                                                            s.id === RefundStatuses.REJECTED
                                                    )
                                                )}
                                            />
                                        </Form.Field>
                                        {values.status === RefundStatuses.REJECTED && (
                                            <Form.FastField
                                                label="Причина отклонения"
                                                name="rejection_comment"
                                                disabled={!canEditRefund}
                                            >
                                                <Textarea minRows={3} />
                                            </Form.FastField>
                                        )}
                                    </Popup.Content>
                                    <Popup.Footer>
                                        <Button theme="secondary" onClick={closeStatusPopup}>
                                            Закрыть
                                        </Button>
                                        {values.status === RefundStatuses.REJECTED && (
                                            <Button theme="dangerous" type="submit">
                                                Отменить заказ
                                            </Button>
                                        )}
                                        {values.status === RefundStatuses.CONFIRMED && (
                                            <Button type="submit">Подтвердить заказ</Button>
                                        )}
                                    </Popup.Footer>
                                </>
                            )}
                        </Form>
                    </Popup>
                </>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра деталей возврата
                </h1>
            )}
        </PageWrapper>
    );
}
