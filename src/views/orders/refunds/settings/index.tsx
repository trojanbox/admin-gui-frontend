import { useCallback, useMemo, useState } from 'react';
import * as Yup from 'yup';

import { RefundReason, useCreateRefundReasons, usePatchRefundReasons, useRefundReasons } from '@api/orders';

import { useError, useSuccess } from '@context/modal';

import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Table, {
    Cell,
    Data,
    ExtendedColumn,
    TableHeader,
    TooltipContentProps,
    getSettingsColumn,
} from '@components/Table';
import Form from '@components/controls/Form';
import Textarea from '@components/controls/Textarea';
import Popup from '@components/controls/future/Popup';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, scale, typography } from '@scripts/gds';

import PlusIcon from '@icons/plus.svg';

import useRefundsAccess from '../useRefundsAccess';

const columns: ExtendedColumn[] = [
    {
        accessor: 'id',
        Header: 'ID',
    },
    {
        accessor: 'code',
        Header: 'Символьный код',
    },
    {
        accessor: 'name',
        Header: 'Название',
    },
    {
        accessor: 'description',
        Header: 'Описание',
    },
    {
        accessor: 'created_at',
        Header: 'Дата создания',
        Cell: ({ value }) => <Cell value={value} type="datetime" />,
    },
    {
        accessor: 'updated_at',
        Header: 'Дата изменения',
        Cell: ({ value }) => <Cell value={value} type="datetime" />,
    },
    { ...getSettingsColumn({ columnsToDisable: [] }) },
];

export default function RefundSettings() {
    const [isOpen, setIsOpen] = useState(false);
    const [activeRow, setActiveRow] = useState<RefundReason | null>(null);
    const closePopup = useCallback(() => {
        setIsOpen(false);
        // чтобы не портить анимацию
        setTimeout(() => setActiveRow(null), 300);
    }, []);

    const { canViewTableRefunds, canCreateNewRefund, canEditRefund } = useRefundsAccess();

    const { data, error } = useRefundReasons();
    const createReason = useCreateRefundReasons();
    const updateReason = usePatchRefundReasons();
    useError(error);
    useError(createReason.error);
    useError(updateReason.error);
    useError(createReason.error);
    useSuccess(createReason.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(updateReason.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');

    const editRow = useCallback((row: Data | Data[] | undefined) => {
        if (row && !Array.isArray(row)) {
            setActiveRow(row as RefundReason);
            setIsOpen(true);
        }
    }, []);

    const tooltipContent: TooltipContentProps[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Изменить причину',
                action: editRow,
                onDisable: () => canEditRefund,
            },
        ],
        [canEditRefund, editRow]
    );

    const initialValues = useMemo(
        () => ({
            code: activeRow?.code || '',
            name: activeRow?.name || '',
            description: activeRow?.description || '',
        }),
        [activeRow]
    );

    return (
        <PageWrapper h1={canViewTableRefunds ? 'Настройка причин возврата' : ''}>
            {canViewTableRefunds ? (
                <>
                    <Block>
                        <Block.Body>
                            <Table
                                renderHeader={() => (
                                    <TableHeader css={{ justifyContent: 'space-between' }}>
                                        <span>Всего причин: {data?.data.length}</span>
                                        {canCreateNewRefund && (
                                            <Button Icon={PlusIcon} onClick={() => setIsOpen(true)}>
                                                Добавить причину
                                            </Button>
                                        )}
                                    </TableHeader>
                                )}
                                columns={columns}
                                data={data?.data || []}
                                disableSortBy
                                allowRowSelect={false}
                                tooltipContent={tooltipContent}
                                onRowClick={canEditRefund ? editRow : undefined}
                            />
                        </Block.Body>
                    </Block>
                    <Popup onClose={closePopup} open={isOpen}>
                        <Popup.Header title={`${activeRow ? 'Редактирование' : 'Создание'} причины возврата`} />
                        <Form
                            initialValues={initialValues}
                            validationSchema={Yup.object().shape({
                                code: Yup.string().required(ErrorMessages.REQUIRED),
                                name: Yup.string().required(ErrorMessages.REQUIRED),
                            })}
                            onSubmit={({ code, name, description }) => {
                                const newData = {
                                    code: (code as string).toUpperCase(),
                                    name,
                                    description: description || null,
                                };
                                if (activeRow) {
                                    updateReason.mutate({ id: activeRow.id, ...newData });
                                } else {
                                    createReason.mutate(newData);
                                }
                                closePopup();
                            }}
                        >
                            <Popup.Content>
                                <Form.FastField
                                    name="name"
                                    label="Название причины возврата"
                                    css={{ marginBottom: scale(2) }}
                                />
                                <Form.FastField
                                    name="code"
                                    label="Символьный код причины возврата"
                                    css={{ marginBottom: scale(2) }}
                                />
                                <Form.FastField name="description" label="Детальное описание причины возврата">
                                    <Textarea minRows={2} />
                                </Form.FastField>
                            </Popup.Content>
                            <Popup.Footer>
                                <Button theme="secondary" onClick={closePopup}>
                                    Отменить
                                </Button>
                                <Button type="submit">{activeRow ? 'Обновить' : 'Создать'}</Button>
                            </Popup.Footer>
                        </Form>
                    </Popup>
                </>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра настроек причин возврата
                </h1>
            )}
        </PageWrapper>
    );
}
