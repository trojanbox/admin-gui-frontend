import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { Refund, useOrderSources, useRefundStatuses, useRefundsSearch } from '@api/orders';
import { adminUsersOptionsByValuesFn, adminUsersSearchFn, useAdminUsers } from '@api/units';

import { useError } from '@context/modal';

import Block from '@components/Block';
import Circle from '@components/Circle';
import PageWrapper from '@components/PageWrapper';
import Table, {
    Cell,
    Data,
    ExtendedColumn,
    TableEmpty,
    TableFooter,
    TableHeader,
    TooltipContentProps,
    getSettingsColumn,
} from '@components/Table';
import Form, { NuberFieldValue } from '@components/controls/Form';
import MultiSelect from '@components/controls/MultiSelect';
import AutocompleteAsync from '@components/controls/future/AutocompleteAsync';

// import CalendarRange from '@components/controls/CalendarRange';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { getOptionName, getTotal, getTotalPages, toSelectItems } from '@scripts/helpers';
import { useActivePage, useFiltersHelper, useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';

import ResetIcon from '@icons/small/reset.svg';
import SettingIcon from '@icons/small/settings.svg';

import { getRefundStatusColor } from './helpers';
import useRefundsAccess from './useRefundsAccess';

const columns: ExtendedColumn[] = [
    {
        accessor: 'id',
        Header: 'ID',
    },
    {
        accessor: 'source',
        Header: 'Канал',
    },
    {
        accessor: 'responsible',
        Header: 'Ответственный',
        Cell: ({ value }) => value || '-',
    },
    {
        accessor: 'reasons',
        Header: 'Причина возврата',
        Cell: ({ value }: { value: Refund['reasons'] }) => <Cell value={value?.map(r => r.name)} type="array" />,
    },
    {
        accessor: 'order_number',
        Header: 'Номер заказа',
    },
    {
        accessor: 'status',
        Header: 'Статус',
        Cell: ({ value }: { value: { id: number; name: string } }) => (
            <>
                <Circle css={{ marginRight: scale(1, true), background: getRefundStatusColor(value.id) }} />
                {value.name}
            </>
        ),
    },
    {
        accessor: 'created_at',
        Header: 'Дата и время создания',
        Cell: ({ value }) => <Cell value={value} type="datetime" />,
    },
    getSettingsColumn(),
];

type Values = {
    id: NuberFieldValue;
    source: number[];
    order_id: NuberFieldValue;
    responsible_id: number | null;
    status: number[];
};

const emptyInitialValues: Values = {
    id: '',
    source: [],
    order_id: '',
    responsible_id: null,
    status: [],
    // client: '',
    // date_from: NaN,
    // date_to: NaN,
};

export default function Refunds() {
    const { push, pathname } = useRouter();

    const { sort, itemsPerPageCount, setItemsPerPageCount, setSort } = useTableList({});

    const { canViewListingAndFilters, canViewDetailPage, canViewTableRefunds } = useRefundsAccess();

    const page = useActivePage();
    const { initialValues: values, filtersActive, URLHelper } = useFiltersHelper<Values>(emptyInitialValues);

    const { data, error } = useRefundsSearch({
        filter: {
            id: values.id || undefined,
            order_id: values.order_id || undefined,
            responsible_id: values.responsible_id || undefined,
            status: values.status.length > 0 ? values.status : undefined,
            source: values.source.length > 0 ? values.source : undefined,
        },
        include: ['reasons', 'order'],
        sort: sort ? [sort] : undefined,
        pagination: {
            type: 'offset',
            limit: itemsPerPageCount,
            offset: (page - 1) * itemsPerPageCount,
        },
    });
    useError(error);

    const pages = getTotalPages(data, itemsPerPageCount);
    const total = getTotal(data);
    useRedirectToNotEmptyPage({ activePage: page, itemsPerPageCount, total });

    const { data: sourcesData, error: sourcesError } = useOrderSources();
    useError(sourcesError);

    const { data: statusData, error: statusesError } = useRefundStatuses();
    useError(statusesError);

    const responsible = useMemo(
        () => [...new Set(data?.data.map(d => d.responsible_id) || [])].filter(Boolean),
        [data?.data]
    );

    const { data: responsibleData } = useAdminUsers({ filter: { id: responsible as number[] } });

    const tableData = useMemo(
        () =>
            data?.data.map(({ source, status, order, responsible_id, ...rest }) => ({
                ...rest,
                source: getOptionName(sourcesData?.data, source),
                status: {
                    name: getOptionName(statusData?.data, status),
                    id: status,
                },
                order_number: order?.number,
                responsible: responsibleData?.data.find(r => r.id === responsible_id)?.full_name,
            })) || [],
        [data?.data, responsibleData?.data, sourcesData?.data, statusData?.data]
    );

    const onRowClick = useCallback(
        (row: Data | Data[] | undefined) => {
            if (!Array.isArray(row) && canViewDetailPage) {
                push(`${pathname}/${row?.id}`);
            }
        },
        [pathname, push, canViewDetailPage]
    );

    const tooltipContent: TooltipContentProps[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать заявку',
                action: onRowClick,
                onDisable: () => canViewDetailPage,
            },
        ],
        [canViewDetailPage, onRowClick]
    );

    return (
        <PageWrapper h1={canViewListingAndFilters ? 'Возвраты' : ''}>
            {canViewListingAndFilters ? (
                <>
                    <Block css={{ marginBottom: scale(2) }}>
                        <Form<Values> initialValues={values} onSubmit={URLHelper} enableReinitialize>
                            <Block.Body css={{ borderRadius: 0 }}>
                                <Layout cols={{ xxxl: 4, md: 3, sm: 2, xs: 1 }}>
                                    <Layout.Item col={1}>
                                        <Form.FastField label="ID" name="id" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field label="Канал" name="source">
                                            <MultiSelect items={toSelectItems(sourcesData?.data)} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.FastField label="ID заказа" name="order_id" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.FastField label="Ответственный" name="responsible_id">
                                            <AutocompleteAsync
                                                asyncSearchFn={adminUsersSearchFn}
                                                asyncOptionsByValuesFn={adminUsersOptionsByValuesFn}
                                            />
                                        </Form.FastField>
                                    </Layout.Item>
                                    {/* <Layout.Item col={1}>
                                <Form.FastField label="Клиент" name="client" />
                            </Layout.Item> */}
                                    <Layout.Item col={1}>
                                        <Form.Field label="Статус возврата" name="status">
                                            <MultiSelect items={toSelectItems(statusData?.data)} />
                                        </Form.Field>
                                    </Layout.Item>

                                    {/* <Layout.Item col={2}>
                                <Form.FastField label="Дата создания заявки" name="date">
                                    <CalendarRange nameFrom="date_from" nameTo="date_to" />
                                </Form.FastField>
                            </Layout.Item> */}
                                </Layout>
                            </Block.Body>

                            <Block.Footer css={{ justifyContent: 'flex-end' }}>
                                <div css={{ display: 'flex' }}>
                                    {filtersActive && (
                                        <Form.Reset
                                            theme="fill"
                                            Icon={ResetIcon}
                                            type="button"
                                            initialValues={emptyInitialValues}
                                            onClick={() => push(pathname)}
                                        >
                                            Сбросить
                                        </Form.Reset>
                                    )}

                                    <Button theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                        Применить
                                    </Button>
                                </div>
                            </Block.Footer>
                        </Form>
                    </Block>
                    <Block>
                        <Block.Body>
                            <Table
                                columns={columns}
                                data={tableData}
                                onSortingChange={setSort}
                                renderHeader={() => (
                                    <TableHeader css={{ justifyContent: 'space-between' }}>
                                        <span>Всего заявок: {total}</span>
                                        {canViewTableRefunds && (
                                            <Link href="/orders/refunds/settings" passHref>
                                                <Button as="a" theme="fill" Icon={SettingIcon}>
                                                    Настройка причин возврата
                                                </Button>
                                            </Link>
                                        )}
                                    </TableHeader>
                                )}
                                tooltipContent={tooltipContent}
                                onRowClick={onRowClick}
                            />
                            {tableData.length === 0 ? (
                                <TableEmpty
                                    filtersActive={filtersActive}
                                    titleWithFilters="Заявки на возврат не найдены"
                                    titleWithoutFilters="Заявок на возврат нет"
                                />
                            ) : (
                                <TableFooter
                                    pages={pages}
                                    itemsPerPageCount={itemsPerPageCount}
                                    setItemsPerPageCount={setItemsPerPageCount}
                                />
                            )}
                        </Block.Body>
                    </Block>
                </>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра списка возвратов
                </h1>
            )}
        </PageWrapper>
    );
}
