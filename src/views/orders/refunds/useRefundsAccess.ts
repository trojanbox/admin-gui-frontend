import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

const useRefundsAccess = () => {
    const { data: userData } = useCurrentUser();

    const viewListingAndFilters = 201;
    const viewDetailPage = 202;
    const viewMainTab = 203;
    const viewProductsTab = 204;
    const viewAttachmentsTab = 205;
    const editOnlyRefundsData = 206;
    const editResponsibleForRefund = 207;
    const loadAttachmentsData = 208;
    const deleteAttachmentsData = 209;
    const viewTableRefunds = 210;
    const createNewRefund = 211;
    const editRefund = 212;

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canViewDetailPage = useMemo(
        () => !!userData?.data.rights_access.includes(viewDetailPage),
        [userData?.data.rights_access]
    );
    const canViewMainTab = useMemo(
        () => !!userData?.data.rights_access.includes(viewMainTab),
        [userData?.data.rights_access]
    );
    const canViewProductsTab = useMemo(
        () => !!userData?.data.rights_access.includes(viewProductsTab),
        [userData?.data.rights_access]
    );
    const canViewAttachmentsTab = useMemo(
        () => !!userData?.data.rights_access.includes(viewAttachmentsTab),
        [userData?.data.rights_access]
    );
    const canEditOnlyRefundsData = useMemo(
        () => !!userData?.data.rights_access.includes(editOnlyRefundsData),
        [userData?.data.rights_access]
    );
    const canEditResponsibleForRefund = useMemo(
        () => !!userData?.data.rights_access.includes(editResponsibleForRefund),
        [userData?.data.rights_access]
    );
    const canLoadAttachmentsData = useMemo(
        () => !!userData?.data.rights_access.includes(loadAttachmentsData),
        [userData?.data.rights_access]
    );
    const canDeleteAttachmentsData = useMemo(
        () => !!userData?.data.rights_access.includes(deleteAttachmentsData),
        [userData?.data.rights_access]
    );
    const canViewTableRefunds = useMemo(
        () => !!userData?.data.rights_access.includes(viewTableRefunds),
        [userData?.data.rights_access]
    );
    const canCreateNewRefund = useMemo(
        () => !!userData?.data.rights_access.includes(createNewRefund),
        [userData?.data.rights_access]
    );
    const canEditRefund = useMemo(
        () => !!userData?.data.rights_access.includes(editRefund),
        [userData?.data.rights_access]
    );

    return {
        canViewListingAndFilters,
        canViewDetailPage,
        canViewMainTab,
        canViewProductsTab,
        canViewAttachmentsTab,
        canEditOnlyRefundsData,
        canEditResponsibleForRefund,
        canLoadAttachmentsData,
        canDeleteAttachmentsData,
        canViewTableRefunds,
        canCreateNewRefund,
        canEditRefund,
    };
};

export default useRefundsAccess;
