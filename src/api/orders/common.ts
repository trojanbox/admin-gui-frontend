import { useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonOption, CommonResponse, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import { OMSSettings, OMSSettingsMutate } from './types';

export const useOrderStatuses = () =>
    useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: 'orderStatuses',
        queryFn: () => apiClient.get('orders/order-statuses'),
    });

export const useOrderSources = (enabled: boolean = true) =>
    useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: 'orderSources',
        queryFn: () => apiClient.get('orders/order-sources'),
        enabled,
    });

export const usePaymentMethods = () =>
    useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: 'paymentMethods',
        queryFn: () => apiClient.get('orders/payment-methods'),
    });

export const usePaymentStatuses = () =>
    useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: 'paymentStatuses',
        queryFn: () => apiClient.get('orders/payment-statuses'),
    });

export const useDeliveryStatuses = () =>
    useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: 'deliveryStatuses',
        queryFn: () => apiClient.get('orders/delivery-statuses'),
    });

export const useShipmentStatuses = () =>
    useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: 'shipmentStatuses',
        queryFn: () => apiClient.get('orders/shipment-statuses'),
    });

export const useRefundStatuses = () =>
    useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: 'refundStatuses',
        queryFn: () => apiClient.get('orders/refund-statuses'),
    });

const settingsUpdater = (
    prevSettings: CommonResponse<OMSSettings[]> | undefined,
    newSettings: CommonResponse<OMSSettings[]>
) =>
    prevSettings
        ? {
              ...prevSettings,
              data: prevSettings.data.map(setting => {
                  const newItem = newSettings.data.find(n => n.id === setting.id);
                  return newItem || setting;
              }),
          }
        : prevSettings;

const omsSettingsURL = 'orders/oms-settings';
const omsSettingsKey = 'orders-oms-settings';

export const useOmsSettings = () =>
    useQuery<CommonResponse<OMSSettings[]>, FetchError>({
        queryKey: [omsSettingsKey],
        queryFn: () => apiClient.get(omsSettingsURL),
    });

export const useOmsSettingsMeta = (enabled = true) =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: `${omsSettingsKey}-meta`,
        queryFn: () => apiClient.get(`${omsSettingsURL}:meta`),
        enabled,
    });

export const useUpdateOMSSettings = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<OMSSettings[]>, FetchError, OMSSettingsMutate[]>(
        data => apiClient.patch(omsSettingsURL, { data: { settings: data } }),
        {
            onSuccess: newSettings => {
                queryClient.setQueryData<CommonResponse<OMSSettings[]> | undefined>([omsSettingsKey], prevSettings =>
                    settingsUpdater(prevSettings, newSettings)
                );
            },
        }
    );
};

const basketsSettingsURL = 'orders/baskets-settings';
const basketsSettingsKey = 'orders-baskets-settings';

export const useBasketsSettings = () =>
    useQuery<CommonResponse<OMSSettings[]>, FetchError>({
        queryKey: [basketsSettingsKey],
        queryFn: () => apiClient.get(basketsSettingsURL),
    });

export const useBasketSettingsMeta = (enabled = true) =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: `${basketsSettingsKey}-meta`,
        queryFn: () => apiClient.get(`${basketsSettingsURL}:meta`),
        enabled,
    });

export const useUpdateBasketsSettings = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<OMSSettings[]>, FetchError, OMSSettingsMutate[]>(
        data => apiClient.patch(basketsSettingsURL, { data: { settings: data } }),
        {
            onSuccess: newSettings => {
                queryClient.setQueryData<CommonResponse<OMSSettings[]> | undefined>(
                    [basketsSettingsKey],
                    prevSettings => settingsUpdater(prevSettings, newSettings)
                );
            },
        }
    );
};
