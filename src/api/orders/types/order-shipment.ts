import { Seller, Store } from '@api/units';

import { BasketItem } from './order-basket';

export interface OrderShipment {
    id: number;
    number: string;
    delivery_id: number;
    seller_id: number;
    store_id: number;
    status_at: string;
    cost: number;
    width: number;
    height: number;
    length: number;
    weight: number;
    created_at: string;
    updated_at: string;
    status: number;
    // TODO: Объект типа Delivery. Сравнить не счем было, если в oas появиться, то нужно добавить
    // delivery: object
    order_items: BasketItem[];
    store: Store;
    seller: Seller;
}
