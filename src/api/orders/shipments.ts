import { useMutation, useQueryClient } from 'react-query';

import { CommonResponse } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import { OrderShipment } from './types/order-shipment';

export const useShipmentChange = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<OrderShipment[]>, FetchError, { id: number; status: number }>(
        ({ id, status }) => apiClient.patch(`orders/shipments/${id}`, { data: { status } }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries('orders');
            },
        }
    );
};
