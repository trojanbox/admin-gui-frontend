import { useQuery } from 'react-query';

import { FetchError, Meta } from '@api/common/types';

import { apiClient } from '..';

export const useCustomerOrdersMeta = (enabled = true) =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'customerOrdersMeta',
        queryFn: () => apiClient.get('customers/orders:meta'),
        enabled,
    });
