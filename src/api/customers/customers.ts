import { useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonResponse, CommonSearchParams, FileUpload, Meta } from '@api/common/types';

import { FetchError, apiClient } from '..';
import { Customer, CustomerMutate, CustomerMutateWithId, CustomersFilter } from './types/customers';

const CUSTOMER_URL = 'customers/customers';

export const useCustomers = (data: CommonSearchParams<CustomersFilter, string | string[]> = {}, enabled = true) =>
    useQuery<CommonResponse<Customer[]>, FetchError>({
        queryKey: ['customers', data],
        queryFn: () => apiClient.post(`${CUSTOMER_URL}:search`, { data }),
        enabled,
    });

export const useCustomersMeta = () =>
    useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: ['customers-meta'],
        queryFn: () => apiClient.get(`${CUSTOMER_URL}:meta`),
    });

export const useCustomer = (id: string | number | undefined | null, enabled: boolean = true) =>
    useQuery<CommonResponse<Customer>, FetchError>({
        queryKey: ['customer', id],
        queryFn: () => apiClient.get(`${CUSTOMER_URL}/${id}`),
        enabled: !!id && enabled,
    });

export const useCreateCustomer = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<Customer>, FetchError, CustomerMutate>(
        data => apiClient.post(CUSTOMER_URL, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(['customers']);
            },
        }
    );
};

export const useUpdateCustomer = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Customer>, FetchError, CustomerMutateWithId>(
        ({ id, ...data }) => apiClient.put(`${CUSTOMER_URL}/${id}`, { method: 'PUT', data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(['customers']);
                queryClient.invalidateQueries(['customer']);
            },
        }
    );
};

export const useDeleteCustomer = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<Customer>, FetchError, number>(id => apiClient.delete(`${CUSTOMER_URL}/${id}`), {
        onSuccess: () => {
            queryClient.invalidateQueries(['customers']);
            queryClient.invalidateQueries(['customer']);
        },
    });
};

export const useChangeCustomerStatus = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<Customer>, FetchError, number>(
        id => apiClient.post(`${CUSTOMER_URL}/${id}:change-status-if-profile-filled`),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(['customers']);
                queryClient.invalidateQueries(['customer']);
            },
        }
    );
};

export const useUploadCustomerAvatar = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<Customer>, FetchError, FileUpload>(
        ({ id, file }) => apiClient.post(`${CUSTOMER_URL}/${id}:upload-avatar`, { data: file }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(['customers']);
                queryClient.invalidateQueries(['customer']);
            },
        }
    );
};

export const useDeleteCustomerAvatar = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<Customer>, FetchError, number>(
        id => apiClient.post(`${CUSTOMER_URL}/${id}:delete-avatar`),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(['customers']);
                queryClient.invalidateQueries(['customer']);
            },
        }
    );
};

export const useCustomerDeletePersonalData = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<any>, FetchError, number>(
        id => apiClient.post(`${CUSTOMER_URL}/${id}:delete-personal-data`),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(['customers']);
                queryClient.invalidateQueries(['customer']);
            },
        }
    );
};
