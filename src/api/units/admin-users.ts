import { useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonResponse, FilterResponse, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import { AdminRoleAddFields, AdminRoleDeleteFields, AdminUser, AdminUserSearch } from './types';

const ADMIN_USERS_BASE_URL = 'units/admin-users';
const ADMIN_USER_BASE_URL = 'units/admin-user';
const QUERY_KEY_ADMINS = 'admin-users';
const QUERY_KEY_ADMIN = 'admin-user';

export const useAdminUsers = (data: AdminUserSearch, enabled: boolean = true) =>
    useQuery<CommonResponse<AdminUser[]>, FetchError>({
        queryKey: [QUERY_KEY_ADMINS, data],
        queryFn: () => apiClient.post(`${ADMIN_USERS_BASE_URL}:search`, { data }),
        enabled,
    });

export const useAdminUsersMeta = (enabled: boolean = true) =>
    useQuery<{ data: Meta }, FetchError>({
        enabled,
        queryKey: `${QUERY_KEY_ADMINS}-meta`,
        queryFn: () => apiClient.get(`${ADMIN_USERS_BASE_URL}:meta`),
    });

export const useAdminUser = (id?: string, include?: string | string[], enabled: boolean = true) =>
    useQuery<CommonResponse<AdminUser>, FetchError>({
        queryKey: [QUERY_KEY_ADMIN, id],
        queryFn: () => apiClient.get(`${ADMIN_USERS_BASE_URL}/${id}${include ? `?include=${include}` : ''}`),
        enabled,
    });

export const useCreateAdminUser = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<AdminUser>, FetchError, Partial<AdminUser>>(
        data => apiClient.post(ADMIN_USERS_BASE_URL, { data }),
        { onSuccess: () => queryClient.invalidateQueries(QUERY_KEY_ADMINS) }
    );
};

export const useUpdateAdminUser = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<AdminUser>, FetchError, Partial<AdminUser>>(
        ({ id, ...data }) => apiClient.patch(`${ADMIN_USERS_BASE_URL}/${id}`, { data }),
        {
            onSuccess: (data, variables) => {
                queryClient.invalidateQueries(QUERY_KEY_ADMINS);
                if (variables?.disableInvalidate === false) queryClient.invalidateQueries(QUERY_KEY_ADMIN);
            },
        }
    );
};

export const useAddAdminUserRole = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<AdminUser>, FetchError, AdminRoleAddFields>(
        ({ id, ...data }) => apiClient.post(`${ADMIN_USERS_BASE_URL}/${id}:add-roles`, { data }),
        {
            onSuccess: (data, variables) => {
                if (variables?.disableInvalidate === false) queryClient.invalidateQueries(QUERY_KEY_ADMIN);
            },
        }
    );
};

export const useDeleteAdminUserRole = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<AdminUser>, FetchError, AdminRoleDeleteFields>(
        ({ id, ...data }) => apiClient.post(`${ADMIN_USERS_BASE_URL}/${id}:delete-role`, { data }),
        {
            onSuccess: (data, variables) => {
                if (variables?.disableInvalidate === false) queryClient.invalidateQueries(QUERY_KEY_ADMIN);
            },
        }
    );
};

export const useRefreshPasswordTokenAdminUser = () =>
    useMutation<CommonResponse<null>, FetchError, number>(id =>
        apiClient.post(`${ADMIN_USERS_BASE_URL}/${id}:refresh-password-token`)
    );

export const useDeleteAdminUser = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, number>(
        id => apiClient.delete(`${ADMIN_USERS_BASE_URL}/${id}`),
        { onSuccess: () => queryClient.invalidateQueries(QUERY_KEY_ADMINS) }
    );
};

export const getAdminUserEnumValues = async (query: string) => {
    const result: CommonResponse<FilterResponse> = await apiClient.post(`${ADMIN_USER_BASE_URL}-enum-values:search`, {
        data: {
            filter: {
                query,
                id: [],
            },
        },
    });
    if (result && result.data) {
        return result.data.map(i => ({
            value: i.id,
            label: i.title,
        }));
    }
    return [];
};

export const getUserById = async (id: number) => {
    try {
        const res = await apiClient.post(`${ADMIN_USER_BASE_URL}-enum-values:search`, {
            data: { filter: { query: '', id: [id] } },
        });
        return res.data;
    } catch {
        return [];
    }
};

export const adminUsersSearchFn = async (query: string) => {
    const res = await apiClient.post(`${ADMIN_USER_BASE_URL}-enum-values:search`, {
        data: { filter: { query } },
    });
    return {
        options: res.data.map((i: { title: string; id: string }) => ({
            key: i.title,
            value: i.id,
        })),
        hasMore: false,
    };
};

export const adminUsersOptionsByValuesFn = async (vals: string[]) => {
    const res = await apiClient.post(`${ADMIN_USER_BASE_URL}-enum-values:search`, {
        data: { filter: { query: '', id: vals } },
    });
    return res.data.map((i: { title: string; id: string }) => ({
        key: i.title,
        value: i.id,
    }));
};
