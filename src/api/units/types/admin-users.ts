import { OffsetPaginationQuery } from '@api/common/types';

import { Role } from './common';

export interface AdminUserMutate {
    active?: boolean;
    login?: string;
    last_name: string;
    first_name: string;
    middle_name: string;
    email: string;
    phone?: string;
    timezone?: string;
}

export interface AdminUser extends AdminUserMutate {
    id: number;
    full_name: string;
    created_at: string;
    updated_at: string;
    roles: Role[];
    /** if we need NOT to invalidate query manual */
    disableInvalidate?: boolean;
}

export interface AdminUserFilter {
    email?: string;
    phone?: string;
    id?: number | number[];
    login?: string;
    active?: boolean;
    role?: number;
}

export interface AdminUserSearch {
    sort?: string[] | string;
    include?: string[];
    pagination?: OffsetPaginationQuery;
    filter?: AdminUserFilter;
}

export interface AdminRoleAddFields {
    id: number;
    roles: number[];
    expires: string | null;
    /** if we need NOT to invalidate query manual */
    disableInvalidate?: boolean;
}

export interface AdminRoleDeleteFields {
    id: number;
    role_id: number;
    /** if we need NOT to invalidate query manual */
    disableInvalidate?: boolean;
}
