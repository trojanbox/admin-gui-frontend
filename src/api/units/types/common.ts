export interface Role {
    id: number;
    title: string;
    created_at: string;
    updated_at: string;
    created_by: string;
    updated_by: string;
    active: boolean;
    rights_access: string[];
    expires: string;
}

export interface CreateRoleData {
    id?: number;
    title: string;
    active: boolean;
    rights_access: number[];
}

export interface RightAccessData {
    section: string;
    items: {
        id: number | string;
        title: string;
    }[];
}