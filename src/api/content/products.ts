import { useMutation, useQueryClient } from 'react-query';

import { FetchError, apiClient } from '..';
import {
    ProductAddNameplatesRequest,
    ProductAddNameplatesResponse,
    ProductDeleteNameplatesRequest,
    ProductDeleteNameplatesResponse,
} from './types/products';

const API_URL = 'cms/products';

export const useProductAddNameplates = (productId: number) => {
    const queryClient = useQueryClient();

    return useMutation<ProductAddNameplatesResponse, FetchError, ProductAddNameplatesRequest>(
        data => apiClient.post(`${API_URL}/${productId}:add-nameplates`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries([`product-${productId}`]);
            },
        }
    );
};

export const useProductDeleteNameplates = (productId: number) => {
    const queryClient = useQueryClient();

    return useMutation<ProductDeleteNameplatesResponse, FetchError, ProductDeleteNameplatesRequest>(
        data => apiClient.delete(`${API_URL}/${productId}:delete-nameplates`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries([`product-${productId}`]);
            },
        }
    );
};
