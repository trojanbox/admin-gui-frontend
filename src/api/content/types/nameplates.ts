import { Product } from '@api/catalog';
import { CommonResponse, CommonSearchParams } from '@api/common/types';

export interface NameplateProduct {
    id: number;
    nameplate_id: number;
    product_id: number;
    created_at: string;
    updated_at: string;
    product: Product;
}

export interface NewNameplate {
    name: string;
    code: string;
    background_color: string;
    text_color: string;
    is_active: boolean;
}

export interface Nameplate extends NewNameplate {
    id: number;
    created_at: string;
    updated_at: string;

    products?: NameplateProduct[];
}

type SearchNameplateFilter = Partial<{
    id: number | number[];
    product_id: number;
    name_like: string;
    is_active: boolean;
    created_at_from: string;
    created_at_to: string;
    update_at_from: string;
    update_at_to: string;
}>;

export type SearchNameplatesRequest = CommonSearchParams<SearchNameplateFilter, string | string[]>;
export type SearchNameplatesResponse = CommonResponse<Nameplate[]>;

export type SearchNameplateProductsRequest = CommonSearchParams<{
    nameplate_id: number;
}, string | string[]>;
export type SearchNameplateProductsResponse = CommonResponse<NameplateProduct[]>;

export type UpdateNameplateRequest = Partial<NewNameplate> & { id: number };
export type UpdateNameplateResponse = CommonResponse<Nameplate>;

export type CreateNameplateRequest = NewNameplate;
export type CreateNameplateResponse = CommonResponse<Nameplate>;

export type GetNameplateResponse = CommonResponse<Nameplate>;

export interface NameplateBindProductsRequest {
    ids: number[];
}

export interface NameplateUnbindProductsRequest {
    ids: number[];
}
