import { CommonResponse } from '@api/common/types';

export type ProductAddNameplatesRequest = { ids: number[] };
export type ProductAddNameplatesResponse = CommonResponse<null>;

export type ProductDeleteNameplatesRequest = { ids: number[] };
export type ProductDeleteNameplatesResponse = CommonResponse<null>;
