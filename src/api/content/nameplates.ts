import { useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonResponse, Meta } from '@api/common/types';

import { FetchError, apiClient } from '..';
import {
    CreateNameplateRequest,
    CreateNameplateResponse,
    GetNameplateResponse,
    NameplateBindProductsRequest,
    SearchNameplateProductsRequest,
    SearchNameplateProductsResponse,
    SearchNameplatesRequest,
    SearchNameplatesResponse,
    UpdateNameplateRequest,
    UpdateNameplateResponse,
} from './types/nameplates';

const API_URL = 'cms/nameplates';

const QueryKeys = {
    LIST: 'cms-nameplates',
    DETAILS: (id?: number) => (typeof id === 'number' ? ['cms-nameplate', id] : ['cms-nameplate']),
    META: 'cms-nameplates-meta',
    PRODUCT_LIST: 'cms-nameplate-products',
    PRODUCT_META: 'cms-nameplate-products-meta',
};

export const useNameplate = (
    id: number,
    include: string[] = [],
    enabled = typeof id === 'number' && !Number.isNaN(id)
) =>
    useQuery<GetNameplateResponse, FetchError>({
        enabled,
        queryKey: QueryKeys.DETAILS(id),
        queryFn: () =>
            apiClient.get(`${API_URL}/${id}`, {
                params: {
                    include,
                },
            }),
    });

export const fetchSearchNameplates = (data: SearchNameplatesRequest) =>
    apiClient.post(`${API_URL}:search`, { data }) as Promise<SearchNameplatesResponse>;

export const useSearchNameplates = (data: SearchNameplatesRequest, enabled = true) =>
    useQuery<SearchNameplatesResponse, FetchError>({
        enabled,
        queryKey: [QueryKeys.LIST, data],
        queryFn: () => fetchSearchNameplates(data),
    });

export const useNameplatesMeta = (enabled = true) =>
    useQuery<{ data: Meta }, FetchError>({
        enabled,
        queryKey: [QueryKeys.META],
        queryFn: () => apiClient.get(`${API_URL}:meta`),
    });

export const useNameplateProductsMeta = (enabled = true) =>
    useQuery<{ data: Meta }, FetchError>({
        enabled,
        queryKey: [QueryKeys.PRODUCT_META],
        queryFn: () => apiClient.get(`${API_URL}/nameplate-products:meta`),
    });

export const useSearchNameplateProducts = (
    data: SearchNameplateProductsRequest,
    includes: string[] = [],
    enabled = true
) =>
    useQuery<SearchNameplateProductsResponse, FetchError>({
        enabled,
        queryKey: [QueryKeys.PRODUCT_LIST, data],
        queryFn: () =>
            apiClient.post(`${API_URL}/nameplate-products:search`, {
                params: {
                    include: includes,
                },
                data,
            }),
    });

export const useCreateNameplate = () => {
    const queryClient = useQueryClient();

    return useMutation<CreateNameplateResponse, FetchError, CreateNameplateRequest>(
        data => apiClient.post(`${API_URL}`, { data }),
        {
            onSuccess: response => {
                queryClient.invalidateQueries([QueryKeys.LIST]);
                queryClient.invalidateQueries(QueryKeys.DETAILS(response?.data?.id));
            },
        }
    );
};

export const useUpdateNameplate = () => {
    const queryClient = useQueryClient();

    return useMutation<UpdateNameplateResponse, FetchError, UpdateNameplateRequest>(
        ({ id, ...data }) => apiClient.patch(`${API_URL}/${id}`, { data }),
        {
            onSuccess: response => {
                queryClient.invalidateQueries([QueryKeys.LIST]);
                queryClient.invalidateQueries(QueryKeys.DETAILS(response?.data?.id));
            },
        }
    );
};

export const useDeleteNameplate = () => {
    const queryClient = useQueryClient();

    return useMutation<UpdateNameplateResponse, FetchError, { id: number }>(
        ({ id }) => apiClient.delete(`${API_URL}/${id}`),
        {
            onSuccess: response => {
                queryClient.invalidateQueries([QueryKeys.LIST]);
                queryClient.invalidateQueries(QueryKeys.DETAILS(response?.data?.id));
            },
        }
    );
};

export const useBindNameplateProducts = (nameplateId: number) => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, NameplateBindProductsRequest>(
        data => apiClient.post(`${API_URL}/${nameplateId}:add-products`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries([QueryKeys.PRODUCT_LIST]);
            },
        }
    );
};

export const useUnbindNameplateProducts = (nameplateId: number) => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, NameplateBindProductsRequest>(
        data => apiClient.delete(`${API_URL}/${nameplateId}:delete-products`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries([QueryKeys.PRODUCT_LIST]);
            },
        }
    );
};
