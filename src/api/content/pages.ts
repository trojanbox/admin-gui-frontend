import { useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonResponse, CommonSearchParams, FetchError, Meta } from '@api/common/types';
import { Page, PageFilter, PageMutate, PageMutateWithId } from '@api/content/types/pages';
import { apiClient } from '@api/index';

const PAGE_URL = 'cms/pages';
const PAGE_KEY = 'page';
const LIST_KEY = `${PAGE_KEY}s`;

export const usePages = (data: CommonSearchParams<PageFilter, string>, enabled: boolean = true) =>
    useQuery<CommonResponse<Page[]>, FetchError>({
        enabled,
        queryKey: [LIST_KEY, data],
        queryFn: () => apiClient.post(`${PAGE_URL}:search`, { data }),
    });

export const usePageDetail = (id?: number | string) =>
    useQuery<CommonResponse<Page>, FetchError>({
        enabled: !!id,
        queryKey: [PAGE_KEY, id],
        queryFn: () => apiClient.get(`${PAGE_URL}/${id}`),
    });

export const usePagesMeta = () =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'pagesMeta',
        queryFn: () => apiClient.get(`${PAGE_URL}:meta`),
    });

export const useCreatePage = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Page>, FetchError, PageMutate>(data => apiClient.post(PAGE_URL, { data }), {
        onSuccess: data => {
            queryClient.setQueryData([PAGE_KEY, `${data?.data?.id}`], data);
            queryClient.invalidateQueries(LIST_KEY);
        },
    });
};

export const usePageUpdate = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Page>, FetchError, PageMutateWithId>(
        page => {
            const { id, ...pageData } = page;
            return apiClient.patch(`${PAGE_URL}/${id}`, { data: pageData });
        },
        {
            onSuccess: data => {
                queryClient.setQueryData([PAGE_KEY, `${data?.data?.id}`], data);
                queryClient.invalidateQueries(LIST_KEY);
            },
        }
    );
};

export const usePageDelete = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Page>, FetchError, number>(id => apiClient.delete(`${PAGE_URL}/${id}`), {
        onSuccess: () => queryClient.invalidateQueries(LIST_KEY),
    });
};
