import { useMutation, useQuery, useQueryClient } from 'react-query';

import type { CommonResponse, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import {
    Discount,
    DiscountMassBindProductsParams,
    DiscountMassUnbindProductsParams,
    DiscountProduct,
    DiscountStatus,
    DiscountType,
    DiscountValueType,
    DiscountsStatusFormData,
    NewDiscount,
    SearchDiscountProductsRequest,
    SearchDiscountsRequest,
    UpdateDiscount,
} from './types';

export const DISCOUNTS_BASE_URL = 'marketing/discounts';
export const DISCOUNT_BASE_URL = 'marketing/discount';

export const useDiscounts = (data: SearchDiscountsRequest = {}, enabled = true) =>
    useQuery<CommonResponse<Discount[]>, FetchError>({
        queryKey: ['discounts', data],
        queryFn: () => apiClient.post(`${DISCOUNTS_BASE_URL}:search`, { data }),
        enabled,
    });

export const useDiscountsMeta = () =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'discounts-meta',
        queryFn: () => apiClient.get(`${DISCOUNTS_BASE_URL}:meta`),
    });

export const useDiscountsStatusChange = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, DiscountsStatusFormData>(
        data => apiClient.post(`${DISCOUNTS_BASE_URL}:mass-status-update`, { data }),
        {
            onSuccess: () => queryClient.invalidateQueries('discounts'),
        }
    );
};

export const useDiscount = (id?: number | null, include?: string) =>
    useQuery<CommonResponse<Discount>, FetchError>({
        queryKey: ['discount', id],
        queryFn: () => apiClient.get(`${DISCOUNTS_BASE_URL}/${id}${include ? `?include=${include}` : ''}`),
        enabled: !!id,
    });

export const useDiscountCreate = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Discount>, FetchError, NewDiscount>(
        data => apiClient.post(`${DISCOUNTS_BASE_URL}`, { data }),
        {
            onSuccess: () => queryClient.invalidateQueries('discounts'),
        }
    );
};

export const useDiscountEdit = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Discount>, FetchError, UpdateDiscount>(
        data => apiClient.patch(`${DISCOUNTS_BASE_URL}/${data.id}`, { data }),
        {
            onSuccess: res => {
                queryClient.invalidateQueries('discounts');
                queryClient.invalidateQueries(['discount', res.data?.id]);
            },
        }
    );
};

export const useDiscountBindProducts = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, DiscountMassBindProductsParams>(
        ({ discount_id: id, ...data }) => apiClient.post(`${DISCOUNTS_BASE_URL}/${id}/products`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries('discounts');
                queryClient.invalidateQueries(['discount']);
                queryClient.invalidateQueries(['discounts-products']);
            },
        }
    );
};

export const useDiscountUnbindProducts = (discountId: number) => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, DiscountMassUnbindProductsParams>(
        data => apiClient.delete(`${DISCOUNTS_BASE_URL}/${discountId}/products`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries('discounts');
                queryClient.invalidateQueries(['discount']);
                queryClient.invalidateQueries(['discounts-products']);
            },
        }
    );
};

export const useDeleteDiscount = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, number | string>(
        id => apiClient.delete(`${DISCOUNTS_BASE_URL}/${id}`),
        {
            onSuccess: () => {
                queryClient.invalidateQueries('discounts');
            },
        }
    );
};

export const useDiscountTypes = () =>
    useQuery<CommonResponse<DiscountType[]>, FetchError>({
        queryKey: ['discount-types'],
        queryFn: () => apiClient.get(`${DISCOUNT_BASE_URL}-types`),
    });

export const useDiscountStatuses = () =>
    useQuery<CommonResponse<DiscountStatus[]>, FetchError>({
        queryKey: ['discount-statuses'],
        queryFn: () => apiClient.get(`${DISCOUNT_BASE_URL}-statuses`),
    });

export const useDiscountValueTypes = () =>
    useQuery<CommonResponse<DiscountValueType[]>, FetchError>({
        queryKey: ['discount-value-types'],
        queryFn: () => apiClient.get(`${DISCOUNT_BASE_URL}-value-types`),
    });

export const useDiscountProductsMeta = () =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'discounts-products-meta',
        queryFn: () => apiClient.get(`${DISCOUNTS_BASE_URL}/discount-products:meta`),
    });

export const useDiscountProducts = (data: SearchDiscountProductsRequest, include: string[] = [], enabled = true) =>
    useQuery<CommonResponse<DiscountProduct[]>, FetchError>({
        queryKey: ['discounts-products', data],
        queryFn: () =>
            apiClient.post(`${DISCOUNTS_BASE_URL}/discount-products:search`, {
                data,
                params: {
                    include,
                },
            }),
        enabled,
    });
