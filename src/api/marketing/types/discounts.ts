import { Product } from '@api/catalog';
import { CommonSearchParams, OffsetPaginationQuery } from '@api/common/types';

export interface DiscountOffer {
    id?: number;
    discount_id: number;
    offer_id: number;
    except: boolean;
}

export interface DiscountBrand {
    id?: number;
    discount_id: number;
    brand_id: number;
    except: boolean;
}

export interface DiscountCategory {
    id?: number;
    discount_id: number;
    category_id: number;
}
export interface DiscountCondition {
    id?: number;
    discount_id: number;
    type: number;
    condition?: {};
}

export interface NewDiscount {
    /**
     * Тип скидки
     * @see DiscountType
     */
    type: number;
    name: string;
    /**
     * Тип значения скидки
     * @see DiscountValueType
     */
    value_type: number;
    value: number;

    /**
     * Статус скидки
     * @see DiscountStatus
     */
    status: number;

    start_date?: string;
    end_date?: string;

    /**
     * Скидка действительна только по промокоду
     */
    promo_code_only: boolean;
}

export type UpdateDiscount = Partial<
    Pick<NewDiscount, 'name' | 'value_type' | 'value' | 'status' | 'start_date' | 'end_date' | 'promo_code_only'>
> & {
    id: number;
};

export interface Discount extends NewDiscount {
    id: number;
    created_at: string;
    updated_at: string;

    products: { id: number; product_id: number; created_at: string; updated_at: string; product?: Product }[];
}

export interface SearchDiscountsRequest {
    filter?: {
        id?: number;
        seller_id?: number;
        user_id?: number;
        type?: number;
        name_like?: string;
        value_type?: number;
        value?: number;
        status?: number;
        start_date?: string;
        start_date_from?: string;
        end_date?: string;
        end_date_to?: string;
        created_at_from?: string;
        created_at_to?: string;
        is_unlimited?: boolean;
        promo_code_only?: boolean;
    };
    include?: [];
    pagination?: OffsetPaginationQuery;
}

export type SearchDiscountProductsRequest = CommonSearchParams<
    {
        discount_id: number;
    },
    string | string[]
>;

export interface DiscountProduct {
    id: number;
    product_id: number;
    created_at: string;
    updated_at: string;
    product: Product;
}

export interface DiscountsStatusFormData {
    id: (number | undefined)[];
    status: number;
}

export interface DiscountStatus {
    id: number;
    name: string;
}
export interface DiscountType {
    id: number;
    name: string;
}
export interface DiscountValueType {
    id: number;
    name: string;
}

export enum DiscountValueTypeEnum {
    PERCENT = 1,
    RUB = 2,
}

export interface DiscountConditionType {
    id: number;
    name: string;
    props: string[];
}

export interface DiscountConditionTypeProp {
    id: string;
    name: string;
    type: string;
}

export interface DiscountMassBindProductsParams {
    discount_id: number;
    products: { product_id: number }[];
}

export interface DiscountMassUnbindProductsParams {
    ids: number[];
}
