import { useMutation, useQuery, useQueryClient } from 'react-query';

import { ApiError, CommonResponse, CommonSearchParams, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import {
    Product,
    ProductAttributeValue,
    ProductCreateParams,
    ProductDetail,
    ProductFilter,
    ProductGroupCreateParams,
    ProductGroupCreateResponse,
    ProductGroupFilterParams,
    ProductGroupFilterResponse,
    ProductGroupSearchOneResponse,
    ProductGroupSearchParams,
    ProductGroupSearchResponse,
    ProductGroupTypeParams,
    ProductGroupTypeResponse,
    ProductImage,
    ProductMassPatchByQueryRequest,
    ProductMassPatchRequest,
    ProductMassPatchResponse,
    ProductPreloadImage,
    ProductsTypes,
    Property,
} from './types';

const API_URL = 'catalog/products';
export const COMMON_ATTRIBUTES_KEY = 'products-common-attributes';

export const useProducts = (data: CommonSearchParams<Partial<ProductFilter>, string | string[]>, isEnabled = true) =>
    useQuery<CommonResponse<Product[]>, FetchError>({
        enabled: isEnabled,
        queryKey: ['products', data],
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
    });

export const useProductsCommonAttributes = (data: CommonSearchParams<Partial<ProductFilter>>) =>
    useQuery<CommonResponse<Property[]>, FetchError>({
        queryKey: [COMMON_ATTRIBUTES_KEY, data],
        queryFn: () => apiClient.post(`${API_URL}:common-attributes`, { data }),
        cacheTime: 10000,
    });

export const useProductsMeta = (enabled = true) =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'products-meta',
        queryFn: () => apiClient.get(`${API_URL}:meta`),
        enabled,
    });

export const useProblemProducts = (
    data: CommonSearchParams<Partial<ProductFilter>, string | string[]>,
    isEnabled = true
) =>
    useQuery<CommonResponse<Product[]>, FetchError>({
        enabled: isEnabled,
        queryKey: 'problem-products',
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
    });

// export const useProductsTypes = (isEnabled = true) =>
//     useQuery<CommonResponse<Product[]>, FetchError>({
//         enabled: isEnabled,
//         queryKey: ['productsTypes'],
//         queryFn: () => apiClient.get(`${API_URL}/product-types`),
//     });

export const useProductGroupFilters = (data: ProductGroupFilterParams) =>
    useQuery<CommonResponse<ProductGroupFilterResponse[]>, FetchError>({
        enabled: typeof data.category === 'number',
        queryKey: ['product-group-filters', data],
        queryFn: () => apiClient.post(`cms/product-group-filters:search`, { data }),
    });

/** TODO методы cms должны лежать в соответствующей папочке api */
export const useProductGroupTypes = (data: ProductGroupTypeParams) =>
    useQuery<ProductGroupTypeResponse, FetchError>({
        queryKey: ['product-group-types', data],
        queryFn: () => apiClient.post(`cms/product-group-types:search`, { data }),
    });

export const useProductGroups = (data: ProductGroupSearchParams) =>
    useQuery<ProductGroupSearchResponse, FetchError>({
        queryKey: ['product-groups', data],
        queryFn: () => apiClient.post(`cms/product-groups:search`, { data }),
    });

export const useProductGroup = (data: ProductGroupSearchParams) =>
    useQuery<ProductGroupSearchOneResponse | undefined, FetchError>({
        enabled: typeof data.filter?.id === 'number',
        queryKey: [`product-group-${data.filter?.id || -1}`, data.filter?.id],
        queryFn: () => {
            if (!data.filter?.id) return new Promise(resolve => resolve(undefined));
            return apiClient.post(`cms/product-groups:search-one`, { data });
        },
    });

export const useCreateProductGroup = () => {
    const queryClient = useQueryClient();

    return useMutation<ProductGroupCreateResponse, FetchError, ProductGroupCreateParams>(
        data => apiClient.post('cms/product-groups', { data }),
        {
            onSuccess: () => queryClient.invalidateQueries('product-groups'),
        }
    );
};

export const useMassPatchProducts = () => {
    const queryClient = useQueryClient();

    return useMutation<ProductMassPatchResponse, FetchError, ProductMassPatchRequest>(
        data => apiClient.post(`catalog/products:mass-patch`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(['products']);
                queryClient.invalidateQueries(['product']);
            },
        }
    );
};

export const useMassPatchProductsByQuery = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, ProductMassPatchByQueryRequest>(
        data => apiClient.post(`catalog/products:mass-patch-by-query`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(['products']);
                queryClient.invalidateQueries(['product']);
            },
        }
    );
};

export const useUpdateProductGroup = () => {
    const queryClient = useQueryClient();

    return useMutation<
        ProductGroupSearchOneResponse,
        FetchError,
        ProductGroupCreateParams & {
            id: number;
        }
    >(data => apiClient.put(`cms/product-groups/${data.id}`, { data }), {
        onSuccess: ({ data }) => {
            queryClient.invalidateQueries('product-groups');
            queryClient.invalidateQueries(`product-group-${data.id}`);
        },
    });
};

export const useProductDetail = ({ id, include }: { id: string; include?: string | string[] }, enabled = true) =>
    useQuery<CommonResponse<ProductDetail>, FetchError>({
        queryKey: [`product-${id}`],
        queryFn: () => apiClient.get(`${API_URL}/${id}${include ? `?include=${include}` : ''}`),
        enabled,
    });

export const useProductDetailCreate = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<ProductDetail>, FetchError, ProductCreateParams>(
        data => apiClient.post(`${API_URL}`, { data }),
        {
            onSuccess: data => {
                queryClient.invalidateQueries([`product-${data?.data?.id}`]);
                queryClient.invalidateQueries('products');
            },
        }
    );
};

export const useProductDetailUpdate = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<ProductDetail>, FetchError, { id: number; data: Partial<ProductCreateParams> }>(
        productData => {
            const { id, ...data } = productData;
            return apiClient.put(`${API_URL}/${id}`, { ...data });
        },
        {
            onSuccess: data => {
                queryClient.invalidateQueries([`product-${data?.data?.id}`]);
                queryClient.invalidateQueries('products');
            },
        }
    );
};

export const useProductDetailDelete = () => {
    const queryClient = useQueryClient();
    return useMutation<CommonResponse<ProductDetail>, FetchError, number>(id => apiClient.delete(`${API_URL}/${id}`), {
        onSuccess: () => {
            queryClient.invalidateQueries('products');
        },
    });
};

export const useProductDetailAttributesUpdate = () =>
    useMutation<CommonResponse<ProductDetail>, FetchError, { id: number; values: ProductAttributeValue[] }>(data => {
        const { id, values } = data;
        return apiClient.patch(`${API_URL}/${id}/attributes`, { data: { values } });
    });

export const useProductPreloadImage = () =>
    useMutation<CommonResponse<ProductPreloadImage>, FetchError, { formData: FormData }>(data =>
        apiClient.post(`${API_URL}:preload-image`, { data: data.formData })
    );

export const useProductUploadImage = () =>
    useMutation<CommonResponse<ProductImage>, FetchError, { id: number; data: ProductImage }>(data =>
        apiClient.post(`${API_URL}/${data?.id}:upload-image`, { data: data.data })
    );

export const useProductUpdateAllImages = () =>
    useMutation<CommonResponse<ProductImage[]>, FetchError, { id: number; imgs: any }>(dt => {
        const { id, imgs } = dt;
        return apiClient.put(`${API_URL}/${id}/images`, { data: { images: imgs } });
    });

export const useProductDeleteImage = () =>
    useMutation<CommonResponse<null>, FetchError, { id: number; file_id: number }>(dt => {
        const { id, file_id } = dt;
        return apiClient.post(`${API_URL}/${id}:delete-image`, { data: { file_id } });
    });

export const useProductUpdateSeveralImages = () =>
    useMutation<CommonResponse<ProductImage[]>, FetchError, { id: number; imgs: any }>(dt => {
        const { id, imgs } = dt;
        return apiClient.patch(`${API_URL}/${id}/images`, { data: { images: imgs } });
    });

/* export const useMutateProductDetailImageDelete = () =>
    useMutation<CommonResponse<ProductImage>, FetchError, { id: number }>(({ id }) =>
        apiClient.delete(`${API_URL}/images/${id}`)
    ); */

/* export const useUploadProductGroupFile = () =>
    useMutation<
        CommonResponse<{
            url: string;
        }>,
        FetchError,
        {
            id: number;
            file: FormData;
        }
    >(({ id, file }) => apiClient.post(`cms/product-groups/${id}:upload-file`, { data: file })); */

export const useDeleteProductGroupFile = () =>
    useMutation<
        CommonResponse<null>,
        FetchError,
        {
            id: number;
        }
    >(({ id }) => apiClient.post(`cms/product-groups/${id}:delete-file`));

export const useDeleteProductGroup = () => {
    const queryClient = useQueryClient();

    return useMutation<
        {
            data: null;
            errors?: ApiError[];
        },
        FetchError,
        {
            id: number;
        }
    >(({ id }) => apiClient.delete(`cms/product-groups/${id}`), {
        onSuccess: () => {
            queryClient.invalidateQueries('product-groups');
        },
    });
};

export const useProductsTypes = (isEnabled = true) =>
    useQuery<CommonResponse<ProductsTypes[]>, FetchError>({
        enabled: isEnabled,
        queryKey: ['productsTypes'],
        queryFn: () => apiClient.get(`${API_URL}/product-types`),
    });
