import { OffsetPaginationQuery } from '@api/common/types';

import { Property } from './properties';

export interface CategoriesFilter {
    id?: number | number[] | null;
    name?: string;
    code?: string[];
    parent_id?: number | null;
    is_real_active?: boolean;
    is_root?: boolean;
    root_id?: number | null;
}

export interface Category {
    id: number;
    name: string;
    created_at: string;
    updated_at: string;
    code: string;
    parent_id: number | null;
    is_active: boolean;
    is_real_active: boolean;
    is_inherits_properties: boolean;
    properties: Property[];
    hidden_properties: Property[];
}

export interface CategoryTreeItem {
    id: number;
    name: string;
    code: string;
    expanded?: boolean;
    children: CategoryTreeItem[];
}

export interface CategoriesData {
    sort?: string[];
    include?: string[];
    pagination?: OffsetPaginationQuery;
    filter?: CategoriesFilter;
}

export interface CategoryFormData {
    name: string;
    code: string;
    parent_id: number | null;
}

export interface CategoryItemFormData extends CategoryFormData {
    id: number;
}

export interface CategoryBindProperty {
    id: number;
    is_required: boolean | undefined;
}
export interface CategoryBindPropertyData {
    id: number;
    replace: boolean;
    properties: CategoryBindProperty[];
}
