export interface PropertyDirectoryItem {
    id: number;
    name?: string;
    code: string;
    property_id: number;
    url?: string;
    type?: string;
    preload_file_id?: string;
    value?: string;
}

export interface PropertyFields {
    name?: string;
    display_name?: string;
    type?: string;
    is_active?: boolean;
    is_public?: boolean;
    is_gluing?: boolean;
    hint_value?: string;
    hint_value_name?: string;
    is_multiple?: boolean;
    is_filterable?: boolean;
    is_color?: boolean;
    is_system?: boolean;
    is_common?: boolean;
    is_required?: boolean;
    has_directory?: boolean;
}

export interface PropertyData extends PropertyFields {
    id?: number;
}
export interface Property extends PropertyFields {
    attributeValue: any;
    id: number;
    code: string;
    created_at: string;
    updated_at: string;
    property_id: number;
    is_inherited: boolean;
    directory: PropertyDirectoryItem[];
}
export interface PropertiesFilters {
    id?: number[];
    name?: string | null;
    name_like?: string;
    code?: string[];
    exclude_id?: number[];
    is_common?: boolean;
    is_active?: boolean;
}

export interface PropertyDirectoryFilters extends PropertiesFilters {
    property_id?: number | number[];
    value?: string;
}

export interface DirectoryData {
    id?: number;
    name: string | undefined;
    value: string | undefined;
    preload_file_id?: number;
    propertyId?: number;
}

export interface ChangeDirectoryData {
    directoryId: number;
    name?: string;
    value?: string | number;
    preload_file_id?: number;
}

export enum QUERY_KEYS {
    PROPERTIES = 'properties',
    PROPERTY = 'property',
}

export interface DirectoryPreloadImage {
    preload_file_id: number;
    url: string;
}
export interface DirectoryPreloadFile {
    preload_file_id: number;
    url: string;
}
