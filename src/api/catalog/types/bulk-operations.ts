export enum CatalogBulkOperationStatus {
    NEW = 1,
    PROCESSING = 2,
    FINISHED = 3,
    ERRORED = 4,
}

export enum CatalogBulkOperationAction {
    patch = 'patch',
}

export enum CatalogBulkOperationEntity {
    product = 'product',
}

export type CatalogBulkOperationsFilter = Partial<{
    id: number | number[];
    status: CatalogBulkOperationStatus | CatalogBulkOperationStatus[];
    action: CatalogBulkOperationAction | CatalogBulkOperationAction[];
    entity: CatalogBulkOperationEntity | CatalogBulkOperationEntity[];

    created_at_from: string;
    created_at_to: string;

    updated_at_from: string;
    updated_at_to: string;
}>

export interface CatalogBulkOperation {
    id: number;
    created_by: string | null;
    action: CatalogBulkOperationAction;
    entity: CatalogBulkOperationEntity;
    status: CatalogBulkOperationStatus;
    ids: number[];
    ids_string: string;
    error_message: string | null;
    created_at: string;
    updated_at: string;
    errors: {
        operation_id: number;
        entity_id: number;
        message: string;
    }[];
}
