export interface ProductGroupsFilter {
    id?: number;
    category_id?: number;
    name_like?: string;
    product_name_like?: string;
    product_barcode_like?: string;
    product_vendor_code_like?: string;
}

export interface ProductGroupsUpdate {
    id: number;
    data: {
        category_id: number;
        main_product_id: number;
        name: string;
        is_active: boolean;
    };
}

export interface ProductGroupsFields {
    category_id: number;
    main_product_id: number;
    name: string;
    is_active: boolean;
}

export interface ProductGroupsUpdateProducts {
    id: number;
    replace: boolean;
    products: {
        id: number;
        vendore_code?: string;
        barcode?: string;
    }[];
}
