export interface BrandsFilter {
    id?: string[];
    name?: string;
    code?: string;
    is_active?: boolean;
}

export interface BrandBase {
    name?: string;
    code?: string;
    description?: string;
    created_at?: string;
    updated_at?: string;
    image_url?: string;
    logo_url?: string;
    is_active?: boolean;
}

export interface BrandBaseWithId extends BrandBase {
    id: number;
}

export interface Brand extends BrandBaseWithId {
    name: string;
    preload_file_id?: number;
}

export interface BrandsImageMutateParams {
    id: number;
    file: FormData;
}

export interface BrandPreloadImage {
    preload_file_id: number;
    url: string;
}
