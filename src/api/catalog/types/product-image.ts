export interface ProductImage {
    id?: number;
    url?: string;
    preload_file_id?: number;
    name?: string;
    sort?: number;
}

export interface ProductPreloadImage {
    preload_file_id: number;
    url: string;
}
