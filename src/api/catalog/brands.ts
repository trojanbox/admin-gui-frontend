import { useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import { Brand, BrandBaseWithId, BrandPreloadImage, BrandsFilter, BrandsImageMutateParams } from './types';

const BRANDS_BASE_URL = 'catalog/brands';
const BRANDS_KEY = 'brands';

export const useBrands = (data?: CommonSearchParams<BrandsFilter, string>, enabled = true) =>
    useQuery<CommonResponse<Brand[]>, FetchError>({
        queryKey: [BRANDS_KEY, data],
        queryFn: () => apiClient.post(`${BRANDS_BASE_URL}:search`, { data }),
        enabled,
    });

export const useBrandsMeta = () =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: `${BRANDS_KEY}-meta`,
        queryFn: () => apiClient.get(`${BRANDS_BASE_URL}:meta`),
    });

export const useBrand = (id: number, enabled = true) =>
    useQuery<CommonResponse<Brand>, FetchError>({
        queryKey: [BRANDS_KEY, id],
        queryFn: () => apiClient.get(`${BRANDS_BASE_URL}/${id}`),
        enabled,
    });

export const useCreateBrand = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Brand>, FetchError, Omit<Brand, 'id'>>(
        data => apiClient.post(BRANDS_BASE_URL, { data }),
        {
            onSuccess: () => queryClient.invalidateQueries(BRANDS_KEY),
        }
    );
};

export const useEditBrand = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Brand>, FetchError, BrandBaseWithId>(
        data => apiClient.patch(`${BRANDS_BASE_URL}/${data.id}`, { data }),
        { onSuccess: () => queryClient.invalidateQueries(BRANDS_KEY) }
    );
};

export const useDeleteBrand = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, number>(id => apiClient.delete(`${BRANDS_BASE_URL}/${id}`), {
        onSuccess: () => queryClient.invalidateQueries(BRANDS_KEY),
    });
};

export const useBrandPreloadImage = () =>
    useMutation<CommonResponse<BrandPreloadImage>, FetchError, { formData: FormData }>(data =>
        apiClient.post(`${BRANDS_BASE_URL}:preload-image`, { data: data.formData })
    );

export const useMutateBrandImage = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Brand>, FetchError, BrandsImageMutateParams>(
        ({ id, file }) => apiClient.post(`${BRANDS_BASE_URL}/${id}:upload-image`, { data: file }),
        { onSuccess: () => queryClient.invalidateQueries(BRANDS_KEY) }
    );
};

export const useDeleteBrandImage = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Brand>, FetchError, number>(
        id => apiClient.post(`${BRANDS_BASE_URL}/${id}:delete-image`),
        { onSuccess: () => queryClient.invalidateQueries(BRANDS_KEY) }
    );
};
