import { useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import {
    ProductGroupsBindData,
    ProductGroupsData,
    ProductGroupsFields,
    ProductGroupsFilter,
    ProductGroupsUpdate,
    ProductGroupsUpdateProducts,
} from './types';

const baseURL = 'catalog/product-groups';

export const useProductsGroupsMeta = () =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'product-groups-meta',
        queryFn: () => apiClient.get(`${baseURL}:meta`),
    });

export const useProductsGroups = (data: CommonSearchParams<ProductGroupsFilter, string>, enabled = true) =>
    useQuery<CommonResponse<ProductGroupsData[]>, FetchError>({
        queryKey: ['product-groups', data],
        queryFn: () => apiClient.post(`${baseURL}:search`, { data }),
        enabled,
    });

export const useProductsGroupsOne = (data: CommonSearchParams<ProductGroupsFilter, string>, enabled = true) =>
    useQuery<CommonResponse<ProductGroupsData>, FetchError>({
        queryFn: () => apiClient.post(`${baseURL}:search-one?include=products`, { data: JSON.stringify(data) }),
        enabled,
    });

export const useProductsGroupsCreate = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<ProductGroupsData>, FetchError, ProductGroupsFields>(
        productsGroup => apiClient.post(`${baseURL}`, { data: productsGroup }),
        { onSuccess: () => queryClient.invalidateQueries('product-groups') }
    );
};

export const useProductsGroupsUpdate = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<ProductGroupsData>, FetchError, ProductGroupsUpdate>(
        productsGroup => {
            const { id, data } = productsGroup;
            return apiClient.patch(`${baseURL}/${id}`, { data });
        },
        {
            onSuccess: data => {
                queryClient.invalidateQueries(`product-groups-${data?.data?.id}`);
                queryClient.invalidateQueries(`product-groups`);
            },
        }
    );
};

export const useProductsGroupDetail = (id?: number, enabled = true) =>
    useQuery<CommonResponse<ProductGroupsData>, FetchError>({
        queryKey: `product-groups-${id}`,
        queryFn: () => apiClient.get(`${baseURL}/${id}?include=category,products`),
        enabled,
    });

export const useProductsGroupsUpdateProducts = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<ProductGroupsBindData>, FetchError, ProductGroupsUpdateProducts>(
        productsGroup => {
            const { id, ...data } = productsGroup;
            return apiClient.post(`${baseURL}/${id}:bind-products`, { data });
        },
        {
            onSuccess: data => {
                queryClient.invalidateQueries(`product-groups-${data?.data?.product_group?.id}`);
                queryClient.invalidateQueries(`product-groups`);
            },
        }
    );
};

export const useProductsGroupsDelete = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<any>, FetchError, number>(id => apiClient.delete(`${baseURL}/${id}`), {
        onSuccess: data => {
            queryClient.invalidateQueries(`product-groups-${data?.data?.id}`);
            queryClient.invalidateQueries(`product-groups`);
        },
    });
};
