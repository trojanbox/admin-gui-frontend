import { useQuery } from 'react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import { CatalogBulkOperation, CatalogBulkOperationsFilter } from './types/bulk-operations';

const BASE_URL = 'catalog/bulk-operations';
const BASE_KEY = 'catalog-bulk';

export const useCatalogBulkOperations = (
    data?: CommonSearchParams<CatalogBulkOperationsFilter, string>,
    enabled = true
) =>
    useQuery<CommonResponse<CatalogBulkOperation[]>, FetchError>({
        queryKey: [BASE_KEY, data],
        queryFn: () => apiClient.post(`${BASE_URL}:search`, { data, params: {
            include: 'errors'
        } }),
        enabled,
    });

export const useCatalogBulkOperationsMeta = (enabled = true) =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: `${BASE_KEY}-meta`,
        queryFn: () => apiClient.get(`${BASE_URL}:meta`),
        enabled,
    });
