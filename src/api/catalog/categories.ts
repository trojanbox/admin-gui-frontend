import { useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import { COMMON_ATTRIBUTES_KEY } from './products';
import {
    CategoriesFilter,
    Category,
    CategoryBindPropertyData,
    CategoryFormData,
    CategoryItemFormData,
    CategoryTreeItem,
} from './types';

const API_URL = 'catalog/categories';
const QUERY_KEY = 'categories';

export const useCategoriesMeta = () =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'categories-meta',
        queryFn: () => apiClient.get(`${API_URL}:meta`),
    });

export const useCategoryPropertiesMeta = () =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'category-properties-meta',
        queryFn: () => apiClient.get(`catalog/category-properties:meta`),
    });

export const useCategories = (data: CommonSearchParams<CategoriesFilter, string>, enabled = true) =>
    useQuery<CommonResponse<Category[]>, FetchError>({
        queryKey: [QUERY_KEY, data],
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
        enabled,
    });

export const useCategoriesTree = (data?: CommonSearchParams<CategoriesFilter>, enabled = true) =>
    useQuery<CommonResponse<CategoryTreeItem[]>, FetchError>({
        queryKey: [`${QUERY_KEY}-tree`, data],
        queryFn: () => apiClient.post(`${API_URL}:tree`, { data }),
        enabled,
    });

export const useCategoryDetail = (id?: number, enabled = true) =>
    useQuery<CommonResponse<Category>, FetchError>({
        queryKey: `category-${id}`,
        queryFn: () => apiClient.get(`${API_URL}/${id}?include=properties,hidden_properties`),
        enabled,
    });

export const useCategoryCreate = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Category>, FetchError, CategoryFormData>(
        category => apiClient.post(API_URL, { data: category }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QUERY_KEY);
                queryClient.invalidateQueries(`${QUERY_KEY}-tree`);
            },
        }
    );
};

export const useCategoryUpdate = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Category>, FetchError, CategoryItemFormData>(
        category => {
            const { id, ...categoryData } = category;
            return apiClient.put(`${API_URL}/${id}`, { data: categoryData });
        },
        {
            onSuccess: data => {
                queryClient.invalidateQueries(QUERY_KEY);
                queryClient.invalidateQueries(`category-${data?.data?.id}`);
                queryClient.invalidateQueries(['products']);
                queryClient.invalidateQueries([COMMON_ATTRIBUTES_KEY]);
            },
        }
    );
};

export const useCategoryDelete = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, number>(id => apiClient.delete(`${API_URL}/${id}`), {
        onSuccess: () => {
            queryClient.invalidateQueries(QUERY_KEY);
            queryClient.invalidateQueries(`${QUERY_KEY}-tree`);
            queryClient.invalidateQueries([COMMON_ATTRIBUTES_KEY]);
        },
    });
};

export const useCategoryBindProperties = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Category>, FetchError, CategoryBindPropertyData>(
        properties => {
            const { id, ...bindProperties } = properties;

            return apiClient.post(`${API_URL}/${id}:bind-properties`, { data: bindProperties });
        },
        {
            onSuccess: (data, variables) => {
                queryClient.invalidateQueries(`category-${variables.id}`);
                queryClient.invalidateQueries('product');
                queryClient.invalidateQueries('products');
            },
        }
    );
};
