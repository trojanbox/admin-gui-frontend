import { QueryClient, useMutation, useQuery, useQueryClient } from 'react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';

import { FetchError, apiClient } from '../index';
import {
    ChangeDirectoryData,
    DirectoryData,
    DirectoryPreloadFile,
    DirectoryPreloadImage,
    PropertiesFilters,
    Property,
    PropertyData,
    PropertyDirectoryFilters,
    PropertyDirectoryItem,
    PropertyFields,
    QUERY_KEYS,
} from './types';

const baseURL = 'catalog/properties';

const updateProperties = (queryClient: QueryClient) => {
    queryClient.invalidateQueries(QUERY_KEYS.PROPERTIES);
    queryClient.invalidateQueries(QUERY_KEYS.PROPERTY);
};

export const useProperties = (data: CommonSearchParams<PropertiesFilters, string>, enabled = true) =>
    useQuery<CommonResponse<Property[]>, FetchError>({
        queryKey: [QUERY_KEYS.PROPERTIES, data],
        queryFn: () => apiClient.post(`${baseURL}:search`, { data }),
        enabled,
    });

export const usePropertiesMeta = () =>
    useQuery<{ data: Meta }, FetchError>({
        queryKey: 'propertiesMeta',
        queryFn: () => apiClient.get(`${baseURL}:meta`),
    });

export const usePropertiesTypes = () =>
    useQuery<{ data: { id: string; name: string }[] }, FetchError>({
        queryKey: 'propertiesTypes',
        queryFn: () => apiClient.get(`${baseURL}/properties-types`),
    });

export const useProperty = (id?: number) =>
    useQuery<CommonResponse<Property>, FetchError>({
        queryKey: [QUERY_KEYS.PROPERTY, id],
        queryFn: () => apiClient.get(`${baseURL}/${id}`, { params: { include: 'directory' } }),
        enabled: !!id,
    });

export const usePropertyCreate = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Property>, FetchError, PropertyFields>(
        productType => apiClient.post(`${baseURL}`, { data: productType }),
        { onSuccess: () => updateProperties(queryClient) }
    );
};

export const usePropertyChange = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<Property>, FetchError, PropertyData>(
        property => {
            const { id, ...propertyData } = property;
            return apiClient.patch(`${baseURL}/${id}`, { data: propertyData });
        },
        { onSuccess: () => updateProperties(queryClient) }
    );
};

export const usePropertyRemove = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<null>, FetchError, number>(id => apiClient.delete(`${baseURL}/${id}`), {
        onSuccess: () => updateProperties(queryClient),
    });
};

export const usePropertyDirectoryCreate = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<PropertyDirectoryItem>, FetchError, DirectoryData>(
        directory => {
            const { propertyId, ...directoryData } = directory;
            return apiClient.post(`${baseURL}/${propertyId}:add-directory`, { data: directoryData });
        },
        {
            onSuccess: () => updateProperties(queryClient),
        }
    );
};

export const usePropertyDirectoryMassCreate = () => {
    const queryClient = useQueryClient();

    return useMutation<
        CommonResponse<PropertyDirectoryItem[]>,
        FetchError,
        { propertyId: number; items: DirectoryData[] }
    >(
        data => {
            const { propertyId, items } = data;
            return apiClient.post(`${baseURL}/${propertyId}:mass-add-directory`, { data: { items } });
        },
        {
            onSuccess: () => updateProperties(queryClient),
        }
    );
};

export const usePropertyDirectories = (
    data: CommonSearchParams<PropertyDirectoryFilters>,
    enabled = true,
    customKey = ''
) =>
    useQuery<CommonResponse<PropertyDirectoryItem[]>, FetchError>({
        queryKey: [customKey.length ? customKey : 'properties', data],
        queryFn: () => apiClient.post(`${baseURL}/directory:search`, { data }),
        enabled,
    });

export const usePropertyDirectoryChange = () => {
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<PropertyDirectoryItem>, FetchError, ChangeDirectoryData>(
        directory => {
            const { directoryId, ...directoryData } = directory;
            return apiClient.put(`${baseURL}/directory/${directoryId}`, { data: directoryData });
        },
        {
            onSuccess: () => updateProperties(queryClient),
        }
    );
};

export const usePropertyDirectoryRemove = () =>
    useMutation<CommonResponse<PropertyDirectoryItem>, FetchError, number>(id =>
        apiClient.delete(`${baseURL}/directory/${id}`)
    );

// добавил propertyId, что бы можно было замапать id атрибутов к которому грузилось изображение
export const useDirectoryPreloadImage = () =>
    useMutation<
        CommonResponse<DirectoryPreloadImage>,
        FetchError,
        { formData: FormData; propertyId?: number | string }
    >(async data => {
        const res = await apiClient.post(`${baseURL}/directory:preload-image`, { data: data.formData });
        return { ...res, ...(data?.propertyId && { propertyId: data?.propertyId }) };
    });

export const useDirectoryPreloadFile = () =>
    useMutation<CommonResponse<DirectoryPreloadFile>, FetchError, { formData: FormData; propertyId?: number | string }>(
        async data => {
            const res = await apiClient.post(`${baseURL}/directory:preload-file`, { data: data.formData });
            return { ...res, ...(data?.propertyId && { propertyId: data?.propertyId }) };
        }
    );
