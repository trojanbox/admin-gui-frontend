import type { AppProps } from 'next/app';
import Head from 'next/head';
import { FC, useState } from 'react';

import { useCurrentUser } from '@api/auth';
import { AuthLocalStorageKeys, removeAuthInfo } from '@api/auth/helpers';
import { apiClient } from '@api/index';

import { useAuth } from '@context/auth';
import { useModalsContext } from '@context/modal';

import Header from '@containers/Header';
import SidebarContainer from '@containers/Sidebar';

import AppProviders from '@components/AppProviders';
import Auth from '@components/Auth';
import Footer from '@components/Footer';
import '@components/controls/Popup/styles.scss';

import { ModalMessages } from '@scripts/constants';
import { scale } from '@scripts/gds';
import { useMedia, useMount } from '@scripts/hooks';

const AppContent: FC<AppProps> = ({ Component, pageProps }) => {
    const { user, setUser } = useAuth();
    const { appendModal } = useModalsContext();
    const { md } = useMedia();

    useMount(() => {
        const token = localStorage.getItem(AuthLocalStorageKeys.TOKEN) || '';
        setUser(token);
    });

    const [isAuthLoading, setAuthLoading] = useState(false);
    const { data: userData } = useCurrentUser();

    return (
        <>
            <Head>
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
                <link rel="manifest" href="/site.webmanifest" />
                <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
                <meta name="msapplication-TileColor" content="#da532c" />
                <meta name="theme-color" content="#ffffff" />
                {/* this string is required according to Ensi license */}
                <meta name="generator" content="Ensi Platform" />
            </Head>

            {user ? (
                <div css={{ [md]: { paddingBottom: scale(6) } }}>
                    <Header
                        user={userData?.data}
                        onLogout={async () => {
                            try {
                                await apiClient.logOut();
                            } catch (error: any) {
                                appendModal({ message: error.message, theme: 'error' });
                            }
                            setUser('');
                            removeAuthInfo();
                        }}
                    />
                    <SidebarContainer>
                        <div css={{ height: 'calc(100% - 44px)' }}>
                            {userData?.data && <Component {...pageProps} />}
                        </div>
                        <Footer />
                    </SidebarContainer>
                </div>
            ) : (
                <Auth
                    isLoading={isAuthLoading}
                    logIn={async vals => {
                        setAuthLoading(true);

                        try {
                            const promise = apiClient.logIn(vals);

                            appendModal({
                                promise,
                                loading: {
                                    title: ModalMessages.LOADING,
                                    message: '',
                                },
                                error: {
                                    title: 'Ошибка авторизации',
                                    message: ModalMessages.ERROR_UPDATE,
                                },
                                success: {
                                    title: 'Успешно авторизован',
                                    message: '',
                                },
                            });

                            const data = await promise;
                            setUser(data.data.access_token);
                        } catch (error: any) {
                            console.error(error);
                        } finally {
                            setAuthLoading(false);
                        }
                    }}
                />
            )}
        </>
    );
};

function MyApp(props: AppProps) {
    return (
        <AppProviders>
            <AppContent {...props} />
        </AppProviders>
    );
}
export default MyApp;
