# Admin-gui-frontend

## Резюме

Название: admin-gui-frontend

Назначение: Frontend часть административного интерфейса

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/adminGui)

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/admin-gui/job/admin-gui-frontend/

URL: https://admin-gui-frontend-master-dev.ensi.tech/

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
